using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using Firebase.Auth;

public class DatabaseManager : MonoBehaviour
{
    private static DatabaseManager _instance;
    public static DatabaseManager Instance { get { return _instance; } }
    public DatabaseReference DBreference;
    public FirebaseUser user;
    public Rune rune;

    public Rune rune2;
    // Start is called before the first frame update
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void InitializedDatabase(FirebaseUser _user, DatabaseReference dbRef)
    {
        DBreference = dbRef;
        user = _user;
    }
    public void UpdateUsername()
    {
        StartCoroutine(AuthManager.Instance.UpdateUsernameAuth("swarpi"));
        StartCoroutine(UpdateUsernameDatabase("swarpi"));
    }

    private IEnumerator UpdateUsernameDatabase(string _username)
    {
        //Set the currently logged in user username in the database
        Debug.Log(DBreference + " db reference ");
        var Child = DBreference.Child("users");
        var User = Child.Child(AuthManager.Instance.User.UserId);
        var DBTask = User.Child("username").SetValueAsync(_username);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Database username is now updated
        }
    }
    private IEnumerator UpdateRune(Rune rune)
    {
        //Set the currently logged in user xp
        var DBTask = DBreference.Child("users").Child(AuthManager.Instance.User.UserId).Child("rune").SetValueAsync(rune.ToDictionary());

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Xp is now updated
        }
    }

    
    public IEnumerator UpdateHeroContainer(HeroInventoryContainer heroContainer)
    {
        //Set the currently logged in user xp
        var DBTask = DBreference.Child("users").Child(AuthManager.Instance.User.UserId).Child("heroes").SetValueAsync(heroContainer.ToDictionary());

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Xp is now updated
        }
    }

    public IEnumerator LoadUserDataHeroContainer()
    {
        //Get the currently logged in user data
        var DBTask = DBreference.Child("users").Child(AuthManager.Instance.User.UserId).GetValueAsync();

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else if (DBTask.Result.Value == null)
        {
            //No data exists yet
            Debug.Log("no data exists");
        }
        else
        {
            //Data has been retrieved
            DataSnapshot snapshot = DBTask.Result;
           // playerInventory.heroInventoryContainer.heroIDList = HeroIdentifier.Instance
           //     .GetHeroSaveDataFromDict((Dictionary<string, System.Object>)snapshot.Child("heroes").Value);
            yield return HeroIdentifier.Instance
                .GetHeroSaveDataFromDict((Dictionary<string, System.Object>)snapshot.Child("heroes").Value);
        }
    }
    public IEnumerator UpdatePlayerCurrency(PlayerCurrency currency)
    {
        //Set the currently logged in user xp
        var DBTask = DBreference.Child("users").Child(AuthManager.Instance.User.UserId).Child("Currencies").SetValueAsync(currency.ToDictionary());

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else
        {
            //Xp is now updated
        }
    }
    public IEnumerator LoadPlayerCurrency()
    {
        //Get the currently logged in user data
        var DBTask = DBreference.Child("users").Child(AuthManager.Instance.User.UserId).GetValueAsync();

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }
        else if (DBTask.Result.Value == null)
        {
            //No data exists yet
            Debug.Log("no data exists");
        }
        else
        {
            //Data has been retrieved
            DataSnapshot snapshot = DBTask.Result;
            // playerInventory.heroInventoryContainer.heroIDList = HeroIdentifier.Instance
            //     .GetHeroSaveDataFromDict((Dictionary<string, System.Object>)snapshot.Child("heroes").Value);
            yield return (Dictionary<string, System.Object>)snapshot.Child("Currencies").Value;
        }
    }
}
