using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class StageEnemy
{
    public string enemy;
    public int x_pos;
    public int y_pos;
    public int level;
    public int rank;

}
[System.Serializable]
public class StageInfo
{
    public string stageName;
    public int levelID;
    public int stageID;
    public int number_of_waves;
    public StageEnemy[][] enemyArray;
    // rewards
    public int goldReward;
    public int playerExp;
}
[System.Serializable]
public class StageInfos
{
    public StageInfo[] stages;

}
