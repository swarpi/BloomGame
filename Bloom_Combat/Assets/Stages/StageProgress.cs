using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StageProgress : MonoBehaviour
{
    private static StageProgress _instance;
    public static StageProgress Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        LoadAllStages();
    }

    public TextAsset jsonFileStage;
    public StageInfo currentStage;
    List<StageInfo> allStages;

    public void LoadAllStages()
    {
        string tempString = Resources.Load("StageInfo").ToString();
        allStages = JsonConvert.DeserializeObject<StageInfos>(tempString).stages.OfType<StageInfo>().ToList();
    }

    public StageInfo LoadCurrentStage(int stageId)
    {
        currentStage = allStages.Find(x => x.stageID == stageId);


        return currentStage;
    }
}
