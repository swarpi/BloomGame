using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelDisplayer : MonoBehaviour
{
    public GameObject levelDisplayGO;
    public GameObject levelInfoGO;
    public GameObject heroCharacterGO;

    public Level currentLevel;

    public void Start()
    {
        LoadLevel(1);
    }
    public void LoadLevel(int levelID)
    {
        currentLevel = Array.Find(Level.GetAllInstances<Level>(),
            level => level.levelID == levelID);
        DisplayLevel();
        Debug.Log(currentLevel.stageIDs.Length);
    }

    void DisplayLevel()
    {
        GameObject levelBackground = Instantiate(currentLevel.background);
        // put the background into the right position
        levelBackground.transform.SetParent(levelDisplayGO.transform);
        levelBackground.transform.SetSiblingIndex(0);
        levelBackground.transform.localPosition = Vector3.zero;

        // fill chapter info box
        levelInfoGO.transform.Find("chapter").GetComponent<Text>().text = "Chapter " + currentLevel.chapter;
        levelInfoGO.transform.Find("levelName").GetComponent<Text>().text = currentLevel.levelName;
        levelInfoGO.transform.Find("stage").GetComponent<Text>().text = "Stage " + GameProgress.Instance.stageID;
        SetHero(levelBackground);
    }

    void SetHero(GameObject levelBackground)
    {
        heroCharacterGO.transform.localPosition = levelBackground.transform.Find("stages").transform.Find(GameProgress.Instance.stageID.ToString()).transform.localPosition + new Vector3(0,100,0);
    }

    public void StartStage()
    {
        // display dialogue if there is one
        SceneManager.LoadScene("CombatScene");
    }
}
