using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    public HeroInventory heroInventory;
    public PlayerInventoryContainer itemInventory;
    public PlayerCurrency currencies;
    public List<Rune> runeList;
    public List<AscensionMaterial> itemList;
    private void Awake()
    {
        for (int i = 0; i < 20; i++)
        {
            Rune tempRune = RuneManager.GenerateRandomRune();
            runeList.Add(tempRune);
        }
    }


}
