using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PlayerCurrency : MonoBehaviour
{
    [SerializeField]
    int gold;
    [SerializeField]
    int playerExp;

    public void IncreaseGold(int amount)
    {
        gold += amount;
    }

    public void DecreaseGold(int amount)
    {
        gold -= amount;
    }

    public void IncreasePlayerExp(int amount)
    {
        playerExp += amount;
        // if player exp over a certain threshhold level up player
        // take overflowing exp to next level
        // trigger events with level up
    }

    public Dictionary<string, System.Object> ToDictionary()
    {
        Dictionary<string, System.Object> result = new Dictionary<string, object>();
        result["player Exp"] = playerExp;
        result["gold"] = gold;
        return result;
    }
    public void SaveCurrencies()
    {
        StartCoroutine(DatabaseManager.Instance.UpdatePlayerCurrency(this));
    }
    public void LoadCurrencies()
    {
        StartCoroutine(LoadPlayerCurrencies());
    }
    public IEnumerator LoadPlayerCurrencies()
    {
        CoroutineWithData cd = new CoroutineWithData(DatabaseManager.Instance, DatabaseManager.Instance.LoadPlayerCurrency());
        yield return cd.coroutine;
        gold = int.Parse(((Dictionary<string, System.Object>)cd.result)["gold"].ToString());
        playerExp = int.Parse(((Dictionary<string, System.Object>)cd.result)["player Exp"].ToString());
    }

}
