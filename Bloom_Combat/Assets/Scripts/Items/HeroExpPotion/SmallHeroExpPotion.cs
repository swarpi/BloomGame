using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallHeroExpPotion : HeroExpPotion
{
    public SmallHeroExpPotion() : base()
    {
        potionType = ExpPotionType.Small;
        experience = 50;
        cost = 100;
    }
}
