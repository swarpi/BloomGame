using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LargeHeroExpPotion : HeroExpPotion
{
    public LargeHeroExpPotion() : base()
    {
        potionType = ExpPotionType.Large;
        experience = 200;
        cost = 300;
    }
}
