using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ExpPotionType 
{ 
    Small,
    Medium,
    Large
}

public class HeroExpPotion : Item
{
    public ExpPotionType potionType;
    public int experience = 50;
    public int cost = 100;

    public HeroExpPotion()
    {
        itemType = ItemType.Potion;
    }
}
