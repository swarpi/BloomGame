using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediumHeroExpPotion : HeroExpPotion
{
    public MediumHeroExpPotion() : base()
    {
        potionType = ExpPotionType.Medium;
        experience = 100;
        cost = 200;
    }
}
