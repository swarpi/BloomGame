using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class ExpiditionManager : MonoBehaviour
{
        public Expidition tempExpidition;
    public Expidition expiditionOne;
    public Expidition expiditionTwo;
    public GameObject expiditionTypeGO;
    public GameObject expiditionTimeAndDifSettings;
    public GameObject expiditionTimeGO;
    public GameObject expiditionDifficultyGO;
    public ExpiditionHolder expiditionHolder;

    // team selection
    public GameObject expiditionHeroSelectionGO;
    public HeroInventory heroInventory;
    public GameObject[] championsFrameArray;
    public GameObject heroFramePrefab;
    public GameObject contentGO;

    public GameObject expiditionSettingsGO;

    // dispatched screen
    public GameObject expiditionDispatchedGO;
    public GameObject expiditionDispatchedTimeGO;
    Expidition expiditionDisplayed;
    bool isDispatchDisplay = false;
    int secondsLeft = 30;
    bool takingAway = false;

    private void Start()
    {
        Button[] expiditionTypeButtons = expiditionTypeGO.GetComponentsInChildren<Button>();
        foreach(Button button in expiditionTypeButtons)
        {
            button.onClick.AddListener(() => ChooseExpiditionType(button.gameObject.name));
        }
        Button[] expiditionTimeButtons = expiditionTimeGO.GetComponentsInChildren<Button>();
        foreach (Button button in expiditionTimeButtons)
        {
            button.onClick.AddListener(() => ChooseExpiditionTime(button.gameObject.name));
        }
        Button[] expiditionDiffButtons = expiditionDifficultyGO.GetComponentsInChildren<Button>();
        foreach (Button button in expiditionDiffButtons)
        {
            button.onClick.AddListener(() => ChooseExpiditionDifficulty(button.gameObject.name));
        }
    }
    public ExpiditionReward CalculateExpiditionRewards(Expidition expidition)
    {
        ExpiditionReward rewards = new ExpiditionReward();
        rewards.goldReward = expidition.goldReward;
        rewards.mainRewards = new Dictionary<Item.ItemType, int>();
        rewards.rewardItems = new List<Item>();
        for(int i = 0; i < expidition.expiditionRewards.Length; i ++)
        {
            Debug.Log("reward type " + expidition.expiditionRewards[i]);
            switch (expidition.expiditionRewards[i])
            {
                case Item.ItemType.Rune:
                    for(int j = 0; j < expidition.averageRewardAmount[i]; j++)
                    {
                        // need to add grade depending on expidition difficulty
                        Rune tempRune = RuneManager.GenerateRandomRune();
                        rewards.rewardItems.Add(tempRune);
                    }
                    break;
                case Item.ItemType.Potion:
                    for (int j = 0; j < expidition.averageRewardAmount[i]; j++)
                    {
                        // need to add grade depending on expidition difficulty
                        HeroExpPotion heroExpPotion = ExpiditionRewardCalculator.GenerateHeroExpPotion(expidition.expiditionDifficulty);
                        Debug.Log(heroExpPotion);
                        rewards.rewardItems.Add(heroExpPotion);
                    }
                    break;
            }
        }


        return rewards;
    }

    public void ChooseExpiditionType(string expiditionType)
    {
        if (tempExpidition == null)
        {
            tempExpidition = new Expidition();
            Enum.TryParse(expiditionType, out ExpiditionType expidition);
            tempExpidition.expiditionType = expidition;
        }
        else return;
        expiditionTypeGO.SetActive(false);
        expiditionTimeAndDifSettings.SetActive(true);
    }
    public void ChooseExpiditionTime(string time)
    {
        if (tempExpidition != null)
        {
            tempExpidition.expiditionDuration = int.Parse(time);
        }
    }
    public void ChooseExpiditionDifficulty(string difficulty)
    {
        if (tempExpidition != null)
        {
            Enum.TryParse(difficulty, out ExpiditionDifficulty expidition);
            Debug.Log(expidition);
            tempExpidition.expiditionDifficulty = expidition;
        }
    }

    public void ConfirmExpiditionSettings()
    {
        Debug.Log("choosing team for Expidition: " + tempExpidition.expiditionType + tempExpidition.expiditionDuration + tempExpidition.expiditionDifficulty);
        expiditionSettingsGO.SetActive(false);
        expiditionHeroSelectionGO.SetActive(true);
        LoadMetaInfos(tempExpidition);
        LoadTeamDisplay();

    }
    public void LoadMetaInfos(Expidition expidition)
    {
        GameObject metaInfosGO = expiditionHeroSelectionGO.transform.Find("Expidition Meta Infos").gameObject;
        metaInfosGO.transform.Find("Type").gameObject.GetComponent<Text>().text = expidition.expiditionType.ToString();
        metaInfosGO.transform.Find("Time").gameObject.GetComponent<Text>().text = expidition.expiditionDuration.ToString();
        metaInfosGO.transform.Find("Difficulty").gameObject.GetComponent<Text>().text = expidition.expiditionDifficulty.ToString();
        //contentGO = expiditionHeroSelectionGO.transform.Find("Content").gameObject;
    }

    public void LoadTeamDisplay()
    {
        PopulateGrid();
    }
    // need to adapt size so it fits properely
    void PopulateGrid()
    {
        List<HeroSaveData> heroList = heroInventory.heroInventoryContainer.heroIDList;
        championsFrameArray = new GameObject[heroList.Count];
        for (int i = 0; i < heroList.Count; i++)
        {

            GameObject display = Instantiate(heroFramePrefab, contentGO.transform);
            Hero hero = HeroIdentifier.Instance.GetHeroGOFromHeroID(heroList[i].heroID);
            display.name += i;
            if (hero == null)
            {
                Debug.Log("error no hero found");
                return;
            }
            display.transform.Find("Hero Name").GetComponent<Text>().text = hero.uiname;
            Debug.Log(display.GetComponent<Button>());
            display.GetComponent<Button>().onClick.AddListener(() => OnHeroClicked());
            championsFrameArray[i] = display;
        }
    }
    public void OnHeroClicked()
    {
        //get clicked champion ui name
        string name = EventSystem.current.currentSelectedGameObject.transform.name;

        //calculate index from name
        string defaultName = "hero image(Clone)";
        int championFrameIndex = int.Parse(name.Substring(defaultName.Length));
        Debug.Log("clicked on a hero");
        //message shop from click
        PlaceHeroOnField(championFrameIndex);
    }
    void PlaceHeroOnField(int index)
    {
        Hero hero = HeroIdentifier.Instance.GetHeroGOFromHeroID(heroInventory.heroInventoryContainer.heroIDList[index].heroID);
        tempExpidition.dispatchedHeroes.Add(hero);
        Debug.Log("clicked on a hero " + HeroIdentifier.Instance.GetHeroGOFromHeroID(heroInventory.heroInventoryContainer.heroIDList[index].heroID));
    }

    public void StartExpidition()
    {
        // time calc to server
        int dayTimeinSeconds = DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second;
        StartExpidition(dayTimeinSeconds);
    }

    public bool StartExpidition(int startTime)
    {
        // check if player has enough energy
        // check if player meets the requirements
        expiditionOne = expiditionHolder.GetExpidition(tempExpidition.expiditionType, tempExpidition.expiditionDifficulty,tempExpidition.expiditionDuration);
        expiditionOne.dispatchedHeroes = tempExpidition.dispatchedHeroes;
        expiditionOne.expiditionFinishTime = startTime + tempExpidition.expiditionDuration * 3600;
        DisplayExpiditionDispatched(expiditionOne);
        return true;
    }
    void DisplayExpiditionDispatched(Expidition expidition)
    {
        expiditionDisplayed = expidition;
        isDispatchDisplay = true;
        expiditionDispatchedGO.SetActive(true);
    }

    private void Update()
    {
        if(!takingAway && isDispatchDisplay)
        {
            int dayTimeinSeconds = DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second;
            int leftOverTime =  expiditionDisplayed.expiditionFinishTime - dayTimeinSeconds;
            if(leftOverTime > 0)
            {
                int hoursLeft = leftOverTime / 3600;
                int minutesLeft = (leftOverTime / 60) % 60;
                int secondsLeft = leftOverTime % 60 % 60;
                if(hoursLeft < 10)
                {
                    expiditionDispatchedTimeGO.transform.Find("Hour").GetComponent<Text>().text = "0" + hoursLeft;
                }
                else
                {
                    expiditionDispatchedTimeGO.transform.Find("Hour").GetComponent<Text>().text ="" + hoursLeft;
                }
                if (minutesLeft < 10)
                {
                    expiditionDispatchedTimeGO.transform.Find("Minute").GetComponent<Text>().text = "0" + minutesLeft;
                }
                else
                {
                    expiditionDispatchedTimeGO.transform.Find("Minute").GetComponent<Text>().text = "" + minutesLeft;
                }
                if (secondsLeft < 10)
                {
                    expiditionDispatchedTimeGO.transform.Find("Second").GetComponent<Text>().text = "0" + secondsLeft;
                }
                else
                {
                    expiditionDispatchedTimeGO.transform.Find("Second").GetComponent<Text>().text = "" + secondsLeft;
                }

                StartCoroutine(TimerTake());
            }
        }
    }

    IEnumerator TimerTake()
    {
        takingAway = true;
        yield return new WaitForSeconds(1);
        takingAway = false;
    }

    public void FinishExpidition()
    {
        int dayTimeinSeconds = DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second;
        CalculateExpiditionRewards(expiditionOne);
        // fully completed expidition
        if(expiditionOne.expiditionFinishTime <= dayTimeinSeconds)
        {
            Debug.Log("expidition finished");
        }
        // calculate how much done of expidition
        else
        {

        }
    }
}
