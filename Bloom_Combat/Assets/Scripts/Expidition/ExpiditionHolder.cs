using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpiditionHolder : MonoBehaviour
{
    [SerializeField]
    List<Expidition> allExpiditions;

    public Expidition GetExpidition(ExpiditionType type, ExpiditionDifficulty expiditionDifficulty, int expiditionDuration)
    {
        return allExpiditions.Find(x => x.expiditionType == type && x.expiditionDifficulty == expiditionDifficulty && x.expiditionDuration == expiditionDuration);
    }
}
