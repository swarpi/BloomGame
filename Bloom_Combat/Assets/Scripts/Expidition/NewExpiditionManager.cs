using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NewExpiditionManager : MonoBehaviour
{
    public GameObject expiditionSettingsGO;
    public GameObject expiditionHeroSelectionGO;

    public GameObject expiditionTypeGO;
    public GameObject expiditionTypeContentGO;
    public GameObject expiditionDifficultyGO;
    public GameObject expiditionDifficultyContntGO;
    public GameObject expiditionTimeGO;
    public GameObject expiditionTimeContentGO;

    public ExpiditionAnimation expiditionAnimation;
    public ExpiditionHeroSelection ExpiditionHeroSelection;



    public GameObject expiditionButtonPrefab;

    public Expidition tempExpidition;
    // TODO animations of GO // tween
    // TODO display chosing type/diff/time at the side
    // go back and forth between typ/diff/time
    // display hero character after clicking on them

    private void Start()
    {
        OpenExpditionSelection();
        InitExpiditionType(2);
        InitExpiditionDifficulty(2);
        InitExpiditionTime();
        Button[] expiditionTypeButtons = expiditionTypeGO.transform.Find("selection").GetComponentsInChildren<Button>();
        foreach (Button button in expiditionTypeButtons)
        {
            button.onClick.AddListener(() => ChooseExpiditionType(button.gameObject.name));
        }
        Button[] expiditionTimeButtons = expiditionTimeGO.transform.Find("selection").GetComponentsInChildren<Button>();
        foreach (Button button in expiditionTimeButtons)
        {
            button.onClick.AddListener(() => ChooseExpiditionTime(button.gameObject.name));
        }
        Button[] expiditionDiffButtons = expiditionDifficultyGO.transform.Find("selection").GetComponentsInChildren<Button>();
        foreach (Button button in expiditionDiffButtons)
        {
            button.onClick.AddListener(() => ChooseExpiditionDifficulty(button.gameObject.name));
        }
    }

    public void InitExpiditionType(int progress)
    {
        // rune exidition
        CreateExpiditionButton("Rune").transform.SetParent(expiditionTypeContentGO.transform);

        CreateExpiditionButton("HeroExp").transform.SetParent(expiditionTypeContentGO.transform);
    }
    public void InitExpiditionDifficulty(int progress)
    {
        // rune exidition
        CreateExpiditionButton("B1").transform.SetParent(expiditionDifficultyContntGO.transform);

        CreateExpiditionButton("B2").transform.SetParent(expiditionDifficultyContntGO.transform);

        CreateExpiditionButton("B3").transform.SetParent(expiditionDifficultyContntGO.transform);
    }
    public void InitExpiditionTime()
    {
        // rune exidition
        CreateExpiditionButton("2").transform.SetParent(expiditionTimeContentGO.transform);

        CreateExpiditionButton("6").transform.SetParent(expiditionTimeContentGO.transform);

        CreateExpiditionButton("10").transform.SetParent(expiditionTimeContentGO.transform);

        CreateExpiditionButton("16").transform.SetParent(expiditionTimeContentGO.transform);
    }

    public GameObject CreateExpiditionButton(string name)
    {
        GameObject expiditionButton = Instantiate(expiditionButtonPrefab);
        expiditionButton.name = name;
        expiditionButton.GetComponentInChildren<Text>().text = name;
        return expiditionButton;
    }

    public void OpenExpditionSelection()
    {
        tempExpidition = new Expidition();
    }
    public void ChooseExpiditionType(string expiditionType)
    {
        if (tempExpidition != null)
        {
            Enum.TryParse(expiditionType, out ExpiditionType expidition);
            tempExpidition.expiditionType = expidition;
            OpenExpiditionDifficulty();
            // make coroutine to wait a bit
            expiditionTypeGO.transform.Find("blocker").gameObject.SetActive(true);
            expiditionTypeGO.transform.Find("blocker").GetComponentInChildren<Text>().text = expiditionType;

        }
    }
    public void ChooseExpiditionDifficulty(string difficulty)
    {
        if (tempExpidition != null)
        {
            Enum.TryParse(difficulty, out ExpiditionDifficulty expidition);
            tempExpidition.expiditionDifficulty = expidition;
            OpenExpiditionTime();
            expiditionDifficultyGO.transform.Find("blocker").gameObject.SetActive(true);
            expiditionDifficultyGO.transform.Find("blocker").GetComponentInChildren<Text>().text = difficulty;
            // go to hero selection
        }
    }
    public void ChooseExpiditionTime(string time)
    {
        if (tempExpidition != null)
        {
            tempExpidition.expiditionDuration = int.Parse(time);
            ConfirmExpiditionSettings();
        }
    }
    public void OpenExpiditionType()
    {
        tempExpidition.expiditionType = ExpiditionType.None;
        tempExpidition.expiditionDifficulty = ExpiditionDifficulty.None;
        tempExpidition.expiditionDuration = -1;

        CloseExpiditionDifficulty();
        CloseExpiditionTime();
        expiditionTypeGO.transform.Find("blocker").gameObject.SetActive(false);
    }
    public void OpenExpiditionDifficulty()
    {
        tempExpidition.expiditionDifficulty = ExpiditionDifficulty.None;
        if(tempExpidition.expiditionType == ExpiditionType.None)
        {
            Debug.Log("please choose and expidition type first");
            return;
        }
        expiditionAnimation.OpenDifficulty();
        expiditionDifficultyGO.transform.Find("blocker").gameObject.SetActive(false);

        CloseExpiditionTime();
    }
    public void OpenExpiditionTime()
    {
        tempExpidition.expiditionDuration = -1;
        if (tempExpidition.expiditionType == ExpiditionType.None || tempExpidition.expiditionDifficulty == ExpiditionDifficulty.None)
        {
            Debug.Log("please choose and expidition type or expidition difficulty first");
            return;
        }
        expiditionAnimation.OpenTime();
        expiditionTimeGO.transform.Find("blocker").gameObject.SetActive(false);
    }
    void CloseExpiditionDifficulty()
    {
        expiditionAnimation.CloseDifficulty();
        expiditionDifficultyGO.transform.Find("blocker").gameObject.SetActive(true);
        expiditionDifficultyGO.transform.Find("blocker").GetComponentInChildren<Text>().text = "DIFFICULTY";
    }
    void CloseExpiditionTime()
    {
        expiditionAnimation.CloseTime();
        expiditionTimeGO.transform.Find("blocker").gameObject.SetActive(true);
        expiditionTimeGO.transform.Find("blocker").GetComponentInChildren<Text>().text = "TIME";
    }

    public void ConfirmExpiditionSettings()
    {
        Debug.Log("choosing team for Expidition: " + tempExpidition.expiditionType + tempExpidition.expiditionDifficulty + tempExpidition.expiditionDuration);
        expiditionSettingsGO.SetActive(false);
        expiditionHeroSelectionGO.SetActive(true);
        LoadMetaInfos(tempExpidition);
        ExpiditionHeroSelection.LoadHeroDisplay(tempExpidition);
    }

    public void LoadMetaInfos(Expidition expidition)
    {
        GameObject metaInfosGO = expiditionHeroSelectionGO.transform.Find("Expidition Meta Infos").gameObject;
        metaInfosGO.transform.Find("Type").gameObject.GetComponent<Text>().text = expidition.expiditionType.ToString();
        metaInfosGO.transform.Find("Time").gameObject.GetComponent<Text>().text = expidition.expiditionDuration.ToString();
        metaInfosGO.transform.Find("Difficulty").gameObject.GetComponent<Text>().text = expidition.expiditionDifficulty.ToString();
        //contentGO = expiditionHeroSelectionGO.transform.Find("Content").gameObject;
    }
    
}
