using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpiditionRewardCalculator
{
    public static HeroExpPotion GenerateHeroExpPotion(ExpiditionDifficulty difficulty)
    {
        HeroExpPotion tempPotion = new HeroExpPotion();
        int number = Random.Range(0, 100);
        switch (difficulty)
        {
            case ExpiditionDifficulty.B1:
                tempPotion = new SmallHeroExpPotion();
                break;
            case ExpiditionDifficulty.B2:
                tempPotion = new SmallHeroExpPotion();
                break;
            case ExpiditionDifficulty.B3:
                if (number < 80)
                    tempPotion = new SmallHeroExpPotion();
                else
                    tempPotion = new MediumHeroExpPotion();
                break;
            case ExpiditionDifficulty.B4:
                if (number < 50)
                    tempPotion = new SmallHeroExpPotion();
                else
                    tempPotion = new MediumHeroExpPotion();
                break;
            case ExpiditionDifficulty.B5:
                if (number < 20)
                    tempPotion = new SmallHeroExpPotion();
                else
                    tempPotion = new MediumHeroExpPotion();
                break;
            case ExpiditionDifficulty.B6:
                if (number < 20)
                    tempPotion = new SmallHeroExpPotion();
                else if (number < 80)
                    tempPotion = new MediumHeroExpPotion();
                else
                    tempPotion = new LargeHeroExpPotion();
                break;
        }
        return tempPotion;
    }
}
