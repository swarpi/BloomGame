using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ExpiditionReward
{
    public Dictionary<Item.ItemType, int> mainRewards;
    public List<Item> rewardItems;
    public int goldReward;
}
