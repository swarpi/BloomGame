using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ExpiditionHeroSelection : MonoBehaviour
{
    // team selection
    public HeroInventory heroInventory;
    public GameObject[] championsFrameArray;
    public GameObject heroFramePrefab;
    public GameObject contentGO;

    public GameObject heroFieldGO;
    public Hero[] heroField;

    Expidition expidition;
    public void LoadHeroDisplay(Expidition expidition)
    {
        this.expidition = expidition;
        CreateField();
        PopulateGrid();
    }
    public void CreateField()
    {
        heroField = new Hero[4];
    }
    // need to adapt size so it fits properely
    void PopulateGrid()
    {
        List<HeroSaveData> heroList = heroInventory.heroInventoryContainer.heroIDList;
        championsFrameArray = new GameObject[heroList.Count];
        for (int i = 0; i < heroList.Count; i++)
        {
            GameObject display = Instantiate(heroFramePrefab, contentGO.transform);
            Hero hero = HeroIdentifier.Instance.GetHeroGOFromHeroID(heroList[i].heroID);
            display.name += i;
            if (hero == null)
            {
                Debug.Log("error no hero found");
                return;
            }
            display.transform.Find("Hero Name").GetComponent<Text>().text = hero.uiname;
            Debug.Log(display.GetComponent<Button>());
            display.GetComponent<Button>().onClick.AddListener(() => OnHeroClicked());
            championsFrameArray[i] = display;
        }
    }
    public void OnHeroClicked()
    {
        //get clicked champion ui name
        string name = EventSystem.current.currentSelectedGameObject.transform.name;

        //calculate index from name
        string defaultName = "hero image(Clone)";
        int championFrameIndex = int.Parse(name.Substring(defaultName.Length));
        Debug.Log("clicked on a hero");
        //message shop from click
        PlaceHeroOnField(championFrameIndex);
    }
    void PlaceHeroOnField(int index)
    {
        Hero hero = HeroIdentifier.Instance.GetHeroGOFromHeroID(heroInventory.heroInventoryContainer.heroIDList[index].heroID);
        if (HeroOnField(hero)) return;
        int emptyIndex = -1;
        for(int i = 0; i< heroField.Length; i++)
        {
            if(heroField[i] == null)
            {
                emptyIndex = i;
                break;
            }
        }
        if(emptyIndex == -1)
        {
            Debug.Log("already placed max amount of heros");
            return;
        }
        heroField[emptyIndex] = hero;

        heroFieldGO.transform.GetChild(emptyIndex).GetComponentInChildren<Text>().text = hero.heroID;

        //tempExpidition.dispatchedHeroes.Add(hero);
        Debug.Log("clicked on a hero " + HeroIdentifier.Instance.GetHeroGOFromHeroID(heroInventory.heroInventoryContainer.heroIDList[index].heroID));
    }

    public void RemoveHeroFromField()
    {
        int index = EventSystem.current.currentSelectedGameObject.transform.GetSiblingIndex();
        Debug.Log(index);
        heroField[index] = null;

        heroFieldGO.transform.GetChild(index).GetComponentInChildren<Text>().text = "";
    }

    bool HeroOnField(Hero hero)
    {
        if (heroField.Any(x =>
        {
            if (x == null) return false;
            return x == hero;
        }))
        {
            Debug.Log(hero.heroID + " is already on board");
            return true;
        }
        else return false;
    }
    
}
