using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ExpiditionType
{
    None,
    Rune,
    HeroExp
}
public enum ExpiditionDifficulty
{
    None,
    B1,
    B2,
    B3,
    B4,
    B5,
    B6
}
[CreateAssetMenu(fileName = "DefaultExpidition", menuName = "Expidition", order = 1)]
public class Expidition : ScriptableObject
{
    public int expiditionDuration;

    public int expiditionFinishTime;

    public ExpiditionType expiditionType;
    // swap to enum later
    public ExpiditionDifficulty expiditionDifficulty;

    public int goldReward;
    [SerializeField]
    public Item.ItemType[] expiditionRewards;
    
    public int[] averageRewardAmount;

    public List<Hero> dispatchedHeroes;
    // additional bonis

    // requirements
    public Expidition()
    {
        dispatchedHeroes = new List<Hero>();
        expiditionType = ExpiditionType.None;
        expiditionDifficulty = ExpiditionDifficulty.None;

    }

}
