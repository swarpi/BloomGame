using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpiditionAnimation : MonoBehaviour
{
    public GameObject typeGO;
    public GameObject difficultyGO;
    public GameObject timeGO;
    public void Start()
    {
        //transform.LeanMoveLocal
        CloseDifficulty();
        CloseTime();
    }

    public void OpenType()
    {
        //typeGO.transform
    }

    public void OpenDifficulty()
    {
        difficultyGO.transform.LeanMoveLocalX(320, 0.5f);
    }

    public void CloseDifficulty()
    {
        difficultyGO.transform.LeanMoveLocalX(1280, 0.5f);
    }
    public void OpenTime()
    {
        timeGO.transform.LeanMoveLocal(new Vector2(640, 0), 0.5f);
    }

    public void CloseTime()
    {
        timeGO.transform.LeanMoveLocalX(1600, 0.5f);
    }
}
