using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Effect_type
{
    hardCC,
    blind,
    buff,
    debuff,
    heal,
    shield,
    mark        //marked?
}
public class CombatEffect 
{
    public string name;
    public float duration;
    public Effect_type type;
    
    public CombatEffect(string name, float duration, Effect_type type)
    {
        this.name = name;
        this.duration = duration;
        this.type = type;

    }

    public virtual void Effect()
    {

    }

    public virtual bool Update(float deltaTime)
    {
        duration -= deltaTime;
        if(duration <= 0)
        {
            this.EffectEnd();
            return false;
        }
        else
        {
            return true;
        }
    }

    public virtual void EffectEnd()
    {

    }
}

public class CombatEffectStun : CombatEffect
{
    public CombatEffectStun(string name, float duration, Effect_type type) : base(name, duration, type)
    {

    }


}

public class CombatEffectSleep : CombatEffect
{
    public CombatEffectSleep(string name, float duration, Effect_type type) : base(name, duration, type)
    {
        
    }
}

public class CombatEffectHeal : CombatEffect
{
    public float amount;
    public RefFloat toHeal;
    public CombatEffectHeal(string name, float duration, Effect_type type, float amount, RefFloat toHeal) : base(name, duration, type)
    {
        this.amount = amount;
        this.toHeal = toHeal;
    }

    public override void Effect()
    {
        toHeal.f += amount;
    }
}

public class CombatEffectBuff : CombatEffect
{
    public RefFloat[] toBuff;
    public float[] values;
    public CombatEffectBuff(string name, float duration, Effect_type type, RefFloat[] toBuff, float[] values) : base(name, duration, type)
    {
        this.toBuff = toBuff;
        this.values = values;
    }

    public override void Effect()
    {
        for (int i = 0; i < toBuff.Length; i++)
        {
            toBuff[i].f += values[i];
        }
    }
}

public class CombatEffectDebuff : CombatEffect
{
    public RefFloat[] toDebuff;
    public float[] values;
    public CombatEffectDebuff(string name, float duration, Effect_type type, RefFloat[] toDebuff, float[] values) : base(name, duration, type)
    {
        this.toDebuff = toDebuff;
        this.values = values;
    }

    public override void Effect()
    {
        for (int i = 0; i < toDebuff.Length; i++) 
        {
            toDebuff[i].f -= values[i];
        }
    }

    public override void EffectEnd()
    {
        for (int i = 0; i < toDebuff.Length; i++)
        {
            toDebuff[i].f += values[i];
        }
    }
}
