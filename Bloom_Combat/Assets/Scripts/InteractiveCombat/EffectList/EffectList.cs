using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Till TODO List for every Effect Like CC�s, Buffs etc so that a character can get targeted and effected by differnt unique effects of the same type
public class EffectList 
{
    private HeroControllerCombat2D hero;

    private List<CombatEffect> effectList = new List<CombatEffect>();
    private bool isSleeping = false;
    
 
    public void AddDebuff(string name, float duration, Effect_type type, RefFloat[] toDebuff, float[] values)
    {
        CombatEffectDebuff debuff = new CombatEffectDebuff(name, duration, type, toDebuff, values);
        if (Add(debuff))
        {
            debuff.Effect();
        }
    } 
    public void UpdateList(float deltaTime)
    {
       for(int i = effectList.Count - 1; i >= 0; i--)
       {
            if (!effectList[i].Update(deltaTime))
            {
                effectList.RemoveAt(i);
            }
               
            
       }
}

    public bool CheckType(Effect_type type)
    {
        return effectList.Exists(x => x.type == type);
    }

    
    public bool CheckSleep()
    {
        return isSleeping;
    }

    public void WakeUp()
    {
        isSleeping = false;
    }


    private bool Add(CombatEffect cE)
    {
        CombatEffect tmp = effectList.Find(x => x.name == cE.name);
        if (tmp != null)
        {
            tmp.duration = cE.duration;
            Debug.Log("Debuffduration = " + tmp.duration);
            return false;
        }
        else
        {
            effectList.Add(cE);
            return true;
        }
    }
}
