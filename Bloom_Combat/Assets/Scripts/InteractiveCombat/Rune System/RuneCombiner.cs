using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuneCombiner : MonoBehaviour
{
    public GameObject contentGO;
    public GameObject runeImage;
    public GameObject runeImageMaterial;

    public GameObject mainRuneGO;
    public GameObject materialRuneGO;

    public GameObject runeStarGO;
    public GameObject runeStarPrefab;
    public GameObject runeStarPrefabNew;

    Rune selectedRune;
    Rune selectedMaterialRune;

    GameObject selectedMaterialRuneGO;
    //public RuneDisplayer runeDisplayer;
    

    public void OpenRuneCombiner()
    {
        RuneDisplayer.Instance.DisplayRunesToContentGO(contentGO);
        RuneDisplayer.Instance.onFilterApplied += RefreshRuneCombine;
        RefreshRuneCombine();
    }
    public void SelectRuneToCombine(Rune rune, GameObject runeGo)
    {
        if (selectedRune == null)
        {
            if (RuneManager.IsRuneMaxStar(rune.runeLevel, rune.stars))
            {
                Debug.Log("this rune reached its max level already, try to increase its grade");
                return;
            }
            DisplayRuneAsMainRune(rune, runeGo);
            RuneDisplayer.Instance.FilterRunesByStars(rune.stars);
            RuneDisplayer.Instance.FilterRunesBySlot(rune.runeSlot);
        }
        else if(selectedMaterialRune == null)
        {
            if (rune.stars != selectedRune.stars)
            {
                Debug.Log("the runes dont have the same stars");
                return;
            }
            DisplayRuneAsCombineMaterial(rune, runeGo);
        }
    }
    public void DeselectRuneToCombine(Rune rune, GameObject runeGo)
    {
        if(selectedRune == rune)
        {
            // remove the image of the main rune
            RefreshRuneCombine();
        }
        else if(selectedMaterialRune == rune)
        {
            selectedMaterialRune = null;
            Destroy(runeImageMaterial.transform.GetChild(0).gameObject);
            runeImageMaterial.SetActive(false);
            materialRuneGO.SetActive(false);

            runeGo.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
            runeGo.GetComponentInChildren<Button>().onClick.AddListener(delegate { SelectRuneToCombine(rune, runeGo); });

            selectedMaterialRuneGO = null;
        }
    }
    public void DisplayRuneAsMainRune(Rune rune, GameObject runeGo)
    {
        Debug.Log("trying to display rune");
        selectedRune = rune;
        runeImage.SetActive(true);

        runeGo.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
        runeGo.GetComponentInChildren<Button>().onClick.AddListener(delegate { DeselectRuneToCombine(rune, runeGo); });

        // instantiate a new prefab
        GameObject goCopy = RuneDisplayer.Instance.CreateRuneGOClone(rune, runeImage);
        // adding listener to gameobject on highlight reel on top
        goCopy.GetComponentInChildren<Button>().onClick.AddListener(delegate { DeselectRuneToCombine(rune, runeGo); });

        // display the rune info for mainstat
        mainRuneGO.SetActive(true);
        mainRuneGO.transform.Find("MainStat").Find("name").GetComponent<Text>().text = rune.mainStat.type.ToString();
        mainRuneGO.transform.Find("MainStat").Find("value").GetComponent<Text>().text = rune.mainStat.value.ToString();
        // SubStats
        // all runes have atleast 1 substat so dont do anything with first one
        mainRuneGO.transform.Find("SubStats").GetChild(0).Find("name").GetComponent<Text>().text = rune.subStats[0].type.ToString();
        mainRuneGO.transform.Find("SubStats").GetChild(0).Find("value").GetComponent<Text>().text = rune.subStats[0].value.ToString();
        mainRuneGO.transform.Find("SubStats").GetChild(0).gameObject.SetActive(true);
        // try to acces the second substat, if it doenst have it, the rune will throw an exception and then set the object it to not active
        try
        {
            mainRuneGO.transform.Find("SubStats").GetChild(1).Find("name").GetComponent<Text>().text = rune.subStats[1].type.ToString();
            mainRuneGO.transform.Find("SubStats").GetChild(1).Find("value").GetComponent<Text>().text = rune.subStats[1].value.ToString();
            mainRuneGO.transform.Find("SubStats").GetChild(1).gameObject.SetActive(true);
        }
        catch (ArgumentOutOfRangeException e)
        {
            mainRuneGO.transform.Find("SubStats").GetChild(1).gameObject.SetActive(false);
        }
        // try to acces the third substat, if it doenst have it, the rune will throw an exception and then set the object it to not active
        try
        {
            mainRuneGO.transform.Find("SubStats").GetChild(2).Find("name").GetComponent<Text>().text = rune.subStats[2].type.ToString();
            mainRuneGO.transform.Find("SubStats").GetChild(2).Find("value").GetComponent<Text>().text = rune.subStats[2].value.ToString();
            mainRuneGO.transform.Find("SubStats").GetChild(2).gameObject.SetActive(true);
        }
        catch (ArgumentOutOfRangeException e)
        {
            mainRuneGO.transform.Find("SubStats").GetChild(2).gameObject.SetActive(false);
        }

        // show the rune stars 
        runeStarGO.SetActive(true);
        for(int i = 0; i < rune.stars; i++)
        {
            GameObject star = Instantiate(runeStarPrefab);
            star.transform.SetParent(runeStarGO.transform);
        }
        GameObject starNew = Instantiate(runeStarPrefabNew);
        starNew.transform.SetParent(runeStarGO.transform);
    }
    public void DisplayRuneAsCombineMaterial(Rune rune, GameObject runeGo)
    {
        selectedMaterialRuneGO = runeGo;
        selectedMaterialRune = rune;
        runeImageMaterial.SetActive(true);
        materialRuneGO.SetActive(true);

        runeGo.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
        runeGo.GetComponentInChildren<Button>().onClick.AddListener(delegate { DeselectRuneToCombine(rune, runeGo); });

        // instantiate a new prefab
        GameObject goCopy = RuneDisplayer.Instance.CreateRuneGOClone(rune, runeImageMaterial);

        // adding listener to gameobject on highlight reel on top
        goCopy.GetComponentInChildren<Button>().onClick.AddListener(delegate { DeselectRuneToCombine(rune, runeGo); });

        // display the rune info for mainstat
        materialRuneGO.transform.Find("MainStat").Find("name").GetComponent<Text>().text = rune.mainStat.type.ToString();
        materialRuneGO.transform.Find("MainStat").Find("value").GetComponent<Text>().text = rune.mainStat.value.ToString();
        // SubStats
        materialRuneGO.transform.Find("SubStats").GetChild(0).Find("name").GetComponent<Text>().text = rune.subStats[0].type.ToString();
        materialRuneGO.transform.Find("SubStats").GetChild(0).Find("value").GetComponent<Text>().text = rune.subStats[0].value.ToString();
        materialRuneGO.transform.Find("SubStats").GetChild(0).gameObject.SetActive(true);
        try
        {
            materialRuneGO.transform.Find("SubStats").GetChild(1).Find("name").GetComponent<Text>().text = rune.subStats[1].type.ToString();
            materialRuneGO.transform.Find("SubStats").GetChild(1).Find("value").GetComponent<Text>().text = rune.subStats[1].value.ToString();
            materialRuneGO.transform.Find("SubStats").GetChild(1).gameObject.SetActive(true);
        }
        catch (ArgumentOutOfRangeException e)
        {
            materialRuneGO.transform.Find("SubStats").GetChild(1).gameObject.SetActive(false);
        }
        try
        {
            materialRuneGO.transform.Find("SubStats").GetChild(2).Find("name").GetComponent<Text>().text = rune.subStats[2].type.ToString();
            materialRuneGO.transform.Find("SubStats").GetChild(2).Find("value").GetComponent<Text>().text = rune.subStats[2].value.ToString();
            materialRuneGO.transform.Find("SubStats").GetChild(2).gameObject.SetActive(true);
        }
        catch (ArgumentOutOfRangeException e)
        {
            materialRuneGO.transform.Find("SubStats").GetChild(2).gameObject.SetActive(false);
        }
    }

    public void RefreshRuneCombine()
    {
        selectedRune = null;
        selectedMaterialRune = null;
        selectedMaterialRuneGO = null;
        RuneDisplayer.Instance.UpdateAllRunesDisplay();
        foreach (Transform runeGo in contentGO.transform)
        {
            Rune tempRune = runeGo.GetComponent<RuneContainer>().rune;
            runeGo.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
            runeGo.GetComponentInChildren<Button>().onClick.AddListener(delegate { SelectRuneToCombine(tempRune, runeGo.gameObject); });
        }

        // clear the runeImage
        if(runeImage.transform.childCount > 0)
        {
            Destroy(runeImage.transform.GetChild(0).gameObject);
            runeImage.SetActive(false);
            mainRuneGO.SetActive(false);
            foreach(Transform starGO in runeStarGO.transform)
            {
                Destroy(starGO.gameObject);
            }
        }
        // clear the material runeImage
        if (runeImageMaterial.transform.childCount > 0)
        {
            Destroy(runeImageMaterial.transform.GetChild(0).gameObject);
            runeImageMaterial.SetActive(false);
            materialRuneGO.SetActive(false);
        }
    }

    public void CombineRune()
    {
        if (selectedRune.stars != selectedMaterialRune.stars)
        {
            Debug.Log("ups the runes dont have the same stars");
            return;
        }
        if (selectedRune.runeSlot != selectedMaterialRune.runeSlot)
        {
            Debug.Log("ups the runes dont have the same slot");
            return;
        }
        if (RuneManager.IsRuneMaxStar(selectedRune.runeLevel, selectedRune.stars))
        {
            Debug.Log("this rune reached its max level already, try to increase its grade");
            return;
        }
        RuneDisplayer.Instance.runeList.Remove(selectedMaterialRune);
        selectedRune.stars++;
        RuneDisplayer.Instance.RefreshRuneInventory();
        // remove other rune

        RefreshRuneCombine();

    }


}
