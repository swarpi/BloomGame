using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RuneDisplayer : MonoBehaviour
{
    public GameObject contentGO;
    public GameObject runePrefab;
    public GameObject runeInfoPrefab;
    public GameObject runeInfoButtonPrefab;
    public GameObject itemInventory;
    public List<Rune> runeList;

    private static RuneDisplayer _instance;
    public static RuneDisplayer Instance { get { return _instance; } }
    public PlayerInventory playerInventory;
    public RuneLevelUpgrader runeLevelUpgrader;
    public RuneCombiner runeCombiner;

    public Canvas myCanvas;
    private float timer;
    float seconds;
    bool isPointerDown;
    bool isPointerUp;
    bool isHolding = false;
    Rune selectedRune;
    GameObject runeInfoGO = null;
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    void Start()
    {
        //DontDestroyOnLoad(this);
        //runeLevelUpgrader = GetComponent<RuneLevelUpgrader>();
        //runeCombiner = GetComponent<RuneCombiner>();
        
        //InitAllRunesToDisplay(contentGO);
        //InitRuneInventory();

    }
    private void Update()
    {
        // if we want to differentiate between clicking and holding

        if (isPointerDown)
        {
            timer += Time.deltaTime;
            if (timer > 0.15f && !isHolding)
            {
                runeInfoGO = HoldaButton();
            }
        }
        if (isPointerUp)
        {
            if (timer < 0.15f)
            {
                runeInfoGO = ClickedOnButton();
            }
            else
            {
                // destroy the button
                Destroy(runeInfoGO);
            }

            timer = 0f;
            isPointerUp = false;
            isHolding = false;
        }
    }

    void InitAllRunesToDisplay(GameObject content)
    {
        foreach (Transform child in contentGO.transform)
        {
            Destroy(child.gameObject);
        }
        contentGO = content;
        runeList = playerInventory.runeList;
        Debug.Log(runeList.Count);
        foreach (Rune newRune in runeList)
        {
            GameObject tempRuneGO = Instantiate(runePrefab);
            RuneContainer runeContainer = tempRuneGO.GetComponent<RuneContainer>();
            runeContainer.scrollView = contentGO.transform.parent.parent.GetComponent<ScrollRect>();
            tempRuneGO.transform.Find("slot").GetComponent<Text>().text = newRune.runeSlot.ToString();
            tempRuneGO.transform.Find("Mainstat").GetComponent<Text>().text = newRune.mainStat.type.ToString();
            tempRuneGO.transform.Find("stars").GetComponent<Text>().text = newRune.stars.ToString();
            tempRuneGO.name = newRune.runeId;
            tempRuneGO.transform.SetParent(contentGO.transform);
            tempRuneGO.GetComponent<RuneContainer>().rune = newRune;
        }
    }

    // give runesGO the ability to detect hold and click
    public void InitRuneInventory()
    {
        foreach (Transform child in contentGO.transform)
        {
            RuneContainer runeContainer = child.GetComponent<RuneContainer>();
            runeContainer.scrollView = contentGO.transform.parent.parent.GetComponent<ScrollRect>();
            Rune tempRune = runeContainer.rune;
            EventTrigger eventrTrigger = child.transform.Find("background").GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            entry.callback.AddListener((data) => { HoldButtonDown((PointerEventData)data, tempRune); });
            eventrTrigger.triggers.Add(entry);
            EventTrigger.Entry entryUp = new EventTrigger.Entry();
            entryUp.eventID = EventTriggerType.PointerUp;
            entryUp.callback.AddListener((data) => { HoldButtonUp((PointerEventData)data); });
            eventrTrigger.triggers.Add(entryUp);
        }
    }

    public GameObject ClickedOnButton()
    {
        Debug.Log("clicked on a button" + selectedRune.runeSlot);
        // instantiate runebutton
        return FillRuneInfoGO(selectedRune,true);
    }
    public GameObject HoldaButton()
    {
        isHolding = true;
        Debug.Log("hold a button");
        // instantiate runeinfo
        GameObject go = FillRuneInfoGO(selectedRune, false);
        Debug.Log(go.name);
        return go;
    }

    public void HoldButtonDown(PointerEventData data,Rune rune)
    {
        selectedRune = rune;
        isPointerDown = true;
        isPointerUp = false;
    }

    public void HoldButtonUp(PointerEventData data)
    {
        isPointerDown = false;
        isPointerUp = true;
    }
    public void CancelRuneUIButton()
    {
        Destroy(runeInfoGO);
    }
    public GameObject FillRuneInfoGO(Rune rune,bool withButtons)
    {
        GameObject runeGO;
        if (withButtons)
            runeGO = Instantiate(runeInfoButtonPrefab);
        else
            runeGO = Instantiate(runeInfoPrefab);
        runeGO.transform.SetParent(myCanvas.transform);
        //runeGO.transform.Find("runeIcon").GetComponent = runeIcon
        // Rune name
        runeGO.transform.Find("RuneName").Find("runeName").GetComponent<Text>().text = rune.runeId;
        runeGO.transform.Find("RuneName").Find("grade").GetComponent<Text>().text = rune.runeLevel.ToString();
        // Rune set
        //runeGO.transform.Find("Set").Find("setIcon").GetComponent<Text>().text = rune.runeSet.ToString();
        runeGO.transform.Find("Set").Find("setName").GetComponent<Text>().text = rune.runeSet.ToString();
        //runeGO.transform.Find("Set").Find("required").GetComponent<Text>().text = rune.runeSet.ToString();
        //runeGO.transform.Find("Set").Find("explanation").GetComponent<Text>().text = rune.runeSet.ToString();
        // Stars
        for(int i = 0; i < rune.stars; i++)
        {
            runeGO.transform.Find("Stars").GetChild(i).gameObject.SetActive(true);
        }
        // Level
        runeGO.transform.Find("Level").GetComponent<Text>().text = rune.level.ToString();
        // MainStat
        //runeGO.transform.Find("MainStat").Find("icon").GetComponent<Text>().text = rune.runeLevel.ToString();
        runeGO.transform.Find("MainStat").Find("name").GetComponent<Text>().text = rune.mainStat.type.ToString();
        runeGO.transform.Find("MainStat").Find("value").GetComponent<Text>().text = rune.mainStat.value.ToString();
        // SubStats
        for (int i = 0; i < rune.subStats.Count; i++)
        {
            runeGO.transform.Find("SubStats").GetChild(i).gameObject.SetActive(true);
            runeGO.transform.Find("SubStats").GetChild(i).Find("name").GetComponent<Text>().text = rune.subStats[i].type.ToString();
            runeGO.transform.Find("SubStats").GetChild(i).Find("value").GetComponent<Text>().text = rune.subStats[i].value.ToString();
        }
        // Cancel button
        runeGO.transform.Find("Cancel").GetComponent<Button>().onClick.AddListener(CancelRuneUIButton);
        if (withButtons)
        {
            // find the previous window and destroy it
            CancelRuneUIButton();
            // give the buttons functionality
            runeGO.transform.localPosition = Vector3.zero;
            //Levelup
            runeGO.transform.Find("Levelup").GetComponent<Button>().onClick.AddListener(delegate { OpenRuneLevelUp(rune); });
            runeGO.transform.Find("Upgrade").GetComponent<Button>().onClick.AddListener(delegate { runeCombiner.OpenRuneCombiner(); });
            runeGO.transform.Find("Upgrade").GetComponent<Button>().onClick.AddListener(delegate { CancelRuneUIButton(); });
        }
        else
        {
            //check if should display left or right of the mouse
            Vector3 offSet = new Vector3(400, -100, 0); // this is for right side
            runeGO.transform.position = Input.mousePosition + offSet;
        }

        return runeGO;
    }
    public void UpdateAllRunesDisplay()
    {
        foreach (Transform child in contentGO.transform)
        {
            Rune childRune = child.GetComponent<RuneContainer>().rune;
            child.transform.Find("slot").GetComponent<Text>().text = childRune.runeSlot.ToString();
            child.transform.Find("Mainstat").GetComponent<Text>().text = childRune.mainStat.type.ToString();
            child.transform.Find("stars").GetComponent<Text>().text = childRune.stars.ToString();
            child.GetComponentInChildren<Image>().color = Color.white;
        }
    }

    public void RefreshRuneInventory()
    {
        InitAllRunesToDisplay(contentGO);
    }
    public void OpenRuneLevelUp(Rune rune)
    {
        CancelRuneUIButton();
        
        runeLevelUpgrader.OpenRuneLevelUpgrader(rune, FillRuneInfoGO(rune,false));
    }
    public void DisplayRunesToContentGO(GameObject content)
    {
        InitAllRunesToDisplay(content);
        itemInventory.SetActive(false);
    }

    public GameObject CreateRuneGOClone(Rune rune, GameObject image)
    {
        GameObject goCopy = Instantiate(RuneDisplayer.Instance.runePrefab);
        goCopy.transform.Find("slot").GetComponent<Text>().text = rune.runeSlot.ToString();
        goCopy.transform.Find("Mainstat").GetComponent<Text>().text = rune.mainStat.type.ToString();
        goCopy.transform.Find("stars").GetComponent<Text>().text = rune.stars.ToString();
        goCopy.name = rune.runeId;
        // TODO: animate that goCopy moves to the runeImageMaterial pos
        goCopy.transform.SetParent(image.transform);
        goCopy.transform.localPosition = Vector3.zero;

        return goCopy;
    }
    public void ShowAllRunes()
    {
        foreach (Transform child in contentGO.transform)
        {
            child.gameObject.SetActive(true);
        }
    }
    public void ShowAlphaRunes()
    {
        List<Rune> alphaRunes = runeList.FindAll(x => x.runeSlot == RuneSlot.Alpha);
        foreach (Transform child in contentGO.transform)
        {
            if (alphaRunes.Find(x => x.runeId == child.name) == null)
            {
                child.gameObject.SetActive(false);
            }
            else
            {
                child.gameObject.SetActive(true);
            }
        }

        FilterApplied();
    }
    public void ShowBetaRunes()
    {
        List<Rune> betaRunes = runeList.FindAll(x => x.runeSlot == RuneSlot.Beta);
        foreach (Transform child in contentGO.transform)
        {
            if (betaRunes.Find(x => x.runeId == child.name) == null)
            {
                child.gameObject.SetActive(false);
            }
            else
            {
                child.gameObject.SetActive(true);
            }
        }
        FilterApplied();
    }
    public void ShowGammaRunes()
    {
        List<Rune> gammaRunes = runeList.FindAll(x => x.runeSlot == RuneSlot.Gamma);
        foreach (Transform child in contentGO.transform)
        {
            if (gammaRunes.Find(x => x.runeId == child.name) == null)
            {
                child.gameObject.SetActive(false);
            }
            else
            {
                child.gameObject.SetActive(true);
            }
        }
        FilterApplied();
    }

    public event Action onFilterApplied;
    public void FilterApplied()
    {
        if(onFilterApplied != null)
        {
            onFilterApplied();
        }
    }

    public void FilterRunesByStars(int stars)
    {
        List<Rune> runeStars = runeList.FindAll(x => x.stars == stars);
        foreach (Transform child in contentGO.transform)
        {
            if (runeStars.Find(x => x.runeId == child.name) == null)
            {
                child.GetComponentInChildren<Image>().color = Color.gray;
                //child.gameObject.SetActive(false);
            }
        }
    }

    public void FilterRunesBySlot(RuneSlot slot)
    {
        List<Rune> runeSlots = runeList.FindAll(x => x.runeSlot == slot);
        foreach (Transform child in contentGO.transform)
        {
            if (runeSlots.Find(x => x.runeId == child.name) == null)
            {
                // grey out and maybe remove all listeners on them
                child.GetComponentInChildren<Image>().color = Color.gray;
                child.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
                //child.gameObject.SetActive(false);
            }
        }
    }

    public void RemoveGreyColor()
    {
        foreach(Transform child in contentGO.transform)
        {
            child.GetComponentInChildren<Image>().color = Color.white;
        }
    }
}
