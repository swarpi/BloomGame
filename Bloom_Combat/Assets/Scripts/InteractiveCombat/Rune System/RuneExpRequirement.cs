using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class RuneExpRequirement
{
    public int level;
    public int requiredExp;
    public int requiredDust;
    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
}
[System.Serializable]
public class RuneExpRequirements
{
    public RuneExpRequirement[] runeExpRequirements;
    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
}
