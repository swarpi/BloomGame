using System;
using System.Collections;
using System.Collections.Generic;
public enum RuneStatType
{
    MainStat,
    SubStat
}
public enum RuneSlot
{
    Alpha, Beta, Gamma, DummyRune
}
public enum RuneSetBonus
{
    ATKSet,
    CRITDMGSet,
    DummySet

}

public enum MainStats
{
    CritDamage,
    AttackDamage,
    ElementalBonusDamage,
    Health,
    Arnor,
    MagicResist,
    Effectivity,
    Tenacity
}

public enum SubStats
{
    CritRate,
    AttackDamage,
    ElementalBonusDamage,
    Health,
    Arnor,
    MagicResist,
    Effectivity,
    Tenacity
}
[System.Serializable]
public struct MainStat
{
    public MainStats type;
    public float value;

    public Dictionary<string, Object> ToDictionary()
    {
        Dictionary<string, Object> result = new Dictionary<string, Object>();
        result["type"] = (int)type;
        result["value"] = value;

        return result;
    }
}
[System.Serializable]
public class SubStat : IEquatable<SubStat>
{
    public SubStats type;
    public float value;
    public SubStat(SubStats type, float value)
    {
        this.type = type;
        this.value = value;
    }

    public bool Equals(SubStat other)
    {
        return this.type == other.type;
    }

    public Dictionary<string, Object> ToDictionary()
    {
        Dictionary<string, Object> result = new Dictionary<string, Object>();
        result["type"] = type.ToString();
        result["value"] = value;

        return result;
    }
}
public enum RuneGrade
{
    Wood,
    Bronze,
    Silver,
    Gold
}
// need to derive from item, but make item an interface
[System.Serializable]
public class Rune : Item
{
    public RuneSlot runeSlot;
    public RuneGrade runeLevel = RuneGrade.Wood;
    public RuneSetBonus runeSet;
    public MainStat mainStat;
    public List<SubStat> subStats = new List<SubStat>();
    public int stars;
    public int level = 1;
    public int experience;
    public string runeId;
    public Rune()
    {
        itemType = ItemType.Rune;
    }
    public Rune(RuneSlot _runeSlot, RuneSetBonus _runeSet) : this()
    {
        runeSlot = _runeSlot;
        runeSet = _runeSet;
        subStats.Add(new SubStat(0, 0));
    }
    public void UpgradeRuneLevel()
    {
        runeLevel++;
    }

    public Dictionary<string, Object> ToDictionary()
    {
        Dictionary<string, Object> result = new Dictionary<string, Object>();
        result["rune Slot"] = runeSlot.ToString();
        result["rune level"] = (int)runeLevel;
        result["main stat"] = mainStat.ToDictionary();
        result["rune set"] = runeSet.ToString();
        List<Object> substatList = new List<Object>();
        foreach (SubStat substat in subStats)
        {
            substatList.Add(substat.ToDictionary());
        }
        result["sub stats"] = substatList;
        //for (int i = 0; i < subStats.Count; i++)
        //{
        //    string resultName = "sub stat" + i;
        //    result[resultName] = subStats[i].ToDictionary();
        //}
        return result;
    }
}
