using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuneLevelUpgrader : MonoBehaviour
{
    public GameObject runeInfo;
    public GameObject runeDust;
    public GameObject runeLevelupGO;

    int runeDustOwned = 20000;

    int requiredDust;
    int requiredDustMaxLevel;
    public Rune selectedRune;
    public GameObject runeInfoGO;

    public void OpenRuneLevelUpgrader(Rune rune,GameObject runeInfoGO)
    {
        selectedRune = rune;
        this.runeInfoGO = runeInfoGO;
        runeInfoGO.transform.position = runeInfo.transform.position;
        DisplayRune(rune);
        runeLevelupGO.SetActive(true);
        //runeDisplayer.onFilterApplied += RefreshRuneCombine;
    }

    public void DisplayRune(Rune rune)
    {
        requiredDust = RuneManager.GetRequiredDust(rune.level);
        // calculate the max amount of dust the runes need to reach its max level
        requiredDustMaxLevel = RuneManager.GetRequiredDustToMaxLevel(rune);

        // Level
        runeInfoGO.transform.Find("Level").GetComponent<Text>().text = rune.level.ToString();
        // MainStat
        //runeGO.transform.Find("MainStat").Find("icon").GetComponent<Text>().text = rune.runeLevel.ToString();
        runeInfoGO.transform.Find("MainStat").Find("name").GetComponent<Text>().text = rune.mainStat.type.ToString();
        runeInfoGO.transform.Find("MainStat").Find("value").GetComponent<Text>().text = rune.mainStat.value.ToString();
        // SubStats
        for (int i = 0; i < rune.subStats.Count; i++)
        {
            runeInfoGO.transform.Find("SubStats").GetChild(i).gameObject.SetActive(true);
            runeInfoGO.transform.Find("SubStats").GetChild(i).Find("name").GetComponent<Text>().text = rune.subStats[i].type.ToString();
            runeInfoGO.transform.Find("SubStats").GetChild(i).Find("value").GetComponent<Text>().text = rune.subStats[i].value.ToString();
        }
        // rune dust requirement
        runeDust.SetActive(true);
        runeDust.transform.Find("required").GetComponent<Text>().text = requiredDust.ToString();
        runeDust.transform.Find("requiredMax").GetComponent<Text>().text = requiredDustMaxLevel.ToString();
        // if over 1000 => 1k
        string transformedRuneDustOwned = runeDustOwned.ToString();
        if (runeDustOwned > 1000)
        {
            double tempRuneDust = (double)runeDustOwned / 1000;
            transformedRuneDustOwned = tempRuneDust + "k";
        }
        runeDust.transform.Find("owned").GetComponent<Text>().text = transformedRuneDustOwned;

        // Cancel button
        runeInfoGO.transform.Find("Cancel").GetComponent<Button>().onClick.RemoveAllListeners();
        runeInfoGO.transform.Find("Cancel").GetComponent<Button>().onClick.AddListener(CancelRuneLevelUp);
    }

    public void TryToConfirmOneLevelUp()
    {
        // check if player has enough dust
        if (runeDustOwned < requiredDust)
        {
            Debug.Log("not enough dust");
            return;
        }
        // check if player has enough gold to do it
        ConfirmOneLevel();
    }
    public void TryToConfirmMaxLevelUp()
    {
        // check if player has enough dust
        if (runeDustOwned < requiredDustMaxLevel)
        {
            Debug.Log("not enough dust");
            return;
        }
        // check if player has enough gold to do it
        ConfirmMaxLevel();
    }
    public bool ConfirmOneLevel()
    {
        // and update savedata
        //ResetAmountSelected();
        if(!RuneManager.LevelUpRune(selectedRune))
        {
            return false;
        }
        runeDustOwned -= requiredDust;
        DisplayRune(selectedRune);
        return true;
    }
    public bool ConfirmMaxLevel()
    {
        // and update savedata
        //ResetAmountSelected();
        if (!RuneManager.LevelUpRuneMaxLevel(selectedRune))
        {
            return false;
        }
        runeDustOwned -= requiredDustMaxLevel;
        DisplayRune(selectedRune);
        return true;
    }
    public void CancelRuneLevelUp()
    {
        runeLevelupGO.SetActive(false);
        Destroy(runeInfoGO);
    }

}
