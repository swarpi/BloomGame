using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RuneManager
{
    public static Rune rune;
    public static TextAsset jsonFile;
    private static RuneManager instance;
    public static TextAsset jsonFileExp;
    static RuneExpRequirements expArray;

    static Dictionary<RuneGrade, int> runeGradeMaxStars= new Dictionary<RuneGrade, int>()
    {
        { RuneGrade.Wood,   0  },
        { RuneGrade.Bronze, 1  },
        { RuneGrade.Silver, 2  },
        { RuneGrade.Gold,   3  }
    };
    static Dictionary<int, int> runeStarsMaxLevel = new Dictionary<int, int>()
    {
        { 0, 5   },
        { 1, 8   },
        { 2, 11  },
        { 3, 14  }
    };
    static Dictionary<RuneGrade, float> runeGradeToDust = new Dictionary<RuneGrade, float>()
    {
        {RuneGrade.Wood,    1    },
        {RuneGrade.Bronze,  1.2f },
        {RuneGrade.Silver,  1.4f },
        {RuneGrade.Gold,    1.6f }
    };
    static Dictionary<int, int> runeLevelToDust = new Dictionary<int, int>()
    {
        { 0, 50   },
        { 1, 80   },
        { 2, 110  },
        { 3, 140  },
        { 4, 170  },
        { 5, 200  },
        { 6, 230  },
        { 7, 260  },
        { 8, 290  },
    };
    static List<int> runeSubstatLevelUpgrade = new List<int>()
    {
        5,
        8,
        11,
        14
    };
    public static RuneManager Instance()
    {

        instance = new RuneManager(jsonFile);
        return instance;
    }
    private RuneManager(TextAsset json)
    {
        jsonFile = json;
    }
    public static Rune GenerateRandomRune()
    {
        jsonFile = Resources.Load("RuneStatsValues") as TextAsset;
        Rune tempRune = new Rune();
        // init runeslot
        tempRune.runeSlot = GenerateRuneSlot();
        int runeSlotInt = (int)tempRune.runeSlot;
        //init mainStats
        tempRune.mainStat.type = (MainStats)Random.Range(0, 2);
        tempRune.mainStat.value = GetRuneStatsValue("mainStat", (int)tempRune.mainStat.type).InitialValue[runeSlotInt];
        // init Substat, get Number of Substat slots first
        if (tempRune.runeSlot == RuneSlot.Gamma)
        {
            SubStats sub = GenerateSubStats();
            float value = GetRuneStatsValue("subStat", (int)sub).InitialValue[runeSlotInt];
            tempRune.subStats.Add(new SubStat(sub, value));
        }
        else if (tempRune.runeSlot == RuneSlot.Beta)
        {
            // first substat
            SubStats sub = GenerateSubStats();
            float value = GetRuneStatsValue("subStat", (int)sub).InitialValue[runeSlotInt];
            tempRune.subStats.Add(new SubStat(sub, value));
            //second substat, need to check if new substat is the first one
            sub = GenerateSubStats((tempRune.subStats));
            value = GetRuneStatsValue("subStat", (int)sub).InitialValue[runeSlotInt];
            tempRune.subStats.Add(new SubStat(sub, value));
        }
        else if (tempRune.runeSlot == RuneSlot.Alpha)
        {
            // first substat
            SubStats sub = GenerateSubStats();
            float value = GetRuneStatsValue("subStat", (int)sub).InitialValue[runeSlotInt];
            tempRune.subStats.Add(new SubStat(sub, value));
            //second substat, need to check if new substat is the first one
            sub = GenerateSubStats((tempRune.subStats));
            value = GetRuneStatsValue("subStat", (int)sub).InitialValue[runeSlotInt];
            tempRune.subStats.Add(new SubStat(sub, value));
            //third substat, need to check if new substat is the first or second one
            sub = GenerateSubStats((tempRune.subStats));
            value = GetRuneStatsValue("subStat", (int)sub).InitialValue[runeSlotInt];
            tempRune.subStats.Add(new SubStat(sub, value));
        }
        //init RuneSet
        tempRune.runeSet = (RuneSetBonus)Random.Range(0, 2);
        tempRune.runeLevel = (RuneGrade)Random.Range(0, 4);
        tempRune.runeId = System.Guid.NewGuid().ToString();
        rune = tempRune;
        return tempRune;
    }
    // generate rune slot depending on the ratio of the runeSlot
    public static RuneSlot GenerateRuneSlot(int alphaRatio = 1, int betaRatio = 2, int gammRatio = 3)
    {
        int totalRatio = alphaRatio + betaRatio + gammRatio;
        int value = Random.Range(0, totalRatio);

        if ((value -= alphaRatio) < 0)
        {
            return RuneSlot.Alpha;
        }
        if ((value -= betaRatio) < 0)
        {
            return RuneSlot.Beta;
        }
        if ((value -= gammRatio) < 0)
        {
            return RuneSlot.Gamma;
        }
        return RuneSlot.Gamma;
    }

    public static SubStats GenerateSubStats(List<SubStat> existingSubStats = null)
    {
        SubStats subStat = (SubStats)Random.Range(0, 3);
        // check if the rune has the substat already
        if (existingSubStats != null)
        {
            if (existingSubStats.Contains(new SubStat(subStat, 0)))
            {
                // recursive until the substat isnt on the rune
                return GenerateSubStats(existingSubStats);
            }
        }

        return subStat;
    }
    public static RuneStatsValue GetRuneStatsValue(string type, int keyword)
    {
        RuneStatsValues array = JsonUtility.FromJson<RuneStatsValues>(jsonFile.text);
        foreach (RuneStatsValue value in array.runeStatsValues)
        {
            if (value.Type != type || value.KeyWord != keyword) continue;
            return value;
        }
        return null;
    }

    public static Rune ConvertRuneFromDictionary(Dictionary<string, System.Object> result)
    {
        Rune rune = new Rune();

        System.Enum.TryParse(result["rune Slot"].ToString(), out RuneSlot runeslotEnum);
        rune.runeSlot = runeslotEnum;
        System.Enum.TryParse(result["rune level"].ToString(), out RuneGrade runegradeEnum);
        rune.runeLevel = runegradeEnum;
        Dictionary<string, System.Object> mainstat = (Dictionary<string, System.Object>)result["main stat"];
        System.Enum.TryParse(mainstat["type"].ToString(), out MainStats runemainStatEnum);
        rune.mainStat.type = runemainStatEnum;
        rune.mainStat.value = float.Parse(mainstat["value"].ToString());
        System.Enum.TryParse(result["rune set"].ToString(), out RuneSetBonus runeSetEnum);
        rune.runeSet = runeSetEnum;
        List<System.Object> substatList = (List<System.Object>)result["sub stats"];
        foreach (System.Object substat in substatList)
        {
            Dictionary<string, System.Object> dicSub = (Dictionary<string, System.Object>)substat;
            System.Enum.TryParse(dicSub["type"].ToString(), out SubStats subStatsEnum);
            float value = rune.mainStat.value = float.Parse(dicSub["value"].ToString());
            SubStat tempSubstat = new SubStat(subStatsEnum, value);
            rune.subStats.Add(tempSubstat);
        }

        return rune;
    }

    public static bool LevelUpRune(Rune rune)
    {
        // check if max level reached
        if (IsRuneMaxLevel(rune.stars, rune.level))
        {
            Debug.Log("max level reached");
            return false;
        }
        rune.level++;
        // increase mainstat
        rune.mainStat.value = IncreaseMainStat(rune);
        // increase substat
        if (runeSubstatLevelUpgrade.Contains(rune.level))
        {
            Debug.Log("increasing substat");
            IncreaseSubstat(rune.subStats, rune.runeSlot);
        }

        return true;
    }

    public static bool LevelUpRuneMaxLevel(Rune rune)
    {
        // check if max level reached
        if (IsRuneMaxLevel(rune.stars, rune.level))
        {
            Debug.Log("max level reached");
            return false;
        }
        for (int i = rune.level; i < RuneManager.GetRuneMaxLevel(rune.stars); i++)
        {
            LevelUpRune(rune);
        }
        //rune.level = RuneManager.GetRuneMaxLevel(rune.stars);
        return true;
    }
    static float IncreaseMainStat(Rune rune)
    {
        float mainStatValue = rune.mainStat.value;
        RuneStatsValue runeStatsValue = GetRuneStatsValue("mainStat", (int)rune.mainStat.type);
        mainStatValue += runeStatsValue.ValuePerLevel[(int)rune.runeSlot];
        return mainStatValue;
    }
    //TODO change parameter to rune
    static void IncreaseSubstat(List<SubStat> substats, RuneSlot slot)
    {
        // choose a random substat to upgrade
        int subStatIndex = Random.Range(0, substats.Count);
        SubStat subStat = substats[subStatIndex];

        // get the upgrade range of the substat
        RuneStatsValue runeStatsValue = GetRuneStatsValue("subStat", (int)subStat.type);
        string upgradeRangeString = runeStatsValue.UpgradeRange[(int)slot];
        string[] values = upgradeRangeString.Split('-');
        int upgradeInt = Random.Range(int.Parse(values[0]), int.Parse(values[1]));

        subStat.value += upgradeInt;
    }
    public static int GetRequiredDustToMaxLevel(Rune rune)
    {
        int requiredDustMaxLevel = 0;
        for (int i = rune.level; i < RuneManager.GetRuneMaxLevel(rune.stars); i++)
        {
            requiredDustMaxLevel += RuneManager.GetRequiredDust(i);
        }
        return requiredDustMaxLevel;
    }
    public static int GetRequiredExp(int level)
    {
        jsonFileExp = Resources.Load("RuneExpRequirements") as TextAsset;
        expArray = JsonUtility.FromJson<RuneExpRequirements>(jsonFileExp.text);
        List<RuneExpRequirement> expList = expArray.runeExpRequirements.OfType<RuneExpRequirement>().ToList();
        return expList.Find(x => x.level == (level + 1)).requiredExp;
    }

    public static int GetRequiredDust(int level)
    {
        jsonFileExp = Resources.Load("RuneExpRequirements") as TextAsset;
        expArray = JsonUtility.FromJson<RuneExpRequirements>(jsonFileExp.text);
        List<RuneExpRequirement> expList = expArray.runeExpRequirements.OfType<RuneExpRequirement>().ToList();
        return expList.Find(x => x.level == (level + 1)).requiredDust;
    }
    public static int GetRuneMaxLevel(int runeStars)
    {
        return runeStarsMaxLevel[runeStars];
    }
    public static bool IsRuneMaxLevel(int runeStars ,int runeLevel)
    {
        if(runeStarsMaxLevel[runeStars] > runeLevel)
        {
            return false;
        }
        return true;
    }

    public static bool IsRuneMaxStar(RuneGrade runeGrade, int runeStars)
    {
        if(runeGradeMaxStars[runeGrade] > runeStars)
        {
            return false;
        }
        return true;
    }

    public static int GetRuneDustAmount(RuneGrade runeGrade, int runeLevel)
    {
        int totalAmount = 0;
        totalAmount += runeLevelToDust[runeLevel];
        totalAmount = (int)(totalAmount * runeGradeToDust[runeGrade]);

        return totalAmount;
    }
}
