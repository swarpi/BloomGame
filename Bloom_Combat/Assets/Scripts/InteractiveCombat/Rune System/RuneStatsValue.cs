using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class RuneStatsValue
{
    public string Type;
    public int KeyWord;
    public int[] InitialValue;
    public int ValuePerRank;
    public float[] ValuePerLevel;
    public string[] UpgradeRange;
    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }

    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
}
[System.Serializable]
public class RuneStatsValues
{
    public RuneStatsValue[] runeStatsValues;
}
