using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuneDisenchanter : MonoBehaviour
{
    public List<Rune> runeList;
    public GameObject contentGO;
    public GameObject runePrefab;
    public GameObject disenchantPopup;
    public GameObject menuButtons;
    public GameObject runeArrangementButtons;

    public Text arrangeButtonText;
    public Text totalDisenchantAmountText;
    public List<GameObject> selectedRunesToDisenchant;
    bool isArranging = false;


    public PlayerInventory playerInventory;

    public int totalDisenchantAmount;
    public int totalSellAmount;

    void Start()
    {
        selectedRunesToDisenchant = new List<GameObject>();
        runeList = playerInventory.runeList;
        //DisplayAllRunesInventory();
    }

    public void ArrangeRunes()
    {
        if (!isArranging)
        {
            isArranging = true;
            foreach (Transform child in contentGO.transform)
            {
                Rune tempRune = child.GetComponent<RuneContainer>().rune;
                child.GetComponentInChildren<Button>().onClick.AddListener(delegate { SelectRuneToDisenchant(tempRune, child.gameObject); });
            }
            menuButtons.SetActive(false);
            runeArrangementButtons.SetActive(true);
            arrangeButtonText.text = "Cancel";
        }
        else
        {
            isArranging = false;
            foreach (Transform child in contentGO.transform)
            {
                child.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
                child.GetComponentInChildren<Image>().color = Color.white;
            }
            menuButtons.SetActive(true);
            runeArrangementButtons.SetActive(false);
            totalDisenchantAmount = 0;
            totalSellAmount = 0;
            selectedRunesToDisenchant = new List<GameObject>();
            arrangeButtonText.text = "Arrange";
        }

    }
    

    public void SelectRuneToDisenchant(Rune rune, GameObject runeGO)
    {
        int disEnchantAmount = RuneManager.GetRuneDustAmount(rune.runeLevel,rune.level);
        int sellAmount = disEnchantAmount / 2;
        totalDisenchantAmount += disEnchantAmount;
        totalSellAmount += sellAmount;
        selectedRunesToDisenchant.Add(runeGO);

        runeGO.GetComponentInChildren<Image>().color = Color.gray;
        runeGO.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
        runeGO.GetComponentInChildren<Button>().onClick.AddListener(delegate { DeselectRuneToDisenchant(rune, runeGO); });
        // runedisenchantamount
    }
    public void DeselectRuneToDisenchant(Rune rune, GameObject runeGO)
    {
        int disEnchantAmount = RuneManager.GetRuneDustAmount(rune.runeLevel, rune.level);
        int sellAmount = disEnchantAmount / 2;
        totalDisenchantAmount -= disEnchantAmount;
        totalSellAmount += sellAmount;
        selectedRunesToDisenchant.Remove(runeGO);

        runeGO.GetComponentInChildren<Image>().color = Color.white;
        runeGO.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
        runeGO.GetComponentInChildren<Button>().onClick.AddListener(delegate { SelectRuneToDisenchant(rune, runeGO); });
        // runedisenchantamount
    }

    public void DisenchantRunes()
    {
        disenchantPopup.SetActive(true);
        totalDisenchantAmountText.text = totalDisenchantAmount.ToString();
    }

    public void ConfirmDisenchantment()
    {
        // give player dust
        Debug.Log("player got " + totalDisenchantAmount + " dust");
        // remove rune from player
        foreach(GameObject runeGo in selectedRunesToDisenchant)
        {
            Destroy(runeGo);
        }
        CancelDisenchantment();
        //DisplayAllRunesInventory();
    }

    public void CancelDisenchantment()
    {
        isArranging = true;
        ArrangeRunes();
        disenchantPopup.SetActive(false);
    }
}
