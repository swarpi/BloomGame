/*using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RuneUpgrader : MonoBehaviour
{
    public List<Rune> runeList;
    public GameObject contentGO;
    public GameObject runePrefab;
    public GameObject runeInfo;
    public GameObject runeLevel;
    public GameObject runeImage;
    public GameObject runeImageMaterial;
    public GameObject runeDust;

    public PlayerInventory playerInventory;

    public Text currentExpUI;
    public Text requiredExpUI;
    public Text currentLevelUI;
    public Image expImage;


    public int runeDustOwned;

    float current;
    float required;
    RuneGrade currenGrade;
    int currentStars;
    int currentLevel;
    int requiredDust;
    int requiredDustMaxLevel;

    public bool isDisEnchanting;
    public int totalDisEnchantAmount;

    public Rune selectedRune;
    public Rune selectedMaterialRune;
    // Start is called before the first frame update
    void Start()
    {

        //DisplayAllRunesInventoryFirstTime();

        runeList = playerInventory.runeList;
        //DisplayAllRunesInventory();
        //DisplayRune(runeList[0]);
    }

    void DisplayAllRunesInventoryFirstTime()
    {
        for (int i = 0; i < 10; i++)
        {
            Rune tempRune = RuneManager.GenerateRandomRune();
            runeList.Add(tempRune);
            GameObject tempRuneGO = Instantiate(runePrefab);
            tempRuneGO.transform.Find("slot").GetComponent<Text>().text = tempRune.runeSlot.ToString();
            tempRuneGO.transform.Find("Mainstat").GetComponent<Text>().text = tempRune.mainStat.type.ToString();
            tempRuneGO.transform.SetParent(contentGO.transform);
            tempRuneGO.GetComponentInChildren<Button>().onClick.AddListener(delegate { DisplayRune(tempRune); });
            tempRuneGO.GetComponentInChildren<Button>().onClick.AddListener(delegate { SelectRuneToDisenchant(tempRune, tempRuneGO); });
        }
    }

    void DisplayAllRunesInventory()
    {
        foreach (Transform child in contentGO.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        foreach (Rune newRune in runeList)
        {
            GameObject tempRuneGO = Instantiate(runePrefab);
            tempRuneGO.transform.Find("slot").GetComponent<Text>().text = newRune.runeSlot.ToString();
            tempRuneGO.transform.Find("Mainstat").GetComponent<Text>().text = newRune.mainStat.type.ToString();
            tempRuneGO.transform.SetParent(contentGO.transform);
            tempRuneGO.GetComponentInChildren<Button>().onClick.AddListener(delegate { DisplayRune(newRune); });
            tempRuneGO.GetComponentInChildren<Button>().onClick.AddListener(delegate { SelectRuneToDisenchant(newRune,tempRuneGO); });
        }
    }

    void DisplayAllRunesForEnchanting(Rune rune)
    {
        foreach (Transform child in contentGO.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        List<Rune> newRuneList = runeList.FindAll(x => x.stars == rune.stars);
        foreach(Rune newRune in newRuneList)
        {
            if (newRune == rune) continue;
            GameObject tempRuneGO = Instantiate(runePrefab);
            tempRuneGO.transform.Find("slot").GetComponent<Text>().text = newRune.runeSlot.ToString();
            tempRuneGO.transform.Find("Mainstat").GetComponent<Text>().text = newRune.mainStat.type.ToString();
            tempRuneGO.transform.SetParent(contentGO.transform);
            tempRuneGO.GetComponent<Button>().onClick.AddListener(delegate { DisplayRuneAsEnchantMaterial(newRune); });
        }

    }

    public void DisplayRune(Rune rune)
    {
        if (isDisEnchanting) return;
        selectedRune = rune;
        currenGrade = rune.runeLevel;
        currentLevel = rune.level;*//**//*
        currentStars = rune.stars;
        current = rune.experience;
        requiredDust = RuneManager.GetRequiredDust(currentLevel);
        requiredDustMaxLevel = 0;
        for (int i = currentLevel; i < RuneManager.GetRuneMaxLevel(currentStars); i++)
        {
            requiredDustMaxLevel += RuneManager.GetRequiredDust(i);
        }
        required = RuneManager.GetRequiredExp(currentLevel);

        // rune image
        runeImage.SetActive(true);
        runeImage.transform.Find("slot").GetComponent<Text>().text = rune.runeSlot.ToString();
        runeImage.transform.Find("Mainstat").GetComponent<Text>().text = rune.mainStat.type.ToString();
        runeImage.transform.Find("Stars").GetComponent<Text>().text = rune.stars.ToString();

        UpdateInfoBox();
        // rune text info
        runeInfo.transform.Find("Rune Slot").GetComponent<Text>().text = rune.runeSlot.ToString();
        runeInfo.transform.Find("Rune Grade").GetComponent<Text>().text = rune.runeLevel.ToString();
        runeInfo.transform.Find("Rune Stars").GetComponent<Text>().text = rune.stars.ToString();

        // rune dust requirement
        runeDust.SetActive(true);
        runeDust.transform.Find("required").GetComponent<Text>().text = requiredDust.ToString();
        runeDust.transform.Find("requiredMax").GetComponent<Text>().text = requiredDustMaxLevel.ToString();
        // if over 1000 => 1k
        string transformedRuneDustOwned = runeDustOwned.ToString();
        if(runeDustOwned > 1000)
        {
            double tempRuneDust = (double)runeDustOwned / 1000;
            transformedRuneDustOwned = tempRuneDust + "k";
        }
        runeDust.transform.Find("owned").GetComponent<Text>().text = transformedRuneDustOwned;
    }

    public void DisplayRuneAsEnchantMaterial(Rune rune)
    {
        selectedMaterialRune = rune;
        runeImageMaterial.SetActive(true);
        runeImageMaterial.transform.Find("slot").GetComponent<Text>().text = rune.runeSlot.ToString();
        runeImageMaterial.transform.Find("Mainstat").GetComponent<Text>().text = rune.mainStat.type.ToString();
        runeImageMaterial.transform.Find("Stars").GetComponent<Text>().text = rune.stars.ToString();
    }

    public void SelectRuneToDisenchant(Rune rune, GameObject runeGO)
    {
        if (!isDisEnchanting) return;
        Debug.Log(runeGO.GetComponent<Button>().onClick.GetPersistentEventCount() + " amount ");
        int disEnchantAmount = 100;
        totalDisEnchantAmount += disEnchantAmount;
        runeGO.GetComponent<Image>().color = Color.gray;
        runeGO.GetComponent<Button>().onClick.RemoveListener(delegate { SelectRuneToDisenchant(rune, runeGO); });
        runeGO.GetComponent<Button>().onClick.AddListener(delegate { DeselectRuneToDisenchant(rune, runeGO); });
        // runedisenchantamount
    }
    public void DeselectRuneToDisenchant(Rune rune, GameObject runeGO)
    {
        if (!isDisEnchanting) return;
        Debug.Log("Deselecting rune");
        int disEnchantAmount = 100;
        totalDisEnchantAmount -= disEnchantAmount;
        runeGO.GetComponent<Image>().color = Color.white;
        runeGO.GetComponent<Button>().onClick.AddListener(delegate { SelectRuneToDisenchant(rune, runeGO); });
        // runedisenchantamount
    }
    public void ChangeToDisEnchanting()
    {
        isDisEnchanting = !isDisEnchanting;
    }
    void UpdateInfoBox()
    {
        currentExpUI.text = "" + current;
        requiredExpUI.text = "" + required;
        currentLevelUI.text = "" + selectedRune.level;
*//*        smallAmountText.text = "" + potions.FindAll(x => x.potionType == ExpPotionType.Small).Count;
        mediumAmountText.text = "" + potions.FindAll(x => x.potionType == ExpPotionType.Medium).Count;
        largeAmountText.text = "" + potions.FindAll(x => x.potionType == ExpPotionType.Large).Count;*//*
        float quotient = current / required;
        expImage.fillAmount = quotient;
        // update potion amount
        
    }


    public void GiveCharm(int amount)
    {
        if (RuneManager.IsRuneMaxLevel(currentStars, currentLevel))
        {
            Debug.Log("max level reached");
            return;
        }
        // need to add restriction if over lvl 50, then every 10 levels
        // TODO adapt main stats
        current += amount;
        currentExpUI.text = "" + current;
        float quotient = current / required;
        expImage.fillAmount = quotient;
        // TODO: implement for hero exp in server
        int whileStopper = 0;
        while (current >= required && whileStopper < 100)
        {
            current = current - required;
            if (RuneManager.IsRuneMaxLevel(currentStars, currentLevel))
            {
                current = 0;
                currentExpUI.text = "MAX";
                return;
            }
            currentLevel++;
            required = RuneManager.GetRequiredExp(currentLevel);
            UpdateInfoBox();
            whileStopper++;
        }
    }
    public void TryToConfirmOneLevelUp()
    {
        // check if max level reached
        if (RuneManager.IsRuneMaxLevel(selectedRune.stars, selectedRune.level))
        {
            Debug.Log("max level reached");
            return;
        }
        // check if player has enough dust
        if (runeDustOwned < requiredDust)
        {
            Debug.Log("not enough dust");
            return;
        }
        // check if player has enough gold to do it
        ConfirmOneLevel();
    }
    public void TryToConfirmMaxLevelUp()
    {
        // check if max level reached
        if (RuneManager.IsRuneMaxLevel(selectedRune.stars, selectedRune.level))
        {
            Debug.Log("max level reached");
            return;
        }
        // check if player has enough dust
        if (runeDustOwned < requiredDustMaxLevel)
        {
            Debug.Log("not enough dust");
            return;
        }
        // check if player has enough gold to do it
        ConfirmMaxLevel();
    }
    public bool ConfirmOneLevel()
    {
        // and update savedata
        //ResetAmountSelected();
        runeDustOwned -= requiredDust;
        selectedRune.level ++;
        DisplayRune(selectedRune);
        //hero.heroExp = (int)current;
        //hero.heroLevel = currentLevel;
        //HeroIdentifier.Instance.AdaptHeroStatsToLevel(hero);
        return true;
    }
    public bool ConfirmMaxLevel()
    {
        // and update savedata
        //ResetAmountSelected();
        runeDustOwned -= requiredDustMaxLevel;
        selectedRune.level = RuneManager.GetRuneMaxLevel(selectedRune.stars);
        DisplayRune(selectedRune);
        //hero.heroExp = (int)current;
        //hero.heroLevel = currentLevel;
        //HeroIdentifier.Instance.AdaptHeroStatsToLevel(hero);
        return true;
    }
    public void EnchantRune()
    {
        if (!RuneManager.IsRuneMaxStar(selectedRune.runeLevel, selectedRune.stars))
        {
            DisplayAllRunesForEnchanting(selectedRune);
        }
        Debug.Log("this rune reached its max level already, try to increase its grade");
    }

    public void ConfirmEnchant()
    {
        if(selectedRune.stars != selectedMaterialRune.stars)
        {
            Debug.Log("ups these runes do not have the same amount of stars");
            return;
        }
        if(RuneManager.IsRuneMaxStar(selectedRune.runeLevel, selectedRune.stars))
        {
            Debug.Log("this rune reached its max level already, try to increase its grade");
            return;
        }
        selectedRune.stars++;
        runeList.Remove(selectedMaterialRune);
        CancelEnchant();
    }

    public void CancelEnchant()
    {
        runeImage.SetActive(false);
        runeImageMaterial.SetActive(false);
        DisplayAllRunesInventory();
    }
}
*/