using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum AbilityIndex
{
    First,
    Second
}
[CreateAssetMenu(fileName = "New Ability", menuName = "ability")]
public class AbilityScriptable : ScriptableObject
{
    public Sprite icon;
    public string abilityName;
    public string hero;
    // either ability one or ability two
    public AbilityIndex abilityIndex;


    public void UseAbility()
    {
        Debug.Log("using ability " + abilityName + " on rank " + abilityIndex.ToString());
    }
}
