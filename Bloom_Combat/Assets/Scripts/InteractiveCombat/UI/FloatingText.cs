﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Floating text display damage numbers champions take
/// </summary>
public class FloatingText : MonoBehaviour
{
    private CanvasGroup canvasGroup;
    private Vector3 moveDirection;
    private float timer = 0;

    ///How fast text moves
    float speed = 3;

    ///How long the text is visible
    float fadeOutTime = 0.5f;

    void Update()
    {
        this.transform.parent.transform.position = this.transform.parent.transform.position + moveDirection * speed * Time.deltaTime;

        timer += Time.deltaTime;
        float fade = (fadeOutTime - timer) / fadeOutTime;
        canvasGroup.alpha = fade;
        if (fade <= 0)
            Destroy(this.gameObject.transform.parent.gameObject);
    }
    /// <summary>
    /// Called when floating text is created
    /// </summary>
    /// <param name="startPosition"></param>
    /// <param name="v"></param>
    public void Init(float v)
    {
        canvasGroup = this.GetComponent<CanvasGroup>();
        this.GetComponent<TextMesh>().text = Mathf.Round(v).ToString();

        moveDirection = new Vector3(Random.Range(-0.2f, 0.2f), Random.Range(-0.2f, 0.2f), 0 );
    }
}
