using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainSettingsMenu : MonoBehaviour
{
    public static MainSettingsMenu current;
    private void Awake()
    {
        current = this;
    }
    public void ChangeToCampaignScene()
    {
        SceneChangedTrigger("CampaignScene");
    }
    // change name
    public void ChangeToCampScene()
    {
        SceneChangedTrigger("CampScene");
    }
    // change name
    public void ChangeToTrainingScene()
    {
        SceneChangedTrigger("TrainingScene");
    }
    public void ChangeToHeroInventoryScene()
    {
        SceneChangedTrigger("HeroInventoryScene");
    }
    // Start is called before the first frame update

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // if scene is campaign, camp, trainig scene keep it active
        // otherwise hide it
    }
    public event Action<string> onSceneChanged;

    public void SceneChangedTrigger(string sceneName)
    {
        if(onSceneChanged != null)
        {
            onSceneChanged(sceneName);
        }
    }
    
}
