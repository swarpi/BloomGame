using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampController : MonoBehaviour
{
    public GameObject campUI;
    // Start is called before the first frame update
    private void Start()
    {
        MainSettingsMenu.current.onSceneChanged += OnSceneLoaded;
    }

    void OnSceneLoaded(string scene)
    {
        if (scene == "CampScene")
            campUI.SetActive(true);
        else
            campUI.SetActive(false);
    }
}
