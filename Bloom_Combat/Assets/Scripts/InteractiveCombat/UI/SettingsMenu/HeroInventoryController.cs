using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HeroInventoryController : MonoBehaviour
{
    public GameObject heroInventoryUI;
    public GameObject heroFramePrefab;
    public int numberToCreate;

    public GameObject contentGO;
    public HeroInventory heroInventory;
    public GameObject[] championsFrameArray;
    public HeroHighlightManager heroHighlighter;

    public GameObject generalUI;
    // Start is called before the first frame update
    private void Start()
    {
        MainSettingsMenu.current.onSceneChanged += OnSceneLoaded;
    }

    void OnSceneLoaded(string scene)
    {
        if (scene == "HeroInventoryScene")
        {
            heroInventoryUI.SetActive(true);
            PopulateGrid();
            //CreateHeroGridPositions();
        }
        else
            heroInventoryUI.SetActive(false);
    }


    // need to adapt size so it fits properely
    void PopulateGrid()
    {
        List<HeroSaveData> heroList = heroInventory.heroInventoryContainer.heroIDList;
        championsFrameArray = new GameObject[heroList.Count];
        for (int i = 0; i< heroList.Count; i++)
        {

            GameObject display = Instantiate(heroFramePrefab, contentGO.transform);
            Hero hero = HeroIdentifier.Instance.GetHeroGOFromHeroID(heroList[i].heroID);
            display.name += i;
            if(hero == null)
            {
                Debug.Log("error no hero found");
                return;
            }
            display.transform.Find("Hero Name").GetComponent<Text>().text = hero.uiname;

            display.GetComponent<Button>().onClick.AddListener(OnHeroClicked);
            championsFrameArray[i] = display;
        }
    }


    public void OnHeroClicked()
    {
        //get clicked champion ui name
        string name = EventSystem.current.currentSelectedGameObject.transform.name;

        //calculate index from name
        string defaultName = "hero image(Clone)";
        int championFrameIndex = int.Parse(name.Substring(defaultName.Length));

        //message shop from click
        ShowcaseOneHero(championFrameIndex);
    }



    void ShowcaseOneHero(int index)
    {
        Debug.Log("clicked on a hero");
        generalUI.SetActive(false);
        heroHighlighter.LoadHeroHighlightScene(HeroIdentifier.Instance.GetHeroGOFromHeroID(heroInventory.heroInventoryContainer.heroIDList[index].heroID));
    }
}
