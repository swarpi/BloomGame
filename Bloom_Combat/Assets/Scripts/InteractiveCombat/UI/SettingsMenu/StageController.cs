using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StageController : MonoBehaviour
{
    // Display stage
    public Text currentStageText;
    string defaultStage = "Stage ";
    public GameObject campaignUI;
    // Start battle
    // Start is called before the first frame update
    private void Start()
    {
        MainSettingsMenu.current.onSceneChanged += OnSceneLoaded;

    }

    void OnSceneLoaded(string scene)
    {
        if(scene == "CampaignScene")
        {
            campaignUI.SetActive(true);
            currentStageText.text = defaultStage + GameProgress.Instance.currentLevel.levelID + " - " + GameProgress.Instance.currentStage.stageID;
        }
            
        else
            campaignUI.SetActive(false);
    }


    public void MoveToCombatScreen()
    {
        SceneManager.LoadScene("StageSelection");
    }
    public static void MoveToCombatScreenFromCombat()
    {
        // 
        SceneManager.LoadScene("StageSelection");
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
