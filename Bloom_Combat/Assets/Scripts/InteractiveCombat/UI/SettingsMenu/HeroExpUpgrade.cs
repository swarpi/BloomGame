using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HeroExpUpgrade : MonoBehaviour
{
    public GameObject heroExpUpgradeGO;
    public GameObject heroAscensionGO;

    public HeroStatsDisplay heroStatsDisplay;

    Hero hero;

    public Text currentExpUI;
    public Text requiredExpUI;
    public Text currentLevelUI;
    public Image expImage;

    public float current;
    public float required;
    int currentLevel;

    public int smallExpPotion = 500;
    public int mediumExpPotion = 2000;
    public int largeExpPotion = 6000;

    public Text smallAmountText;
    public Text mediumAmountText;
    public Text largeAmountText;

    public Text smallAmountSelectedText;
    public Text mediumAmountSelectedText;
    public Text largeAmountSelectedText;

    int smallAmountSelected = 0;
    int mediumAmountSelected = 0;
    int largeAmountSelected = 0;

    List<HeroExpPotion> potions;
    List<Item> heroInv;
    public void GetAllHeroExpPotions()
    {
        List<Item> tempList = heroInv.FindAll(x => x.itemType == Item.ItemType.Potion);
        for(int i = 0; i < tempList.Count; i++)
        {
            potions.Add((HeroExpPotion) tempList[i]);
        }
    }
    private void Start()
    {
        heroStatsDisplay = GetComponent<HeroStatsDisplay>();
        heroInv = new List<Item>();
        potions = new List<HeroExpPotion>();
        for(int i = 0; i < 10; i++)
        {
            heroInv.Add(new SmallHeroExpPotion());
            heroInv.Add(new MediumHeroExpPotion());
        }
        for (int i = 0; i < 5; i++)
        {
            heroInv.Add(new LargeHeroExpPotion());
        }
        GetAllHeroExpPotions();
    }
    public void LoadHeroExpPopUp(Hero hero)
    {
        this.hero = hero;
        current = hero.heroExp;
        currentLevel = hero.heroLevel;
        required = HeroIdentifier.Instance.GetRequiredExp(currentLevel);
        heroStatsDisplay.LoadHeroStats(hero);
        UpdateInfoBox();
    }
    public void UpdateInfoBox()
    {
        // need to add restriction if over lvl 50, then every 10 levels
        if (hero.heroLevel >= 30)
        {
            if (currentLevel % 10 == 0)
            {
                CloseExpOpenAsc();
                return;
            }
        }
        else OpenExpCloseAsc();
        currentExpUI.text = "" + current;
        requiredExpUI.text = "" + required;
        currentLevelUI.text = "" + currentLevel;
        smallAmountText.text = "" + potions.FindAll(x => x.potionType == ExpPotionType.Small).Count;
        mediumAmountText.text = "" + potions.FindAll(x => x.potionType == ExpPotionType.Medium).Count;
        largeAmountText.text = "" + potions.FindAll(x => x.potionType == ExpPotionType.Large).Count;
        float quotient = current / required;
        expImage.fillAmount = quotient;
        //heroStatsDisplay.UpgradeHeroStatsLVL(currentLevel);
        // update potion amount
    }
    public void OpenExpCloseAsc()
    {
        heroExpUpgradeGO.SetActive(true);
        heroAscensionGO.SetActive(false);
    }
    public void CloseExpOpenAsc()
    {
        heroExpUpgradeGO.SetActive(false);
        heroAscensionGO.SetActive(true);
        LoadHeroAscension();
    }
    public int heroStage;
    Dictionary<AscensionMaterial, int> materialDic;
    public void LoadHeroAscension()
    {
        heroStage = (hero.heroLevel - 30) / 10 + 1;
        materialDic = AscensionMaterialManager.Instance.GetAscensionMaterial(hero.zoodiacSign, heroStage);
        int counter = 0;
        GameObject materialsGO = heroAscensionGO.transform.Find("Materials").gameObject;
        foreach (KeyValuePair<AscensionMaterial, int> kvp in materialDic.OrderByDescending(key => key.Value))
        {
            materialsGO.transform.GetChild(counter).Find("name").GetComponent<Text>().text = kvp.Key.ascensionName;
            materialsGO.transform.GetChild(counter).Find("amount").GetComponent<Text>().text = kvp.Value.ToString();
            counter++;
        }
    }
    // TODO add logic
    public void TryToConfirmAscension()
    {
        // check if player has enough material
        foreach (KeyValuePair<AscensionMaterial, int> kvp in materialDic.OrderByDescending(key => key.Value))
        {

        }
        // check if player has enough gold
        if (ConfirmAscension())
        {
            Debug.Log("Ascension successful");
        }
        Debug.Log("Ascension failed");
    }
    bool ConfirmAscension()
    {
        // increase ascension stage of hero
        return true;
    }
    public void GiveSmallExpPotion()
    {
        GiveExpPotion(smallExpPotion);
        smallAmountSelected++;
        if(smallAmountSelected == 0)
        {
            smallAmountSelectedText.text = "";
        }
        else
        {
            smallAmountSelectedText.transform.parent.gameObject.SetActive(true);
            smallAmountSelectedText.text = smallAmountSelected.ToString();
        }
    }
    public void GiveMediumExpPotion()
    {
        GiveExpPotion(mediumExpPotion);
        mediumAmountSelected++;
        if (mediumAmountSelected == 0)
        {
            mediumAmountSelectedText.text = "";
        }
        else
        {
            mediumAmountSelectedText.transform.parent.gameObject.SetActive(true);
            mediumAmountSelectedText.text = mediumAmountSelected.ToString();
        }
    }
    public void GiveLargeExpPotion()
    {
        GiveExpPotion(largeExpPotion);
        largeAmountSelected++;
        if (largeAmountSelected == 0)
        {
            largeAmountSelectedText.text = "";
        }
        else
        {
            largeAmountSelectedText.transform.parent.gameObject.SetActive(true);
            largeAmountSelectedText.text = largeAmountSelected.ToString();
        }
    }
    public void GiveExpPotion(int amount)
    {
        // need to add restriction if over lvl 50, then every 10 levels
        if(currentLevel >= 30)
        {
            if(currentLevel%10 == 0)
            {
                Debug.Log("need to ascend hero to continue level up");
                return;
            }
        }
        current += amount;
        currentExpUI.text = "" + current;
        float quotient = current / required;
        expImage.fillAmount = quotient;

        if (current >= required)
        {
            current = current - required;
            currentLevel++;
            required = HeroIdentifier.Instance.GetRequiredExp(currentLevel);
            UpdateInfoBox();
        }
    }
    public void TakeExpPotionSmall()
    {
        if (smallAmountSelected <= 0)
        {
            Debug.Log(" not enough Small Potions selected");
            return;
        }
        TakeExpPotion(smallExpPotion);
        smallAmountSelected--;
        if (smallAmountSelected == 0)
        {
            smallAmountSelectedText.text = "";
            smallAmountSelectedText.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            smallAmountSelectedText.text = smallAmountSelected.ToString();
        }
    }
    public void TakeExpPotionMedium()
    {
        if (mediumAmountSelected <= 0)
        {
            Debug.Log(" not enough Medium Potions selected");
            return;
        }
        TakeExpPotion(mediumExpPotion);
        mediumAmountSelected--;
        if (mediumAmountSelected == 0)
        {
            mediumAmountSelectedText.text = "";
            mediumAmountSelectedText.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            mediumAmountSelectedText.text = mediumAmountSelected.ToString();
        }
    }
    public void TakeExpPotionLarge()
    {
        if(largeAmountSelected <= 0)
        {
            Debug.Log(" not enough Large Potions selected");
            return;
        }
        TakeExpPotion(largeExpPotion);
        largeAmountSelected--;
        if (largeAmountSelected == 0)
        {
            largeAmountSelectedText.text = "";
            largeAmountSelectedText.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            largeAmountSelectedText.text = largeAmountSelected.ToString();
        }
    }
    public void TakeExpPotion(int amount)
    {
        // need to add restriction if over lvl 50, then every 10 levels
        
        current -= amount;
        currentExpUI.text = "" + current;
        float quotient = current / required;
        expImage.fillAmount = quotient;
        if (current < 0)
        {
            currentLevel--;
            required = HeroIdentifier.Instance.GetRequiredExp(currentLevel);
            current = required + current;
            UpdateInfoBox();
        }
    }
    public void TryToConfirmHeroExp()
    {
        ConfirmHeroExp();
    }
    public bool ConfirmHeroExp()
    {
        // check if player has enough gold to do it
        // and update savedata
        ResetAmountSelected();
        hero.heroExp = (int) current;
        hero.heroLevel = currentLevel;
        //HeroIdentifier.Instance.AdaptHeroStatsToLevel(hero);
        UpdateInfoBox();
        return true;
    }

    public void CancelUpgrade()
    {
        ResetAmountSelected();
    }

    void ResetAmountSelected()
    {
        smallAmountSelected = 0;
        mediumAmountSelected = 0;
        largeAmountSelected = 0;

        smallAmountSelectedText.text = "";
        smallAmountSelectedText.transform.parent.gameObject.SetActive(false);
        mediumAmountSelectedText.text = "";
        mediumAmountSelectedText.transform.parent.gameObject.SetActive(false);
        largeAmountSelectedText.text = "";
        largeAmountSelectedText.transform.parent.gameObject.SetActive(false);
    }
}

