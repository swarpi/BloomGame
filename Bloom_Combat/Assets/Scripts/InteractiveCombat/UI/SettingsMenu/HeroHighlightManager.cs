using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroHighlightManager : MonoBehaviour
{
    public GameObject heroHighLightUI;
    public GameObject heroExpPopUpUI;
    HeroExpUpgrade heroExpUpgrade;
    Hero hero;
    public Text heroName;
    public Text heroLevel;
    public Text heroHP;
    public Text heroAD;

    public Text currentExp;
    public Text requiredExp;
    public Image expImage;

    public int smallExpPotion = 50;
    public int mediumExpPotion = 100;
    public int largeExpPotion = 200;

    private void Start()
    {
        heroExpUpgrade = GetComponent<HeroExpUpgrade>();
    }
    //public void LoadHeroHighlightScene(string scene)
    //{
    //    if (scene == "HeroHighlightScene")
    //    {
    //        heroHighLightUI.SetActive(true);
    //        //LoadHeroInfo();
    //        //CreateHeroGridPositions();
    //    }
    //    else
    //        heroHighLightUI.SetActive(false);
    //}
    public void LoadHeroHighlightScene(Hero hero)
    {
        heroHighLightUI.SetActive(true);
        this.hero = hero;
        LoadHeroInfo();
    }
    public void LoadHeroInfo()
    {
        heroName.text = hero.heroID;
        heroLevel.text = "Level " + hero.heroLevel;
        heroHP.text ="HP "+ hero.stats.health;
        heroAD.text = "AD " + hero.stats.attackDamage;
        LoadHeroExp();
    }

    public void LoadHeroExp()
    {
        float current = hero.heroExp;
        float required = HeroIdentifier.Instance.GetRequiredExp(hero.heroLevel);
        currentExp.text = "" + current;
        requiredExp.text = "" + required;
        float quotient = current / required;
        expImage.fillAmount = quotient;
    }

    public void LoadHeroExpPopUp()
    {
        heroExpPopUpUI.SetActive(true);
        heroExpUpgrade.LoadHeroExpPopUp(hero);
    }

    public void CancelUpgrade()
    {
        heroExpUpgrade.CancelUpgrade();
        heroExpPopUpUI.SetActive(false);
        LoadHeroInfo();
    }
}
