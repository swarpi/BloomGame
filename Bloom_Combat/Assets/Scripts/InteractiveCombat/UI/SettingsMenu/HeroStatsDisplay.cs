using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroStatsDisplay : MonoBehaviour
{
    public GameObject heroStatsGO;
    public Hero tempHero;

    GameObject hp;
    GameObject armor;
    GameObject mr;
    GameObject atk;
    GameObject ebd;
    GameObject CR;
    GameObject CD;
    GameObject speed;
    GameObject eff;
    GameObject ten;

    public void LoadHeroStats(Hero hero)
    {
        tempHero = hero;
        // hero name
        heroStatsGO.transform.Find("HeroName").GetComponent<Text>().text = hero.heroID;
        // HP
        hp = heroStatsGO.transform.Find("HP").gameObject;
        hp.transform.Find("Value").GetComponent<Text>().text = hero.stats.health.ToString();
        // Armor
        armor = heroStatsGO.transform.Find("Armor").gameObject;
        armor.transform.Find("Value").GetComponent<Text>().text = hero.stats.armor.ToString();
        // MR
        mr = heroStatsGO.transform.Find("Magic Resist").gameObject;
        mr.transform.Find("Value").GetComponent<Text>().text = hero.stats.magicResist.ToString();
        // atk
        atk = heroStatsGO.transform.Find("ATK").gameObject;
        atk.transform.Find("Value").GetComponent<Text>().text = hero.stats.attackDamage.ToString();
        // ebd
        ebd = heroStatsGO.transform.Find("EBD").gameObject;
        ebd.transform.Find("Value").GetComponent<Text>().text = hero.stats.elementalBonusDamage.ToString();
        // CR
        CR = heroStatsGO.transform.Find("Crit Rate").gameObject;
        string crValue = hero.stats.critChance * 100 + "%";
        CR.transform.Find("Value").GetComponent<Text>().text = crValue;
        // CD
        CD = heroStatsGO.transform.Find("Crit Damage").gameObject;
        string cdValue = hero.stats.critDamage * 100 + "%";
        CD.transform.Find("Value").GetComponent<Text>().text = cdValue;
        // speed
        speed = heroStatsGO.transform.Find("Speed").gameObject;
        speed.transform.Find("Value").GetComponent<Text>().text = hero.stats.speed.ToString();
        // eff
        eff = heroStatsGO.transform.Find("Effectivity").gameObject;
        string effValue = hero.stats.effectivity * 100 + "%";
        eff.transform.Find("Value").GetComponent<Text>().text = effValue;
        // ten
        ten = heroStatsGO.transform.Find("Tenacity").gameObject;
        string tenValue = hero.stats.tenacity * 100 + "%";
        ten.transform.Find("Value").GetComponent<Text>().text = tenValue;
    }

    public void UpgradeHeroStatsLVL(int level)
    {
        if(tempHero.heroLevel != level)
        {
            Stats newStats = HeroIdentifier.Instance.GetHeroStatsFromsLevel(tempHero, level);
            //HP
            GameObject hpChange = hp.transform.Find("Change").gameObject;
            hpChange.SetActive(true);
            hpChange.transform.Find("new Value").GetComponent<Text>().text = "" + (newStats.health - tempHero.stats.health);
            hpChange.transform.Find("Increase").gameObject.SetActive(true);
            //Armor
            GameObject armorChange = armor.transform.Find("Change").gameObject;
            armorChange.SetActive(true);
            armorChange.transform.Find("new Value").GetComponent<Text>().text = "" + (newStats.armor - tempHero.stats.armor);
            armorChange.transform.Find("Increase").gameObject.SetActive(true);
            //MR
            GameObject mrChange = mr.transform.Find("Change").gameObject;
            mrChange.SetActive(true);
            mrChange.transform.Find("new Value").GetComponent<Text>().text = "" + (newStats.magicResist - tempHero.stats.magicResist);
            mrChange.transform.Find("Increase").gameObject.SetActive(true);
            //ATK
            GameObject atkChange = atk.transform.Find("Change").gameObject;
            atkChange.SetActive(true);
            atkChange.transform.Find("new Value").GetComponent<Text>().text = "" + (newStats.attackDamage - tempHero.stats.attackDamage);
            atkChange.transform.Find("Increase").gameObject.SetActive(true);
        }
        else
        {
            //HP
            GameObject hpChange = hp.transform.Find("Change").gameObject;
            hpChange.SetActive(false);
            //Armor
            GameObject armorChange = armor.transform.Find("Change").gameObject;
            armorChange.SetActive(false);
            //MR
            GameObject mrChange = mr.transform.Find("Change").gameObject;
            mrChange.SetActive(false);
            //ATK
            GameObject atkChange = atk.transform.Find("Change").gameObject;
            atkChange.SetActive(false);
        }
    }
}
