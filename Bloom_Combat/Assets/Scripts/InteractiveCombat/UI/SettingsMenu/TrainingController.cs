using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingController : MonoBehaviour
{
    public GameObject trainingUI;
    // Start is called before the first frame update
    private void Start()
    {
        MainSettingsMenu.current.onSceneChanged += OnSceneLoaded;
    }

    void OnSceneLoaded(string scene)
    {
        if (scene == "TrainingScene")
            trainingUI.SetActive(true);
        else
            trainingUI.SetActive(false);
    }
}
