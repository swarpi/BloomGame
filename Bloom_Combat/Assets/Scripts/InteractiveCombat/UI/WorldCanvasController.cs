﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Helper class to display the HealthBar and FloatingText Canvas UI in WorldSpace
/// </summary>
public class WorldCanvasController : MonoBehaviour
{
    public GameObject floatingTextPrefab;
    public GameObject floatingTextCritPrefab;

    public GameObject heroUIDisplay;


    private void Start()
    {

        //worldCanvas.transform.rotation = cameraCanvas.transform.rotation;
    }
    /// <summary>
    /// For Creating a new FloatingText
    /// </summary>
    /// <param name="position"></param>
    /// <param name="v"></param>
    public void AddDamageText(Vector3 position, float v,bool isCrit)
    {
        GameObject go;
        if (isCrit)
        {
            go = Instantiate(floatingTextCritPrefab);
        }

        else
        {
            go = Instantiate(floatingTextPrefab);
        }
        //go.transform.SetParent(worldCanvas.transform);q 
        go.transform.position = position;
        go.GetComponentInChildren<FloatingText>().Init(v);
        //go.transform.rotation = cameraCanvas.transform.rotation;
    }

    /// <summary>
    /// For Creating a new HealthBar
    /// </summary>
    /// <param name="position"></param>
    /// <param name="v"></param>

    public void InitHeroUI(GameObject heroGO)
    {
        // init the correct position
        Transform character = heroGO.transform.GetChild(1);
        GameObject heroUIDisplayGO = Instantiate(heroUIDisplay);
        heroUIDisplayGO.transform.SetParent(heroGO.transform);
        HeroUIDisplays uiDisplay = heroUIDisplayGO.GetComponentInChildren<HeroUIDisplays>();
        uiDisplay.Init(heroGO);
        heroUIDisplayGO.transform.position = character.position;
        // init the correct team color

        if(heroGO.GetComponent<HeroController2D>().teamID == 1)
        {
            uiDisplay.fillImageHealth.color = Color.red;
        }
    }
}