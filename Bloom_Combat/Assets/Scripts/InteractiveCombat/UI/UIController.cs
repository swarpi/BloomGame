﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Updates and controls UI elements
/// </summary>
public class UIController : MonoBehaviour
{
    public HeroInventory heroInventory;

    public GameObject[] championsFrameArray;
    public GameObject[] bonusPanels;
    public GameObject heroDisplay;
    public GameObject heroDisplayPanel;

    public GameObject synergyDisplay;
    public GameObject synergyDisplayPanel;


    public Text timerText;
    public Text championCountText;
    public Text goldText;
    public Text hpText;

    public Canvas combatCanvas;
    public Canvas prepCanvas;
    public Canvas winCanvas;
    public Canvas lossCanvas;
    public Canvas activeCanvas;

    private void Start()
    {
        activeCanvas = prepCanvas;
    }

    /// <summary>
    /// Called when a chamipon panel clicked on shop UI
    /// </summary>
    /// 
    public void OnHeroClicked()
    {
        //get clicked champion ui name
        string name = EventSystem.current.currentSelectedGameObject.transform.parent.name;

        //calculate index from name
        string defaultName = "champion container(Clone)";
       
        int championFrameIndex = int.Parse(name.Substring(defaultName.Length));

        //message shop from click
        OnHeroIconClicked(championFrameIndex);
    }

    public void OnHeroIconClicked(int index)
    {
        if (CombatController.Instance.combatField.heroList.Count > 4)
        {
            Debug.Log("already 4 heros on board");
            return;
        }
        CombatController.Instance.combatField.PlaceHeroOnBoard((heroInventory.heroList[index]));
        bool isSucces = true;
        if (isSucces)
        {
            ChangeHeroFrameAfterSelected(heroInventory.heroList[index], true);
        }
    }


    // Fill the UI container with the heroes from hero Inventory
    public void LoadHeroDisplay(List<Hero> heroList)
    {
        SetupHeroDisplay(heroList);
        int i = 0;
        foreach (Hero hero in heroList)
        {
            LoadShopItem(hero, i);
            i++;
        }
    }

    // Setup the UI container
    void SetupHeroDisplay(List<Hero> heroList)
    {
        championsFrameArray = new GameObject[heroList.Count];
        for (int i = 0; i < heroList.Count; i++)
        {
            GameObject display = Instantiate(heroDisplay);
            display.name += i;
            display.transform.SetParent(heroDisplayPanel.transform);
            display.transform.Find("champion").gameObject.GetComponent<Button>().onClick.AddListener(OnHeroClicked);
            championsFrameArray[i] = display;


        }
    }


    // grey out the champion frame if selected if not put it back to white
    public void ChangeHeroFrameAfterSelected(Hero hero, bool putOnField)
    {
        if (putOnField)
        {
            foreach (GameObject heroFrame in championsFrameArray)
            {
                Transform top = heroFrame.transform.Find("champion").Find("top");
                if (top.Find("Name").GetComponent<Text>().text == hero.uiname)
                {
                    top.Find("Image").gameObject.GetComponent<Image>().color = Color.grey;
                }
            }
        }
        if (!putOnField)
        {
            foreach (GameObject heroFrame in championsFrameArray)
            {
                Transform top = heroFrame.transform.Find("champion").Find("top");
                if (top.Find("Name").GetComponent<Text>().text == hero.uiname)
                {
                    top.Find("Image").gameObject.GetComponent<Image>().color = Color.white;
                }
            }
        }
        //foreach (Hero hero in heroInventory.heroList)
        //{
        //    if (autoCombatController.heroList.Any(x => x.GetComponent<HeroController>().heroName == hero.heroID))
        //    {
        //        championsFrameArray[index].transform.Find("champion").Find("top").Find("Image").gameObject.GetComponent<Image>().color = Color.grey;
        //    }
        //    else
        //    {
        //        championsFrameArray[index].transform.Find("champion").Find("top").Find("Image").gameObject.GetComponent<Image>().color = Color.white;
        //    }
        //}

    }

    /// <summary>
    /// displays champion info to given index on UI
    /// </summary>
    /// <param name="champion"></param>
    /// <param name="index"></param>
    void LoadShopItem(Hero hero, int index)
    {
        //get unit frames
        Transform championUI = championsFrameArray[index].transform.Find("champion");
        Transform top = championUI.Find("top");
        Transform name = top.Find("Name");
        //Transform image = top.Find("Image");

/*        Debug.Log(hero.uiname);*/
        //assign texts from champion info to unit frames
        name.GetComponent<Text>().text = hero.uiname;
    }
    public class DisplayStruct
    {
        public Sprite icon;
        public string name;
        public int currentAmount;
        public int maxAmount;

        public DisplayStruct(Sprite _icon, string _name, int _currentAmount, int _maxAmount)
        {
            icon = _icon;
            name = _name;
            currentAmount = _currentAmount;
            maxAmount = _maxAmount;
        }

        public bool Equals(string comparer)
        {
            if(comparer == name)
            {
                return true;
            }
            return false;
        }
    }
    List<DisplayStruct> tempDisplayList;
    Dictionary<DisplayStruct, GameObject> synergyDisplayedDict = new Dictionary<DisplayStruct, GameObject>();
    public void LoadSyngeryDisplay(Dictionary<HeroClassBonus, int> activeClassBonuses, Dictionary<HeroOriginBonus, int> activeOriginBonuses)
    {
        // maybe place origin and class in different positions
        // create synergy display for each synergy
        // class
        tempDisplayList = new List<DisplayStruct>();
        // classes
        LoadClassBonusDisplay(activeClassBonuses);
        // origins
        LoadOriginBonusDisplay(activeOriginBonuses);
        // check if there are synergies that are no longer on the field
        NoLongerOnField();
        // change color depending if synergy is active and maybe which bonus is applied
    }
    void LoadClassBonusDisplay(Dictionary<HeroClassBonus, int> activeClassBonuses)
    {
        foreach (KeyValuePair<HeroClassBonus, int> heroClass in activeClassBonuses)
        {
            //Debug.Log("displaying synergies");
            // set the icon of the origin 
            Sprite icon = heroClass.Key.icon;
            // set the origin name
            string name = heroClass.Key.displayName;
            // set the currentAmount
            int currentValue = heroClass.Value;
            // set the maxAmount
            int maxValue = heroClass.Key.requiredNumberEffect1;

            DisplayStruct potential = new DisplayStruct(icon, name, currentValue, maxValue);
            tempDisplayList.Add(potential);
            DisplaySynergyDisplay();
        }
    }
    void LoadOriginBonusDisplay(Dictionary<HeroOriginBonus, int> activeOriginBonuses)
    {
        foreach (KeyValuePair<HeroOriginBonus, int> heroOrigin in activeOriginBonuses)
        {
            //Debug.Log("displaying synergies");
            // set the icon of the origin 
            Sprite icon = heroOrigin.Key.icon;
            // set the origin name
            string name = heroOrigin.Key.displayName;
            // set the currentAmount
            int currentValue = heroOrigin.Value;
            // set the maxAmount
            int maxValue = heroOrigin.Key.requiredNumberEffect1;

            DisplayStruct potential = new DisplayStruct(icon, name, currentValue, maxValue);
            tempDisplayList.Add(potential);
            DisplaySynergyDisplay();
        }

    }
    void NoLongerOnField()
    {
        // if the tempDisplay isnt the same size as the dict
        // it means that on the field are more gameObject than in the list
        // find from the dict the one that isnt in the list and remove it
        if(tempDisplayList.Count != synergyDisplayedDict.Count)
        {
            List<DisplayStruct> objectsToRemove = new List<DisplayStruct>();
            foreach (KeyValuePair<DisplayStruct, GameObject> displayedSynergy in synergyDisplayedDict)
            {
                // check if there are synergies which were previously on the field but are not anymore on the field
                if(tempDisplayList.Find(x => x.name == displayedSynergy.Key.name) == null)
                {
                    
                    Destroy(displayedSynergy.Value.gameObject);
                    objectsToRemove.Add(displayedSynergy.Key);
                }
            }
            // needs this extra step because we cant modify the dictionary during a loop
            foreach(DisplayStruct removeIt in objectsToRemove)
            {
                synergyDisplayedDict.Remove(removeIt);
            }
        }
    }
    bool UpdateSynergyOnField(DisplayStruct newSynergy)
    {
        foreach (KeyValuePair<DisplayStruct, GameObject> displayedSynergy in synergyDisplayedDict)
        {
            // if the synergy is already on the field update its value
            if (newSynergy.name == displayedSynergy.Key.name)
            {
                displayedSynergy.Value.transform.Find("Text").Find("currentAmount").GetComponent<TextMeshProUGUI>().text = "" + newSynergy.currentAmount;
                return true;
            }
        }
        return false;
    }
    public void DisplaySynergyDisplay()
    {
        foreach(DisplayStruct display in tempDisplayList)
        {
            // check if the synergy is alread on the field, if not add a new display
            if (UpdateSynergyOnField(display)) continue;
            GameObject tempDisplay = Instantiate(synergyDisplay);
            tempDisplay.transform.SetParent(synergyDisplayPanel.transform);
            // set the icon of the origin 
            //tempDisplay.name = display.name;
            tempDisplay.transform.Find("background").GetComponent<Image>().sprite = display.icon;
            // set the origin name
            tempDisplay.transform.Find("Text").Find("Name").GetComponent<TextMeshProUGUI>().text = display.name;
            // set the currentAmount
            tempDisplay.transform.Find("Text").Find("currentAmount").GetComponent<TextMeshProUGUI>().text = "" + display.currentAmount;
            // set the maxAmount
            tempDisplay.transform.Find("Text").Find("maxAmount").GetComponent<TextMeshProUGUI>().text = "" + display.maxAmount;
            // add the created display to the dictionary of existing displays on the field
            synergyDisplayedDict.Add(display, tempDisplay);
        }
    }

    public void UpdateUICombat(GameStage gameStage)
    {
        switch (gameStage)
        {
            case GameStage.Preparation:
                UpdateUICombatPreperation();
                break;
            case GameStage.Combat:
                UpdateUICombatBattle();
                break;
            case GameStage.Loss:
                UpdateUICombatFinish(false);
                break;
            case GameStage.Win:
                UpdateUICombatFinish(true);
                break;

        }

    }

    private void UpdateUICombatPreperation()
    {
        activeCanvas.gameObject.SetActive(false);
        prepCanvas.gameObject.SetActive(true);

        activeCanvas = prepCanvas;
    }

    private void UpdateUICombatBattle()
    {
        activeCanvas.gameObject.SetActive(false);
        combatCanvas.gameObject.SetActive(true);
        activeCanvas = combatCanvas;
    }

    private void UpdateUICombatFinish(bool playerWin)
    {
        activeCanvas.gameObject.SetActive(false);

        if (playerWin)
        {
            winCanvas.gameObject.SetActive(true);
            activeCanvas = winCanvas;
        }
        else
        {
            lossCanvas.gameObject.SetActive(true);
            activeCanvas = lossCanvas;
        }
    }



    /// <summary>
    /// sets timer visibility
    /// </summary>
    /// <param name="b"></param>
    public void SetTimerTextActive(bool b)
    {
        timerText.gameObject.SetActive(b);

        //placementText.SetActive(b);
    }

    /// <summary>
    /// displays loss screen when game ended
    /// </summary>
    public void ShowLossScreen()
    {
        SetTimerTextActive(false);
        //shop.SetActive(false);
       // gold.SetActive(false);
        

        //restartButton.SetActive(true);
    }

    /// <summary>
    /// displays game screen when game started
    /// </summary>
    public void ShowGameScreen()
    {
        SetTimerTextActive(true);
        //shop.SetActive(true);
        //gold.SetActive(true);


        //restartButton.SetActive(false);
    }

}
