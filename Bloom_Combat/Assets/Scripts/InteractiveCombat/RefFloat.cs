using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RefFloat
{
    public float f;

    public RefFloat(float f)
    {
        this.f = f;
    }
}