using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Stats
{
    // offensive
    public float attackDamage = 5;
    public float elementalBonusDamage = 0;
    public float critChance = 0.15f;
    public float critDamage = 1.5f;
    public float speed = 100;

    public float attackRange = 3;

    // defensive
    public float health = 100;
    public float armor = 20;
    public float magicResist = 20;


    // utility
    public float loo = 50;
    public float looStart = 0;
    public float looPerHitTaken = 5;
    public float looPerHitGiven = 5;
    public float effectivity = 0.0f;
    public float tenacity = 0.0f;

}
