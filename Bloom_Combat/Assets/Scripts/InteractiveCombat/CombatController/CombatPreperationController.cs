using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CombatPreperationController : MonoBehaviour
{
    public GameObject combatPrepUI;
    public GameObject combatUI;
    public GameObject combatEndUI;

    public GameObject cardSide;

    public Text stageNameTextGO;
    public UIController uIController;
    public HeroInventory heroInventory;
    public TileMap tileMap;
    public StageInfo currentStageInfo;

    public int numberOfWaves;
    private void Start()
    {
        LoadStage();
        CombatField.onHeroListChanged += UpdateCardSide;
    }
    public void LoadStage()
    {
        // load background
        LoadStageInfo();
        LoadTileMap();
        LoadPlayerHeros();
        LoadEnemy();
    }
    //LoadStage
    public void LoadStageInfo()
    {
        //currentStage = GameProgress.Instance.currentStage;
        //stageNameTextGO.text = "Stage: " + currentStage.name;
        currentStageInfo = StageProgress.Instance.LoadCurrentStage(GameProgress.Instance.stageID);
        numberOfWaves = currentStageInfo.number_of_waves;
        //Debug.Log("stageInfo from stageProgress " + StageProgress.Instance.LoadCurrentStage(1).enemyArray[0].Length);
    }
    //Load Map
    public void LoadTileMap()
    {
        tileMap.GenerateTileMap();
    }
    //Load hero inventory to place them on the field
    public void LoadPlayerHeros()
    {
        uIController.LoadHeroDisplay(heroInventory.heroList);
    }
    //LoadEnemy
    public void LoadEnemy()
    {
        CombatController.Instance.combatField.AddEnemy(currentStageInfo);
    }
    //StartBattle
    public void StartBattle()
    {
        combatPrepUI.SetActive(false);
        combatUI.SetActive(true);
        tileMap.HideAllTileSprites();
        CombatController.Instance.StartBattle();
        // trigger quest
    }
    //maybe post battle

    public void PlayerLost()
    {
        // display lose screen
        combatEndUI.transform.Find("Lose").gameObject.SetActive(true);
        // give option to leave or try again
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void LoadLevelDisplayer()
    {
        SceneManager.LoadScene("StageSelection");
    }
    public void PlayerWon()
    {
        // trigger dialogues

        // open victory screen after dialogue finished
        // display victory screen
        Transform winGO = combatEndUI.transform.Find("Win");
        winGO.gameObject.SetActive(true);
        // load rewards
        winGO.Find("Rewards").Find("Gold").Find("amount").GetComponent<Text>().text = currentStageInfo.goldReward.ToString();
        winGO.Find("Rewards").Find("PlayerExp").Find("amount").GetComponent<Text>().text = currentStageInfo.playerExp.ToString();
        // give player rewards

        // increase stage
        GameProgress.Instance.stageID += 1;
        //currentStageInfo = StageProgress.Instance.LoadCurrentStage(currentStageInfo.stageID + 1);
        //give player option to go to next stage
    }
        
    public void UpdateCardSide()
    {
        // update left side
        for(int i = 0; i < CombatController.Instance.combatField.leftHeroList.Count; i ++)
        {
            if (CombatController.Instance.combatField.leftHeroList[i] != null)
            {
                cardSide.transform.Find("Left").GetChild(i).gameObject.SetActive(true);
                cardSide.transform.Find("Left").GetChild(i).Find("name").GetComponent<Text>().text = CombatController.Instance.combatField.leftHeroList[i].GetComponent<HeroController2D>().heroName;
            }
            else
            {
                cardSide.transform.Find("Left").GetChild(i).gameObject.SetActive(false);
                cardSide.transform.Find("Left").GetChild(i).Find("name").GetComponentInChildren<Text>().text = "";
            }
        }
        // update right side
        for (int i = 0; i < CombatController.Instance.combatField.rightHeroList.Count; i++)
        {
            if (CombatController.Instance.combatField.rightHeroList[i] != null)
            {
                cardSide.transform.Find("Right").GetChild(i).gameObject.SetActive(true);
                cardSide.transform.Find("Right").GetChild(i).Find("name").GetComponent<Text>().text = CombatController.Instance.combatField.rightHeroList[i].GetComponent<HeroController2D>().heroName;
            }
            else
            {
                cardSide.transform.Find("Right").GetChild(i).gameObject.SetActive(false);
                cardSide.transform.Find("Right").GetChild(i).Find("name").GetComponentInChildren<Text>().text = "";
            }
        }
    }

    private void OnDestroy()
    {
        CombatField.onHeroListChanged -= UpdateCardSide;
    }
}
