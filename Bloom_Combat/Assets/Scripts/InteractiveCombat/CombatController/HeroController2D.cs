using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum AICombatState
{
    LookingForTarget,
    WalkingToTarget,
    InCombat,
    InBetweenCombatEnd,
    InBetweenCombatStart
}
public class HeroController2D : MonoBehaviour
{
    public static int TEAMID_PLAYER = 0;
    public static int TEAMID_AI = 1;

    public AICombatState currentCombatState;
    public HeroControllerCombat2D heroControllerCombat;
    public WorldCanvasController worldCanvasController;

    [HideInInspector]
    public int placeOrder;

    public Vector3 gridTargetPosition;

    [HideInInspector]
    ///Team of this champion, can be player = 0, or enemy = 1
    public int teamID = 0;

    [HideInInspector]
    public Hero hero;
    // unique identifier for hero in combat
    public string heroID;
    [HideInInspector]
    public string heroName;

    public GameObject target;
    public TileField targetTile;

    // Use this for the hero position on the field !
    public TileField currentTilePos;
    public TileField nextTile;

    public bool isAttacking = false;
    public bool attackFinished = true;
    public float attackTimer = 0f;
    public float attackTimer2 = 0f;
    float moveSpeed = 3f;

    // for drag and drop during precombat
    float timer;
    bool isPointerDown;
    bool isPointerUp;
    public float startPosX;
    public float startPosY;
    private int gridPositionX = 0;
    private int gridPositionY = 0;
    public void Init(Hero _hero, int _teamID, int _gridPositionX, int _gridPositionY)
    {
        hero = _hero;
        teamID = _teamID;
        heroName = _hero.heroID;
        heroID = _hero.heroID + Guid.NewGuid();
        gridPositionX = _gridPositionX;
        gridPositionY = _gridPositionY;
        heroControllerCombat = this.GetComponent<HeroControllerCombat2D>();
        worldCanvasController = GameObject.Find("Worldcanvas Controller").GetComponent<WorldCanvasController>();
        worldCanvasController.InitHeroUI(this.gameObject);
        heroControllerCombat.Init(this, hero, teamID);
        SetCurrentTile(TileMap.tileMap[_gridPositionX, _gridPositionY]);

        CombatController.Instance.onHeroDeath += OnHeroDeath;
    }


    private void Update()
    {
        if(CombatController.Instance.currentGameStage == GameStage.Preparation)
        {
            // if we want to differentiate between clicking and holding
            if (isDragging)
            {
                if (isPointerDown && teamID == 0)
                {
                    timer += Time.deltaTime;
                    if (timer > 0.1f) // time until it is registered as holding instead of clicking
                    {
                        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                        this.gameObject.transform.localPosition = new Vector3(mousePosition.x - startPosX, mousePosition.y - startPosY, 0);
                        //transform.Translate(mousePosition);
                    }
                }
            }
            else
            {
                this.transform.position = currentTilePos.GetPositionOnTile(this);
                if (isPointerUp && teamID == 0)
                {
                    if (timer < 0.15f) // clicking
                    {
                        ClickedOnHero();
                    }
                    else // release hold
                    {
                        // destroy the button
                        ReleaseHolding();

                    }
                    timer = 0f;
                    isPointerUp = false;
                }
            }

        }
        else if(CombatController.Instance.currentGameStage == GameStage.InBetweenCombat)
        {
            // walk to the end of the screen, normally triggered when going to the next wave
            if (currentCombatState == AICombatState.InBetweenCombatEnd)
            {
                Vector2 currentWayPoint = new Vector3(0, transform.position.y);
                if (0.05 > Vector2.Distance(transform.position, currentWayPoint))
                {
                    return;
                }
                transform.position = Vector2.MoveTowards(transform.position, currentWayPoint, moveSpeed * Time.deltaTime);
                return;
            }
            // walk from the start of the screen, normally triggered at a start of a new wave
            if (currentCombatState == AICombatState.InBetweenCombatStart)
            {

                Vector2 currentWayPoint = new Vector2(TileMap.allMapGridPositions[currentTilePos.position.X, currentTilePos.position.Y].x, transform.position.y);
                Debug.Log(currentWayPoint);
                if (0.05 > Vector2.Distance(transform.position, currentWayPoint))
                {
                    Debug.Log("target reached");
                    return;
                }
                transform.position = Vector2.MoveTowards(transform.position, currentWayPoint, moveSpeed * Time.deltaTime);
                return;
            }
            // this is for enemies to put them on their real position at the start
            else
            {
                this.transform.position = currentTilePos.GetPositionOnTile(this);
            }
        }
        else if(CombatController.Instance.currentGameStage == GameStage.Combat && !heroControllerCombat.isDead)
        {

            // only look for a new target after your target which you attacked died
            if (target == null)
            {
                currentCombatState = AICombatState.LookingForTarget;
            }
            if (currentCombatState != AICombatState.InCombat)
                target = FindTarget();
            if (target != null)
            {
                if (target.GetComponent<HeroControllerCombat2D>().isDead || target.GetComponent<HeroControllerCombat2D>().isUntargettable) //target champion is dead
                {
                    //remove target if targetchampion is dead 
                    target = null;
                    currentCombatState = AICombatState.LookingForTarget;
                    StopAllCoroutines();
                }

                if (!isAttacking)
                {
                    isAttacking = true;
                    //float distance = Vector3.Distance(this.transform.position, target.transform.position);
                    try
                    {
                        float distance = Math.Abs(currentTilePos.position.getX() - target.GetComponent<HeroController2D>().currentTilePos.position.getX());
                        //Debug.Log("attack distance " + distance);
                        if (distance < hero.stats.attackRange)
                        {
                            if (Time.time >= attackTimer)
                            {
                                heroControllerCombat.DoAttack();
                                attackTimer = Time.time + 100f / hero.stats.speed;
                            }
                        }
                        else
                        {
                            currentCombatState = AICombatState.WalkingToTarget;
                        }
                    }
                    // during the attack the enemy could die so need to catch it
                    catch(NullReferenceException e)
                    {
                        Debug.Log("no target found");
                    }
                    isAttacking = false;
                }
            }
        }

    }

    public bool isDragging;

    public void OnMouseDown()
    {
        isDragging = true;
        isPointerDown = true;
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        startPosX = mousePosition.x - this.transform.localPosition.x;
        startPosY = mousePosition.y - this.transform.localPosition.y;
        isPointerUp = false;
    }

    public void OnMouseUp()
    {
        isDragging = false;
        isPointerDown = false;
        isPointerUp = true;

    }
    void ClickedOnHero()
    {
        CombatController.Instance.combatField.RemoveChampionFromArray(currentTilePos.position.getX(), currentTilePos.position.getY(), this.gameObject);
        Destroy(this.gameObject);
        CombatController.Instance.uIController.ChangeHeroFrameAfterSelected(this.hero, false);
    }
    void ReleaseHolding()
    {
        int layerMask = 11;
        layerMask = ~layerMask;
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 1f, layerMask);
        if (hit.collider != null)
        {
            TileField newTile = hit.collider.gameObject.GetComponent<TileField>();
            if (newTile.heroOnTileOne)
            {
                // if there is a hero on this tile set to my old position
                SetCurrentTile(currentTilePos);
            }
            else if (newTile.isPlayerTile)
            {
                // try to set hero position on new tile
                if (!SetCurrentTile(newTile))
                {
                    // if you couldnt set hero on new tile set to my old position
                    SetCurrentTile(currentTilePos);
                }
                else
                {
                    // if we were able to change to newtile need to adapt my position on the grid(combatcontroller) and also change my local gridposition
                    CombatController.Instance.combatField.gridHeroArray[gridPositionX, gridPositionY] = null;
                    gridPositionX = newTile.position.X;
                    gridPositionY = newTile.position.Y;
                    CombatController.Instance.combatField.gridHeroArray[gridPositionX, gridPositionY] = this.gameObject;
                }
            }
        }
    }
    public bool SetCurrentTile(TileField newTile)
    {
        // if the newTile is empty... remove hero from his tile
        if (newTile == null)
        {
            currentTilePos.RemoveHero(this);
            currentTilePos = newTile;
            return true;
        }
        // if the hero has no tile position
        if (currentTilePos == null)
        {
            currentTilePos = newTile;
            return currentTilePos.AddHero(this);
        }
        if (currentTilePos.RemoveHero(this))
        {
            currentTilePos = newTile;
            return currentTilePos.AddHero(this);
        }
        return false;
    }
    private void OnHeroDeath(GameObject hero)
    {
        /*        if (hero.GetComponent<HeroController2D>() != this) return;
                //Debug.Log(heroID + " died");
                //championAnimation.DoDeath();
                currentTilePos.RemoveHero(this);
                //heroController.ResetHeroTile();
                CombatController.Instance.OnHeroDeath();
                if (teamID == 0)
                {
                    CombatField.heroList.Remove(this.gameObject);
                }
                if (teamID == 1)
                {
                    CombatField.aiHeroList.Remove(this.gameObject);
                }
                // refactor death to hero controller
                Destroy(this.gameObject);
                //this.gameObject.SetActive(false);
                //CombatController.Instance.onHeroDeath -= OnHeroDeath;*/

        if (hero.GetComponent<HeroController2D>() != this) return;

        SetCurrentTile(null);

        this.gameObject.SetActive(false);
        CombatController.Instance.OnHeroDeath();
        CombatController.Instance.onHeroDeath -= OnHeroDeath;

    }
    public void WalkToEndOfScreen()
    {
        currentCombatState = AICombatState.InBetweenCombatEnd;
/*        Vector2 currentWayPoint = new Vector3(0, transform.position.y);
        while (true)
        {
            if (0.05 > Vector2.Distance(transform.position, currentWayPoint))
            {
                return;
            }
            transform.position = Vector2.MoveTowards(transform.position, currentWayPoint, moveSpeed * Time.deltaTime);
        }*/

    }
    public void WalkFromStartOfScreen()
    {
        currentCombatState = AICombatState.InBetweenCombatStart;
        transform.position = new Vector2(-18, transform.position.y);
    }
    public void MoveTowards(TileField target)
    {
        StopCoroutine(WalkForward(target));
        StartCoroutine(WalkForward(target));
    }

    IEnumerator WalkForward(TileField target)
    {
        if ((currentCombatState == AICombatState.LookingForTarget || currentCombatState == AICombatState.WalkingToTarget) && !heroControllerCombat.isDead)
        {
            nextTile = target;
            Vector3 currentWayPoint = Vector3.zero;
            if (target.heroOnTileOne == null)
            {
                // if there is no hero where i want to go, go to field[1]
                currentWayPoint = target.myFields[1].transform.position;
            }
            else
            {
                // if there is a hero where i want to go, go to field[3] instead
                currentWayPoint = target.myFields[3].transform.position;
            }

            while (true)
            {
                //Debug.Log("Distance " + Vector2.Distance(transform.position, currentWayPoint));
                if (0.05 > Vector2.Distance(transform.position, currentWayPoint))
                {
                    SetCurrentTile(TileMap.tileMap[target.position.getX(), target.position.getY()]);
                    // maybe announce that i have reached my target, so the next steps can be calculated
                    yield break;
                }
                if (heroControllerCombat.isDead == true || heroControllerCombat.isMoveable == false)
                {
                    yield break;
                }
                else if (target == null)
                {
                    yield break;
                }
                //rotate towards target
                //RotateTowards(currentWayPoint);
                //transform.position = currentWayPoint;
                transform.position = Vector2.MoveTowards(transform.position, currentWayPoint, moveSpeed * Time.deltaTime);
                // stop if i am dead or my target is dead

                yield return null;
            }
        }
        else
        {
            yield break;
        }
    }

    public void UseAbility(AbilityIndex index, int level)
    {
        switch (level)
        {
            case 1: CombatController.Instance.GetLoo(10);
                break;
            case 2:
                CombatController.Instance.GetLoo(15);
                break;
            case 3:
                CombatController.Instance.GetLoo(20);
                break;
        }
        if(index == AbilityIndex.First)
            heroControllerCombat.DoAbilityOne(level);
        if(index == AbilityIndex.Second)
            heroControllerCombat.DoAbilityTwo(level);
    }

    public void UseUltimate()
    {
        if (CombatController.Instance.UseLoo())
        {
            heroControllerCombat.DoUltimate();
        }
        Debug.Log("not enough loo");
    }

    public void Evolve()
    {
        heroControllerCombat.DoEvolutionTwo();
    }

    /// <summary>
    /// Called when gamestage.combat starts
    /// </summary>
    public void OnCombatStart()
    {
        isDragging = false;
        currentCombatState = AICombatState.LookingForTarget;
    }


    /// <summary>
    /// Find the a champion the the closest world position
    /// </summary>
    /// <returns></returns>
    public GameObject FindTarget(int newTeamID = -1)
    {
        // only necessary if i get charmed 
        int tempTeamID = teamID;
        if (newTeamID >= 0)
        {
            tempTeamID = newTeamID;
        }
        GameObject closestEnemy = null;
        float bestDistance = 1000;
        //find enemy
        if (tempTeamID == TEAMID_PLAYER)
        {
            foreach (GameObject enemy in CombatController.Instance.combatField.aiHeroList)
            {
                // if i would target myself when charmed for example
                HeroController2D enemyHeroController = enemy.GetComponent<HeroController2D>();
                if (enemyHeroController == this.heroControllerCombat) continue;
                if (enemyHeroController.heroControllerCombat.isDead == false)
                {
                    // find closest field
                    //float distance = Vector2.Distance(currentTilePos.position.getVector(), enemyHeroController.currentTilePos.position.getVector());
                    float distance = Math.Abs(currentTilePos.position.getX() - enemyHeroController.currentTilePos.position.getX());
                    if (distance <= bestDistance)
                    {
                        bestDistance = distance;
                        closestEnemy = enemy;
                        targetTile = closestEnemy.GetComponent<HeroController2D>().currentTilePos;
                    }
                }
            }

        }
        else if (tempTeamID == TEAMID_AI)
        {
            foreach (GameObject enemy in CombatController.Instance.combatField.heroList)
            {
                // if i would target myself when charmed for example
                // if i would target myself when charmed for example
                HeroController2D enemyHeroController = enemy.GetComponent<HeroController2D>();
                if (enemyHeroController == this.heroControllerCombat) continue;
                if (enemyHeroController.heroControllerCombat.isDead == false)
                {
                    // find closest field
                    float distance = Vector2.Distance(currentTilePos.position.getVector(), enemyHeroController.currentTilePos.position.getVector());
                    if (distance <= bestDistance)
                    {
                        bestDistance = distance;
                        closestEnemy = enemy;
                        targetTile = closestEnemy.GetComponent<HeroController2D>().currentTilePos;
                    }
                }
            }

        }

        return closestEnemy;
    }

    /// <summary>
    /// Looks for new target to attack if there is any
    /// </summary>
    public void TryAttackNewTarget()
    {
        //find closest enemy
        target = FindTarget();

        //if target found
        if (target != null)
        {
            //set pathfinder target

        }
    }
    private void OnDestroy()
    {
        CombatController.Instance.onHeroDeath -= OnHeroDeath;
/*        SetCurrentTile(null);
        heroControllerCombat.isDead = true;*/
    }
}
