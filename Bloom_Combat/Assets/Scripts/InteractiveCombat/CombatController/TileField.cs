using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct Point
{
    public int X;
    public int Y;


    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int getX()
    {
        return X;
    }
    // refactor name to getZ
    public int getY()
    {
        return Y;
    }

    public Vector2 getVector()
    {
        return new Vector2(X, Y);
    }
}
public class TileField : MonoBehaviour
{
    // Start is called before the first frame update
    public Point position;
    public string tileName;
    public TileField nextTile;
    public TileField prevTile;
    public List<Field> myFields;
    public bool isPlayerTile = false;
    public bool passable;

    public HeroController2D heroOnTileOne;
    public HeroController2D heroOnTileTwo;
    public HeroController2D heroOnTileThree;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Vector3 GetPositionOnTile(HeroController2D heroController)
    {
        if(heroOnTileOne == heroController)
        {
            // bottom right field (1.0)
            return myFields[1].transform.position;
        }
        if (heroOnTileTwo == heroController)
        {
            // top left field (0.1)
            return myFields[2].transform.position;
        }
        if (heroOnTileThree == heroController)
        {
            // bottom right field (0.0)
            return myFields[0].transform.position;
        }
        return Vector3.zero;
    }

    public bool AddHero(HeroController2D heroController)
    {
        if (heroOnTileOne == null)
        {
            heroOnTileOne = heroController;
            passable = false;
            return true;
        }
        if(heroOnTileTwo == null)
        {
            heroOnTileTwo = heroController;
            passable = false;
            return true;
        }
        if (heroOnTileThree == null)
        {
            heroOnTileThree = heroController;
            passable = false;
            return true;
        }
        else return false;
        //heroOnTile = herocontroller;

    }
    public bool RemoveHero(HeroController2D heroController)
    {
        if (heroOnTileOne == heroController)
        {
            heroOnTileOne = null;
            passable = true;

            return true;
        }
        if (heroOnTileTwo == heroController)
        {
            heroOnTileTwo = null;
            passable = true;

            return true;
        }
        if (heroOnTileThree == heroController)
        {
            heroOnTileThree = null;
            passable = true;

            return true;
        }
        else return false;

    }
}
