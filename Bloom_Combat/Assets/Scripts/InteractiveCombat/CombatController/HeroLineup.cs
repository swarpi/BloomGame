using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroLineup
{
    public Dictionary<string, HeroLineupPosition> heroLineup = new Dictionary<string, HeroLineupPosition>();

    public void SaveJsonData()
    {
        HeroLineUpSaveData sd = new HeroLineUpSaveData();
        PopulateSaveData(sd);

        if (FileManager.WriteToFile("HeroLineup.dat", sd.ToJson()))
        {
            Debug.Log("Save sucessfull");
        }
    }

    public void PopulateSaveData(HeroLineUpSaveData a_SaveData)
    {
        a_SaveData.heroLineup = heroLineup;
    }

    public void LoadJsonData()
    {
        if (FileManager.LoadFromFile("HeroLineup.dat", out var json))
        {
            HeroLineUpSaveData sd = new HeroLineUpSaveData();
            sd.LoadFromJson(json);

            LoadFromSaveData(sd);
            Debug.Log("Load complete");

        }
    }

    public void LoadFromSaveData(HeroLineUpSaveData a_SaveData)
    {
        heroLineup = a_SaveData.heroLineup;
        Debug.Log("values " + heroLineup.Values);
    }

}

public class HeroLineupPosition
{
    public int side;
    public int x_pos;
    public int y_pos;

    public HeroLineupPosition(int side, int x_pos, int y_pos)
    {
        this.side = side;
        this.x_pos = x_pos;
        this.y_pos = y_pos;
    }
}
[System.Serializable]
public class HeroLineUpSaveData
{
    public Dictionary<string, HeroLineupPosition> heroLineup;

    public string ToJson()
    {
        return JsonConvert.SerializeObject(heroLineup, Formatting.Indented);
    }

    public void LoadFromJson(string a_Json)
    {
        heroLineup = JsonConvert.DeserializeObject<Dictionary<string, HeroLineupPosition>>(a_Json);
    }
}
