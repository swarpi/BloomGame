using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UltButtonContainer : MonoBehaviour
{
    public GameObject blocker;
    public HeroController2D heroController;

    private void Start()
    {
        blocker = transform.Find("blocker").gameObject;
    }
    public void CheckIfHeroIsDead()
    {
        // skip buttons that are not active
        if (!gameObject.activeSelf)
        {
            return;
        }
        try
        {
            if (heroController.heroControllerCombat.isDead)
            {
                blocker.SetActive(true);
            }
            else
            {
                blocker.SetActive(false);
            }
        }catch(NullReferenceException)
        {
            blocker.SetActive(true);
        }

    }
}
