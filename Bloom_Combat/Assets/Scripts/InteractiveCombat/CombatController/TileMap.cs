using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMap : MonoBehaviour
{
    public GameObject tilePrefab;
    public Transform mapStartPosition;
    [SerializeField]
    float xOffset;
    [SerializeField]
    float yOffset;
    [SerializeField]
    float angle;

    public static int mapSizeX = 6;
    public static int mapSizeY = 3;

    public static TileField[,] tileMap;

    // not really needed
    public static Vector3[,] allMapGridPositions;

    public static GameObject[,] playerMapTileGO;
    public static GameObject[,] enemyMapTileGO;
    // Start is called before the first frame update
    public void GenerateTileMap()
    {
        tileMap = new TileField[mapSizeX, mapSizeY];
        allMapGridPositions = new Vector3[mapSizeX, mapSizeY];

        playerMapTileGO = new GameObject[mapSizeX / 2, mapSizeY];
        enemyMapTileGO = new GameObject[mapSizeX / 2, mapSizeY];

        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                float xPosition = xOffset * x;
                float yPosition = yOffset * y;

                Vector3 position = mapStartPosition.position + new Vector3(xPosition, yPosition, 0);
                allMapGridPositions[x, y] = position;

                GameObject tile = Instantiate(tilePrefab,position,Quaternion.identity);
                tile.transform.SetParent(this.transform);
                tile.name = "Tile_" + x + "_" + y;

                TileField tempTile = tile.GetComponent<TileField>();
                tempTile.tileName = "Tile_" + x + "_" + y;
                // mark player tiles
                if (x < mapSizeX / 2)
                {
                    tempTile.isPlayerTile = true;
                    playerMapTileGO[x, y] = tile;
                }
                // mark enemy tiles
                else
                {
                    tempTile.isPlayerTile = false;
                    enemyMapTileGO[x-3, y] = tile;
                }
                tempTile.position.X = x;
                tempTile.position.Y = y;
                // add previous tiles
                if(x != 0)
                {
                    tempTile.prevTile = tileMap[x - 1, y];
                    tileMap[x - 1, y].nextTile = tempTile;
                }
                tileMap[x, y] = tempTile;
            }
        }
        mapStartPosition.rotation = Quaternion.Euler(angle, 0, 0);
    }

    public static bool isLegalFieldPlayer(int x, int y)
    {

        if (playerMapTileGO[x, y] != null && tileMap[x, y].isPlayerTile)
        {
            return true;
        }
        return false;
    }

    public void HideAllTileSprites()
    {
        foreach(Transform tile in mapStartPosition)
        {
            tile.Find("picture").gameObject.SetActive(false);
        }
    }
}
