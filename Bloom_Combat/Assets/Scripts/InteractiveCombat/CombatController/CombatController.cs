using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
public enum GameStage { Preparation, Combat,InBetweenCombat, Loss, Win };
public class CombatController : MonoBehaviour
{
    public GameStage currentGameStage;
    public float combatTimer = 0;
    public int combatTimerSeconds = 0;
    public UIController uIController;
    public HeroInventory heroInventory;
    public CombatField combatField;
    public CombatPreperationController combatPreperationController;
    public AbilityCardSpawner rightSideSpawner;
    public AbilityCardSpawner leftSideSpawner;
    [HideInInspector]
    public int currentWave = 0;
    HeroLineup lineUp = new HeroLineup();

    public GameObject ultButtonGO;

    public LooBar looBar;
    int endRoundCounter = 0;

    public GameObject combatloadingScreen;

    private static CombatController _instance;
    public static CombatController Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    private void Start()
    {
        currentGameStage = GameStage.Preparation;
        uIController = this.GetComponent<UIController>();
    }
    public void StartBattle()
    {
        endRoundCounter = 0;
        currentWave = 0;
        if (currentGameStage == GameStage.Preparation)
        {
            currentGameStage = GameStage.Combat;
            // add heros to the right side of the card spawner
            List<HeroController2D> rightSideHeros = new List<HeroController2D>();

            List<HeroController2D> leftSideHeros = new List<HeroController2D>();

            combatField.leftHeroList.RemoveAll(x => x == null);
            combatField.rightHeroList.RemoveAll(x => x == null);

            foreach(GameObject go in combatField.leftHeroList)
            {
                leftSideHeros.Add(go.GetComponent<HeroController2D>());
            }
            foreach (GameObject go in combatField.rightHeroList)
            {
                rightSideHeros.Add(go.GetComponent<HeroController2D>());
            }
            leftSideSpawner.StartCombat(leftSideHeros);
            rightSideSpawner.StartCombat(rightSideHeros);
            looBar.currentLoo = 0;
            // remove all nulls
            combatField.heroList.RemoveAll(x => x == null);
            combatField.StartCombat();
            InitUltButtons();
            // save hero lineup
            SaveHeroLineup();
        }
    }
    //TODO check when to trigger it agaain or refactor updating when hero is dead somewhere else
    public void InitUltButtons()
    {
        // update left side
        for (int i = 0; i < combatField.leftHeroList.Count; i++)
        {
            if (combatField.leftHeroList[i] != null)
            {
                int number = i;
                HeroController2D heroController = combatField.leftHeroList[number].GetComponent<HeroController2D>();
                GameObject ultButton = ultButtonGO.transform.Find("Left").GetChild(number).gameObject;
                ultButton.SetActive(true);
                ultButton.transform.Find("name").GetComponent<Text>().text = heroController.heroName;
                ultButton.GetComponentInChildren<Button>().onClick.AddListener(delegate { UseUlt(heroController); });
                ultButton.GetComponent<UltButtonContainer>().heroController = heroController;
            }
            else
            {
                ultButtonGO.transform.Find("Left").GetChild(i).gameObject.SetActive(false);
                ultButtonGO.transform.Find("Left").GetChild(i).Find("name").GetComponentInChildren<Text>().text = "";
            }
        }
        // update right side
        for (int i = 0; i < combatField.rightHeroList.Count; i++)
        {
            if (combatField.rightHeroList[i] != null)
            {
                int number = i;
                GameObject ultButton = ultButtonGO.transform.Find("Right").GetChild(number).gameObject;
                HeroController2D heroController = combatField.rightHeroList[number].GetComponent<HeroController2D>();
                ultButton.SetActive(true);
                ultButton.transform.Find("name").GetComponent<Text>().text = heroController.heroName;
                ultButton.GetComponent<Button>().onClick.AddListener(delegate { UseUlt(heroController); });
                ultButton.GetComponent<UltButtonContainer>().heroController = heroController;
            }
            else
            {
                ultButtonGO.transform.Find("Right").GetChild(i).gameObject.SetActive(false);
                ultButtonGO.transform.Find("Right").GetChild(i).Find("name").GetComponentInChildren<Text>().text = "";
            }
        }
    }
    public void UpdateUltButtons()
    {
        foreach(Transform child in ultButtonGO.transform.Find("Left").transform)
        {
            child.GetComponentInChildren<UltButtonContainer>().CheckIfHeroIsDead();
        }
        foreach (Transform child in ultButtonGO.transform.Find("Right").transform)
        {
            child.GetComponentInChildren<UltButtonContainer>().CheckIfHeroIsDead();
        }
    }
    public void UseUlt(HeroController2D heroController)
    {
        Debug.Log("trying to use ult of " + heroController.heroName);
        heroController.UseUltimate();
    }
    public bool UseLoo()
    {
        if(looBar.currentLoo >= 100)
        {
            looBar.UseLoo();
            return true;
        }
        return false;
    }
    public void GetLoo(float amount)
    {
        looBar.GetLoo(amount);
    }
    public void SaveHeroLineup()
    {
        HeroLineup lineup = new HeroLineup();
        lineup.heroLineup = new Dictionary<string, HeroLineupPosition>();
        // update left side
        for (int i = 0; i < combatField.leftHeroList.Count; i++)
        {
            if (combatField.leftHeroList[i] != null)
            {
                HeroController2D controller = combatField.leftHeroList[i].GetComponent<HeroController2D>();
                lineup.heroLineup.Add(controller.heroName, new HeroLineupPosition(0, controller.currentTilePos.position.getX(), controller.currentTilePos.position.getY()));
            }
            else
            {
                lineup.heroLineup.Add("none", new HeroLineupPosition(0, 0, 0));
            }
        }
        // update right side
        for (int i = 0; i < combatField.rightHeroList.Count; i++)
        {
            if (combatField.rightHeroList[i] != null)
            {
                HeroController2D controller = combatField.rightHeroList[i].GetComponent<HeroController2D>();
                lineup.heroLineup.Add(controller.heroName, new HeroLineupPosition(1, controller.currentTilePos.position.getX(), controller.currentTilePos.position.getY()));
            }
            else
            {
                lineup.heroLineup.Add("none", new HeroLineupPosition(0, 0, 0));
            }
        }

        lineup.SaveJsonData();
    }
    public void TestLoad()
    {
        lineUp.LoadJsonData();
        LoadHeroLineup(lineUp);
    }
    //TODO save and load hero line up from json
    public void LoadHeroLineup(HeroLineup lineup)
    {
        // need to differentiate between odd and even to put on the right side
        for (int index = 0; index < lineup.heroLineup.Count; index += 2)
        {
            var item = lineup.heroLineup.ElementAt(index);
            // indetify hero then put it on board
            Hero hero = HeroIdentifier.Instance.GetHeroGOFromHeroID(item.Key);
            GameObject heroControllerGO =  combatField.PlaceHeroOnBoard(hero);
            HeroController2D heroController = heroControllerGO.GetComponent<HeroController2D>();
            // set them on the right position
            var itemValue = item.Value;
            heroController.SetCurrentTile(TileMap.tileMap[itemValue.x_pos, itemValue.y_pos]);
        }
        for (int index = 1; index < lineup.heroLineup.Count; index += 2)
        {
            var item = lineup.heroLineup.ElementAt(index);
            var itemValue = item.Value;
            Hero hero = HeroIdentifier.Instance.GetHeroGOFromHeroID(item.Key);
            GameObject heroControllerGO = combatField.PlaceHeroOnBoard(hero);
            HeroController2D heroController = heroControllerGO.GetComponent<HeroController2D>();
            heroController.SetCurrentTile(TileMap.tileMap[itemValue.x_pos, itemValue.y_pos]);
        }
        foreach (KeyValuePair<string, HeroLineupPosition> entry in lineup.heroLineup)
        {
            Debug.Log(entry.Key);
            Debug.Log(entry.Value.side);
            Debug.Log(entry.Value.x_pos + " " + entry.Value.y_pos);
            // do something with entry.Value or entry.Key
        }
    }
    /// <summary>
    /// Called when champion health goes belove 0
    /// </summary>
    /// // TODO maybe when onherodeath is called put it in a queue so not multiple instances of this method are active
    public void OnHeroDeath()
    {
        UpdateUltButtons();
        bool allDeadEnemy = IsAllEnemyHeroDead();
        bool allDeadAlly = IsAllAllyHeroDead();
        if (allDeadEnemy || allDeadAlly)
        {
            if (allDeadAlly)
            {
                EndRound(false);
            }
            if (allDeadEnemy)
            {
                EndRound(true);
            }
        }
    }
    public event Action<GameObject> onHeroDeath;
    public void HeroDeathEvent(GameObject hero)
    {
        if(onHeroDeath != null)
        {
            onHeroDeath(hero);
        }
    }

    /// <summary>
    /// Checks if all champion is dead
    /// </summary>
    /// <returns></returns>
    private bool IsAllEnemyHeroDead()
    {
        int championCount = 0;
        int championDead = 0;
        foreach (GameObject hero in combatField.aiHeroList)
        {
            HeroControllerCombat2D hc = hero.GetComponent<HeroControllerCombat2D>();
            championCount++;

            if (hc.isDead)
                championDead++;
        }
        if (championDead == championCount)
            return true;

        return false;
    }
    /// <summary>
    /// Returns true if all the champions are dead
    /// </summary>
    /// <returns></returns>
    private bool IsAllAllyHeroDead()
    {

        int championCount = 0;
        int championDead = 0;
        //start own champion combat

        foreach (GameObject hero in combatField.heroList)
        {
            HeroControllerCombat2D hc = hero.GetComponent<HeroControllerCombat2D>();
            championCount++;

            if (hc.isDead)
                championDead++;
        }
        if (championDead == championCount)
            return true;

        return false;
    }
    
    public bool LastWave()
    {
        currentWave++;
        if (currentWave >= combatPreperationController.numberOfWaves)
        {
            return true;
        }
        else return false;
    }
    IEnumerator HerosWalkToEndOfScreen()
    {
        currentGameStage = GameStage.InBetweenCombat;
        foreach(GameObject hero in combatField.heroList)
        {
            HeroController2D hc = hero.GetComponent<HeroController2D>();
            if (hc.heroControllerCombat.isDead) continue;
            else
                hc.WalkToEndOfScreen();
        }
        yield return new WaitForSeconds(1.5f);
        StartCoroutine(LoadNextWave());
    }

    IEnumerator LoadNextWave()
    {
        combatloadingScreen.SetActive(true);
        combatPreperationController.LoadEnemy();
        yield return new WaitForSeconds(1f);
        combatloadingScreen.SetActive(false);
        StartCoroutine(HerosWalkToStartOfScreen());
    }
    IEnumerator HerosWalkToStartOfScreen()
    {
        lineUp.LoadJsonData();
        foreach (GameObject hero in combatField.heroList)
        {
            HeroController2D hc = hero.GetComponent<HeroController2D>();
            if (hc.heroControllerCombat.isDead) continue;
            else
            {
                hc.SetCurrentTile(TileMap.tileMap[lineUp.heroLineup[hc.heroName].x_pos, lineUp.heroLineup[hc.heroName].y_pos]);
                hc.WalkFromStartOfScreen();
            }
        }
        yield return new WaitForSeconds(3f);
        currentGameStage = GameStage.Combat;
        endRoundCounter = 0;
    }
    /// <summary>
    /// Ends the round
    /// </summary>
    private void EndRound(bool playerWin)
    {
        if (playerWin)
        {
            // counter ensure that not multiple objects call end round and therefore skip rounds
            endRoundCounter++;
            if (endRoundCounter > 1) return;
            if (!LastWave())
            {
                StartCoroutine(HerosWalkToEndOfScreen());
/*                currentGameStage = GameStage.Preparation;
                //StartCoroutine(PlaceHeroBackToStartPosition());
                combatPreperationController.LoadEnemy();
                currentGameStage = GameStage.Combat;*/
            }
            else
            {
                currentGameStage = GameStage.Win;
                //uiController.UpdateUICombat(currentGameStage);
                // give player the rewards
                // next stage
                // RestorHeroOnGrid();
                combatPreperationController.PlayerWon();
                Debug.Log("player won");
                //GameProgress.Instance.GoToNextStage();
            }

        }
        else
        {
            endRoundCounter++;
            if (endRoundCounter > 1) return;
            currentGameStage = GameStage.Loss;
            Debug.Log("player lost");
            combatPreperationController.PlayerLost();
            //uiController.UpdateUICombat(currentGameStage);
            //RestorHeroOnGrid();
            // repeat battle or leave battle
        }
    }




}
