using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroPositionController2D : MonoBehaviour
{
    public static void SetGridPosition(int _gridPositionX, int _gridPositionY, HeroController heroController)
    {
        heroController.gridPositionX = _gridPositionX;
        heroController.gridPositionZ = _gridPositionY;


        //set new target when chaning grid position
        heroController.gridTargetPosition = TileMap.allMapGridPositions[_gridPositionX, _gridPositionY];
    }
}
