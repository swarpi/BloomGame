using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MoveCalculator2D : MonoBehaviour
{
    public List<HeroController2D> allHoersInCombat;
    public Queue<HeroMovement2D> heroMovements;
    // List of all units in combat
    // dictionary of unit and their next step
    // Start is called before the first frame update
    void Start()
    {

    }
    private float nextUpdate = 1;
    float startWait = 1f;
    float currentTime = 0f;
    // Update is called once per frame
    void Update()
    {
        if (CombatController.Instance.currentGameStage == GameStage.Combat)
        {
            if (currentTime > startWait)
            {       // If the next update is reached
                if (Time.time >= nextUpdate)
                {
                    // Change the next update (current second+1)
                    nextUpdate = Time.time + 0.55f;
                    // potential problem : if we look to fast for a new step, heroes didnt go to their target fast enough
                    //                      therefore two heros might go to the same tile;
                    //                      so be careful of slow heros
                    DoStepsQueue();
                }
            }
            else
            {
                currentTime += Time.deltaTime;
            }
            // test the stepqueue
/*            if (Input.GetButtonDown("Jump"))
            {
                //DoStepsQueue();
            }*/
        }

    }
    public void GetAllHeroesInCombat()
    {
        allHoersInCombat = new List<HeroController2D>();
        // add all players
        foreach (GameObject hero in CombatController.Instance.combatField.heroList)
        {
            allHoersInCombat.Add(hero.GetComponent<HeroController2D>());
        }
        // add all enemies
        foreach (GameObject hero in CombatController.Instance.combatField.aiHeroList)
        {
            allHoersInCombat.Add(hero.GetComponent<HeroController2D>());
        }
    }



    public void CalculateStepsQueue()
    {
        GetAllHeroesInCombat();
        heroMovements = new Queue<HeroMovement2D>();

        foreach (HeroController2D hero in allHoersInCombat)
        {
            if (hero == null || hero.heroControllerCombat.isDead) return;
            Debug.Log(hero);
            List<TileField> tileList = FindPath2D(hero.currentTilePos, hero.targetTile);
            //List<Tile> tileList = PathFinder.FindPath(hero.GetCurrentTile(), hero.targetTile, hero);
            if (tileList == null) return;
            if (tileList.Count > 0
                && hero.currentCombatState != AICombatState.InCombat)
            {
                TileField nextStep = tileList[0];
                // no conflicting next steps and my next step is not my target already
                // CheckAndAddStep(hero);
                if (!heroMovements.Any(x => x.move == nextStep) && nextStep != hero.targetTile)
                {
                    // add my step to the queue
                    heroMovements.Enqueue(new HeroMovement2D(hero.heroID, nextStep));
                }
            }
        }
    }

    List<TileField> FindPath2D(TileField start, TileField end)
    {
        List<TileField> tilePath = new List<TileField>();
        if (start == null || end == null) return null;
        // if enemy is right of me
        if (start.position.getX() < end.position.getX())
        {
            for (int i = start.position.getX() +1 ; i < end.position.getX(); i++)
            {
                tilePath.Add(TileMap.tileMap[i, start.position.getY()]);
            }
        }
        // if enemy ist left of me
        if (start.position.getX() > end.position.getX())
        {
            for (int i = start.position.getX() - 1; i > end.position.getX(); i--)
            {
                tilePath.Add(TileMap.tileMap[i, start.position.getY()]);
            }
        }
        return tilePath;
    }

    public void DoStepsQueue()
    {
        //Debug.Log("doing steps");
        CalculateStepsQueue();

        while (heroMovements.Count > 0)
        {
            HeroMovement2D hm = heroMovements.Dequeue();
            HeroController2D hero = allHoersInCombat.Find(x => x.heroID == hm.heroID);
            hero.MoveTowards(hm.move);
        }
    }
    
/*    public static Tile FindFurthestPlayerTile(Tile startPosition)
    {
        Tile prevTile = startPosition;
        Vector2 startingPoint = new Vector2(startPosition.transform.position.getX(), startPosition.transform.position.getY());
        float distance = 0f;
        for (int x = 0; x < MapGenerator.hexMapSizeX / 2; x++)
        {
            for (int z = 0; z < MapGenerator.hexMapSizeZ; z++)
            {
                if (MapGenerator.playerMapGO[x, z] == null) continue;
                Tile tempTile = MapGenerator.playerMapGO[x, z].GetComponent<Tile>();
                Vector2 newPoint = new Vector2(tempTile.transform.position.getX(), tempTile.transform.position.getY());
                float newDistance = Vector2.Distance(startingPoint, newPoint);
                if (distance < newDistance && tempTile.heroOnTile == null)
                {
                    distance = newDistance;
                    prevTile = tempTile;
                }
            }
        }
        return prevTile;
    }

    public static GameObject FindFurthestEnemyUnit(GameObject startPosition)
    {
        GameObject prevGO = startPosition;
        Vector3 startingPoint = startPosition.transform.position;
        float distance = 0f;
        foreach (GameObject enemy in AIopponent.aiHeroList)
        {
            Vector3 enemyPoint = enemy.transform.position;
            float newDistance = Vector3.Distance(startingPoint, enemyPoint);
            if (distance < newDistance)
            {
                distance = newDistance;
                prevGO = enemy;
            }
        }
        return prevGO;
    }*/
}
public struct HeroMovement2D
{
    public string heroID;
    public TileField move;

    public HeroMovement2D(string _heroID, TileField _move)
    {
        heroID = _heroID;
        move = _move;
    }

}
// obsolete because was used for 3d movecalculator
public struct HeroMovement
{
    public string heroID;
    public Tile move;

    public HeroMovement(string _heroID, Tile _move)
    {
        heroID = _heroID;
        move = _move;
    }

}