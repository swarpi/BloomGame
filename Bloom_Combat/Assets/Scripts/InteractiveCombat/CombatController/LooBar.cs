using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LooBar : MonoBehaviour
{
    public Image fillImageLoo;
    public CanvasGroup canvasGroup;
    public float currentLoo;
    float looStart;
    private float nextUpdate;

    private void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Update()
    {
        if (Time.time >= nextUpdate)
        {
            // Change the next update (current second+1)
            nextUpdate = Time.time + 0.05f;
            // potential problem : if we look to fast for a new step, heroes didnt go to their target fast enough
            //                      therefore two heros might go to the same tile;
            //                      so be careful of slow heros
            if(looStart <= currentLoo)
            {
                looStart += 1;
                // make it more dynamic
                fillImageLoo.fillAmount = looStart / 100f;
            }

        }

    }

    public void GetLoo(float amount)
    {
        currentLoo += amount;
    }

    public void UseLoo()
    {
        currentLoo = 0f;
    }
}
