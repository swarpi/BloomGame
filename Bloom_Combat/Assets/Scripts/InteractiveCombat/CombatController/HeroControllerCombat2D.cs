using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EvolutionState
{
    rankOne,
    rankTwo,
    rankThree
}
public class HeroControllerCombat2D : MonoBehaviour
{
    // components
    [HideInInspector]
    public HeroController2D heroController;

    // hero infos
    [HideInInspector]
    public Hero hero;
    ///Maximum health of the champion
    public RefFloat maxHealth;
    ///current health of the champion 
    public RefFloat currentHealth;
    ///current armor of the champion 
    public RefFloat currentArmor;
    ///current magicResistance of the champion 
    public RefFloat currentMagicResistance;
    ///current health of the champion 
    public RefFloat currentShield;
    [HideInInspector]
    ///Maximum health of the champion
    public float maxLoo = 0;
    [HideInInspector]
    ///current health of the champion 
    public float currentLoo = 0;
    [HideInInspector]
    ///Current damage of the champion deals with a attack
    public RefFloat autoAttackDamage;

    // combat infos

    public EvolutionState evoState = EvolutionState.rankOne;
    public bool isAttacking = false;
    public bool isDead = false;
    public bool isUntargettable = false;
    public bool isMoveable = true;

    public EffectList effectList; 

    private void Start()
    {
        //heroControllerMovement = GetComponent<HeroControllerMovement>();
        //championAnimation = GetComponent<ChampionAnimation>();

    }

    private void Update()
    {
        effectList.UpdateList(Time.deltaTime);
    }
    public void Init(HeroController2D _heroController, Hero _hero, int _teamID)
    {
        heroController = _heroController;
        //heroControllerMovement = heroController.heroControllerMovement;
        //championAnimation = heroController.championAnimation;
        //uiController = heroController.uiController;
        //autoCombatController = heroController.autoCombatController;
        hero = _hero;
        maxHealth = new RefFloat(_hero.stats.health);
        currentHealth = new RefFloat(_hero.stats.health);
        currentArmor = new RefFloat(_hero.stats.armor);
        currentMagicResistance = new RefFloat(_hero.stats.magicResist);
        maxLoo = _hero.stats.loo;
        currentLoo = _hero.stats.looStart;
        autoAttackDamage = new RefFloat(_hero.stats.attackDamage);
        Debug.Log("setting player init rank");
        HeroEvolution(1);// start with hero rank 1
        effectList = new EffectList(); 
    }
    public virtual bool DoMove(float distance)
    {
        //isAttacking = true;
        if (currentLoo >= maxLoo)
        {
            //currentLoo -= maxLoo;
            return true;
        }
        //isAttacking = false;
        return false;
    }

    public virtual bool DoAbilityOne(int cardLevel)
    {
        if (effectList.CheckType(Effect_type.hardCC))
            return false;
        else
            Debug.Log("doing ability one of " + hero.heroID + " with level " + cardLevel);
        return true;
    }
    public virtual bool DoAbilityTwo(int cardLevel)
    {
        if (effectList.CheckType(Effect_type.hardCC))
            return false;
        else
            Debug.Log("doing ability one of " + hero.heroID + " with level " + cardLevel);
        return true;
    }

    public virtual bool DoEvolutionOne()
    {
        if (effectList.CheckType(Effect_type.hardCC))
            return false;
        else
        {
            //fancy animation
            evoState = EvolutionState.rankTwo;
            HeroEvolution(2);
        }
        return true;
    }

    public virtual bool DoEvolutionTwo()
    {
        if (effectList.CheckType(Effect_type.hardCC))
            return false;
        else
        {
            //fancy animation
            evoState = EvolutionState.rankThree;
            HeroEvolution(3);
        }
        return true;
    }
    public event Action<int> onHeroEvolution;
    public void HeroEvolution(int rank)
    {
        if (onHeroEvolution != null)
        {
            onHeroEvolution(rank);
        }
    }
    public virtual bool DoUltimate()
    {
        if (effectList.CheckType(Effect_type.hardCC))
            return false;
        else
        {
            if (false/* ult rdy?*/)
                return false;
            else
            {
                if (evoState == EvolutionState.rankOne)
                {
                    Debug.Log("doing ultimate one of " + hero.heroID + " in Evolutionstate " + evoState);
                    DoEvolutionOne();
                    return true;
                }
                else if (evoState == EvolutionState.rankTwo)
                {
                    Debug.Log("doing ultimate one of " + hero.heroID + " in Evolutionstate " + evoState);
                    return true;
                }
                else
                {
                    //enough loo?
                    Debug.Log("doing ultimate one of " + hero.heroID + " in Evolutionstate " + evoState);
                    return true;
                }
            }
        }
    }
    /// <summary>
    /// Start attack against enemy champion
    /// </summary>
    public virtual void DoAttack()
    {
        bool isCrit = false;
        if (effectList.CheckType(Effect_type.hardCC))
        {
            return;
        }
        else
        {
            heroController.currentCombatState = AICombatState.InCombat;
            float randValue = UnityEngine.Random.value;
            if (randValue <= hero.stats.critChance)
                isCrit = true;
            else isCrit = false;
            //championAnimation.DoAttack(true, isCrit);
        }
        if (effectList.CheckType(Effect_type.blind))
            StartCoroutine(DamageTarget(heroController.target, 0, isCrit, false, 0.8f, true)); // need to add miss indicator
        else
            StartCoroutine(DamageTarget(heroController.target, autoAttackDamage.f, isCrit, false, 0.8f, true));
    }

    public virtual IEnumerator DamageTarget(GameObject damageTarget, float damage, bool isCrit = false, bool isMagic = false, float delay = 0.5f, bool isAutoAttack = false, bool isRange = false)
    {
        // delay depending on animation speed or until projectil hit the target
        // try anim.GetCurrentAnimatorStateInfo(layer).length for the animation duration
        if (isRange)
        {
            // wait until projectile hit the target
        }
        else
        {
            yield return new WaitForSeconds(delay);
        }

        if (heroController.target != null)
        {
            try
            {
                currentLoo += hero.stats.looPerHitGiven;
                HeroControllerCombat2D targetHero = damageTarget.GetComponent<HeroControllerCombat2D>();
                float damageGiven = DamageCalculator.CalculateDamageGiven(heroController.hero, damage, isCrit);

                bool isTargetDead = targetHero.OnGotHit(heroController, damageGiven, isCrit, isMagic, isAutoAttack);

                if (isTargetDead)
                    heroController.TryAttackNewTarget();
            } catch (MissingReferenceException e)
            {
                Debug.Log("no target to damage, maybe it is already dead");
            }
        }
        yield return null;
    }

    /// <summary>
    /// Called when this champion takes damage
    /// </summary>
    /// <param name="damage"></param>
    public virtual bool OnGotHit(HeroController2D source, float damage, bool isCrit, bool isMagic, bool isAutoAttack)
    {
        float damageTaken = DamageCalculator.CalculateDamageTaken2D(source, heroController, damage, isMagic);
        if (currentShield.f > 0)
        {
            float overflow = currentShield.f - damageTaken;
            currentShield.f -= damageTaken;
            if (overflow < 0)
            {
                currentHealth.f -= Math.Abs(overflow);
            }
        }
        else
        {
            // wake me up if i am sleeping
            if (effectList.CheckSleep()) effectList.WakeUp();
            currentHealth.f -= damageTaken;
        }

        // increase Loo
        currentLoo += hero.stats.looPerHitTaken;
        //add floating text
        heroController.worldCanvasController.AddDamageText(this.transform.position + new Vector3(0, 1.5f, 0), damageTaken, isCrit);
        //death
        if (currentHealth.f <= 0)
        {
            isDead = true;
            CombatController.Instance.HeroDeathEvent(this.gameObject);
        }
        return isDead;
    }

    public float CalculateCCDuration(float ccBaseDuration, float enemyEffectivity) // Review
    {
        float ccDuration = 0f;
        // -1 for ignore eff/tenacity
        if (enemyEffectivity == -1)
        {
            return ccBaseDuration;
        }
        float calculatedDuration = (enemyEffectivity - hero.stats.tenacity) / 2;
        // if i have more than 100 effectivity than the enemy, increase duration by 50%
        if (calculatedDuration > 50)
        {
            ccDuration = ccBaseDuration * 1.5f;
        }
        // if i have less than 100 effectivity than the enemy, decrease duration by 50%
        else if (calculatedDuration < -50)
        {
            ccDuration = ccBaseDuration * 0.5f;
        }
        // else calculate the duration and add it to 1... 0.4 => 1.4 / -0.3 => 0.7
        else
        {
            ccDuration = ccBaseDuration * (1 + (calculatedDuration / 100));
        }
        return ccDuration;
    }
}

