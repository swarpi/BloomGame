using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CombatField : MonoBehaviour
{
    // array for pre combat placement
    public GameObject[,] gridHeroArray;
    public List<GameObject> heroList;
    public List<GameObject> aiHeroList;
    public Dictionary<HeroClassBonus, int> activeClassBonuses;
    public Dictionary<HeroOriginBonus, int> activeOriginBonuses;
    public List<GameObject> leftHeroList;
    public List<GameObject> rightHeroList;

    private void Awake()
    {
        gridHeroArray = new GameObject[TileMap.mapSizeX/2, TileMap.mapSizeY];
        heroList = new List<GameObject>();
        leftHeroList = new List<GameObject>();
        rightHeroList = new List<GameObject>();

        aiHeroList = new List<GameObject>();
    }

    int counter = 0;
    public void AddEnemy(StageInfo currentStage)
    {
        int currentWave = CombatController.Instance.currentWave;
        aiHeroList = new List<GameObject>();
        //Stage currentStage = GameProgress.Instance.currentStage;
        for (int i = 0; i < currentStage.enemyArray[currentWave].Length; i++)
        {
            counter++;
            // need to create a copy of the scriptable object to not overwrite the info from the player
            Hero hero = HeroIdentifier.Instance.CreateHeroFromID(currentStage.enemyArray[currentWave][i].enemy);
            // read hero level
            hero.heroLevel = currentStage.enemyArray[currentWave][i].level;
            // adapt its stats
            hero = HeroIdentifier.Instance.AdaptHeroStatsToLevel(hero);
            //instantiate champion prefab
            GameObject heroPrefab = Instantiate(hero.prefab);

            //get champion controller
            HeroController2D heroController = heroPrefab.GetComponent<HeroController2D>();

            //setup championcontroller
            heroController.Init(hero, HeroController.TEAMID_AI, currentStage.enemyArray[currentWave][i].x_pos, currentStage.enemyArray[currentWave][i].y_pos);
            //add champion to array
            //set grid position

            //heroController.SetGridPositionNew(EnemyMap.GRIDTYPE_HEXA_MAP, currentStage.enemyPosition[i]);
            aiHeroList.Add(heroPrefab);
            //CalculateBonuses();
            //set position and rotation

        }
    }

    public GameObject PlaceHeroOnBoard(Hero hero)
    {
        if (HeroOnBoard(hero)) return null;

        int emptyIndexX = -1;
        int emptyIndexY = -1;
        // check if spot is occupied, if no set emptyindex to that position
        for (int i = 0; i < gridHeroArray.GetLength(0); i++)
        {
            for (int j = 0; j < gridHeroArray.GetLength(1); j++)
            {
                if (gridHeroArray[i, j] == null && TileMap.isLegalFieldPlayer(i, j))
                {
                    emptyIndexX = i;
                    emptyIndexY = j;
                    break;
                }
                // need to break out of second for
            }
        }
        if (emptyIndexX == -1 || emptyIndexY == -1)
            return null;

        //instantiate champion prefab
        GameObject heroPrefab = Instantiate(hero.prefab);
        
        // clicking on a hero and spawning it on a field where a hero already is shouldnt be allowed


        //get championController
        HeroController2D heroController = heroPrefab.GetComponent<HeroController2D>();
        //setup chapioncontroller
        heroController.Init(hero, HeroController2D.TEAMID_PLAYER,emptyIndexX, emptyIndexY);
        StoreChampionInArray(emptyIndexX, emptyIndexY, heroPrefab);
        return heroPrefab;
    }

    public bool HeroOnBoard(Hero hero)
    {
        if (heroList.Any(x =>
        {
            if (x == null) return false;
        return x.GetComponent<HeroController2D>().heroName == hero.heroID;
        }))
        {
            Debug.Log(hero.heroID + " is already on board");
            return true;
        }
        else return false;
    }
    /// <summary>
    /// Store champion gameobject in array
    /// </summary>
    /// <param name="gridX"></param>
    /// <param name="gridY"></param>
    /// <param name="hero"></param>
    public void StoreChampionInArray(int gridX, int gridY, GameObject hero)
    {
        gridHeroArray[gridX, gridY] = hero;
        // if there is a null in the list replace the null with the hero
        // null comes from removing a hero
        if (heroList.Contains(null))
        {
            int heroIndex = heroList.IndexOf(null);
            heroList[heroIndex] = hero;
            if (heroIndex % 2 == 0)
            {
                int leftHeroIndex = leftHeroList.IndexOf(null);
                leftHeroList[leftHeroIndex] = hero;
            }
            else
            {
                int rightHeroIndex = rightHeroList.IndexOf(null);
                rightHeroList[rightHeroIndex] = hero;
            }
        }
        // if the hero isnt in the list put it into the right side and then into the list
        if (!heroList.Contains(hero))
        {
            if(heroList.Count%2 == 0)
            {
                leftHeroList.Add(hero);
            }
            else
            {
                rightHeroList.Add(hero);
            }
            heroList.Add(hero);
        }
        HeroListChanged();
        // Update combatprep
        //CalculateBonuses();
    }
    /// <summary>
    /// Remove champion from array
    /// </summary>
    /// <param name="gridX"></param>
    /// <param name="gridY"></param>
    /// <param name="hero"></param>
    public void RemoveChampionFromArray(int gridX, int gridZ, GameObject hero)
    {
        gridHeroArray[gridX, gridZ] = null;
        //gridHeroArrayAtStartOfCombat[gridX, gridZ] = null;
        //hero.GetComponent<HeroController>().currentTilePos.RemoveHero();
        int heroIndex = heroList.IndexOf(hero);
        heroList[heroIndex] = null; // null elements are only for card spawner remove them when combat starts
        if(heroIndex % 2 == 0)
        {
            int leftHeroIndex = leftHeroList.IndexOf(hero);
            leftHeroList[leftHeroIndex] = null;
        }
        else
        {
            int rightHeroIndex = rightHeroList.IndexOf(hero);
            rightHeroList[rightHeroIndex] = null;
        }
        HeroListChanged();
        //heroList.Remove(hero);
        //CalculateBonuses();
    }


    public static event Action onHeroListChanged;
    public static void HeroListChanged()
    {
        if (onHeroListChanged != null)
        {
            onHeroListChanged();
        }
    }

    //public static void CalculateBonuses()
    //{
    //    AutoCombatSynergies.CalculateBonuses(out activeClassBonuses, out activeOriginBonuses, heroList);
    //    // need to remove the class and origin effect if it doesnt apply anymore
    //    ApplyClassEffect();
    //    ApplyOriginEffect();
    //    uIController.LoadSyngeryDisplay(activeClassBonuses, activeOriginBonuses);
    //}

    //static void ApplyClassEffect()
    //{
    //    foreach (KeyValuePair<HeroClassBonus, int> classBonuses in activeClassBonuses)
    //    {
    //        if (!classBonuses.Key.ActivateClassEffect(classBonuses.Value, heroList))
    //        {
    //            // remove effect if it was active
    //        }
    //    }
    //}

    //static void ApplyOriginEffect()
    //{
    //    foreach (KeyValuePair<HeroOriginBonus, int> originBonuses in activeOriginBonuses)
    //    {
    //        originBonuses.Key.ActivateOriginEffect(originBonuses.Value, heroList);
    //    }
    //}
    public void StartCombat()
    {
        // start for enemies
        foreach(GameObject enemyGo in aiHeroList)
        {
            enemyGo.GetComponent<HeroController2D>().OnCombatStart();
        }
        // start for allies
        foreach (GameObject allyGo in heroList)
        {
            allyGo.GetComponent<HeroController2D>().OnCombatStart();
        }
    }

    
}
