using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HeroSaveData
{
    ///Projectile prefab to create when champion is attacking
    public string uniqueHeroID;
    public string heroID;
    ///The champion name displayed on the UI frames

    // hero level
    public int heroLevel;
    public int currentExp;
    // required exp pro level
    public List<Rune> equippedRunes = new List<Rune>();
    public Rune alpha1Rune;
    public Rune beta1Rune;
    public Rune beta2Rune;
    public Rune gamma1Rune;
    public Rune gamma2Rune;
    public Rune gamma3Rune;

    public HeroSaveData(string uniqueID)
    {
        heroID = uniqueID;
        heroLevel = 1;
        currentExp = 0;
        for (int i = 0; i< 6; i++)
        {
            equippedRunes.Add(new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet));
        }
        alpha1Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
        beta1Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
        beta2Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
        gamma1Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
        gamma2Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
        gamma3Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
    }
    public HeroSaveData()
    {
        for (int i = 0; i < 6; i++)
        {
            equippedRunes.Add(new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet));
        }
    }

    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }

    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
    public Dictionary<string, System.Object> ToDictionary()
    {
        Dictionary<string, System.Object> result = new Dictionary<string, object>();
        result["heroID"] = heroID;
        result["hero level"] = heroLevel;
        result["current Exp"] = currentExp;
        result["alpha1Rune"] = alpha1Rune.ToDictionary();
        result["beta1Rune"] = beta1Rune.ToDictionary();
        result["beta2Rune"] = beta2Rune.ToDictionary();
        result["gamma1Rune"] = gamma1Rune.ToDictionary();
        result["gamma2Rune"] = gamma2Rune.ToDictionary();
        result["gamma3Rune"] = gamma3Rune.ToDictionary();

        return result;
    }
}
