using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class HeroExpRequirement
{
    public int level;
    public int requiredExp;
    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
}
[System.Serializable]
public class HeroExpRequirements
{
    public HeroExpRequirement[] heroExpRequirements;
    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
}
