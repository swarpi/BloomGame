using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public sealed class GameProgress : MonoBehaviour, ISaveable
{

    private static GameProgress _instance;

    public static GameProgress Instance { get { return _instance; } }


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        DontDestroyOnLoad(this);
    }

    public void SaveJsonData()
    {
        SaveData sd = new SaveData();
        GameProgress.Instance.PopulateSaveData(sd);

        if (FileManager.WriteToFile("GameProgress.dat", sd.ToJson()))
        {
            Debug.Log("Save sucessfull");
        }
    }

    public void PopulateSaveData(SaveData a_SaveData)
    {
        a_SaveData.stage = currentStage.stageID;
        a_SaveData.level = currentLevel.levelID;
    }

    public void LoadJsonData()
    {
        if(FileManager.LoadFromFile("GameProgress.dat", out var json))
        {
            SaveData sd = new SaveData();
            sd.LoadFromJson(json);

            GameProgress.Instance.LoadFromSaveData(sd);
            foreach (int stage in GameProgress.Instance.currentLevel.stageIDs)
            {
                Debug.Log(stage + " stage");
            }
            Debug.Log("Load complete");

        }
    }

    public void LoadFromSaveData(SaveData a_SaveData)
    {
        currentLevel = Array.Find(Level.GetAllInstances<Level>(),
    level => level.levelID == a_SaveData.level);

        currentStage = Array.Find(Stage.GetAllInstances<Stage>(), 
            stage => stage.stageID == a_SaveData.stage 
            && stage.levelID == currentLevel.levelID);
    }
    
    // load from playerdata their game progress
    public Stage currentStage;
    public Level currentLevel;
    public int stageID = 1;
    public void GoToNextStage()
    {
        currentStage = Array.Find(Stage.GetAllInstances<Stage>(),
        stage => stage.stageID == (currentStage.stageID +1)
        && stage.levelID == currentLevel.levelID);

    }
}