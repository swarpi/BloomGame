using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroInventoryContainer : MonoBehaviour
{
    public List<HeroSaveData> heroIDList;

    public void SaveJsonData()
    {
        if (FileManager.WriteToFile("HeroInventory.dat", ToJson()))
        {
            Debug.Log("Save sucessfull");
        }
    }

    public void PopulateSaveData(SaveData a_SaveData)
    {

    }

    public void LoadJsonData()
    {
        if (FileManager.LoadFromFile("HeroInventory.dat", out var json))
        {
            LoadFromJson(json);     
        }
    }

    public void LoadFromSaveData(SaveData a_SaveData)
    {

    }

    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }

    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }

    public Dictionary<string, System.Object> ToDictionary()
    {
        Dictionary<string, System.Object> result = new Dictionary<string, System.Object>();
        List<Dictionary<string, System.Object>> heroList = new List<Dictionary<string, System.Object>>();
        foreach (HeroSaveData hero in heroIDList)
        {
            heroList.Add(hero.ToDictionary());
        }
        result["heroIdList"] = heroList;

        return result;
    }
}
