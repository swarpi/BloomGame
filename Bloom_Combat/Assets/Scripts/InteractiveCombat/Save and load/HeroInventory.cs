using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates and stores champions available, XP and LVL purchase
/// </summary>
public class HeroInventory : MonoBehaviour
{
    public UIController uIController;
    public AutoCombatController autoCombatController;
    public GameData gameData;

    ///Array to store available champions to purchase
    public List<Hero> heroList;
    public HeroInventoryContainer heroInventoryContainer;

    /// Start is called before the first frame update
    void Start()
    {
        //RefreshInventory();
        //InitHeroList();
        //uIController.LoadHeroDisplay(heroList);

        StartCoroutine(FirstLoadHeroInventoryContainer());
    }

    public void AddNewHeroes()
    {
        AddHeroToInventory("Catharina");
        AddHeroToInventory("Tristan");
        AddHeroToInventory("SingctumSword");
        AddHeroToInventory("SingctumSpear");
    }
    IEnumerator FirstLoadHeroInventoryContainer()
    {
        yield return new WaitForSeconds(1f);
        yield return AuthManager.Instance.LoginTestAccount();
        //AddNewHeroes();
        LoadHeroContainer();
    }
    // load the heroes from the container to the actual hero data
    void InitHeroList()
    {
        heroList = new List<Hero>();
        foreach (HeroSaveData heroID in heroInventoryContainer.heroIDList)
        {
            Hero hero = HeroIdentifier.Instance.GetHeroGOFromHeroID(heroID.heroID);
            if(hero == null)
            {
                Debug.Log("hero " + heroID.heroID + " was not found by the identifier");
                return;
            }
            hero.heroLevel = heroID.heroLevel;
            hero.heroExp = heroID.currentExp;
            // adapt its stats based on his level and his runes
            hero = HeroIdentifier.Instance.AdaptHeroStatsToLevel(hero);
            AddAlphaRune(hero, heroID.alpha1Rune);
            AddBetaRune(hero, heroID.beta1Rune, heroID.beta2Rune);
            AddGammaRune(hero, heroID.gamma1Rune, heroID.gamma2Rune, heroID.gamma3Rune);
            heroList.Add(hero);
        }
    }

    public void SaveHeroList()
    {
        List <HeroSaveData> heroSavaDataList = new List<HeroSaveData>();
        foreach (Hero hero in heroList)
        {
            HeroSaveData heroSaveData = heroInventoryContainer.heroIDList.Find(x => x.heroID == hero.heroID);
            if(heroSaveData == null)
            {
                Debug.Log("error couldnt find hero");
                return;
            }
            heroSaveData.heroLevel = hero.heroLevel;
            heroSaveData.currentExp = hero.heroExp;
            heroSaveData.alpha1Rune = hero.alpha1Rune;
            heroSaveData.beta1Rune = hero.beta1Rune;
            heroSaveData.beta2Rune = hero.beta2Rune;
            heroSaveData.gamma1Rune = hero.gamma1Rune;
            heroSaveData.gamma2Rune = hero.gamma2Rune;
            heroSaveData.gamma3Rune = hero.gamma3Rune;
        }

        SaveHeroContainer();
    }
    public void SaveHeroContainer()
    {
        StartCoroutine(DatabaseManager.Instance.UpdateHeroContainer(heroInventoryContainer));
    }
    public void LoadHeroContainer()
    {
        StartCoroutine(LoadHeroInventoryContainer());
    }
    
    public IEnumerator LoadHeroInventoryContainer()
    {
        CoroutineWithData cd = new CoroutineWithData(DatabaseManager.Instance, DatabaseManager.Instance.LoadUserDataHeroContainer());
        yield return cd.coroutine;
        heroInventoryContainer.heroIDList = (List<HeroSaveData>) cd.result;
        InitHeroList();
    }
    void AddAlphaRune(Hero hero, Rune alphaRune)
    {
        hero.alpha1Rune = alphaRune;
    }
    void AddBetaRune(Hero hero, Rune beta1Rune, Rune beta2Rune)
    {
        hero.beta1Rune = beta1Rune;
        hero.beta2Rune = beta2Rune;
    }
    void AddGammaRune(Hero hero, Rune gamma1Rune, Rune gamma2Rune, Rune gamma3Rune)
    {
        hero.gamma1Rune = gamma1Rune;
        hero.gamma2Rune = gamma2Rune;
        hero.gamma3Rune = gamma3Rune;
    }
    public void AddHeroToInventory(string heroName)
    {
        // check if i already have this hero
        Hero newHero = HeroIdentifier.Instance.GetHeroGOFromHeroID(heroName);
        if (heroList.Contains(newHero))
        {
            Debug.Log("you alredy have this hero, take this coin instead");
        }
        else
        {
            heroList.Add(newHero);
            heroInventoryContainer.heroIDList.Add(new HeroSaveData(heroName));
            SaveHeroContainer();
        }
    }


}
