using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HeroIdentifier : MonoBehaviour
{
    public List<Hero> allHeroes = new List<Hero>();
    private static HeroIdentifier _instance;
    public static HeroIdentifier Instance { get { return _instance; } }
    public TextAsset jsonFileStats;
    public TextAsset jsonFileExp;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        jsonFileStats = Resources.Load("HeroStatsPerLevel 1-50") as TextAsset;
        jsonFileExp = Resources.Load("HeroExpRequirements") as TextAsset;
    }

    void Start()
    {
        DontDestroyOnLoad(this);

    }

    public Hero GetHeroGOFromHeroID(string heroID)
    {
        return allHeroes.Find(x => x.heroID == heroID);
    }

    public List<HeroSaveData> GetHeroSaveDataFromDict(Dictionary<string, System.Object> result)
    {
        List<HeroSaveData> heroList = new List<HeroSaveData>();
        List<System.Object> heroIdListDB = (List<System.Object>)result["heroIdList"];
        foreach (System.Object dbHero in heroIdListDB)
        {
            HeroSaveData heroSaveData = new HeroSaveData();
            // need to cast the db hero to a dictionary
            Dictionary<string, System.Object> dicSub = (Dictionary<string, System.Object>)dbHero;
            // read id
            heroSaveData.heroID = dicSub["heroID"].ToString();
            // read level
            heroSaveData.heroLevel = int.Parse(dicSub["hero level"].ToString());
            // read current exp
            heroSaveData.currentExp = int.Parse(dicSub["current Exp"].ToString());
            // read runes
            heroSaveData.alpha1Rune = RuneManager.ConvertRuneFromDictionary(((Dictionary<string, System.Object>) dicSub["alpha1Rune"]));
            heroSaveData.beta1Rune = RuneManager.ConvertRuneFromDictionary(((Dictionary<string, System.Object>)dicSub["beta1Rune"]));
            heroSaveData.beta2Rune = RuneManager.ConvertRuneFromDictionary(((Dictionary<string, System.Object>)dicSub["beta2Rune"]));
            heroSaveData.gamma1Rune = RuneManager.ConvertRuneFromDictionary(((Dictionary<string, System.Object>)dicSub["gamma1Rune"]));
            heroSaveData.gamma2Rune = RuneManager.ConvertRuneFromDictionary(((Dictionary<string, System.Object>)dicSub["gamma2Rune"]));
            heroSaveData.gamma3Rune = RuneManager.ConvertRuneFromDictionary(((Dictionary<string, System.Object>)dicSub["gamma3Rune"]));
            
            heroList.Add(heroSaveData);
        }
        return heroList;
    }

    public Hero AdaptHeroStatsToLevel(Hero hero)
    {
        HeroStatsPerLevel1to50Array array = JsonUtility.FromJson<HeroStatsPerLevel1to50Array>(jsonFileStats.text);
        foreach (HeroStatsPerLevel1to50 value in array.heroStatsPerLevel1To50Array)
        {
            if(value.heroID == hero.heroID)
            {
                List<HeroStatsPerLevel> statsList = value.heroStatsPerLevel.OfType<HeroStatsPerLevel>().ToList();

                HeroStatsPerLevel heroStat = statsList.Find(x => x.level == hero.heroLevel);
                hero.stats.health = heroStat.health;
                hero.stats.attackDamage = heroStat.attackDamage;
                hero.stats.armor = heroStat.armor;
                hero.stats.magicResist = heroStat.magicResist;
            }
        }
        return hero;
    }

    public Stats GetHeroStatsFromsLevel(Hero hero, int level)
    {
        Stats tempStats = new Stats();

        HeroStatsPerLevel1to50Array array = JsonUtility.FromJson<HeroStatsPerLevel1to50Array>(jsonFileStats.text);
        foreach (HeroStatsPerLevel1to50 value in array.heroStatsPerLevel1To50Array)
        {
            if (value.heroID == hero.heroID)
            {
                List<HeroStatsPerLevel> statsList = value.heroStatsPerLevel.OfType<HeroStatsPerLevel>().ToList();

                HeroStatsPerLevel heroStat = statsList.Find(x => x.level == level);
                tempStats.health = heroStat.health;
                tempStats.attackDamage = heroStat.attackDamage;
                tempStats.armor = heroStat.armor;
                tempStats.magicResist = heroStat.magicResist;
            }
        }
        return tempStats;
    }

    public bool IsHeroLevelUp(Hero hero)
    {
        HeroExpRequirements array = JsonUtility.FromJson<HeroExpRequirements>(jsonFileExp.text);
        List<HeroExpRequirement> expList = array.heroExpRequirements.OfType<HeroExpRequirement>().ToList();
        HeroExpRequirement requirement = expList.Find(x => x.level == hero.heroLevel);

        if(hero.heroExp >= requirement.requiredExp)
        {
            return true;
        }
        
        return false;
    }

    public int GetRequiredExp(int level)
    {
        HeroExpRequirements array = JsonUtility.FromJson<HeroExpRequirements>(jsonFileExp.text);
        List<HeroExpRequirement> expList = array.heroExpRequirements.OfType<HeroExpRequirement>().ToList();
        return 200;
        //return expList.Find(x => x.level == (level+1)).requiredExp;
    }


    public Hero CreateHeroFromID(string id)
    {
        Hero hero = ScriptableObject.CreateInstance<Hero>();

        Hero heroCopy = GetHeroGOFromHeroID(id);

        // copy metadata
        hero.prefab = heroCopy.prefab;
        hero.attackProjectile = heroCopy.attackProjectile;
        hero.abilityOne = heroCopy.abilityOne;
        hero.abilityTwo = heroCopy.abilityTwo;
        hero.uiname = heroCopy.uiname;
        hero.heroID = heroCopy.heroID;

        // copy stats
        hero.stats = new Stats();
        hero.stats.attackDamage = heroCopy.stats.attackDamage;
        hero.stats.elementalBonusDamage = heroCopy.stats.elementalBonusDamage;
        hero.stats.critChance = heroCopy.stats.critChance;
        hero.stats.critDamage = heroCopy.stats.critDamage;
        hero.stats.speed = heroCopy.stats.speed;

        hero.stats.attackRange = heroCopy.stats.attackRange;

        hero.stats.health = heroCopy.stats.health;
        hero.stats.armor = heroCopy.stats.armor;
        hero.stats.magicResist = heroCopy.stats.magicResist;

        hero.stats.effectivity = heroCopy.stats.effectivity;
        hero.stats.tenacity = heroCopy.stats.tenacity;
        return hero;
    }
}
