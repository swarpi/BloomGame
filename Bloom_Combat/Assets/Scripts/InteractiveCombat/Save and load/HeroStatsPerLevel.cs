using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class HeroStatsPerLevel
{
    public int level;
    public float health;
    public float armor;
    public float magicResist;
    public float attackDamage;

    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
}
