using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class HeroStatsPerLevel1to50
{
    public string heroID;
    public HeroStatsPerLevel[] heroStatsPerLevel;
    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }

    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
}
[System.Serializable]
public class HeroStatsPerLevel1to50Array
{
    public HeroStatsPerLevel1to50[] heroStatsPerLevel1To50Array;
}
