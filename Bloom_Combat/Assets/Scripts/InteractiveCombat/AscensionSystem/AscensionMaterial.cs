using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ZoodiacSign
{
    Blaze,
    Torrent,
    Blossom,
    Tempest,
    Quake,
    Radiance,
    Umbra
}
public enum AscensionJob
{
    Fishing,
    Gardening,
    Farming,
    Mining,
    Hunting,
    Breeding,
    Lumberjacking
}
[CreateAssetMenu(fileName = "DefaultAscensionMaterial", menuName = "Ascension Material", order = 1)]
public class AscensionMaterial : ScriptableObject
{
    public string ascensionName;
    public AscensionJob ascensionJob;
    public int ascensionStage;
}
