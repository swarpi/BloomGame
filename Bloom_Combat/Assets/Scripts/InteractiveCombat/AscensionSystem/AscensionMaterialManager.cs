using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AscensionMaterialManager : MonoBehaviour
{
    private static AscensionMaterialManager _instance;
    public static AscensionMaterialManager Instance { get { return _instance; } }
    public List<AscensionMaterial> allMaterials;
    public TextAsset jsonFileStats;
    public TextAsset jsonFileExp;

    Dictionary<ZoodiacSign, Dictionary<AscensionJob,int>> zoodiacSignMaterialRequirements = new Dictionary<ZoodiacSign, Dictionary<AscensionJob, int>>()
    {
        {ZoodiacSign.Blaze, new Dictionary<AscensionJob,int>()
        {
            {AscensionJob.Mining,6 },
            {AscensionJob.Lumberjacking,4 },
            {AscensionJob.Hunting,2 },
        }      
        },
        {ZoodiacSign.Torrent, new Dictionary<AscensionJob,int>()
        {
            {AscensionJob.Fishing,6 },
            {AscensionJob.Breeding,4 },
            {AscensionJob.Farming,2 },
        }
        },
        {ZoodiacSign.Blossom, new Dictionary<AscensionJob,int>()
        {
            {AscensionJob.Farming,6 },
            {AscensionJob.Gardening,4 },
            {AscensionJob.Lumberjacking,2 },
        }
        },
        {ZoodiacSign.Tempest, new Dictionary<AscensionJob,int>()
        {
            {AscensionJob.Fishing,6 },
            {AscensionJob.Hunting,4 },
            {AscensionJob.Breeding,2 },
        }
        },
        {ZoodiacSign.Quake, new Dictionary<AscensionJob,int>()
        {
            {AscensionJob.Mining,6 },
            {AscensionJob.Farming,4 },
            {AscensionJob.Gardening,2 },
        }
        },
        {ZoodiacSign.Radiance, new Dictionary<AscensionJob,int>()
        {
            {AscensionJob.Gardening,6 },
            {AscensionJob.Breeding,4 },
            {AscensionJob.Lumberjacking,2 },
        }
        },
        {ZoodiacSign.Umbra, new Dictionary<AscensionJob,int>()
        {
            {AscensionJob.Fishing,6 },
            {AscensionJob.Mining,4 },
            {AscensionJob.Hunting,2 },
        }
        }
    };

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public ZoodiacSign sign;
    public int grade;

    public Dictionary<AscensionJob, int> GetAscensionJobFromZoodiacSign(ZoodiacSign zoodiacSign)
    {
        return zoodiacSignMaterialRequirements[zoodiacSign];
    }
    public List<AscensionMaterial> materialList;
    public Dictionary<AscensionMaterial, int> GetAscensionMaterial(ZoodiacSign zoodiacSign, int stage)
    {
        Dictionary<AscensionMaterial, int> materialDic = new Dictionary<AscensionMaterial, int>();
        Dictionary<AscensionJob, int> jobDic = GetAscensionJobFromZoodiacSign(zoodiacSign);

        materialList = allMaterials.FindAll(x => jobDic.ContainsKey(x.ascensionJob) && x.ascensionStage == stage);

        materialDic.Add(materialList[0], jobDic[materialList[0].ascensionJob]);
        materialDic.Add(materialList[1], jobDic[materialList[1].ascensionJob]);
        materialDic.Add(materialList[2], jobDic[materialList[2].ascensionJob]);
        return materialDic;
    }


    public void Tester()
    {
        Dictionary<AscensionMaterial, int> testDic = GetAscensionMaterial(sign, grade);

        foreach (KeyValuePair<AscensionMaterial, int> kvp in testDic)
        {
            //textBox3.Text += ("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
            Debug.Log(string.Format("Key = {0}, Value = {1}", kvp.Key, kvp.Value));
        }
    }
}
