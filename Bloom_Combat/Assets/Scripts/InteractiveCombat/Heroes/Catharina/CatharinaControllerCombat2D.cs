using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatharinaControllerCombat2D : HeroControllerCombat2D
{
    public override bool DoAbilityOne(int cardLevel) 
    {
        if (base.DoAbilityOne(cardLevel))
        {
            float spelldmg = 1; //ToDO DmgValues + Debuff Values
            switch (cardLevel)
            {
                case 1:
                    //Piercing Thrust(I) -> Charge attack, attack target 
                    if (heroController.target != null)
                    {
                        //Animation
                        StartCoroutine(DamageTarget(heroController.target, spelldmg, false, true, 0.5f));  //delay wegen Charge 
                        Debug.Log("Catharina uses Piercing Thrust(I)");
                    }
                    else
                    {
                        Debug.Log("There is no target :(");
                    }
                    break;

                case 2:
                    //Piercing Thrust(II) -> Charge attack, attack target + armor/mr reduction
                    if (heroController.target != null)
                    {
                        HeroControllerCombat2D enemy = heroController.target.GetComponent<HeroControllerCombat2D>(); 
                        RefFloat[] toDebuff = new RefFloat[2] {enemy.currentArmor, enemy.currentMagicResistance};
                        float[] values = new float[2] {30f, 30f };
                        enemy.effectList.AddDebuff("pierced", 3.0f, Effect_type.debuff, toDebuff, values);
                        StartCoroutine(DamageTarget(heroController.target, spelldmg, false, true, 0.5f));   //delay wegen Charge 
                        Debug.Log("Catharina uses Piercing Thrust(II)");
                    }
                    else
                    {
                        Debug.Log("There is no target :(");
                    }
                    break;

                case 3:
                    //Piercing Thrust(III) -> Charge attack all enemies in a line, attacks them + armor / mr reduction
                    //Animation
                    //delay?
                    Debug.Log("Catharina uses Piercing Thrust(III)");
                    foreach (GameObject enemy in CombatController.Instance.combatField.aiHeroList)
                    {
                        HeroControllerCombat2D enemyHCC2D = enemy.GetComponent<HeroControllerCombat2D>();
                        RefFloat[] toDebuff = new RefFloat[2];
                        float[] values = new float[2] { 30f, 30f };
                        if (enemy.GetComponent<HeroController2D>().currentTilePos.position.getY() == heroController.currentTilePos.position.getY())
                        {
                            toDebuff[0] = enemyHCC2D.currentArmor;
                            toDebuff[1] = enemyHCC2D.currentMagicResistance;
                            enemyHCC2D.effectList.AddDebuff("pierced", 3.0f, Effect_type.debuff, toDebuff, values);
                            StartCoroutine(DamageTarget(enemy, spelldmg));
                        }
                    }
                    break;

                default:
                    Debug.Log("MISTAKE!");
                    break;
            }
        }
        return true;
    }

    public override bool DoAbilityTwo(int cardLevel)
    {
        if (base.DoAbilityTwo(cardLevel))
        {
            switch (evoState)
            {
                case EvolutionState.rankOne:    //Humilation
                    float targetHp = 0;
                    HeroControllerCombat2D target = null;
                    RefFloat[] toDebuff = new RefFloat[1];
                    float[] values = new float[1];
                    switch (cardLevel)
                    {
                        case 1:
                            //lowest flat HP enemy -> debuff AD
                            targetHp = 0;
                            target = null;
                            foreach (GameObject enemy in CombatController.Instance.combatField.aiHeroList)
                            {
                                HeroControllerCombat2D enemyHCC2D = enemy.GetComponent<HeroControllerCombat2D>();
                                if (enemyHCC2D.currentHealth.f < targetHp)
                                {
                                    target = enemyHCC2D;
                                }

                            }

                            toDebuff[0] = target.autoAttackDamage ;
                            values[0] = 30f;
                            target.effectList.AddDebuff("humilated", 3.0f, Effect_type.debuff, toDebuff, values);
                                
                            break;

                        case 2:
                            //lowest flat HP enemy -> debuff AD + Humilation(not targetable for his allies)
                            targetHp = 0;
                            target = null;
                            foreach (GameObject enemy in CombatController.Instance.combatField.aiHeroList)
                            {
                                HeroControllerCombat2D enemyHCC2D = enemy.GetComponent<HeroControllerCombat2D>();
                                if (enemyHCC2D.currentHealth.f < targetHp)
                                {
                                    target = enemyHCC2D;
                                }

                            }

                            toDebuff[0] = target.autoAttackDamage;
                            values[0] = 30f;
                            target.effectList.AddDebuff("humilated", 3.0f, Effect_type.debuff, toDebuff, values);

                            // add Humiliation Effect

                            break;

                        case 3:
                            //all enemies -> debuff AD + Humilation(not targetable for his allies)
                            foreach (GameObject enemy in CombatController.Instance.combatField.aiHeroList)
                            {
                                HeroControllerCombat2D enemyHCC2D = enemy.GetComponent<HeroControllerCombat2D>();
                                toDebuff[0] = enemyHCC2D.autoAttackDamage;
                                values[0] = 30f;
                                enemyHCC2D.effectList.AddDebuff("humilated", 3.0f, Effect_type.debuff, toDebuff, values);
                                
                                // add Humiliation Effect
                            }

                            break;

                        default:
                            Debug.Log("Something went wrong! Impossible CardLevel!");
                            break;
                    }
                    
                    break;

                case EvolutionState.rankTwo:    //Riposte
                    
                    switch (cardLevel)
                    {
                        case 1:
                            //parry stance, can�t Atk -> Ripostes next AutoAtk and marks hit enemy
                            break;

                        case 2:
                            //parry stance, can�t Atk -> Ripostes next AutoAtk and marks all enemies vertically
                            break;

                        case 3:
                            //parry stance, can�t Atk -> Ripostes next AutoAtk and marks hit all enemies vertically and horitonally
                            break;

                        default:
                            Debug.Log("Something went wrong! Impossible CardLevel!");
                            break;
                    }
                    break;

                case EvolutionState.rankThree:  //Lightning Call
                    switch (cardLevel)
                    {
                        case 1:
                            //holds weapon into the sky and shoots lightning bolt to lowest flat hp target, marks enemy, small stun 0,25s
                            break;

                        case 2:
                            //holds weapon into the sky and shoots lightning bolt to lowest flat hp target + closest enemy to the target, marks enemy, small stun 0,25s
                            break;

                        case 3:
                            //holds weapon into the sky and shoots lightning bolt to lowest flat hp target + jumps to all targets, marks enemy, small stun 0,25s
                            break;

                        default:
                            Debug.Log("Something went wrong! Impossible CardLevel!");
                            break;
                    }
                    break;

                default:
                    Debug.Log("Something went wrong! Impossible Evolution State");
                    break;

            }
            
        }
        return true;
    }

    public override bool DoEvolutionOne()
    {
        if (base.DoEvolutionOne())
        {

        }
        return true;
    }

    public override bool DoEvolutionTwo()
    {
        if (base.DoEvolutionTwo())
        {

        }
        return true;
    }

    public override bool DoUltimate()
    {
        if (base.DoUltimate())
        {
            switch (evoState)
            {
                case EvolutionState.rankOne:
                    //transform -> get passive AutoAtk deal onhit magic dmg
                    break;

                case EvolutionState.rankTwo:
                    //All marked enemies get hit by a thunder strike from the sky
                    break;

                case EvolutionState.rankThree:
                    //Fiora ults all enemies + marked enemies get hit by thunder strike
                    break;

                default:
                    Debug.Log("Something went wrong! Impossible Evolutuion State");
                    break;

            }
        }
        return true;
    }
    // Start is called before the first frame update
    //void Start()
    //{ }

    // Update is called once per frame
    //void Update()
    //{}
}
