using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDisappearBehaviour : StateMachineBehaviour
{
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Debug.Log("attack anim finished");

        Destroy(animator.gameObject);
    }
}
