using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AbilityCardSpawner : MonoBehaviour
{

    int cardsOnfield = 0;

    float timer = 0;
    private bool dragging = false;
    public GameObject abilityCardContainer;
    public GameObject realAbilityCard;
    public GameObject abilityWindow;
    public Transform realstartPosition;

    public List<HeroController2D> heroList;

    bool isInBattle = false;

    // timers & restrictions
    float spawnTimer = 0;
    float spawnTimerCooldown = 2f;
    float maxCardAmount = 6f;
    float abilitiesOnField = 0;

    public void StartCombat(List<HeroController2D> _heroList)
    {
        heroList = _heroList;
        isInBattle = true;
        CombatController.Instance.onHeroDeath += RemoveHero;
    }
    // TODO : speed modified based on the amount of heros or speed independed for each hero
    public void NewCardSpawner()
    {
        // create a new container and set it to the right child position in the window
        // container has the drag and drop logic
        GameObject container = Instantiate(abilityCardContainer);
        container.transform.SetParent(abilityWindow.transform);
        container.transform.SetSiblingIndex(0);
        // then create the real card with all the info and pictures
        // ability card follows the container to allow animations
        GameObject abilityCard = Instantiate(realAbilityCard, realstartPosition);
        AbilityScriptable tempAbility = GetAbilityFromRandomHero();
        abilityCard.GetComponent<AbilityCardContainer>().Init(tempAbility, container);

        //abilityCard.GetComponent<Image>().color = new Color(Random.Range(0f, 1f),Random.Range(0f, 1f),Random.Range(0f, 1f));
    }

    AbilityScriptable GetAbilityFromRandomHero()
    {
        int randomHero = Random.Range(0, heroList.Count);
        int randomAbility = Random.Range(1, 3);
        Hero tempHero = heroList[randomHero].hero;
        if(randomAbility == 1)
        {
            return tempHero.abilityOne;
        }
        else
        {
            return tempHero.abilityTwo;
        }
    }
    // Update is called once per frame
    void Update()
    {
        //no heros in our list
        if (heroList.Count <= 0) return;
        if(Time.time >= spawnTimer && abilitiesOnField < maxCardAmount && isInBattle)
        {
            spawnTimer = Time.time + spawnTimerCooldown;
            if (abilityWindow.transform.childCount < maxCardAmount)
            {
                NewCardSpawner();
            }
        }

    }
    private void LateUpdate()
    {
        CheckForUpgrades(); // maybe put a bigger delay for the checking

    }
    void RemoveHero(GameObject hero)
    {
        HeroController2D heroController = hero.GetComponent<HeroController2D>();
        if (heroList.Contains(heroController))
        {
            foreach(Transform card in realstartPosition.transform)
            {
                if(card.gameObject.GetComponent<AbilityCardContainer>().ability.hero == heroController.heroName)
                {
                    int positionIndex = card.GetSiblingIndex();
                    // need to find the index based on the spawner
                    // then use this index to find the gameobject from the abilitywindow
                    // because the abilitywindow spawns the gameobject turned around
                    // get the amount of childs substract the position and then substract one to get the corresponding gameobject
                    // destroying this with the button will automatically destroy the abilitcardcontainer
                    Destroy(this.gameObject.transform.GetChild(transform.childCount - positionIndex -1).gameObject);
                }
            }
            // remove the hero from the list so it wont spawn cards from them
            heroList.Remove(heroController);
            // destroy all cards of that hero
        }
    }
    public void ClickedOnIcon(GameObject go)
    {
        //Destroy(go);
    }
    public void StopDrag(GameObject go)
    {
        //go.GetComponent<AbilityCardContainer>().isDragged = false;
    }
    // need to check if auto combined to level 2 but immediatly need to combine to 3 again
    public void CheckForUpgrades()
    {
        List<DraggableUI> allAbilities = abilityWindow.GetComponentsInChildren<DraggableUI>().ToList<DraggableUI>();
        allAbilities.Reverse();

        for(int i = 0; i< allAbilities.Count - 1; i++)
        {
            AbilityCardContainer myAbility = allAbilities[i].abilityCardContainer;
            AbilityCardContainer nextAbility = allAbilities[i+1].abilityCardContainer;
            if (myAbility.abilityName == nextAbility.abilityName && myAbility.cardLevel == nextAbility.cardLevel)
            {
                myAbility.cardLevel++;
                nextAbility.cardLevel = -1;
                StartCoroutine(CombineCardsAnimation(allAbilities[i], allAbilities[i+1]));
            }
        }
    }


    IEnumerator CombineCardsAnimation(DraggableUI card1, DraggableUI card2)
    {
        yield return new WaitForSeconds(0.2f);
        card2.abilityCardContainer.cardContainer = card1.abilityCardContainer.cardContainer;
        yield return new WaitForSeconds(0.35f);
        try
        {
            Destroy(card2.gameObject);
            if (card1.abilityCardContainer.cardLevel == 2) card1.abilityCardContainer.borderOne.gameObject.SetActive(true);
            if (card1.abilityCardContainer.cardLevel == 3) card1.abilityCardContainer.borderTwo.gameObject.SetActive(true);
        }
        catch(MissingReferenceException e)
        {
            Debug.Log("cant destroy the card to combine, maybe it was already destroyed");
        }


        //card2.gameObject.transform.position = Vector3.Lerp(transform.position, card1.cardContainer.transform.position, 5 * Time.deltaTime);
    }
}
