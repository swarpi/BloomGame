using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaceHolderAbility : MonoBehaviour
{
    public Image image;
    // Start is called before the first frame update
    public float alphaValue = 0.5f;
    private void Start()
    {

    }
    public void Init(Sprite sprite)
    {
        image = this.GetComponent<Image>();
        Color tempColor = image.color;
        tempColor.a = alphaValue;
        //Debug.Log(sprite.name);
        image.sprite = sprite;
        Debug.Log(image.sprite.name);
        //image.color = tempColor;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
