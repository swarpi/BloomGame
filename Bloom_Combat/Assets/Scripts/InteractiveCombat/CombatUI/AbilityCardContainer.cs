using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityCardContainer : MonoBehaviour
{
    // Start is called before the first frame update
    float speed = 10f;
    public GameObject cardContainer;
    public AbilityScriptable ability;
    public bool isFollowing = true;
    public string abilityName;
    public int cardLevel = 1;
    public Image mainImage;
    public Image borderOne;
    public Image borderTwo;
    public void Init(AbilityScriptable _ability, GameObject _cardContainer)
    {
        ability = _ability;
        mainImage.sprite = ability.icon;
        abilityName = ability.abilityName;
        cardContainer = _cardContainer;
        cardContainer.GetComponent<DraggableUI>().abilityCardContainer = this;
    }
    // Update is called once per frame
    void Update()
    {
        if (isFollowing)
        {
            try
            {
                transform.position = Vector3.Lerp(transform.position, cardContainer.transform.position, speed * Time.deltaTime);
            }
            catch(MissingReferenceException e)
            {
                Debug.Log("are you missing a card reference, or maybe it doenst exist anymore");
            }
        }
    }
}
