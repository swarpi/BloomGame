using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DraggableUI : MonoBehaviour,IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField] private RectTransform dragRectTransform;
    Transform parentToReturnTo = null;
    [SerializeField]
    GameObject placeHolderPrefab;
    GameObject placeHolder = null;

    public bool overDropZone = false;
    int startIndex;

    public Image image;

    public AbilityCardContainer abilityCardContainer;
    AbilityCardSpawner abilityCardSpawner;
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(UseAbility);
        abilityCardSpawner = GetComponentInParent<AbilityCardSpawner>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        // create a placeholder
        placeHolder = Instantiate(placeHolderPrefab);
        // adds the ability icon to the placeholder but with a lower alpha value
        placeHolder.GetComponent<PlaceHolderAbility>().Init(abilityCardContainer.ability.icon);
        placeHolder.transform.SetParent(this.transform.parent);
        // add a layout element to the placeholder object
        LayoutElement le = placeHolder.AddComponent<LayoutElement>();
        le.preferredWidth = this.GetComponent<LayoutElement>().preferredWidth;
        le.preferredHeight = this.GetComponent<LayoutElement>().preferredHeight;
        le.flexibleWidth = 0;
        le.flexibleHeight = 0;
        // need my startindex just in case i drop it outside the zone, so it returns to it orginial place
        startIndex = this.transform.GetSiblingIndex();
        placeHolder.transform.SetSiblingIndex(this.transform.GetSiblingIndex());

        parentToReturnTo = this.transform.parent;
        this.transform.SetParent(this.transform.root);

        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        dragRectTransform.anchoredPosition += eventData.delta;

        int newSiblingIndextemp = parentToReturnTo.childCount;
        for (int i = 0; i < parentToReturnTo.childCount; i++)
        {
            if (this.transform.position.y > parentToReturnTo.GetChild(i).position.y)
            {
                newSiblingIndextemp = i;
                if (placeHolder.transform.GetSiblingIndex() < newSiblingIndextemp) newSiblingIndextemp--;
                break;
            }
        }
        placeHolder.transform.SetSiblingIndex(newSiblingIndextemp);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.transform.SetParent(parentToReturnTo);
        if (overDropZone)
        {
            this.transform.SetSiblingIndex(placeHolder.transform.GetSiblingIndex());
            // check for ability combinations
            //abilityCardSpawner.CheckForUpgrades();
        }
        else
        {
            this.transform.SetSiblingIndex(startIndex);
        }
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        Destroy(placeHolder);
    }

    public void UseAbility()
    {
        AbilityScriptable ability = abilityCardContainer.ability;
        abilityCardSpawner.heroList.Find(x => x.heroName == ability.hero).UseAbility(ability.abilityIndex, abilityCardContainer.cardLevel);
        Destroy(this.gameObject);
    }
    private void OnDestroy()
    {
        Destroy(abilityCardContainer.gameObject);
    }
}
