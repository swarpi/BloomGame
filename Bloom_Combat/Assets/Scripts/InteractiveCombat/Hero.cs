using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "DefaultHero", menuName = "Hero", order = 1)]
public class Hero : ScriptableObject
{
    public GameObject prefab;

    ///Projectile prefab to create when champion is attacking
    public GameObject attackProjectile;

    public AbilityScriptable abilityOne;

    public AbilityScriptable abilityTwo;

    ///The champion name displayed on the UI frames
    public string uiname;

    public string heroID;

    public int heroLevel;

    public int heroExp;

    public ZoodiacSign zoodiacSign;

    public HeroOrigin heroOrigin;

    public HeroOrigin heroOrigin_2;

    public Stats stats;

    public Rune alpha1Rune;
    public Rune beta1Rune;
    public Rune beta2Rune;
    public Rune gamma1Rune;
    public Rune gamma2Rune;
    public Rune gamma3Rune;

}
