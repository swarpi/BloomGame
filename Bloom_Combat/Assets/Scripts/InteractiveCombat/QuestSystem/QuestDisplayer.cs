using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestDisplayer : MonoBehaviour
{
    public GameObject questCampaignPrefab;
    public GameObject questRepeatPrefab;

    public GameObject allCampaignQuestGO;
    public GameObject allRepeatingQuestGO;

    public GameObject contentCampaignGO;
    public GameObject contentRepeatGO;



    public void Start()
    {
        DisplayAllCampaignQuests();
        QuestManager.Instance.repeatingQuests.CompleteQuest("summon", 3);
    }
    public void DisplayAllCampaignQuests()
    {
        allCampaignQuestGO.SetActive(true);
        allRepeatingQuestGO.SetActive(false);
        foreach (CampaignQuest campaignQuest in QuestManager.Instance.campaignQuests)
        {
            GameObject questGO = Instantiate(questCampaignPrefab);

            int currentProgress = 5;
            // enable complete button
            if(currentProgress >= campaignQuest.objectiveAmount)
            {
                questGO.transform.Find("collect").gameObject.SetActive(true);
                // give collect button some logic
            }

            questGO.transform.Find("questTitle").GetComponent<Text>().text = campaignQuest.questID;
            questGO.transform.Find("questDescription").GetComponent<Text>().text = campaignQuest.description;
            questGO.transform.Find("progress").Find("required").GetComponent<Text>().text = campaignQuest.objectiveAmount.ToString();
            questGO.transform.Find("progress").Find("currentProgress").GetComponent<Text>().text = currentProgress.ToString();
            questGO.transform.Find("progress").GetComponent<Image>().fillAmount = (float)currentProgress/(float)campaignQuest.objectiveAmount ;
            questGO.transform.SetParent(contentCampaignGO.transform);
        }

    }
    public void DisplayAllWeeklyQuests()
    {
        allCampaignQuestGO.SetActive(false);
        allRepeatingQuestGO.SetActive(true);
        // clear previous objects, only necessary when swapping between daily and weekly
        foreach (Transform child in contentRepeatGO.transform)
        {
            Destroy(child.gameObject);
        }

        foreach(RepeatingQuest weekly in QuestManager.Instance.repeatingQuests.allWeeklyQuests)
        {
            GameObject questGO = Instantiate(questRepeatPrefab);

            int currentProgress = weekly.clearProgress;

            questGO.transform.Find("questTitle").GetComponent<Text>().text = weekly.title;
            questGO.transform.Find("progress").Find("required").GetComponent<Text>().text = weekly.getObjectiveAmount().ToString();
            questGO.transform.Find("progress").Find("currentProgress").GetComponent<Text>().text = currentProgress.ToString();
            questGO.transform.Find("progress").GetComponent<Image>().fillAmount = (float)currentProgress/(float)weekly.getObjectiveAmount();

            questGO.transform.Find("clearPoints").GetComponent<Text>().text = weekly.clearPoints.ToString();
            questGO.transform.SetParent(contentRepeatGO.transform);

            if (weekly.cleared)
            {
                questGO.transform.Find("cleared").gameObject.SetActive(true);
            }

        }
        DisplayWeeklyProgressBar();
    }
    public void DisplayAllDailyQuests()
    {
        allCampaignQuestGO.SetActive(false);
        allRepeatingQuestGO.SetActive(true);
        // clear previous objects, only necessary when swapping between daily and weekly
        foreach (Transform child in contentRepeatGO.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (RepeatingQuest daily in QuestManager.Instance.repeatingQuests.allDailyQuests)
        {
            GameObject questGO = Instantiate(questRepeatPrefab);

            int currentProgress = daily.clearProgress;

            questGO.transform.Find("questTitle").GetComponent<Text>().text = daily.title;
            questGO.transform.Find("progress").Find("required").GetComponent<Text>().text = daily.getObjectiveAmount().ToString();
            questGO.transform.Find("progress").Find("currentProgress").GetComponent<Text>().text = currentProgress.ToString();
            questGO.transform.Find("progress").GetComponent<Image>().fillAmount = (float)currentProgress / (float)daily.getObjectiveAmount();

            questGO.transform.Find("clearPoints").GetComponent<Text>().text = daily.clearPoints.ToString();
            questGO.transform.SetParent(contentRepeatGO.transform);

            if (daily.cleared)
            {
                questGO.transform.Find("cleared").gameObject.SetActive(true);
            }

        }
        DisplayDailyProgressBar();
    }

    void DisplayWeeklyProgressBar()
    {
        Transform progressGO = allRepeatingQuestGO.transform.Find("QuestProgress");
        float quotient = (float)QuestManager.Instance.repeatingQuests.weeklyQuestPoints/100f;

        progressGO.Find("bar").GetComponent<Image>().fillAmount = quotient;

        Transform mileStoneButtons = progressGO.Find("collectButtons");
        // reset all buttons
        foreach(Transform button in mileStoneButtons.transform)
        {
            button.GetComponent<Image>().color = Color.gray;
            button.GetComponent<Button>().onClick.RemoveAllListeners();
        }
        if (quotient >= 0.2)
        {
            Transform twenty = mileStoneButtons.Find("20");
            twenty.GetComponent<Button>().onClick.AddListener(delegate { Debug.Log("20 + weekly"); });
            twenty.GetComponent<Image>().color = Color.white;
        }
        if (quotient >= 0.4)
        {
            Transform forty = mileStoneButtons.Find("40");
            forty.GetComponent<Button>().onClick.AddListener(delegate { Debug.Log("40 + weekly"); });
            forty.GetComponent<Image>().color = Color.white;
        }
        if (quotient >= 0.6)
        {
            Transform sixty = mileStoneButtons.Find("60");
            sixty.GetComponent<Button>().onClick.AddListener(delegate { Debug.Log("60 + weekly"); });
            sixty.GetComponent<Image>().color = Color.white;
        }
        if (quotient >= 0.8)
        {
            Transform eighty = mileStoneButtons.Find("80");
            eighty.GetComponent<Button>().onClick.AddListener(delegate { Debug.Log("80 + weekly"); });
            eighty.GetComponent<Image>().color = Color.white;
        }
        if (quotient >= 1)
        {
            Transform hundred = mileStoneButtons.Find("100");
            hundred.GetComponent<Button>().onClick.AddListener(delegate { Debug.Log("100 + weekly"); });
            hundred.GetComponent<Image>().color = Color.white;
        }

    }
    // TODO maybe refactor daily and weekly mtehods together
    void DisplayDailyProgressBar()
    {
        Transform progressGO = allRepeatingQuestGO.transform.Find("QuestProgress");
        float quotient = (float)QuestManager.Instance.repeatingQuests.dailyQuestPoints / 100f;

        progressGO.Find("bar").GetComponent<Image>().fillAmount = quotient;

        Transform mileStoneButtons = progressGO.Find("collectButtons");
        // reset all buttons
        foreach (Transform button in mileStoneButtons.transform)
        {
            button.GetComponent<Image>().color = Color.gray;
            button.GetComponent<Button>().onClick.RemoveAllListeners();
        }
        if (quotient >= 0.2)
        {
            Transform twenty = mileStoneButtons.Find("20");
            twenty.GetComponent<Button>().onClick.AddListener(delegate { Debug.Log("20 + daily"); });
            twenty.GetComponent<Image>().color = Color.white;
        }
        if (quotient >= 0.4)
        {
            Transform forty = mileStoneButtons.Find("40");
            forty.GetComponent<Button>().onClick.AddListener(delegate { Debug.Log("40 + daily"); });
            forty.GetComponent<Image>().color = Color.white;
        }
        if (quotient >= 0.6)
        {
            Transform sixty = mileStoneButtons.Find("60");
            sixty.GetComponent<Button>().onClick.AddListener(delegate { Debug.Log("60 + daily"); });
            sixty.GetComponent<Image>().color = Color.white;
        }
        if (quotient >= 0.8)
        {
            Transform eighty = mileStoneButtons.Find("80");
            eighty.GetComponent<Button>().onClick.AddListener(delegate { Debug.Log("80 + daily"); });
            eighty.GetComponent<Image>().color = Color.white;
        }
        if (quotient >= 1)
        {
            Transform hundred = mileStoneButtons.Find("100");
            hundred.GetComponent<Button>().onClick.AddListener(delegate { Debug.Log("100 + daily"); });
            hundred.GetComponent<Image>().color = Color.white;
        }

    }
    // questtype should be same name like player statistic key
    public bool CheckIfQuestFinished(string questID)
    {

        return true;
    }

}
