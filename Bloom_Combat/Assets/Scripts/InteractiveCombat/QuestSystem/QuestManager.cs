using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    private static QuestManager _instance;
    public static QuestManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        jsonFileQuest = Resources.Load("CampaignQuests") as TextAsset;
        LoadCampaignQuest();
        LoadDailyAndWeeklyQuests();
    }
    public TextAsset jsonFileQuest;
    public DailyAndWeeklyQuests repeatingQuests = new DailyAndWeeklyQuests();
    public CampaignQuest[] campaignQuests;


    public void LoadCampaignQuest()
    {
        campaignQuests = JsonUtility.FromJson<CampaignQuests>(jsonFileQuest.text).campaignQuests;
    }
    public void LoadDailyAndWeeklyQuests()
    {
        repeatingQuests.initDailyQuests();
        repeatingQuests.initWeeklyQuests();
    }

    public void CompleteCampaignQuest(string questID)
    {
        // call playfabcontroller to check if quest is done
        // give player rewards
    }
    public void PrintCampaignQuest()
    {
        
        foreach(CampaignQuest quest in campaignQuests)
        {
            Debug.Log(quest.questType);
            Debug.Log(quest.objectiveAmount);
            Debug.Log(quest.rewards[1].id + quest.rewards[1].amount);
        }
    }
}
