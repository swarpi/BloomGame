using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class RepeatingQuest
{
	public string id;
	public string title;
	public bool cleared;
	private int objectiveAmount;
	public int clearPoints;
	public int clearProgress;
	public RepeatingQuest(string id,string title, int clearPoints)
	{
		this.id = id;
		this.title = title;
		cleared = false;
		objectiveAmount = 1;
		this.clearPoints = clearPoints;
		clearProgress = 0;

	}

	public RepeatingQuest(string id,string title, int clearPoints, int objectiveAmount)
	{
		this.id = id;
		this.title = title;
		cleared = false;
		this.objectiveAmount = objectiveAmount;
		this.clearPoints = clearPoints;
		clearProgress = 0;
	}

	public int getObjectiveAmount()
	{
		return objectiveAmount;
	}
}
public class DailyAndWeeklyQuests
{
	public List<RepeatingQuest> allDailyQuests;
	public int dailyQuestPoints;

	public List<RepeatingQuest> allWeeklyQuests;
	public int weeklyQuestPoints;
	// list<bool> allDailyQuests;

	// methods to change them

	public List<RepeatingQuest> initDailyQuests()
	{
		dailyQuestPoints = 0;

		allDailyQuests = new List<RepeatingQuest>();

		allDailyQuests.Add(new RepeatingQuest("enterStage", "Enter a Stage", 10));
		allDailyQuests.Add(new RepeatingQuest("completeArena","Complete Arena battle", 10));
		allDailyQuests.Add(new RepeatingQuest("finishExpidition", "Finish an Expidition", 20));
		allDailyQuests.Add(new RepeatingQuest("buildingSystem","Build something or play a job", 10));
		allDailyQuests.Add(new RepeatingQuest("friendInteraction","Interact with a friend", 10));
		allDailyQuests.Add(new RepeatingQuest("levelUpHero","levelUpHero", 10));
		allDailyQuests.Add(new RepeatingQuest("levelUpRune","levelUpRune", 10));

		return allDailyQuests;
	}

	public List<RepeatingQuest> initWeeklyQuests()
	{
		weeklyQuestPoints = 0;

		allWeeklyQuests = new List<RepeatingQuest>();

		allWeeklyQuests.Add(new RepeatingQuest("buyFromShop","Buy 3 things from the shop", 10,3));
		allWeeklyQuests.Add(new RepeatingQuest("summon","Summon 3 Units", 20,3));
		allWeeklyQuests.Add(new RepeatingQuest("finishExpidition","Finish 10 expiditions", 20,10));
		allWeeklyQuests.Add(new RepeatingQuest("completeArena","Complete 10 arena battles", 10,10));
		allWeeklyQuests.Add(new RepeatingQuest("acquireRune","Acquire 5 Runes or Hero Exp Potions", 15,5));
		allWeeklyQuests.Add(new RepeatingQuest("pveMode","Clear PvE mode Once/ Defeat 10 Enemies in PvE Mode", 15,10));
		allWeeklyQuests.Add(new RepeatingQuest("guildMode","Participate in Guild Mode", 10,1));

		return allWeeklyQuests;
	}


	public void CompleteQuest(string questID,int amount = 1)
	{
		// find quest in dailies
		RepeatingQuest daily = allDailyQuests.Find(x => x.id == questID);
		if (daily != null)
		{
			if(!daily.cleared)
			{
				daily.clearProgress += amount;
				daily.cleared = true;
				dailyQuestPoints += daily.clearPoints;
			}
		}

		// find quest in weeklies
		RepeatingQuest weekly = allWeeklyQuests.Find(x => x.id == questID);
		if (weekly != null)
		{
			weekly.clearProgress += amount;
			IsWeeklyQuestComplete(weekly);
		}
	}

	public bool IsWeeklyQuestComplete(RepeatingQuest quest)
	{
		if(quest.clearProgress >= quest.getObjectiveAmount())
		{
			quest.cleared = true;
			weeklyQuestPoints += quest.clearPoints;
		}
		return quest.cleared;
	}

/*	public event Action onQuestCompleted;
	public void QuestCompleted()
	{
		if (onQuestCompleted != null)
		{
			onQuestCompleted();
		}
	}*/
}
