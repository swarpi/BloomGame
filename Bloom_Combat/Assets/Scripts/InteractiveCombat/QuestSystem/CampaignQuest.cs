using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class QuestReward
{
    public string id;
    public int amount;
    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
}
[System.Serializable]
public class CampaignQuest
{
    public string questType;
    public string questID;
    public int objectiveAmount;
    public string description;
    public QuestReward[] rewards;
    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
}

[System.Serializable]
public class CampaignQuests
{
    public CampaignQuest[] campaignQuests;
    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
}