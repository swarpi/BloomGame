using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatistics : MonoBehaviour
{
    public Dictionary<string, int> playerStatistics = new Dictionary<string, int>();


    public void LoadPlayerStats()
    {

    }


    public void UpdatePlayerStats(string statistic, int newValue)
    {
        playerStatistics[statistic] = newValue;
    }
}
