using UnityEngine;

public class UltimateBehaviour : StateMachineBehaviour
{
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Debug.Log("attack anim finished");

        animator.gameObject.transform.parent.GetComponent<ChampionAnimation>().OnUltimateFinished();
    }
}
