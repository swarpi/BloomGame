﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls champion animations
/// </summary>
public class ChampionAnimation : MonoBehaviour
{

    private GameObject characterModel;
    public Animator animator;
    private HeroController heroController;
    private HeroControllerCombat heroControllerCombat;

    private Vector3 lastFramePosition;
    /// Start is called before the first frame update
    void Awake()
    {
        //get character model
        characterModel = this.transform.Find("character").gameObject;

        //get animator
        animator = characterModel.GetComponent<Animator>();
        heroController = this.transform.GetComponent<HeroController>();
        heroControllerCombat = this.transform.GetComponent<HeroControllerCombat>();
    }

    /// Update is called once per frame
    void Update()
    {
        //calculate speed
        float movementSpeed = (this.transform.position - lastFramePosition).magnitude / Time.deltaTime;

        //set movement speed on animator controller
        animator.SetFloat("movementSpeed", movementSpeed);

        //store last frame position
        lastFramePosition = this.transform.position;
    }
    int attackOrder = 0;
    /// <summary>
    /// tells animation to attack or stop attacking
    /// </summary>
    /// <param name="b"></param>
    public void DoAttack(bool b, bool isCrit = false)
    {
        //animator.play
        animator.SetBool("isAttacking", b);
        // swap between attack animation, max should be 2
        animator.SetInteger("attackOrder", attackOrder%2);
        if (isCrit)
            animator.SetBool("isCrit", isCrit);
    }

    /// <summary>
    /// Called when attack animation finished
    /// </summary>
    public void OnAttackAnimationFinished()
    {
        animator.SetBool("isAttacking", false);
        animator.SetBool("isCrit", false);
        attackOrder++;
        heroControllerCombat.OnAttackAnimationFinished();

        //Debug.Log("OnAttackAnimationFinished");

    }

    public void DoAbilityOne(bool b)
    {
        animator.SetBool("isAbilityOne", b);
    }
    public void OnAbilityOneFinished()
    {
        animator.SetBool("isAbilityOne", false);

        heroControllerCombat.OnAbilityOneFinished();

        //Debug.Log("OnAttackAnimationFinished");

    }
    public void DoAbilityTwo(bool b)
    {
        animator.SetBool("isAbilityTwo", b);
    }
    public void OnAbilityTwoFinished()
    {
        animator.SetBool("isAbilityTwo", false);

        heroControllerCombat.OnAbilityTwoFinished();

        //Debug.Log("OnAttackAnimationFinished");

    }
    public void DoUltimate(bool b)
    {
        animator.SetBool("isUltimate", b);
    }
    public void OnUltimateFinished()
    {
        animator.SetBool("isUltimate", false);

        heroControllerCombat.OnUltimateFinished();

        //Debug.Log("OnAttackAnimationFinished");

    }

    public void DoDeath()
    {
        animator.SetBool("isDead", true);
    }

    public void OnDeathFinished()
    {
        animator.SetBool("isDead", false);

        heroController.OnDeath();
    }
    /// <summary>
    /// sets animation state
    /// </summary>
    /// <returns></returns>
    public void IsAnimated(bool b)
    {
        animator.enabled = b;
    }
}
