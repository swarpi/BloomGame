using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;


// main focus game state of combat
public class AutoCombatController : MonoBehaviour
{
    public MapGenerator mapGenerator;
    public InputController inputController;
    public AIopponent aIopponent;
    public UIController uiController;
    public static GameStage currentGameStage;
    public static float combatTimer = 0;
    public static int combatTimerSeconds = 0;
    public int CombatStageDuration = 60;

    public bool isAutoMode = true;

    [HideInInspector]
    public GameObject[] oponentChampionInventoryArray;
    [HideInInspector]
    public GameObject[,] gridHeroArrayAtStartOfCombat;



    [HideInInspector]
    public int currentChampionLimit = 4;
    // Start is called before the first frame update
    void Start()
    {
        mapGenerator = GetComponent<MapGenerator>();
        //gridHeroArray = new GameObject[MapGenerator.hexMapSizeX/2, MapGenerator.hexMapSizeZ];
        gridHeroArrayAtStartOfCombat = new GameObject[MapGenerator.hexMapSizeX/2, MapGenerator.hexMapSizeZ];
        currentGameStage = GameStage.Preparation;
    }

    // Update is called once per frame
    void Update()
    {
        if(currentGameStage == GameStage.Combat)
        {
            combatTimer += Time.deltaTime;
            combatTimerSeconds = (int)combatTimer % 60;
        }

    }

    public void StartBattle()
    {
        if (currentGameStage == GameStage.Preparation)
        {
            //set new game stage
            aIopponent.OnGameStageComplate(currentGameStage);
            currentGameStage = GameStage.Combat;

            //show indicators
            MapGenerator.HideIndicators();

            //start own champion combat
            for (int x = 0; x < MapGenerator.hexMapSizeX/2; x++)
            {
                for (int z = 0; z < MapGenerator.hexMapSizeZ/2; z++)
                {
                    //there is a champion
                    if (AutoCombatField.gridHeroArray[x, z] != null)
                    {
                        //get character
                        HeroController heroController = AutoCombatField.gridHeroArray[x, z].GetComponent<HeroController>();

                        //start combat
                        heroController.OnCombatStart();
                    }

                }
            }

            gridHeroArrayAtStartOfCombat = (GameObject[,])AutoCombatField.gridHeroArray.Clone();
            uiController.UpdateUICombat(GameStage.Combat);
                    //check if we start with 0 champions
                    //if (IsAllChampionDead())
                    //    EndRound();
                }
       
    }
    public void SaveData()
    {
        GameProgress.Instance.SaveJsonData();
    }

    public void ShowAllStages()
    {
        GameProgress.Instance.LoadJsonData();
    }
    
    public void GoToStageSelection()
    {
        SceneManager.LoadScene("CampaignScene");
        StageController.MoveToCombatScreenFromCombat();
    }


    /// <summary>
    /// Called when a champion killd
    /// </summary>
    public virtual void OnChampionDeath()
    {
        bool allDead = IsAllChampionDead();
        if (allDead)
            EndRound(false);
    }
    int counter = 0;
    /// <summary>
    /// Ends the round
    /// </summary>
    public void EndRound(bool playerWin)
    {
        Debug.Log("End round called with " + playerWin);
        if (playerWin)
        {
            // counter ensure that not multiple objects call end rounde and therefore skip rounds
            counter++;
            if (counter > 1) return;
            currentGameStage = GameStage.Win;
            uiController.UpdateUICombat(currentGameStage);
            // give player the rewards
            // next stage
            // RestorHeroOnGrid();
            GameProgress.Instance.GoToNextStage();
        }
        else
        {
            counter = 0;
            currentGameStage = GameStage.Loss;
            uiController.UpdateUICombat(currentGameStage);
            //RestorHeroOnGrid();
            // repeat battle or leave battle
        }
    }

    public void RestorHeroOnGrid()
    {
        for (int x = 0; x < MapGenerator.hexMapSizeX; x++)
        {
            for (int z = 0; z < MapGenerator.hexMapSizeZ; z++)
            {
                // clear all left over heros
                if (AutoCombatField.gridHeroArray[x, z] != null)
                {
                    Destroy(AutoCombatField.gridHeroArray[x, z].GetComponent<HeroController>());
                    AutoCombatField.gridHeroArray[x, z] = null;
                }
                Debug.Log("hero in " + x + z + gridHeroArrayAtStartOfCombat[x, z]);
                // fill grid with heros from the start of combat
                if(gridHeroArrayAtStartOfCombat[x,z] != null)
                {
                    // get champion from champion controller
                    //PlaceHeroOnBoard(gridHeroArrayAtStartOfCombat[x, z].gameObject.GetComponent<HeroController>().hero);
                }

            }
        }
    }

    /// <summary>
    /// Returns true if all the champions are dead
    /// </summary>
    /// <returns></returns>
    private bool IsAllChampionDead()
    {

        int championCount = 0;
        int championDead = 0;
        //start own champion combat

        foreach (GameObject hero in AutoCombatField.heroList)
        {
            HeroControllerCombat hc = hero.GetComponent<HeroControllerCombat>();
            championCount++;

            if (hc.isDead)
                championDead++;
        }
        if (championDead == championCount)
            return true;

        return false;
        //for (int x = 0; x < MapGenerator.hexMapSizeX/2; x++)
        //{
        //    for (int z = 0; z < MapGenerator.hexMapSizeZ; z++)
        //    {
        //        //there is a champion
        //        if (gridHeroArray[x, z] != null)
        //        {
        //            //get character
        //            HeroController heroController = gridHeroArray[x, z].GetComponent<HeroController>();


        //            championCount++;

        //            if (heroController.isDead)
        //                championDead++;

        //        }

        //    }
        //}

        //if (championDead == championCount)
        //{

        //    return true;
        //}
            

        //return false;

    }


    /// <summary>
    /// Resets all owned heros on the grid 
    /// </summary>
    ///gets called by button press 
    public void ResetHero()
    {
        for (int x = 0; x < MapGenerator.hexMapSizeX/2; x++)
        {
            for (int z = 0; z < MapGenerator.hexMapSizeZ; z++)
            {
                //there is a champion
                if (AutoCombatField.gridHeroArray[x, z] != null)
                {
                    //get character
                    HeroController heroController = gridHeroArrayAtStartOfCombat[x, z].GetComponent<HeroController>();

                    //set position and rotation
                    heroController.Reset();

                }
            }
        }
        counter = 0;
    }




}
