﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controlls Enemy champions
/// </summary>
public class AIopponent : MonoBehaviour
{
    //public ChampionShop championShop;
    public HeroInventory heroInventory;
    public MapGenerator mapGenerator;
    public UIController uIController;
    public AutoCombatController autoCombatController;

    public GameObject[,] gridChampionsArray;
    public static List<GameObject> aiHeroList = new List<GameObject>();

    public Dictionary<HeroClassBonus, int> activeClassBonuses;
    public Dictionary<HeroOriginBonus, int> activeOriginBonuses;

    public Dictionary<ChampionType, int> championTypeCount;
    public List<ChampionBonus> activeBonusList;

    ///The damage that player takes when losing a round
    public int championDamage = 2;

    void Start()
    {
        gridChampionsArray = new GameObject[MapGenerator.hexMapSizeX, MapGenerator.hexMapSizeZ];
    }

    /// <summary>
    /// Called when a stage is finished
    /// </summary>
    /// <param name="stage"></param>
    public void OnGameStageComplate(GameStage stage)
    {
        if (stage == GameStage.Preparation)
        {
            //start champion combat
            for (int x = 0; x < MapGenerator.hexMapSizeX; x++)
            {
                for (int z = 0; z < MapGenerator.hexMapSizeZ / 2; z++)
                {
                    //there is a champion
                    if (gridChampionsArray[x, z] != null)
                    {
                        //get character
                        HeroController heroController = gridChampionsArray[x, z].GetComponent<HeroController>();

                        //start combat
                        heroController.OnCombatStart();
                    }

                }
            }
        }

        if (stage == GameStage.Combat)
        {
            //totall damage player takes
            int damage = 0;

            //iterate champions
            //start champion combat
            for (int x = 0; x < MapGenerator.hexMapSizeX; x++)
            {
                for (int z = 0; z < MapGenerator.hexMapSizeZ / 2; z++)
                {
                    //there is a champion
                    if (gridChampionsArray[x, z] != null)
                    {
                        //get character
                        HeroControllerCombat heroController = gridChampionsArray[x, z].GetComponent<HeroControllerCombat>();

                        //calculate player damage for every champion
                        if (heroController.currentHealth > 0)
                            damage += championDamage;
                    }

                }
            }

            //player takes damage


            ResetChampions();


            //AddRandomChampion();
          //  AddRandomChampion();
        }
    }

    /// <summary>
    /// Returns empty position in the map grid
    /// </summary>
    /// <param name="emptyIndexX"></param>
    /// <param name="emptyIndexZ"></param>
    private void GetEmptySlot(out int emptyIndexX, out int emptyIndexZ)
    {
        emptyIndexX = -1;
        emptyIndexZ = -1;

        //get first empty inventory slot
        for (int x = 0; x < MapGenerator.hexMapSizeX; x++)
        {
            for (int z = 0; z < MapGenerator.hexMapSizeZ; z++)
            {
                if (gridChampionsArray[x, z] == null)
                {
                    emptyIndexX = x;
                    emptyIndexZ = z;
                    break;
                }
            }    
        }
    }

/*    public void AddEnemy(Stage currentStage)
    {
        // clear the list
        aiHeroList = new List<GameObject>();

        int indexX;
        int indexZ;
        GetEmptySlot(out indexX, out indexZ);
        //dont add champion if there is no empty slot
        if (indexX == -1 || indexZ == -1)
            return;
        for(int i = 0; i < currentStage.enemyArray.Length; i++)
        {
            Hero hero = currentStage.enemyArray[i].enemy;
            //instantiate champion prefab
            GameObject heroPrefab = Instantiate(hero.prefab);

            //get champion controller
            HeroController heroController = heroPrefab.GetComponent<HeroController>();

            //setup chapioncontroller
            heroController.Init(hero, HeroController.TEAMID_AI);
            //add champion to array
            //set grid position

            //heroController.SetGridPositionNew(EnemyMap.GRIDTYPE_HEXA_MAP, currentStage.enemyPosition[i]);
            HeroPositionController.SetGridPosition(currentStage.enemyArray[i].x_pos, currentStage.enemyArray[i].y_pos, heroController);

            gridChampionsArray[heroController.gridPositionX, heroController.gridPositionZ] = heroPrefab;
            aiHeroList.Add(heroPrefab);
            CalculateBonuses();
            //set position and rotation
            HeroPositionController.SetWorldPosition(heroController);
            HeroPositionController.SetWorldRotation(heroController);
            
        }

    }*/
    void CalculateBonuses()
    {
        AutoCombatSynergies.CalculateBonuses(out activeClassBonuses, out activeOriginBonuses, aiHeroList);
        // need to remove the class and origin effect if it doesnt apply anymore
        ApplyClassEffect();
        ApplyOriginEffect();
    }

    void ApplyClassEffect()
    {
        foreach (KeyValuePair<HeroClassBonus, int> classBonuses in activeClassBonuses)
        {
            classBonuses.Key.ActivateClassEffect(classBonuses.Value, aiHeroList);
        }
    }

    void ApplyOriginEffect()
    {
        foreach (KeyValuePair<HeroOriginBonus, int> originBonuses in activeOriginBonuses)
        {
            originBonuses.Key.ActivateOriginEffect(originBonuses.Value, aiHeroList);
        }
    }
    public void RestartStage()
    {
        uIController.UpdateUICombat(GameStage.Preparation);
    }

    /// <summary>
    /// Resets all owned champions on the grid 
    /// </summary>
    private void ResetChampions()
    {
        for (int x = 0; x < MapGenerator.hexMapSizeX; x++)
        {
            for (int z = 0; z < MapGenerator.hexMapSizeX / 2; z++)
            {
                //there is a champion
                if (gridChampionsArray[x, z] != null)
                {
                    //get character
                    HeroController heroController = gridChampionsArray[x, z].GetComponent<HeroController>();

                    //set position and rotation
                    heroController.Reset();

                }

            }
        }
    }

    /// <summary>
    /// Called when champion health goes belove 0
    /// </summary>
    public void OnChampionDeath()
    {
        bool allDead = IsAllChampionDead();

        if (allDead)
        {
            autoCombatController.EndRound(true);
        }
            

    }


    /// <summary>
    /// Checks if all champion is dead
    /// </summary>
    /// <returns></returns>
    private bool IsAllChampionDead()
    {
        int championCount = 0;
        int championDead = 0;
        foreach(GameObject hero in aiHeroList)
        {
            HeroControllerCombat hc = hero.GetComponent<HeroControllerCombat>();
            championCount++;

            if (hc.isDead)
                championDead++;
        }
        if (championDead == championCount)
            return true;

        return false;

        ////start own champion combat
        //for (int x = 0; x < MapGenerator.hexMapSizeX; x++)
        //{
        //    for (int z = 0; z < MapGenerator.hexMapSizeZ / 2; z++)
        //    {
        //        //there is a champion
        //        if (gridChampionsArray[x, z] != null)
        //        {
        //            //get character
        //            HeroController heroController = gridChampionsArray[x, z].GetComponent<HeroController>();


        //            championCount++;

        //            if (heroController.isDead)
        //                championDead++;

        //        }

        //    }
        //}



    }

   

}
