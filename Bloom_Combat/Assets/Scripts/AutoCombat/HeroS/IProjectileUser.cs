using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IProjectileUser
{
    public void OnProjectileHitAutoAttack(GameObject projectileTarget);

    public void OnProjectileHitAbilityOne(GameObject projectileTarget);

    public void OnProjectileHitAbilityTwo(GameObject projectileTarget);

    public void OnProjectileHitUltimate(GameObject projectileTarget);
}
