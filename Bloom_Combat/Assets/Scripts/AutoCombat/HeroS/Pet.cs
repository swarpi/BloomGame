using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "DefaultPet", menuName = "Pet", order = 1)]
public class Pet : ScriptableObject
{
    public GameObject prefab;

    ///Projectile prefab to create when champion is attacking
    public GameObject attackProjectile;

    ///The champion name displayed on the UI frames
    public string uiname;

    public string petID;

    public Stats petStats;
}
