using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// Display for all hero specific ui infos in combat
public class HeroUIDisplays : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject championGO;
    HeroControllerCombat2D heroControllerCombat;
    //Loo
    public GameObject LooBar;
    public Image fillImageLoo;
    public CanvasGroup canvasGroupLoo;
    //HP
    public GameObject HealthBar;
    public Image fillImageHealth;
    public CanvasGroup canvasGroupHealth;
    // Shield
    public Image fillImageShield;
    // Rank
    public GameObject rankGO;

    private Quaternion cam;
    void Start()
    {
        cam = Camera.main.transform.rotation;
        canvasGroupLoo = LooBar.GetComponent<CanvasGroup>();
        canvasGroupHealth = HealthBar.GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (championGO != null)
        {
            this.transform.rotation = cam;
            // Adjust Loo 
            fillImageLoo.fillAmount = heroControllerCombat.currentLoo / heroControllerCombat.maxLoo;
            if (heroControllerCombat.currentHealth.f <= 0)
                canvasGroupLoo.alpha = 0;
            else
                canvasGroupLoo.alpha = 1;
            // Adjust Health
            fillImageHealth.fillAmount = heroControllerCombat.currentHealth.f / heroControllerCombat.maxHealth.f;
            if (heroControllerCombat.currentShield.f > 0)
            {
                fillImageShield.gameObject.SetActive(true);
                float newCurrentHealth = heroControllerCombat.currentShield.f + heroControllerCombat.currentHealth.f;
                fillImageShield.fillAmount = (newCurrentHealth) / heroControllerCombat.maxHealth.f;
                if(newCurrentHealth > heroControllerCombat.maxHealth.f)
                {
                    fillImageHealth.fillAmount = (heroControllerCombat.currentHealth.f / (heroControllerCombat.currentShield.f + heroControllerCombat.currentHealth.f));
                }
            }
            else
            {

                fillImageShield.gameObject.SetActive(false);
            }
            
            if (heroControllerCombat.currentHealth.f <= 0)
                canvasGroupHealth.alpha = 0;
            else
                canvasGroupHealth.alpha = 1;
        }
        else
        {
            heroControllerCombat.onHeroEvolution -= UpdateLevel;
            Destroy(this.gameObject);
        }
    }

    public void UpdateLevel(int rank)
    {
        Debug.Log("setting hero level to " + rank);
        rankGO.transform.Find("text").GetComponent<Text>().text = rank.ToString();
    }

    /// <summary>
    /// Called when champion created
    /// </summary>
    /// <param name="_championGO"></param>
    public void Init(GameObject _championGO)
    {
        championGO = _championGO;
        heroControllerCombat = championGO.GetComponent<HeroControllerCombat2D>();
        heroControllerCombat.onHeroEvolution += UpdateLevel;
    }


}
