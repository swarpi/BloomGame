using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
// controls the summoned pet
public class PetController : HeroControllerCombat
{
    [HideInInspector]
    ///Team of this champion, can be player = 0, or enemy = 1
    //public int teamID = 0;
    [SerializeField]
    public SummonerHeroController owner;
    public override void UseUltimate()
    {
        base.UseUltimate();
        
    }

    public void InitPet(Hero pet, SummonerHeroController owner)
    {
        hero = pet;
        teamID = owner.teamID;
        this.owner = owner;
    }

}
