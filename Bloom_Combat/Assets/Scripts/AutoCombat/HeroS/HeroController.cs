using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


/// <summary>
/// Controls a single champion movement and combat
/// </summary>
public class HeroController : MonoBehaviour
{
    public static int TEAMID_PLAYER = 0;
    public static int TEAMID_AI = 1;

    public AICombatState currentCombatState;

    [HideInInspector]
    public int gridType = 0;
    [HideInInspector]
    public int gridPositionX = 0;
    [HideInInspector]
    public int gridPositionZ = 0;
    [HideInInspector]
    public int currentGridPositionX = 0;
    [HideInInspector]
    public int currentGridPositionZ = 0;

    [HideInInspector]
    ///Team of this champion, can be player = 0, or enemy = 1
    public int teamID = 0;

    [HideInInspector]
    public Hero hero;
    [HideInInspector]
    public string heroID;
    [HideInInspector]
    public string heroName;
    [HideInInspector]
    public HeroClassBonus heroClassBonus;



    [HideInInspector]
    ///The upgrade level of the champion
    public int lvl = 1;
    [HideInInspector]
    public MapGenerator mapGenerator;
    //private GamePlayController gamePlayController;
    [HideInInspector]
    public AutoCombatController autoCombatController;
    [HideInInspector]
    public AIopponent aIopponent;
    [HideInInspector]
    public ChampionAnimation championAnimation;
    [HideInInspector]
    public WorldCanvasController worldCanvasController;
    [HideInInspector]
    public HeroPositionController heroPositionController;
    [HideInInspector]
    public HeroControllerMovement heroControllerMovement;
    public HeroControllerCombat heroControllerCombat;
    [HideInInspector]
    public Vector3 gridTargetPosition;

    private bool _isDragged = false;


    public bool isAttacking = false;


    //public bool isDead = false;


    [HideInInspector]
    public bool isPetDead = true;



    protected List<Effect> effects;
    private GameObject scripts;

    [HideInInspector]
    public UIController uiController;
    /// Start is called before the first frame update
    void Start()
    {
        
    }

    /// <summary>
    /// When champion created Champion and teamID passed
    /// </summary>
    /// <param name="_champion"></param>
    /// <param name="_teamID"></param>
    public void Init(Hero _hero, int _teamID)
    {
        scripts = GameObject.FindGameObjectWithTag("Script");
        hero = _hero;
        teamID = _teamID;
        heroName = _hero.heroID;
        heroID = _hero.heroID + Guid.NewGuid();
        //store scripts
        //map = GameObject.Find("Scripts").GetComponent<Map>();
        mapGenerator = GameObject.Find("Map Start").GetComponent<MapGenerator>();
        aIopponent = scripts.GetComponent<AIopponent>();

        championAnimation = GetComponent<ChampionAnimation>();
        heroControllerMovement = GetComponent<HeroControllerMovement>();
        heroControllerCombat = GetComponent<HeroControllerCombat>();
        //gamePlayController = GameObject.Find("Scripts").GetComponent<GamePlayController>();
        autoCombatController = scripts.GetComponent<AutoCombatController>();
        worldCanvasController = scripts.GetComponent<WorldCanvasController>();
        uiController = scripts.GetComponent<UIController>();
        // set animation speed to attack speed
        //championAnimation.animator.SetFloat("animAttackSpeed", hero.stats.speed);
        //set stats
        //heroClassBonus = AutoCombatSynergies.IdentifiyClassBonus(hero.heroClass);

        //worldCanvasController.AddHealthBar(this.gameObject);
        //worldCanvasController.AddLooBar(this.gameObject);
        heroControllerCombat.Init(this, _hero, _teamID);
        //worldCanvasController.InitHeroUI(this.gameObject);

        effects = new List<Effect>();

        lastPos = transform.position;
        currentPos = transform.position;

        
    }


    public static Vector3[] path = new Vector3[0];


    public LineRenderer lr;

    public float totalDistance = 0;
    public float totalTime = 0;
 
    private Vector3 lastPos;
    private Vector3 currentPos;

    private Quaternion _lookRotation;
    private Vector3 _direction;
    public Tile nextTile;
    public Tile targetTile;
    [SerializeField]
    private Tile currentTilePos;
    List<Tile> newPath = new List<Tile>();
    //public void OnPathFound(List<Tile> tempPath, bool pathSuccessful)
    //{
    //    if (pathSuccessful)
    //    {
    //        newPath = tempPath;
    //        StopCoroutine("FollowPath");
    //        StartCoroutine("FollowPath");
    //    }

    //}

    public bool attackFinished = true;
    public float attackTimer = 0f;
    public float attackTimer2 = 0f;

    public virtual void Update()
    {

        //if (true) 
        //{

        //    //visualize my path
        //    if (newPath.Count > 0) { 
        //    int currNode = 0;

        //    while (currNode < newPath.Count - 1)
        //    {
        //        Vector3 start = mapGenerator.allMapGridPositions[newPath[currNode].position.X, newPath[currNode].position.Z];
        //        Vector3 end = mapGenerator.allMapGridPositions[newPath[currNode + 1].position.X, newPath[currNode + 1].position.Z];

        //        Debug.DrawLine(start, end, Color.red);
        //        currNode++;
        //    }
        //        //WalkToTile();

        //    }
        //    //StopCoroutine("WalkToTile");
        //    //StartCoroutine("WalkToTile");


        //}
        if (AutoCombatController.currentGameStage == GameStage.Preparation)
        {

            if (_isDragged)
            {
                //Create a ray from the Mouse click position
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                //hit distance
                float enter = 100.0f;
                if (mapGenerator.m_Plane.Raycast(ray, out enter))
                {
                    //Get the point that is clicked
                    Vector3 hitPoint = ray.GetPoint(enter);

                    //new character position
                    Vector3 p = new Vector3(hitPoint.x, 1.0f, hitPoint.z);

                    //move champion
                    this.transform.position = Vector3.Lerp(this.transform.position, p, 0.1f);
                }
            }
            else
            {
                //calc distance
                float distance = Vector3.Distance(gridTargetPosition, this.transform.position);
                if (distance > 0.25f)
                {
                    this.transform.position = gridTargetPosition;
                }
                else
                {
                    this.transform.position = gridTargetPosition;
                }
                {
                    if (teamID == TEAMID_PLAYER)
                    {
                        SetCurrentTile(MapGenerator.tileMap[gridPositionX + 4, gridPositionZ]);
                    }
                    if (teamID == TEAMID_AI)
                    {
                        SetCurrentTile(MapGenerator.tileMap[gridPositionX, gridPositionZ]);
                    }
                }

            }
        }
        if (AutoCombatController.currentGameStage == GameStage.Combat)
        {
            if (heroControllerCombat.isDead || AutoCombatController.combatTimerSeconds < 0.5) return;
            // counter for all effects except stun and sleep
            CountDownEffects();

            if (heroControllerCombat.isSleeping)
            {
                if (heroControllerCombat.sleepTimer < 0)
                {
                    heroControllerCombat.isSleeping = false;

                    championAnimation.IsAnimated(true);

                    if (target != null)
                    {
                        //set pathfinder target

                    }
                }
                heroControllerCombat.sleepTimer -= Time.deltaTime;
                return;
            }
            //check for stuned effect
            if (heroControllerCombat.isStuned)
            {
                if (heroControllerCombat.stunTimer < 0)
                {
                    heroControllerCombat.isStuned = false;

                    championAnimation.IsAnimated(true);

                    if (target != null)
                    {
                        //set pathfinder target

                    }
                }
                heroControllerCombat.stunTimer -= Time.deltaTime;
                return;
            }

            // only look for a new target after your target which you attacked died
            if (target == null)
            {
                currentCombatState = AICombatState.LookingForTarget;
            }
            if (currentCombatState == AICombatState.LookingForTarget)
                target = FindTarget();
            //combat 
            if (target != null)
            {
                // rotate towards target when we are fighting them, otherwise rotate towards out next tile
                if(currentCombatState == AICombatState.InCombat) heroControllerMovement.RotateTowards(target.transform.position);
                targetTile = target.GetComponent<HeroController>().currentTilePos;
                //OnPathFound(PathFinder.FindPath(currentTilePos,target.GetComponent<HeroController>().currentTilePos),true);
                if (target.GetComponent<HeroControllerCombat>().isDead || target.GetComponent<HeroControllerCombat>().isUntargettable) //target champion is dead
                {
                    //remove target if targetchampion is dead 
                    target = null;
                    StopAllCoroutines();
                }
                // only rotate towards target if it is in attack range
                // check if my attack finished already
                else if(!isAttacking && Time.time >= attackTimer)
                {
                    isAttacking = true;
                    //this.transform.LookAt(target.transform, Vector3.up);
                    //calculate distance
                    float distance = Vector3.Distance(this.transform.position, target.transform.position);
                    if (heroControllerCombat.DoMove(distance))
                    {
                        //Debug.Log("doing move" + hero.name);
                    }
                    //if we are close enough to attack 
                    else if (distance < hero.stats.attackRange)
                    {
                        heroControllerCombat.DoAttack();
                    }
                    else
                    {
                        currentCombatState = AICombatState.LookingForTarget;
                    }
                    attackTimer = Time.time + 100f / hero.stats.speed;
                    isAttacking = false;
                }
            }
            if (path != null && path.Length > 1 && lr != null)
            {
                lr.positionCount = path.Length;
                for (int i = 0; i < path.Length; i++)
                {
                    lr.SetPosition(i, path[i]);
                }
            }
        }
    }

    public Tile GetCurrentTile()
    {
        return currentTilePos;
    }

    public bool SetCurrentTile(Tile newTile)
    {
        if (newTile == null)
        {
            currentTilePos.RemoveHero();
            currentTilePos = newTile;
            return true;
        }
        if(currentTilePos == null)
        {
            currentTilePos = newTile;
            return currentTilePos.AddHero(this);
        }
        if (currentTilePos.RemoveHero())
        {
            currentTilePos = newTile;
            return currentTilePos.AddHero(this);
        }
        return false;
    }

    public void ResetHeroTile()
    {
        currentTilePos.RemoveHero();
    }
    void CountDownEffects()
    {
        //Blind
        if (heroControllerCombat.isBlinded)
        {
            if (heroControllerCombat.blindTimer < 0)
            {
                heroControllerCombat.isBlinded = false;
            }
            heroControllerCombat.blindTimer -= Time.deltaTime;
        }
    }
    /// <summary>
    /// Set dragged when moving champion with mouse
    /// </summary>
    public bool IsDragged
    {
        get { return _isDragged; }
        set { _isDragged = value; }
    }

    /// <summary>
    /// Resets champion after combat is over
    /// </summary>
    /// do i need to method ?
    public void Reset()
    {
        //set active
        //this.gameObject.SetActive(true);

        ////reset stats
        ////maxHealth = hero.stats.health * lvl;
        ////currentHealth = hero.stats.health * lvl;
        ////isDead = false;
        //target = null;
        //isAttacking = false;

        ////reset position
        //HeroPositionController.SetWorldPosition(this);
        //HeroPositionController.SetWorldRotation(this);

        ////remove all effects
        ////foreach (Effect e in effects)
        ////{
        ////    e.Remove();
        ////}

        ////effects = new List<Effect>();
    }


    public void GiveHeroExp(int expAmount)
    {
        // add exptAmount to heroStats
        // if certain threshhold reached level up hero

    }

    public void LevelUpHero()
    {
        // increase level, increase stats
    }

    public GameObject target;
    /// <summary>
    /// Find the a champion the the closest world position
    /// </summary>
    /// <returns></returns>
    public GameObject FindTarget(int newTeamID = -1)
    {
        // only necessary if i get charmed 
        int tempTeamID = teamID;
        if(newTeamID >= 0)
        {
            tempTeamID = newTeamID;
        }
        GameObject closestEnemy = null;
        float bestDistance = 10000;
        //find enemy
        if (tempTeamID == TEAMID_PLAYER)
        { 
            foreach(GameObject enemy in AIopponent.aiHeroList)
            {
                // if i would target myself when charmed for example
                HeroControllerCombat heroController = enemy.GetComponent<HeroControllerCombat>();
                if (heroController == this.heroControllerCombat) continue;
                if (heroController.isDead == false)
                {
                    //calculate distance
                    float distance = Vector3.Distance(this.transform.position, enemy.transform.position);

                    //if new this champion is closer then best distance
                    if (distance < bestDistance)
                    {
                        bestDistance = distance;
                        closestEnemy = enemy;
                    }
                }
            }

        }
        else if (tempTeamID == TEAMID_AI)
        {
            foreach (GameObject enemy in AutoCombatField.heroList)
            {
                // if i would target myself when charmed for example
                HeroControllerCombat heroController = enemy.GetComponent<HeroControllerCombat>();
                if (heroController == this.heroControllerCombat) continue;
                if (heroController.isDead == false)
                {
                    //calculate distance
                    float distance = Vector3.Distance(this.transform.position, enemy.transform.position);

                    //if new this champion is closer then best distance
                    if (distance < bestDistance)
                    {
                        bestDistance = distance;
                        closestEnemy = enemy;
                        targetTile = heroController.heroController.currentTilePos;
                    }
                }
            }

        }
        return closestEnemy;
    }
    /// <summary>
    /// Looks for new target to attack if there is any
    /// </summary>
    public void TryAttackNewTarget()
    {
        //find closest enemy
        target = FindTarget();

        //if target found
        if (target != null)
        {
            //set pathfinder target

        }
    }
    /// <summary>
    /// Called when gamestage.combat starts
    /// </summary>
    public void OnCombatStart()
    {
        IsDragged = false;
        currentCombatState = AICombatState.LookingForTarget;
        //this.transform.position = gridTargetPosition;
        //Debug.Log("combat starting with " + );

        //TryAttackNewTarget();

    }
    public void OnDeath()
    {
        this.gameObject.SetActive(false);
    }
    /// <summary>
    /// Add effect to this champion
    /// </summary>
    public void AddEffect(GameObject effectPrefab, float duration)
    {
        if (effectPrefab == null)
            return;

        //look for effect
        bool foundEffect = false;
        foreach (Effect e in effects)
        {
            if (effectPrefab == e.effectPrefab)
            {
                e.duration = duration;
                foundEffect = true;
            }
        }

        //not found effect
        if (foundEffect == false)
        {
            Effect effect = this.gameObject.AddComponent<Effect>();
            effect.Init(effectPrefab, this.gameObject, duration);
            effects.Add(effect);
        }

    }

    /// <summary>
    /// Remove effect when expired
    /// </summary>
    public void RemoveEffect(Effect effect)
    {
        effects.Remove(effect);
        effect.Remove();
    }

}
