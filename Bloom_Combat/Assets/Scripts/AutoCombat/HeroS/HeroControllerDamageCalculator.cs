
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroControllerDamageCalculator : MonoBehaviour
{
    HeroController heroController;
    HeroControllerCombat heroControllerCombat;
    Hero hero;
    // Start is called before the first frame update
    void Start()
    {
        heroController = GetComponent<HeroController>();
        hero = heroController.hero;
    }

    public virtual float CalculateDamageGiven(float baseDamage,bool isCrit, bool isMagicDamage)
    {
        float critDamage = baseDamage;
        if (isCrit)
        {
            critDamage = baseDamage * hero.stats.critDamage; // change with critdamage 
        }
        return critDamage;
        // need to calculate elemental bonus damage if it is magic damage
    }

    /// <summary>
    /// Called when this champion takes damage
    /// </summary>
    /// <param name="damage"></param>
    public virtual bool OnGotHit(float damage, bool isCrit, bool isMagic = false)
    {
        float actualDamage = CalculateDamageTaken(damage, isMagic);
        heroControllerCombat.currentHealth -= actualDamage;
        // increase Loo
        heroControllerCombat.currentLoo += hero.stats.looPerHitTaken;
        //add floating text
        heroController.worldCanvasController.AddDamageText(this.transform.position + new Vector3(0, 2.5f, 0), actualDamage, isCrit);

        //death
        if (heroControllerCombat.currentHealth <= 0)
        {
            heroControllerCombat.isDead = true;
            heroControllerCombat.isPetDead = true;
            heroController.aIopponent.OnChampionDeath();
            heroController.autoCombatController.OnChampionDeath();
            heroController.championAnimation.DoDeath();
            //isDead = true;
            //isPetDead = true;
            //aIopponent.OnChampionDeath();
            //autoCombatController.OnChampionDeath();
            //this.gameObject.SetActive(false);
            //currentTilePos.RemoveHero();
            //Destroy(this.gameObject);
        }
        return heroControllerCombat.isDead;
    }

    public void OnDeath()
    {
        this.gameObject.SetActive(false);
    }
    public virtual float CalculateDamageTaken(float damage, bool isMagic)
    {
        float damageTaken = damage;
        if (isMagic)
        {
            return damageTaken = damage * (100 / (100 + hero.stats.magicResist));
        }
        else
        {
            return damageTaken = damage * (100 / (100 + hero.stats.armor));
        }
    }
}
