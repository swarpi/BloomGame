using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaylorHeroController : HeroControllerCombat
{
    float spinAttackTimer = 0;
    float spinAttackTimerCooldown = 6f;

    float takeCoverTimer = 0;
    float takeCoverTimerCooldown = 10f;

    bool takingCover = false;
    float damageReflectionRate = 0.2f;

    // to keep check of kill contribution
    Dictionary<GameObject,float> lastHitEnemies = new Dictionary<GameObject, float>();
    float lastHitenemyTimerCooldown = 5f;

    bool isHeroMode = false;
    float healAmount = 20f;

    void Update()
    {
        Dictionary<GameObject, float> copyDictionary = new Dictionary<GameObject, float>(lastHitEnemies);
        foreach (KeyValuePair<GameObject,float> enemy in copyDictionary)
        {
            HeroController enemyHC = enemy.Key.GetComponent<HeroController>();
            // check if the enmey was hit in the last 5 secs
            if (enemy.Value < Time.time - lastHitenemyTimerCooldown)
            {
                lastHitEnemies.Remove(enemy.Key);
            }
            else if (enemy.Key.GetComponent<HeroControllerCombat>().isDead)
            {
                lastHitEnemies.Remove(enemy.Key);
                if(isHeroMode) OnGotHeal(healAmount);
            }
        }
    }
    // override to check for assists
    public override IEnumerator DamageTarget(GameObject damageTarget, float damage, bool isCrit = false, bool isMagic = false, float delay = 0.5F, bool isAutoAttack = false,bool isRange = false)
    {
        if (!lastHitEnemies.ContainsKey(damageTarget))
        {
            lastHitEnemies.Add(damageTarget, Time.time);
        }
        else
        {
            lastHitEnemies[damageTarget] = Time.time;
        }
        return base.DamageTarget(damageTarget, damage, isCrit, isMagic, delay, isAutoAttack, isRange);
    }
    public override bool DoMove(float distance)
    {
        if (base.DoMove(distance))
        {
            return true;
        }
        if (spinAttackTimer == 0)
        {
            spinAttackTimer = Time.time + spinAttackTimerCooldown; // first delay before you can use your comboAttack
        }
        // do combo attack if it is not on cooldown and target is in range
        if (Time.time >= spinAttackTimer && distance < hero.stats.attackRange)
        {
            DoSpinAttack();
            spinAttackTimer = Time.time + spinAttackTimerCooldown;
            return true;
        }
        if (takeCoverTimer == 0)
        {
            takeCoverTimer = Time.time + takeCoverTimerCooldown; // first delay before you can use your comboAttack
        }
        // do combo attack if it is not on cooldown and target is in range
        if (Time.time >= takeCoverTimer)
        {
            Debug.Log("doing take cover");
            StartCoroutine(DoTakeCover());
            return true;
        }
        return false;
    }
    void DoSpinAttack()
    {
        championAnimation.DoAbilityOne(true);
        foreach(Tile tile in heroController.GetCurrentTile().neighbours)
        {
            if (tile.heroOnTile == null) continue;
            if(tile.heroOnTile.teamID != teamID)
            {
                StartCoroutine(DamageTarget(tile.heroOnTile.gameObject, 20, false));
            }
        }
    }

    IEnumerator DoTakeCover()
    {
        championAnimation.DoAbilityTwo(true);
        // coverDurationTime adapt to animation length
        float coverDurationTime = 3f;
        takingCover = true;
        yield return new WaitForSeconds(coverDurationTime);
        takingCover = false;
        takeCoverTimer = Time.time + takeCoverTimerCooldown;
    }

    public override void UseUltimate()
    {
        championAnimation.DoUltimate(true);
        StartCoroutine(HeroMode());
        base.UseUltimate();
    }
    
    IEnumerator HeroMode()
    {
        float prevGiven = hero.stats.looPerHitGiven;
        float prevTaken = hero.stats.looPerHitTaken;
        isHeroMode = true;
        float HeroModeduration = 8f;
        hero.stats.looPerHitGiven = 0;
        hero.stats.looPerHitTaken = 0;
        Debug.Log("inside of hero mode");
        // add stats gain
        yield return new WaitForSeconds(HeroModeduration);
        Debug.Log("out of hero mode");
        hero.stats.looPerHitGiven = prevGiven;
        hero.stats.looPerHitTaken = prevTaken;
        isHeroMode = false;
    }
    /// <summary>
    /// Called when this champion takes damage
    /// </summary>
    /// <param name="damage"></param>
    public override bool OnGotHit(HeroController source, float damage, bool isCrit, bool isMagic, bool isAutoAttack)
    {
        float damageTaken = DamageCalculator.CalculateDamageTaken(source, heroController, damage, isMagic);
        if(takingCover)
        {
            StartCoroutine(DamageTarget(source.gameObject, damageTaken * damageReflectionRate,false,false,0));
        }
        currentHealth -= damageTaken;
        // increase Loo
        currentLoo += hero.stats.looPerHitTaken;
        //add floating text
        heroController.worldCanvasController.AddDamageText(this.transform.position + new Vector3(0, 2.5f, 0), damageTaken, isCrit);
        //death
        if (currentHealth <= 0)
        {
            isDead = true;
            isPetDead = true;
            heroController.aIopponent.OnChampionDeath();
            heroController.autoCombatController.OnChampionDeath();
            championAnimation.DoDeath();
            heroController.SetCurrentTile(null);
        }
        return isDead;
    }
}
