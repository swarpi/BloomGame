using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoseThornArrow : Projectile
{
    public float totalDistance = 0f;
    public RoseHeroController shooter;
    public bool splitted = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void Init(GameObject target,int projectileType, RoseHeroController source, bool hasSplitted)
    {
        totalDistance = Vector3.Distance(this.transform.position, target.transform.position);
        base.Init(target, projectileType, source);
        shooter = source;
        splitted = hasSplitted;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();

        // only do it once, if i already splitted once dont do it again
        if (distance < totalDistance/1.5 && splitted == false)
        {
            Debug.Log("splitting");
            foreach(GameObject enemy in AIopponent.aiHeroList)
            {
                if (enemy.GetComponent<HeroControllerCombat>().isDead) continue;
                GameObject projectile = Instantiate(this.gameObject);
                projectile.transform.position = transform.position;

                projectile.GetComponent<RoseThornArrow>().Init(enemy, projectileType, shooter,true);
                
            }
            Destroy(this.gameObject);
            // destroy this gameobject
            // create for each enemy one new arrow and init it with its target position
            // damage enemies on hit
        }

    }

}
