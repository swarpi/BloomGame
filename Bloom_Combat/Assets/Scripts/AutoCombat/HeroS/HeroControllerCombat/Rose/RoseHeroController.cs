using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoseHeroController : HeroControllerCombat, IProjectileUser
{
    float splitShotTimer = 0;
    float splitShotTimerCooldown = 5f;

    public GameObject thornArrowProjectile;
    public GameObject ultimateArrowProjectile;

    public GameObject explosionSystem;
    float explosionDamage = 20f;

    float charmDuration = 2f;

    public override bool DoMove(float distance)
    {

        if (base.DoMove(distance))
        {
            return true;
        }

        if (splitShotTimer == 0)
        {
            splitShotTimer = Time.time + splitShotTimerCooldown; // first delay before you can use your comboAttack
        }
        if (Time.time >= splitShotTimer && distance < hero.stats.attackRange)
        {
            SplitShot();
            splitShotTimer = Time.time + splitShotTimerCooldown;
            return true;
        }

        return false;
    }

    void SplitShot()
    {
        championAnimation.DoAbilityOne(true);
        // thrown arrow
        // at half the distance split the arrow
        // arrow split : create new arrows at the split position
        //               create one arrow for each enemy
        //               let arrows fly to each enemy
    }

    public override void OnAbilityOneFinished()
    {
        //isAttacking = false;
        
        if (heroController.target != null)
        {
            //create projectile if have one
            if (thornArrowProjectile != null && projectileStart != null)
            {
                GameObject projectile = Instantiate(thornArrowProjectile);
                projectile.transform.position = projectileStart.transform.position;

                projectile.GetComponent<RoseThornArrow>().Init(heroController.target,1,this,false);
                // create a new thread that keeps looking at the distance of arrow and the target
            }
        }
    }

    public override void UseUltimate()
    {
        base.UseUltimate();
        championAnimation.DoUltimate(true);
    }

    public override void OnUltimateFinished()
    {
        if (heroController.target != null)
        {
            //create projectile if have one
            if (ultimateArrowProjectile != null && projectileStart != null)
            {
                GameObject projectile = Instantiate(ultimateArrowProjectile);
                projectile.transform.position = projectileStart.transform.position;

                projectile.GetComponent<Projectile>().Init(heroController.target,3,this);
                // create a new thread that keeps looking at the distance of arrow and the target
            }
        }
    }
    public override IEnumerator DamageTarget(GameObject damageTarget, float damage, bool isCrit = false, bool isMagic = false, float delay = 0.5f, bool isAutoAttack = false,bool isRange = false)
    {
        // delay depending on animation speed or until projectil hit the target
        // try anim.GetCurrentAnimatorStateInfo(layer).length for the animation duration
        if (isRange)
        {
            // wait until projectile hit the target
        }
        else
        {
            yield return new WaitForSeconds(delay);
        }

        if (heroController.target != null)
        {
            currentLoo += hero.stats.looPerHitGiven;
            HeroControllerCombat targetHero = damageTarget.GetComponent<HeroControllerCombat>();
            float damageGiven = DamageCalculator.CalculateDamageGiven(heroController.hero, damage, isCrit);
            
            bool isTargetDead = targetHero.OnGotHit(heroController, damageGiven, isCrit, isMagic, isAutoAttack);

            if (isTargetDead)
            {
                StartCoroutine(playExplosion(targetHero));
                // damage all enemies in the area
                heroController.TryAttackNewTarget();
            }
                
        }
        yield return null;
    }

    IEnumerator playExplosion(HeroControllerCombat target)
    {
        Debug.Log("playing explosion");
        GameObject explosion = Instantiate(explosionSystem,target.transform);
        explosion.GetComponent<ParticleSystem>().Play();
        foreach(Tile tile in target.heroController.GetCurrentTile().neighbours)
        {
            if(tile.heroOnTile != null)
            {
                if(tile.heroOnTile.teamID != teamID)
                {
                    Debug.Log("hitting " + tile.heroOnTile);
                    StartCoroutine(DamageTarget(tile.heroOnTile.gameObject, explosionDamage,false,true,0f,false));
                }
            }
        }
        yield return new WaitForSeconds(2f);

        Destroy(explosion);
    }
    public void OnProjectileHitAutoAttack(GameObject projectileTarget)
    {

    }

    public void OnProjectileHitAbilityOne(GameObject projectileTarget)
    {

    }

    public void OnProjectileHitAbilityTwo(GameObject projectileTarget)
    {

    }

    public void OnProjectileHitUltimate(GameObject projectileTarget)
    {
        HeroController targetHC = projectileTarget.GetComponent<HeroController>();
        
        StartCoroutine(CharmEnemy(targetHC));
    }
    IEnumerator CharmEnemy(HeroController targetHC)
    {
        // target finds a new enemy that from his own hero list
        // if teamid = 1 => (1-1) = 0
        // if teamid = 0 => Abs.(0-1) = 1
        Debug.Log("charming enemy");
        targetHC.target =  targetHC.FindTarget(Mathf.Abs(targetHC.teamID - 1));
        yield return new WaitForSeconds(charmDuration);
        targetHC.target = targetHC.FindTarget(Mathf.Abs(targetHC.teamID));
    }
}

