using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShapeshifterHeroController : HeroControllerCombat
{
    public override void UseUltimate()
    {
        ShapeshifterTransform();
        base.UseUltimate();
    }
    public void ShapeshifterTransform()
    {
        this.gameObject.transform.localScale = Vector3.Scale(hero.prefab.transform.localScale, new Vector3(1.2f,1.2f,1.2f));
    }
}
