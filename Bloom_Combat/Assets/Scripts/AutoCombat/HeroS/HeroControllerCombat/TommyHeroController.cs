using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TommyHeroController : HeroControllerCombat
{
    float flashShotTimer = 0;
    float flashShotTimerCooldown = 10f;
    float blindTime = 2f;

    float runOffTimer = 0;
    float runOffTimerCooldown = 50f;
    float runOffhealth = 50f;

    float sleepTime = 2f;
    public override bool DoMove(float distance)
    {

        if (base.DoMove(distance))
        {
            return true;
        }

        if (flashShotTimer == 0)
        {
            flashShotTimer = Time.time + flashShotTimerCooldown; // first delay before you can use your comboAttack
        }
        // do combo attack if it is not on cooldown and target is in range
        if (Time.time >= flashShotTimer && distance < hero.stats.attackRange)
        {
            FlashShot();
            flashShotTimer = Time.time + flashShotTimerCooldown;
            return true;
        }
        // run off when your health is below the thresh hold
        if (Time.time >= runOffTimer)
        {
            // check if i have reached a certain hp threshhold
            if (currentHealth > runOffhealth) return false; 
            StartCoroutine(RunOff());
            runOffTimer = Time.time + runOffTimerCooldown;
            return true;
        }
        return false;
    }

    void FlashShot()
    {
        heroController.target.GetComponent<HeroControllerCombat>().OnGotBlinded(blindTime, hero.stats.effectivity);
    }

    IEnumerator RunOff()
    {
        isUntargettable = true;
        
        heroController.currentCombatState = AICombatState.LookingForTarget;
        Tile newTile = MoveCalculator.FindFurthestPlayerTile(heroController.GetCurrentTile());
        heroController.SetCurrentTile(null);
        heroControllerMovement.MoveTowards(newTile);
        heroController.currentCombatState = AICombatState.InCombat;
        yield return new WaitForSeconds(1f);

        isUntargettable = false;
    }

    public override void UseUltimate()
    {
        base.UseUltimate();
        heroController.target.GetComponent<HeroControllerCombat>().OnGotSlept(sleepTime, hero.stats.effectivity);
    }
}
