using DigitalRuby.LightningBolt;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatharinaHeroController : HeroControllerCombat
{

    public GameObject lightningBoltGO;
    float lightningBoltPassByDamage = 5f;
    float lightningBoltMainDamage = 15f;
    // cooldowns
    float chargeAttackTimer = 0;
    float chargeAttackTimerCooldown = 10f;

    float humiliationTimer = 0;
    float humiliationTimerCooldown = 6f;
    float humiliationDuration = 3f;

    bool isDodging = false;

    int amountOfJumps = 4;
    float ultiDamage = 10f;

    public override bool DoMove(float distance)
    {
        if (base.DoMove(distance))
        {
            return true;
        }
        if (chargeAttackTimer == 0)
        {
            chargeAttackTimer = Time.time + chargeAttackTimerCooldown; // first delay before you can use your comboAttack
        }
        // Throw shield if shield is on me
        if (Time.time >= chargeAttackTimer)
        {
            DoChargeAttack();
            chargeAttackTimer = Time.time + chargeAttackTimerCooldown;
            return true;
        }
        if (humiliationTimer == 0)
        {
            humiliationTimer = Time.time + humiliationTimerCooldown; // first delay before you can use your comboAttack
        }
        if (Time.time >= humiliationTimer)
        {
            StartCoroutine(DoHumiliation());
            humiliationTimer = Time.time + humiliationTimerCooldown;
            return true;
        }
        return false;
    }

    public override bool OnGotHit(HeroController source, float damage, bool isCrit, bool isMagic,bool isAutoAttack)
    {
        if (isDodging) damage = 0;
        return base.OnGotHit(source, damage, isCrit, isMagic, isAutoAttack);
    }

    public void DoChargeAttack()
    {
        championAnimation.DoAbilityOne(true);
    }

    IEnumerator DoHumiliation()
    {
        championAnimation.DoAbilityTwo(true);
        isDodging = true;
        yield return new WaitForSeconds(humiliationDuration);
        isDodging = false;
            
    }

    public override void OnAbilityOneFinished()
    {
        // find furthest enemy
        // instantiate lightning between me and him

        // find all enemies between us
        // damage my target, reduce armor on all enemies on the way

        GameObject lightningBolt = Instantiate(lightningBoltGO);
        LightningBoltScript[] lbScript = lightningBolt.GetComponentsInChildren<LightningBoltScript>();

        GameObject furthestEnemy = MoveCalculator.FindFurthestEnemyUnit(this.gameObject);
        
        foreach(LightningBoltScript lightning in lbScript)
        {
            lightning.StartPosition = transform.position;
            lightning.EndPosition = furthestEnemy.transform.position;
            lightning.Duration = Random.Range(0.01f, 0.09f);
            
        }
        // add new box collider
        GameObject boxCollider = lightningBolt.transform.Find("BoxCollider").gameObject;
        // position to my tile position
        boxCollider.transform.position = heroController.GetCurrentTile().transform.position;
        // size and rotation towards the targettile position
        Vector3 newtarget = furthestEnemy.transform.position;
        newtarget.y = transform.position.y;
        // rotation
        boxCollider.transform.LookAt(newtarget);
        // size
        boxCollider.transform.localScale = new Vector3(1.7f, 1, Vector3.Distance(furthestEnemy.transform.position, this.gameObject.transform.position));
        // move the collider to the midway // (A+B)/2, half the z size
        Vector3 newPosition = (furthestEnemy.transform.position + transform.position) / 2;
        newPosition.y = 1;
        boxCollider.transform.position = newPosition;
        // add trigger to it to know which enemies are hit with the collider
        StartCoroutine(ApplyLightningStrike(boxCollider,furthestEnemy));
    }

    IEnumerator ApplyLightningStrike(GameObject boxCollider, GameObject mainTarget)
    {
        yield return new WaitForSeconds(0.3f);
        foreach(GameObject enemy in boxCollider.GetComponent<CatharinaBoxCollider>().hitEnemies)
        {
            // if it is my intial target deal tons of damage to it
            if(enemy == mainTarget)
            {
                StartCoroutine(DamageTarget(enemy, lightningBoltMainDamage, false, true, 0,false));
                continue;
            }
            // reduce resits of passing by units and maybe deal some damage to them
            if(enemy.GetComponent<HeroControllerCombat>().teamID != teamID)
            {
                // reduce their armor and magic resist
                StartCoroutine(DamageTarget(enemy, lightningBoltPassByDamage, false, true, 0,false));
            }
        }
        yield return new WaitForSeconds(0.3f);
        Destroy(boxCollider.transform.parent.gameObject);
    }

    public override void UseUltimate()
    {
        base.UseUltimate();

        StartCoroutine(AttackAllEnemies());
    }

    IEnumerator AttackAllEnemies()
    {
        // make my character unable to move/ be hit / or able to attack
        isUntargettable = true;
        isMoveable = false;
        isStuned = true;
        stunTimer = float.MaxValue;
        // disable collider so you wont trigger addhero when you jump to other heros
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        heroController.ResetHeroTile();
        // need character to fix its position after the ultimate animation
        Transform character = transform.Find("character").transform;
        int numberOfJumps = amountOfJumps;
        int jumpInt = 0;
        GameObject nextTarget = null;
        List<GameObject> aliveEnemies = new List<GameObject>();
        aliveEnemies = AIopponent.aiHeroList.FindAll(x => x.GetComponent<HeroControllerCombat>().isDead == false);
        for(int i = 0; i< aliveEnemies.Count; i++)
        {
            yield return new WaitForSeconds(0.5f);
            nextTarget = aliveEnemies[i];
            transform.position = nextTarget.transform.position;
            championAnimation.DoUltimate(true);
            // damage this target
            StartCoroutine(DamageTarget(nextTarget, ultiDamage, false, true, 0.1f));
            character.localPosition = Vector3.zero;
            character.localRotation = Quaternion.Euler(0, 0, 0);
            numberOfJumps--;
        }
        // maybe need to adjust this if number of jumps is greater than 4
        // if i have 3 jumps left, it means that i only jumped one time, so there is only one target, therefor jump to that target 3 times
        if(numberOfJumps == 3)
        {
            for (int i = 0; i < numberOfJumps; i++)
            {
                yield return new WaitForSeconds(0.5f);
                nextTarget = aliveEnemies[0];
                transform.position = nextTarget.transform.position;
                championAnimation.DoUltimate(true);
                // damage this target
                StartCoroutine(DamageTarget(nextTarget, ultiDamage, false, true, 0.1f));
                character.localPosition = Vector3.zero;
                character.localRotation = Quaternion.Euler(0, 0, 0);
            }
        }
        // if ihave 2 jumps left, it means there are only 2 targets, so jump to each one of them again
        if(numberOfJumps == 2)
        {

            //check if maybe one enemy died between my jumps or i killed one
            aliveEnemies = AIopponent.aiHeroList.FindAll(x => x.GetComponent<HeroControllerCombat>().isDead == false);
            int numberOfAliveEnemies = aliveEnemies.Count;

            for (int i = 0; i < numberOfJumps; i++)
            {
                yield return new WaitForSeconds(0.5f);
                nextTarget = aliveEnemies[numberOfAliveEnemies - 1];
                transform.position = nextTarget.transform.position;
                championAnimation.DoUltimate(true);
                // damage this target
                StartCoroutine(DamageTarget(nextTarget, ultiDamage, false, true, 0.1f));
                character.localPosition = Vector3.zero;
                character.localRotation = Quaternion.Euler(0, 0, 0);
                // if there are still more than 2 enemies jump to the other one, otherwise jump to that enemy again
                if(numberOfAliveEnemies >= 2)
                {
                    numberOfAliveEnemies--;
                }
            }
        }
        // if i have 1 jump left over, there were 3 targets, then jump to a random target
        if(numberOfJumps == 1)
        {
            // final jump
            yield return new WaitForSeconds(0.5f);
            aliveEnemies = AIopponent.aiHeroList.FindAll(x => x.GetComponent<HeroControllerCombat>().isDead == false);
            jumpInt = Random.Range(0, aliveEnemies.Count - 1);
            nextTarget = aliveEnemies[jumpInt];
            yield return new WaitForSeconds(0.5f);
            transform.position = nextTarget.transform.position;
            championAnimation.DoUltimate(true);
            character.localPosition = Vector3.zero;
            character.localRotation = Quaternion.Euler(0, 0, 0);
        }


        //set my position after my last jump to a random tile of my last target
        foreach(Tile tile in nextTarget.GetComponent<HeroController>().GetCurrentTile().neighbours)
        {
            if(tile.heroOnTile == null)
            {
                heroController.SetCurrentTile(tile);
                transform.position = tile.transform.position;
                break;
            }
        }
        // allow me to get target, to move and to attack
        stunTimer = 0;
        isStuned = false;
        isUntargettable = false;
        isMoveable = true;
        gameObject.GetComponent<CapsuleCollider>().enabled = true;
        yield break;
        //yield return new wa
    }
    public override void OnUltimateFinished()
    {
        base.OnUltimateFinished();

    }
}

