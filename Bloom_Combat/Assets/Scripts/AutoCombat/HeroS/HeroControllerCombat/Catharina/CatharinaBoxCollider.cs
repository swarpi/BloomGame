using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatharinaBoxCollider : MonoBehaviour
{
    public List<GameObject> hitEnemies = new List<GameObject>();
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10)
        {
            hitEnemies.Add(other.gameObject);
        }
    }

}
