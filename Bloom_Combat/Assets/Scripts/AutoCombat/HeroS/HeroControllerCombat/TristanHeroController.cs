using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TristanHeroController : HeroControllerCombat
{
    // Components for my shield
    public Transform shieldPosition;
    public GameObject shield;
    float shieldAllyTime = 6f;
    bool shieldOnMe = true;
    float shieldAmountPerEnemy = 50f;

    // cooldowns
    float throwShieldTimer = 0;
    float throwShieldTimerCooldown = 4f;

    float shieldSlamTimer = 0;
    float shieldSlamTimerCooldown = 6f;
    float stunTimerWithShield = 1f;
    float stunTimerWithoutShield = 1f;
    public override bool DoMove(float distance)
    {
        if (base.DoMove(distance))
        {
            return true;
        }
        if (throwShieldTimer == 0)
        {
            throwShieldTimer = Time.time + throwShieldTimerCooldown; // first delay before you can use your comboAttack
        }
        // Throw shield if shield is on me
        if (Time.time >= throwShieldTimer && shieldOnMe)
        {
            DoShieldThrow();
            throwShieldTimer = Time.time + throwShieldTimerCooldown;
            return true;
        }
        if (shieldSlamTimer == 0)
        {
            shieldSlamTimer = Time.time + shieldSlamTimerCooldown; // first delay before you can use your comboAttack
        }
        if (Time.time >= shieldSlamTimer)
        {
            DoShieldSlam();
            shieldSlamTimer = Time.time + shieldSlamTimerCooldown;
            return true;
        }
        return false;
    }
    GameObject throwTarget;
    void DoShieldThrow()
    {

        List<GameObject> potentialTargetsAll = AutoCombatField.heroList;
        // i am the only player alive
        // sort the list by lowest health percantage
        List<GameObject> potentialTargets = potentialTargetsAll.FindAll(x => x.GetComponent<HeroControllerCombat>().isDead == false);
        if (potentialTargets.Count == 1) return;
        potentialTargets.Sort((x, y) => (x.GetComponent<HeroControllerCombat>().currentHealth / x.GetComponent<HeroControllerCombat>().maxHealth)
                                        .CompareTo(y.GetComponent<HeroControllerCombat>().currentHealth / y.GetComponent<HeroControllerCombat>().maxHealth));
        // if am the lowest throw to the second lowest 
        if(potentialTargets[0] == this.gameObject)
        {
            throwTarget = potentialTargets[1].GetComponent<HeroController>().gameObject;
        }
        else
        {
            throwTarget = potentialTargets[0].GetComponent<HeroController>().gameObject;
        }
        StartCoroutine(ThrowShield());
    }

    IEnumerator ThrowShield()
    {
        //shield.transform.position = throwTarget.transform.Find("character").transform.position;
        championAnimation.DoAbilityOne(true);
        shieldOnMe = false;
        // place the shield on my ally
        shield.transform.SetParent(throwTarget.transform.Find("character").transform);
        shield.transform.localPosition = new Vector3(0, 1.5f, 0.7f);
        shield.transform.localRotation = Quaternion.Euler(0, 0, 0);
        yield return new WaitForSeconds(shieldAllyTime);
        // take back my shield
        TakeBackShield();
    }

    void TakeBackShield()
    {
        championAnimation.DoAbilityOne(true);
        shield.transform.SetParent(shieldPosition);
        shield.transform.localPosition = new Vector3(0, 0.061f, -0.046f);
        shield.transform.localRotation = Quaternion.Euler(-127, 0, 0);
        shieldOnMe = true;
        throwShieldTimer = Time.time + throwShieldTimerCooldown;
    }

    void DoShieldSlam()
    {
        championAnimation.DoAbilityTwo(true);
        if(shieldOnMe)
            heroController.target.GetComponent<HeroControllerCombat>().OnGotStun(stunTimerWithShield, hero.stats.effectivity);
        else
            heroController.target.GetComponent<HeroControllerCombat>().OnGotStun(stunTimerWithoutShield, hero.stats.effectivity);
    }

    public override void UseUltimate()
    {
        int enemyCount = 0;
        base.UseUltimate();
        TakeBackShield();
        championAnimation.DoUltimate(true);
        foreach (Tile tile in heroController.GetCurrentTile().neighbours)
        {
            if (tile.heroOnTile == null) continue;
            if (tile.heroOnTile.teamID != teamID)
            {
                // knock up enemy
                tile.heroOnTile.GetComponent<HeroControllerCombat>().OnGotStun(stunTimerWithoutShield, hero.stats.effectivity);
                enemyCount++;
            }
        }

        CreateShield(enemyCount * shieldAmountPerEnemy);
    }


}
