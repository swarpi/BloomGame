using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyrusHeroController : HeroControllerCombat
{
    public GameObject dagger;
    float comboAttackTimer = 0;
    float comboAttackTimerCooldown = 6f;

    float backStabAttackTimer = 0;
    float backStabAttackTimerCooldown = 12f;
    List<GameObject> backStabEnemyList = new List<GameObject>();

    public override bool DoMove(float distance)
    {
        if (base.DoMove(distance))
        {
            return true;
        }
        if(comboAttackTimer == 0)
        {
            comboAttackTimer = Time.time + 4f; // first delay before you can use your comboAttack
        }
        // do combo attack if it is not on cooldown and target is in range
        if(Time.time >= comboAttackTimer && distance < hero.stats.attackRange)
        {
            DoComboAttack();
            comboAttackTimer = Time.time + comboAttackTimerCooldown;
            return true;
        }
        Vector3 toTarget = (transform.position - heroController.target.transform.position).normalized;
        if (Vector3.Dot(heroController.target.transform.forward, toTarget) <= 0)
        {
            // check if i already backstabbed the enemy and if my backstack is no longer on cooldown
            if (backStabEnemyList.Contains(heroController.target))
            {
                if (Time.time >= backStabAttackTimer)
                {
                    DoBackStabAttack();
                    backStabAttackTimer = Time.time + backStabAttackTimerCooldown;
                    return true;
                }
            }
            // otherwise backstab enemy and add them to my backstabbed list
            else
            {
                DoBackStabAttack();
                backStabAttackTimer = Time.time + backStabAttackTimerCooldown;
                backStabEnemyList.Add(heroController.target);
                return true;
            }
        }
        return false;
    }

    public override void UseUltimate()
    {
        ThrowDagger();
        base.UseUltimate();
    }

    void DoComboAttack()
    {
        championAnimation.DoAbilityOne(true);
        StartCoroutine(DamageTarget(heroController.target,20,true,false));
        Debug.Log("combo Attack");
    }
    void DoBackStabAttack()
    {
        championAnimation.DoAbilityTwo(true);
        StartCoroutine(DamageTarget(heroController.target,30, true,false));
        Debug.Log("backstabbing");
    }

    public void ThrowDagger()
    {
        Debug.Log("throwing dagger");
        // throw dagger at lowest hp enemy
        GameObject projectile = Instantiate(dagger);
        projectile.transform.position = projectileStart.transform.position;
        projectile.GetComponent<Projectile>().Init(heroController.target,4);

        // wait some time, then dash behind target
        StartCoroutine(Jump());
    }


    IEnumerator Jump()
    {
        isUntargettable = true;
        isMoveable = false;
       
        transform.position = new Vector3(100f, 100f, 100f);

        yield return new WaitForSeconds(1f);
        //jump to furthest neighbour tile of target tile

        heroController.target.GetComponent<HeroController>().GetCurrentTile().neighbours.Reverse();
        foreach (Tile possibleTargets in heroController.target.GetComponent<HeroController>().GetCurrentTile().neighbours)
        {
            if (!possibleTargets.heroOnTile && !MoveCalculator.nextStepList.Contains(possibleTargets))
            {
                transform.position = MapGenerator.tileMap[possibleTargets.position.getX(), possibleTargets.position.getY()].gameObject.transform.position;
                heroController.SetCurrentTile(possibleTargets);
                break;
            }
        }
        isUntargettable = false;
        isMoveable = true;
        //this.transform.position = target.transform.position - target.transform.forward * 2f;
    }

}
