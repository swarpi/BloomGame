using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoxisHeroController : HeroControllerCombat
{
    float cocoonStunTimer = 0;
    float cocoonStunTimerCooldown = 20f;
    float cocoonStunTimerDuration = 2f;

    int autoAttackCount = 0;
    public override bool DoMove(float distance)
    {
        if (base.DoMove(distance))
        {
            return true;
        }
        if (cocoonStunTimer == 0)
        {
            cocoonStunTimer = Time.time + cocoonStunTimerCooldown; // first delay before you can use your comboAttack
        }
        if (Time.time >= cocoonStunTimer)
        {
            DoCocoonStun();
            cocoonStunTimer = Time.time + cocoonStunTimerCooldown;
            return true;
        }
        return false;
    }

    public void DoCocoonStun()
    {
        championAnimation.DoAbilityOne(true);
        heroController.target.GetComponent<HeroControllerCombat>().OnGotStun(cocoonStunTimerDuration,hero.stats.effectivity);
    }

    public override void DoAttack()
    {

        heroControllerMovement.RotateTowards(heroController.target.transform.position);
        heroController.currentCombatState = AICombatState.InCombat;
        bool isCrit = false;
        float randValue = UnityEngine.Random.value;
        if (randValue <= hero.stats.critChance)
            isCrit = true;
        else isCrit = false;
        autoAttackCount++;
        if (autoAttackCount % 3 == 0)
        {
            Debug.Log("doing third attack");
            championAnimation.DoAbilityTwo(true);
            // poison enemy
            // or deal extra damage
        }
        else
            championAnimation.DoAttack(true, isCrit);
       
        if (isBlinded)
            StartCoroutine(DamageTarget(heroController.target, 0, isCrit, false, 0.8f, true)); // need to add miss indicator
        else
            StartCoroutine(DamageTarget(heroController.target, autoAttackDamage, isCrit, false, 0.8f, true));
    }

    public override void UseUltimate()
    {
        base.UseUltimate();
        championAnimation.DoUltimate(true);
        // instantiate spider web over game field
        foreach(GameObject enemy in AIopponent.aiHeroList)
        {
            // slow them, reduce their resistence or deal damage over time, make them more vulnerable => take more damage from every source
        }
    }
}
