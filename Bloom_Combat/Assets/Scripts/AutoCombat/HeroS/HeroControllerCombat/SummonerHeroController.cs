using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SummonerHeroController : HeroControllerCombat
{
    public Hero myPet;
    public Transform summonSpot;
    public Transform altSummonSpot;
    HeroController petController;
    HeroControllerCombat petControllerCombat;
    public override void UseUltimate()
    {
        if (petController == null)
        {
            SummonMyPet();
        }
        else if(petControllerCombat != null && petControllerCombat.isDead)
        {
            SummonMyPet();
        }
        else Debug.Log("combined attack");
        base.UseUltimate();
    }
    public void SummonMyPet()
    {
        // need to fix spawn pos of pet
        GameObject pet = Instantiate(myPet.prefab);
        //pet.transform.SetParent(null);
        petController = pet.gameObject.GetComponent<HeroController>();
        petControllerCombat = pet.gameObject.GetComponent<HeroControllerCombat>();
        petController.Init(myPet, this.teamID);
        petController.SetCurrentTile(FindSummonSpot(heroController.GetCurrentTile().neighbours));
        //petController.currentTilePos.AddHero(petController);
        petController.transform.position = MapGenerator.tileMap[petController.GetCurrentTile().position.X, petController.GetCurrentTile().position.getY()].transform.position;
        //gridTargetPosition = summonSpot.position;
        petController.OnCombatStart();
        // player
        if (teamID == 0) AutoCombatField.heroList.Add(pet);
        // opponent
        else if (teamID == 1) AIopponent.aiHeroList.Add(pet);

        //pet.transform.SetParent(this.gameObject.transform);
    }

    public Tile FindSummonSpot(List<Tile> potentialSpots)
    {
        
        foreach (Tile potentialTile in potentialSpots)
        {
            if (potentialTile.heroOnTile == null && !MoveCalculator.nextStepList.Contains(potentialTile))
            {
                return potentialTile;
            }
        }
        foreach (Tile potentialTile in potentialSpots)
        {
            foreach (Tile potentialTile2 in potentialTile.neighbours)
            {
                if (potentialTile2.heroOnTile == null && !MoveCalculator.nextStepList.Contains(potentialTile2))
                {
                    return potentialTile2;
                }
            }
            return FindSummonSpot(potentialTile.neighbours);
        }
        return null;
    }

    //public override bool OnGotHit(float damage)
    //{
    //    currentHealth -= damage;
    //    // increase Loo
    //    currentLoo += hero.stats.looPerHitTaken;
    //    //add floating text
    //    worldCanvasController.AddDamageText(this.transform.position + new Vector3(0, 2.5f, 0), damage,false);

    //    //death
    //    if (currentHealth <= 0)
    //    {
    //        isDead = true;
    //        isPetDead = true;
    //        aIopponent.OnChampionDeath();
    //        autoCombatController.OnChampionDeath();
    //        this.gameObject.SetActive(false);
    //        currentTilePos.RemoveHero();
    //        petController.OnGotHit(float.MaxValue);
    //        //Destroy(this.gameObject);
    //    }
    //    return isDead;
    //}
}
