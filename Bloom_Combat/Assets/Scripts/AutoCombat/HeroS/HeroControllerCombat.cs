using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class HeroControllerCombat : MonoBehaviour
{
    // components
    [HideInInspector]
    public HeroController heroController;
    [HideInInspector]
    public HeroControllerMovement heroControllerMovement;
    // Start is called before the first frame update
    AutoCombatController autoCombatController;
    [HideInInspector]
    public ChampionAnimation championAnimation;
    [HideInInspector]
    public UIController uiController;
    public int teamID;
    public GameObject projectileStart;
    // hero infos
    [HideInInspector]
    public Hero hero;
    [HideInInspector]
    public string heroID;
    [HideInInspector]
    public string heroName;

    [HideInInspector]
    ///Maximum health of the champion
    public float maxHealth = 0;

    ///current health of the champion 
    public float currentHealth = 0;

    ///current health of the champion 
    public float currentShield = 0;
    [HideInInspector]
    ///Maximum health of the champion
    public float maxLoo = 0;
    [HideInInspector]
    ///current health of the champion 
    public float currentLoo = 0;
    [HideInInspector]
    ///Current damage of the champion deals with a attack
    public float autoAttackDamage = 0;

    // combat infos
    public bool isAttacking = false;
    public bool isDead = false;
    public bool isUntargettable = false;
    public bool isMoveable = true;
    [HideInInspector]
    public bool isPetDead = true;

    public bool isStuned = false;
    public float stunTimer = 0;

    public bool isSleeping = false;
    public float sleepTimer = 0;

    public bool isBlinded = false;
    public float blindTimer = 0;


    private void Start()
    {
        heroControllerMovement = GetComponent<HeroControllerMovement>();
        championAnimation = GetComponent<ChampionAnimation>();
        isSleeping = false;
    }
    public void Init(HeroController _heroController,Hero _hero,int _teamID)
    {
        heroController = _heroController;
        heroControllerMovement = heroController.heroControllerMovement;
        championAnimation = heroController.championAnimation;
        uiController = heroController.uiController;
        autoCombatController = heroController.autoCombatController;

        hero = _hero;
        teamID = _teamID;
        heroName = _hero.heroID;
        heroID = _hero.heroID + Guid.NewGuid();

        maxHealth = hero.stats.health;
        currentHealth = hero.stats.health;
        maxLoo = hero.stats.loo;
        currentLoo = hero.stats.looStart;
        autoAttackDamage = hero.stats.attackDamage;
    }
    public virtual bool DoMove(float distance)
    {
        //isAttacking = true;
        if (currentLoo >= maxLoo && autoCombatController.isAutoMode == true)
        {
            UseUltimate();
            //currentLoo -= maxLoo;
            return true;
        }
        //isAttacking = false;
        return false;
    }
    /// <summary>
    /// Start attack against enemy champion
    /// </summary>
    public virtual void DoAttack()
    {
        heroControllerMovement.RotateTowards(heroController.target.transform.position);
        heroController.currentCombatState = AICombatState.InCombat;
        bool isCrit = false;
        float randValue = UnityEngine.Random.value;
        if (randValue <= hero.stats.critChance)
            isCrit = true;
        else isCrit = false;
        championAnimation.DoAttack(true, isCrit);
        if (isBlinded)
            StartCoroutine(DamageTarget(heroController.target, 0, isCrit, false, 0.8f,true)); // need to add miss indicator
        else
            StartCoroutine(DamageTarget(heroController.target, autoAttackDamage, isCrit, false, 0.8f,true));
        
    }

    public virtual IEnumerator DamageTarget(GameObject damageTarget, float damage, bool isCrit = false, bool isMagic = false, float delay = 0.5f,bool isAutoAttack = false, bool isRange = false)
    {
        // delay depending on animation speed or until projectil hit the target
        // try anim.GetCurrentAnimatorStateInfo(layer).length for the animation duration
        if (isRange)
        {
            // wait until projectile hit the target
        }
        else
        {
            yield return new WaitForSeconds(delay);
        }

        if (heroController.target != null)
        {
            currentLoo += hero.stats.looPerHitGiven;
            HeroControllerCombat targetHero = damageTarget.GetComponent<HeroControllerCombat>();
            float damageGiven = DamageCalculator.CalculateDamageGiven(heroController.hero, damage, isCrit);

            bool isTargetDead = targetHero.OnGotHit(heroController, damageGiven, isCrit, isMagic, isAutoAttack);

            if (isTargetDead)
                heroController.TryAttackNewTarget();
        }
        yield return null;
    }
    /// <summary>
    /// Called when this champion takes damage
    /// </summary>
    /// <param name="damage"></param>
    public virtual bool OnGotHit(HeroController source, float damage, bool isCrit, bool isMagic, bool isAutoAttack)
    {
        float damageTaken = DamageCalculator.CalculateDamageTaken(source, heroController, damage, isMagic);
        if(currentShield > 0)
        {
            float overflow = currentShield - damageTaken;
            currentShield -= damageTaken;
            if (overflow < 0)
            {
                currentHealth -= Math.Abs(overflow);
            }
        }
        else
        {
            // wake me up if i am sleeping
            if (isSleeping) isSleeping = false;
            currentHealth -= damageTaken;
        }
        
        // increase Loo
        currentLoo += hero.stats.looPerHitTaken;
        //add floating text
        heroController.worldCanvasController.AddDamageText(this.transform.position + new Vector3(0, 2.5f, 0), damageTaken, isCrit);
        //death
        if (currentHealth <= 0)
        {
            isDead = true;
            isPetDead = true;
            heroController.aIopponent.OnChampionDeath();
            autoCombatController.OnChampionDeath();
            championAnimation.DoDeath();
            heroController.ResetHeroTile();
            if(teamID == 0)
            {
                AutoCombatField.heroList.Remove(this.gameObject);
            }
            if (teamID == 1)
            {
                AIopponent.aiHeroList.Remove(this.gameObject);

            }
        }
        return isDead;
    }
    public virtual void UseUltimate()
    {
        Debug.Log("ultimate used " + hero.name);
        currentLoo = 0;
        //isAttacking = false;
    }
    /// <summary>
    /// Called when attack animation finished
    /// </summary>
    public void OnAttackAnimationFinished()
    {
        //isAttacking = false;

        if (heroController.target != null)
        {
            //create projectile if have one
            if (hero.attackProjectile != null && projectileStart != null)
            {
                GameObject projectile = Instantiate(hero.attackProjectile);
                projectile.transform.position = projectileStart.transform.position;

                projectile.GetComponent<Projectile>().Init(heroController.target,0);
            }
        }
    }
    /// <summary>
    /// Called when A1 animation finished
    /// </summary>
    public virtual void OnAbilityOneFinished()
    {
        OnAttackAnimationFinished();
    }
    /// <summary>
    /// Called when A2 animation finished
    /// </summary>
    public virtual void OnAbilityTwoFinished()
    {
        OnAttackAnimationFinished();
    }
    /// <summary>
    /// Called when Ulti animation finished
    /// </summary>
    public virtual void OnUltimateFinished()
    {
        OnAttackAnimationFinished();
    }
    public virtual float CalculateDamageTaken(float damage, bool isMagic)
    {
        float damageTaken = damage;
        if (isMagic)
        {
            return damageTaken = damage * (100 / (100 + hero.stats.magicResist));
        }
        else
        {
            return damageTaken = damage * (100 / (100 + hero.stats.armor));
        }
    }
    public void CreateShield(float amount,float duration = -1)
    {
        currentShield = amount;
        if(duration > 0)
        {
            StartCoroutine(CreateTemporaryShield(duration));
        }
    }
    IEnumerator CreateTemporaryShield(float duration)
    {
        yield return new WaitForSeconds(duration);
        currentShield = 0f;
    }
    /// <summary>
    /// Called when this champion get stuned
    /// </summary>
    /// <param name="stunEffectPrefab"></param>
    public void OnGotStun(float duration, float enemyEffectivity)
    {
        isStuned = true;
        stunTimer = CalculateCCDuration(duration,enemyEffectivity);

        championAnimation.IsAnimated(false);
    }

    public void OnGotBlinded(float duration, float enemyEffectivity)
    {
        isBlinded = true;
        blindTimer = CalculateCCDuration(duration, enemyEffectivity);
    }

    public void OnGotSlept(float duration, float enemyEffectivity)
    {
        isSleeping = true;
        sleepTimer = CalculateCCDuration(duration, enemyEffectivity);

        championAnimation.IsAnimated(false);
    }

    public void OnGotCharmed(float duration, float enemyEffectivity)
    {
        float charmDuration = CalculateCCDuration(duration, enemyEffectivity);
        StartCoroutine(Charmed(charmDuration));
    }
    IEnumerator Charmed(float charmDuration)
    {
        heroController.target = heroController.FindTarget(Mathf.Abs(teamID - 1));
        yield return new WaitForSeconds(charmDuration);
        heroController.target = heroController.FindTarget(Mathf.Abs(teamID));
    }

    public float CalculateCCDuration(float ccBaseDuration, float enemyEffectivity)
    {
        float ccDuration = 0f;
        // -1 for ignore eff/tenacity
        if(enemyEffectivity == -1)
        {
            return ccBaseDuration;
        }
        float calculatedDuration = (enemyEffectivity - hero.stats.tenacity) / 2;
        // if i have more than 100 effectivity than the enemy, increase duration by 50%
        if(calculatedDuration > 50)
        {
            ccDuration = ccBaseDuration * 1.5f;
        }
        // if i have less than 100 effectivity than the enemy, decrease duration by 50%
        else if(calculatedDuration < -50)
        {
            ccDuration = ccBaseDuration * 0.5f;
        }
        // else calculate the duration and add it to 1... 0.4 => 1.4 / -0.3 => 0.7
        else
        {
            ccDuration = ccBaseDuration *  (1 + (calculatedDuration / 100));
        }
        return ccDuration;
    }
    /// <summary>
    /// Called when this champion get healed
    /// </summary>
    /// <param name="stunEffectPrefab"></param>
    public void OnGotHeal(float f)
    {
        Debug.Log(heroName + " got healed");
        currentHealth += f;
    }



    ///// <summary>
    ///// Add effect to this champion
    ///// </summary>
    //public void AddEffect(GameObject effectPrefab, float duration)
    //{
    //    if (effectPrefab == null)
    //        return;

    //    //look for effect
    //    bool foundEffect = false;
    //    foreach (Effect e in effects)
    //    {
    //        if (effectPrefab == e.effectPrefab)
    //        {
    //            e.duration = duration;
    //            foundEffect = true;
    //        }
    //    }

    //    //not found effect
    //    if (foundEffect == false)
    //    {
    //        Effect effect = this.gameObject.AddComponent<Effect>();
    //        effect.Init(effectPrefab, this.gameObject, duration);
    //        effects.Add(effect);
    //    }

    //}

    ///// <summary>
    ///// Remove effect when expired
    ///// </summary>
    //public void RemoveEffect(Effect effect)
    //{
    //    effects.Remove(effect);
    //    effect.Remove();
    //}
}
