using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class HeroControllerMovement : MonoBehaviour
{
    float speed = 4.5f;
    HeroController heroController;
    HeroControllerCombat heroControllerCombat;
    private void Start()
    {
        heroController = GetComponent<HeroController>();
        heroControllerCombat = GetComponent<HeroControllerCombat>();
        heroControllerCombat.isMoveable = true;
    }
    public void MoveTowards(Tile target)
    {
        StopCoroutine(WalkForward(target));
        StartCoroutine(WalkForward(target));
    }
    IEnumerator WalkForward(Tile target)
    {
        if ((heroController.currentCombatState == AICombatState.LookingForTarget) && !heroControllerCombat.isDead) 
        { 
            heroController.nextTile = target;
            Vector3 currentWayPoint = MapGenerator.allMapGridPositions[target.position.getX(), target.position.getY()];
            while (true)
            {
                if (0.2 > Vector3.Distance(transform.position, currentWayPoint))
                {
                    heroController.SetCurrentTile(MapGenerator.tileMap[target.position.getX(), target.position.getY()]);
                    // maybe announce that i have reached my target, so the next steps can be calculated
                    yield break;
                }
                if (heroControllerCombat.isDead == true || heroControllerCombat.isMoveable == false)
                {
                    yield break;
                }
                else if (heroController.target == null)
                {
                    yield break;
                }
                //rotate towards target
                RotateTowards(currentWayPoint);
                //transform.position = currentWayPoint;
                transform.position = Vector3.MoveTowards(transform.position, currentWayPoint, speed * Time.deltaTime);
                // stop if i am dead or my target is dead

                yield return null;
            }
        }
        else
        {
            yield break;
        }
    }

    public void RotateTowards(Vector3 position)
    {
        Vector3 _direction = (position - transform.position);

        //create the rotation we need to be in to look at the target
        Quaternion _lookRotation = Quaternion.LookRotation(_direction);

        //rotate us over time according to speed until we are in the required rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * 5);

        //Vector3 newLookRotation = Vector3.RotateTowards(transform.forward, _direction, Time.deltaTime * 5, 0.0f);
        //transform.rotation = Quaternion.LookRotation(newLookRotation);
    }
}
