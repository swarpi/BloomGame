﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls Arrows, Spells movement from point A to B
/// </summary>
public class Projectile : MonoBehaviour
{
    ///Movement speed of the projectile
    public float speed;

    ///Time to wait destroying this projectile after reached the target
    public float duration;

    public float distance = 0f;

    private GameObject target;
    IProjectileUser projectileSource;

    private bool isMoving = false;
    // 0 AA
    // 1 ability one
    // 2 ability two
    // 3 ultimate
    public int projectileType = -1;
    
    /// <summary>
    /// Called when projectile created
    /// </summary>
    /// <param name="_target"></param>
    public void Init(GameObject _target, int _projectileType, IProjectileUser _projectileSource = null)
    {
        target = _target;
        projectileType = _projectileType;
        projectileSource = _projectileSource;
        isMoving = true;
    }

    /// Update is called once per frame
    public virtual void Update()
    {
      
        if (isMoving)
        {
            if (target == null)
            {
                Destroy(this.gameObject);
                return;
            }

            Vector3 relativePos = target.transform.position - transform.position;

            // the second argument, upwards, defaults to Vector3.up
            Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
            this.transform.rotation = rotation;


            Vector3 targetPosition = target.transform.position + new Vector3(0, 1, 0);

            float step = speed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(this.transform.position, targetPosition, step);


            distance = Vector3.Distance(this.transform.position, targetPosition);

            if (distance < 0.2f)
            {
               // isMoving = false;

                this.transform.parent = target.transform;

                if (projectileSource == null) {
                    Destroy(this.gameObject, duration);
                    return;
                }
                    
                switch (projectileType)
                {
                    case 0:
                        projectileSource.OnProjectileHitAutoAttack(target); 
                        break;
                    case 1:
                        projectileSource.OnProjectileHitAbilityOne(target);
                        break;
                    case 2:
                        projectileSource.OnProjectileHitAbilityTwo(target);
                        break;
                    case 3:
                        projectileSource.OnProjectileHitUltimate(target);
                        break;
                }
                Destroy(this.gameObject, duration);
            }
           
        }

    }
}
