using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AutoCombatSynergies : MonoBehaviour
{
    // Start is called before the first frame update
    public static List<HeroClassBonus> allHeroClassBonuses;
    public static List<HeroOriginBonus> allHeroOriginBonuses;

    public static HeroClassBonus IdentifyClassBonus(HeroClass heroClass)
    {
        return allHeroClassBonuses.Find(classBonus => classBonus.heroClass == heroClass);
    }
    public static HeroOriginBonus IdentifyOriginBonus(HeroOrigin heroOrigin)
    {
        return allHeroOriginBonuses.Find(originBonus => originBonus.heroOrigin == heroOrigin);
    }


    void Start()
    {
        InstantiateLists();
    }

    void InstantiateLists()
    {
        HeroClassBonus[] tempHeroClassArray = GetComponents<HeroClassBonus>();
        allHeroClassBonuses = new List<HeroClassBonus>();
        allHeroClassBonuses = tempHeroClassArray.ToList();
        HeroOriginBonus[] tempHeroOriginArray = GetComponents<HeroOriginBonus>();
        allHeroOriginBonuses = new List<HeroOriginBonus>();
        allHeroOriginBonuses = tempHeroOriginArray.ToList();
    }

    public static void CalculateBonuses(out Dictionary<HeroClassBonus, int> activeClassBonuses, out Dictionary<HeroOriginBonus, int> activeOriginBonuses, List<GameObject> heroList)
    {
        Dictionary<HeroClassBonus, int>  tempActiveClassBonuses = new Dictionary<HeroClassBonus, int>();
        Dictionary<HeroOriginBonus, int> tempActiveOriginBonuses = new Dictionary<HeroOriginBonus, int>();
        foreach (GameObject heroGo in heroList)
        {
            Hero hero = heroGo.GetComponent<HeroController>().hero;
/*            // class identifier
            HeroClassBonus heroClass = AutoCombatSynergies.IdentifyClassBonus(hero.heroClass);
            // if the class didnt exist add it with value 1
            if (!tempActiveClassBonuses.ContainsKey(heroClass))
            {
                tempActiveClassBonuses.Add(heroClass, 1);
            }
            // else increment the number of elements on the field
            else
            {
                tempActiveClassBonuses[heroClass]++;
            }*/

            // Origin identifier
            HeroOriginBonus heroOrigin = AutoCombatSynergies.IdentifyOriginBonus(hero.heroOrigin);
            // if the class didnt exist add it with value 1
            if (!tempActiveOriginBonuses.ContainsKey(heroOrigin))
            {
                tempActiveOriginBonuses.Add(heroOrigin, 1);
            }
            // else increment the number of elements on the field
            else
            {
                tempActiveOriginBonuses[heroOrigin]++;
            }
            // do the same if the hero has two classes
            // is true if the origin isnt none
            if (hero.heroOrigin_2 != HeroOrigin.None)
            {
                HeroOriginBonus heroOrigin2 = AutoCombatSynergies.IdentifyOriginBonus(hero.heroOrigin_2);
                if (!tempActiveOriginBonuses.ContainsKey(heroOrigin2))
                {
                    tempActiveOriginBonuses.Add(heroOrigin2, 1);
                }
                // else increment the number of elements on the field
                else
                {
                    tempActiveOriginBonuses[heroOrigin2]++;
                }
            }
        }
        activeClassBonuses = tempActiveClassBonuses;
        activeOriginBonuses = tempActiveOriginBonuses;
    }


}
