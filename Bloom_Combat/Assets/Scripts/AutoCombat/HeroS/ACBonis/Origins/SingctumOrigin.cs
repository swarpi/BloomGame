using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingctumOrigin : HeroOriginBonus
{
    // Start is called before the first frame update
    void Start()
    {
        heroOrigin = HeroOrigin.Singctum;
        displayName = "Singctum";
        requiredNumberEffect1 = 1;
        requiredNumberEffect2 = 4;
    }

    protected override void ApplyOriginEffectInCombat(List<GameObject> applicableHeroList)
    {
        Debug.Log("Singctum bonus");
    }
}
