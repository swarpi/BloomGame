using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KiteahOrigin : HeroOriginBonus
{
    // Start is called before the first frame update
    void Start()
    {
        heroOrigin = HeroOrigin.Kiteah;
        displayName = "Kiteah";
        requiredNumberEffect1 = 2;
        requiredNumberEffect2 = 4;
    }

    protected override void ApplyOriginEffectInCombat(List<GameObject> applicableHeroList)
    {
        Debug.Log("kiteah bonus");
    }

}
