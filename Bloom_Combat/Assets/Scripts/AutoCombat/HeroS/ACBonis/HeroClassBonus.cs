using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum HeroClass
{
    None,
    Chimera,
    Shapeshifter,
    Primals,
    Looweaver,
    Dimensioncrawler,
    Guardion,
    Highlander,
    Headhunter,
    Summoner,
    Beastmaster
}
[System.Serializable]
public class HeroClassBonus : MonoBehaviour
{
    public HeroClass heroClass;
    public int requiredNumberEffect1 = 1;
    public int requiredNumberEffect2 = -1;

    ///Displayed name on UI
    public string displayName = "name";

    ///Displayed sprite on the UI
    public Sprite icon;
    public bool ActivateClassEffect(int numberOnField, List<GameObject> applicableHeroList)
    {
        // if condition met
        if(numberOnField >= requiredNumberEffect1)
        {
            ApplyClassEffectInCombat(applicableHeroList);
            return true;
        }
        return false;
       
    }

    protected virtual void ApplyClassEffectInCombat(List<GameObject> applicableHeroList)
    {

    }





}
