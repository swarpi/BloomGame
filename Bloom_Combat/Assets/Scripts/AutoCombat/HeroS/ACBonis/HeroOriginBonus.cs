using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum HeroOrigin
{
    None,
    Singctum,
    Kiteah,
    Growthmoor,
    Borum,
    Arok
}

public class HeroOriginBonus : MonoBehaviour
{
    public HeroOrigin heroOrigin;
    public int requiredNumberEffect1 = 1;
    public int requiredNumberEffect2 = -1;
    public int requiredNumberEffect3 = -1;

    ///Displayed name on UI
    public string displayName = "name";

    ///Displayed sprite on the UI
    public Sprite icon;

    public bool ActivateOriginEffect(int numberOnField, List<GameObject> applicableHeroList)
    {
        // if condition met
        if (numberOnField >= requiredNumberEffect1)
        {
            ApplyOriginEffectInCombat(applicableHeroList);
            return true;
        }
        return false;

    }

    protected virtual void ApplyOriginEffectInCombat(List<GameObject> applicableHeroList)
    {

    }

}
