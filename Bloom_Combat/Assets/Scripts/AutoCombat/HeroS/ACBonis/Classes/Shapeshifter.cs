using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shapeshifter : HeroClassBonus
{
    void Start()
    {
        heroClass = HeroClass.Shapeshifter;
        displayName = "Shapeshifter";
    }
    protected override void ApplyClassEffectInCombat(List<GameObject> applicableHeroList)
    {
        // give pets additional effects
        Debug.Log("shapeShifter bonus activated");
    }
}
