using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCalculator
{
    public static float CalculateDamageGiven(Hero hero, float baseDamage, bool isCrit)
    {
        // maybe calculate any bonus damages
        float critDamage = baseDamage;
        if (isCrit)
        {
            critDamage = baseDamage * hero.stats.critDamage; // change with critdamage 
        }
        return critDamage;
        // need to calculate elemental bonus damage if it is magic damage
    }

    public static float CalculateDamageTaken(HeroController damageSource, HeroController damageReciever,float damage, bool isMagic)
    {
        float damageTaken = damage;
        if (isMagic)
        {
            return damageTaken = damage * (100 / (100 + damageReciever.hero.stats.magicResist));
        }
        else
        {
            return damageTaken = damage * (100 / (100 + damageReciever.hero.stats.armor));
        }
    }

    public static float CalculateDamageTaken2D(HeroController2D damageSource, HeroController2D damageReciever, float damage, bool isMagic)
    {
        float damageTaken = damage;
        if (isMagic)
        {
            return damageTaken = damage * (100 / (100 + damageReciever.hero.stats.magicResist));
        }
        else
        {
            return damageTaken = damage * (100 / (100 + damageReciever.hero.stats.armor));
        }
    }
}
