using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Tile : MonoBehaviour, IHeapItem<Tile>
{
    // 0 for player field, 1 for neutral field, 2 for enemy field
    public int tileType;
    //g cost = distance from starting node
    public int gCost;
    //h cost (heuristic) = distance from end node
    public int hCost;
    // f cost == g + h
    // choose the lowest f cost
    public bool passable = true;
    public string tileName;
    public Point position;
    public List<Tile> neighbours = new List<Tile>();
    public HeroController heroOnTile;
    public Tile parent;
    public int extraCost;
    public Dictionary<string, Tile> heroIDparent = new Dictionary<string, Tile>();
    int heapIndex;
    public List<HeroController> heroList = new List<HeroController>();
    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }


    public bool AddHero(HeroController heroController)
    {
        if (heroOnTile == null)
        {
            heroOnTile = heroController;
            passable = false;
            return true;
        }
        else return false;
        //heroOnTile = herocontroller;

    }
    public bool RemoveHero()
    {
        if (heroOnTile != null)
        {
            heroOnTile = null;
            extraCost = 0;
            passable = true;

            return true;
        }
        else return false;

    }

    public int HeapIndex
    {
        get
        {
            return heapIndex;
        }
        set
        {
            heapIndex = value;
        }
    }

    public int CompareTo(Tile tileToCompare)
    {
        int compare = fCost.CompareTo(tileToCompare.fCost);
        if(compare == 0)
        {
            compare = hCost.CompareTo(tileToCompare.hCost);
        }
        return -compare;
    }

}
