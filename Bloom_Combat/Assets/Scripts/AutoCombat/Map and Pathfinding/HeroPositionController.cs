using UnityEngine;

public class HeroPositionController : MonoBehaviour
{
    /// <summary>
    /// Assign new grid position
    /// </summary>
    /// <param name="_gridPositionX"></param>
    /// <param name="_gridPositionZ"></param>
    /// <param name="heroController"></param>
    public static void SetGridPosition(int _gridPositionX, int _gridPositionZ, HeroController heroController)
    {
        heroController.gridPositionX = _gridPositionX;
        heroController.gridPositionZ = _gridPositionZ;


        //set new target when chaning grid position
        heroController.gridTargetPosition = GetWorldPosition(heroController);
    }
    /// <summary>
    /// Convert grid position to world position
    /// </summary>
    /// <returns></returns>
    public static Vector3 GetWorldPosition(HeroController heroController)
    {
        //get world position
        Vector3 worldPosition = Vector3.zero;
        if (heroController.teamID == 1)
            worldPosition = MapGenerator.enemyMapGridPositions[heroController.gridPositionX, heroController.gridPositionZ];
        else
            worldPosition = MapGenerator.playerMapGridPositions[heroController.gridPositionX, heroController.gridPositionZ];

        return worldPosition;
    }

    /// <summary>
    /// Move to corrent world position
    /// </summary>
    public static void SetWorldPosition(HeroController heroController)
    {

        //get world position
        Vector3 worldPosition = GetWorldPosition(heroController);
        
        //this.transform.position = worldPosition;

        heroController.gridTargetPosition = worldPosition;
    }

    /// <summary>
    /// Set correct rotation
    /// </summary>
    public static void SetWorldRotation(HeroController heroController)
    {
        Vector3 rotation = Vector3.zero;

        if (heroController.teamID == 1)
        {
            rotation = new Vector3(0, 200, 0);
        }
        else if (heroController.teamID == 0)
        {
            rotation = new Vector3(0, 20, 0);
        }

        heroController.gameObject.transform.rotation = Quaternion.Euler(rotation);
    }
}
