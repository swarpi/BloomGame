using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PathRequestManager : MonoBehaviour
{

	static Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();
	static PathRequest currentPathRequest;

	static PathRequestManager instance;

	static bool isProcessingPath;

	void Awake()
	{
		instance = this;
	}

	public static void RequestPath(Tile pathStart, Tile pathEnd, Action<List<Tile>, bool> callback)
	{
		PathRequest newRequest = new PathRequest(pathStart, pathEnd, callback);
		pathRequestQueue.Enqueue(newRequest);
		TryProcessNext();
	}

	static void TryProcessNext()
	{
		if (!isProcessingPath && pathRequestQueue.Count > 0)
		{
			currentPathRequest = pathRequestQueue.Dequeue();
			isProcessingPath = true;
			//PathFinder.FindPath(currentPathRequest.pathStart, currentPathRequest.pathEnd);
		}
	}

	public static void FinishedProcessingPath(List<Tile> path, bool success)
	{
		currentPathRequest.callback(path, success);
		isProcessingPath = false;
		TryProcessNext();
	}

	struct PathRequest
	{
		public Tile pathStart;
		public Tile pathEnd;
		public Action<List<Tile>, bool> callback;

		public PathRequest(Tile _start, Tile _end, Action<List<Tile>, bool> _callback)
		{
			pathStart = _start;
			pathEnd = _end;
			callback = _callback;
		}

	}
}