using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class AutoCombatField : MonoBehaviour
{
    /// <summary>
    /// Logic concerning the field during combat and during prep
    /// </summary>
    /// 
    [HideInInspector]
    public static GameObject[,] gridHeroArray;
    // list of all player heroes in combat;
    public static List<GameObject> heroList = new List<GameObject>();
    public static Dictionary<HeroClassBonus, int> activeClassBonuses;
    public static Dictionary<HeroOriginBonus, int> activeOriginBonuses;
    public static UIController uIController;

    private void Start()
    {
        gridHeroArray = new GameObject[MapGenerator.hexMapSizeX / 2, MapGenerator.hexMapSizeZ];
       
        //inputController = GetComponent<InputController>();
        uIController = GetComponent<UIController>();
    }
    public static bool PlaceHeroOnBoard(Hero hero)
    {
        if (HeroOnBoard(hero)) return false;
        //get first empty inventory slot
        int emptyIndexX = -1;
        int emptyIndexZ = -1;
        // check if spot is occupied, if no set emptyindex to that position
        for (int i = 0; i < gridHeroArray.GetLength(0); i++)
        {
            for (int j = 0; j < gridHeroArray.GetLength(1); j++)
            {
                if (gridHeroArray[i, j] == null && MapGenerator.isLegalFieldPlayer(i, j))
                {
                    emptyIndexX = i;
                    emptyIndexZ = j;
                    break;
                }
                // need to break out of second for
            }
        }

        //return if no slot to add champion
        if (emptyIndexX == -1 || emptyIndexZ == -1)
            return false;

        //instantiate champion prefab
        GameObject heroPrefab = Instantiate(hero.prefab);

        //get championController
        HeroController heroController = heroPrefab.GetComponent<HeroController>();

        //setup chapioncontroller
        heroController.Init(hero, HeroController.TEAMID_PLAYER);

        //set grid position
        //heroController.SetGridPosition(Map.GRIDTYPE_HEXA_MAP, emptyIndexX, emptyIndexZ);
        // HeroPositionController.SetGridPosition(emptyIndexX, emptyIndexZ, heroController);

        //set position and rotation
        HeroPositionController.SetWorldPosition(heroController);
        HeroPositionController.SetWorldRotation(heroController);
        //store champion in inventory array
        StoreChampionInArray(emptyIndexX, emptyIndexZ, heroPrefab);


        //set gold on ui
        //uIController.UpdateUI();

        return true;
    }
    public static bool HeroOnBoard(Hero hero)
    {
        if (heroList.Any(x => x.GetComponent<HeroController>().heroName == hero.heroID))
        {
            Debug.Log(hero.heroID + "is already on board");
            return true;
        }
        else return false;
    }

    /// <summary>
    /// Store champion gameobject in array
    /// </summary>
    /// <param name="triggerinfo"></param>
    /// <param name="champion"></param>
    public static void StoreChampionInArray(int gridX, int gridZ, GameObject hero)
    {
        //assign current trigger to champion
        HeroController heroController = hero.GetComponent<HeroController>();
        //heroController.SetGridPosition(gridType, gridX, gridZ);
        HeroPositionController.SetGridPosition(gridX, gridZ, heroController);
        gridHeroArray[gridX, gridZ] = hero;
        if (!heroList.Contains(hero))
            heroList.Add(hero);
        CalculateBonuses();


    }
    /// <summary>
    /// Remove champion from array
    /// </summary>
    /// <param name="triggerinfo"></param>
    public static void RemoveChampionFromArray(int gridX, int gridZ, GameObject hero)
    {
        gridHeroArray[gridX, gridZ] = null;
        //gridHeroArrayAtStartOfCombat[gridX, gridZ] = null;
        //hero.GetComponent<HeroController>().currentTilePos.RemoveHero();
        heroList.Remove(hero);
        CalculateBonuses();
    }

    /// <summary>
    /// Get champion gameobject from triggerinfo
    /// </summary>
    /// <param name="triggerinfo"></param>
    /// <returns></returns>
    public static GameObject GetChampionFromTriggerInfo(TriggerInfo triggerinfo)
    {
        GameObject championGO = null;

        championGO = gridHeroArray[triggerinfo.gridX, triggerinfo.gridZ];

        return championGO;
    }

   
    public static void HideHeroFromCombat(HeroController hero)
    {
        if(hero.teamID == 0)
        {
            //heroList.Remove(hero);
        }
        else
        {

        }
    }

    public static void CalculateBonuses()
    {
        AutoCombatSynergies.CalculateBonuses(out activeClassBonuses, out activeOriginBonuses, heroList);
        // need to remove the class and origin effect if it doesnt apply anymore
        ApplyClassEffect();
        ApplyOriginEffect();
        uIController.LoadSyngeryDisplay(activeClassBonuses, activeOriginBonuses);
    }

    static void ApplyClassEffect()
    {
        foreach (KeyValuePair<HeroClassBonus, int> classBonuses in activeClassBonuses)
        {
            if(!classBonuses.Key.ActivateClassEffect(classBonuses.Value, heroList))
            {
                // remove effect if it was active
            }
        }
    }

    static void ApplyOriginEffect()
    {
        foreach (KeyValuePair<HeroOriginBonus, int> originBonuses in activeOriginBonuses)
        {
            originBonuses.Key.ActivateOriginEffect(originBonuses.Value, heroList);
        }
    }

}
