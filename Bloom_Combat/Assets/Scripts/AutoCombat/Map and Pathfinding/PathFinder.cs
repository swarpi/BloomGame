using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class PathFinder : MonoBehaviour
{
    // https://www.youtube.com/watch?v=mZfyt03LDH4
    public static List<Tile> FindPath(Tile startTile, Tile targetTile, HeroController hero, Tile ignoreTile = null)
    {
        if (startTile == null || targetTile == null) return null;


        Heap<Tile> openSet = new Heap<Tile>(MapGenerator.MaxSize);
        //List<Tile> openSet = new List<Tile>();
        HashSet<Tile> closedSet = new HashSet<Tile>();
        bool pathSuccess = false;
        openSet.Add(startTile);

        while (openSet.Count > 0)
        {
            // heap optimaztion 
            Tile currenTile = openSet.RemoveFirst();
            //Tile currenTile = openSet[0];
            //for (int i = 1; i < openSet.Count; i++)
            //{
            //    if (openSet[i].fCost < currenTile.fCost || openSet[i].fCost == currenTile.fCost && openSet[i].hCost < currenTile.hCost)
            //    {
            //        currenTile = openSet[i];
            //    }
            //}
            //openSet.Remove(currenTile);
            closedSet.Add(currenTile);

            // if we found our node leave
            if (currenTile == targetTile)
            {
                pathSuccess = true;
                break;

            }
            // check all neighbours
            foreach (Tile neighbour in currenTile.neighbours)
            {       // if neighbour is passable, but not the target     // if my closedset contain my neighbour // neighbour is ignore tile
                if ((!neighbour.passable && neighbour!= targetTile) || closedSet.Contains(neighbour) || neighbour == ignoreTile)
                {
                    continue;
                }

                int newMovementCostToNeighbour = currenTile.gCost + GetDistance(currenTile, neighbour) + neighbour.extraCost;

                if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                {
                    neighbour.gCost = newMovementCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, targetTile);
                    if (!neighbour.heroIDparent.ContainsKey(hero.heroID))
                    {
                        neighbour.heroIDparent.Add(hero.heroID, currenTile);
                    }
                    else
                    {
                        neighbour.heroIDparent[hero.heroID] = currenTile;
                    }

                    if (!openSet.Contains(neighbour))
                    {
                        openSet.Add(neighbour);
                    }
                    else
                    {
                        //openSet.updateItem (neighbour) // needs to be implemented
                    }

                }
            }
        }
        List<Tile> waypoints = new List<Tile>();
        if (pathSuccess)
        {
            waypoints = RetracePath(startTile, targetTile, hero);
        }

        //PathRequestManager.FinishedProcessingPath(waypoints, pathSuccess);
        return waypoints;
        
    }

    static List<Tile> RetracePath(Tile startTile, Tile endTile, HeroController hero)
    {
        List<Tile> tempPath = new List<Tile>();
        Tile currentTile = endTile.heroIDparent[hero.heroID];

        while (currentTile != startTile)
        {
            tempPath.Add(currentTile);
            currentTile = currentTile.heroIDparent[hero.heroID];
        }
        tempPath.Reverse();

        return tempPath;

    }

    static int GetDistance(Tile tileA, Tile tileB)
    {
        int distX = Mathf.Abs(tileA.position.getX() - tileB.position.getX());
        int distZ = Mathf.Abs(tileA.position.getY() - tileB.position.getY());

        if (distX > distZ)
        {
            return distZ + (distX - distZ);
        }
        else
        {
            return distX + (distZ - distX);
        }
    }
}
