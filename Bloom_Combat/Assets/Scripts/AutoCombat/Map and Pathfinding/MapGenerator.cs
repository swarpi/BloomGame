﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public static int GRIDTYPE_OWN_INVENTORY = 0;
    public static int GRIDTYPE_OPONENT_FIELD = 1;
    public static int GRIDTYPE_PLAYER_FIELD = 2;

    public GameObject hexPrefab;
    public GameObject indicatorContainer;
    //to ouline the tiles (see that there are different tiles)
    [Range(0, 1)]
    public float outLinePercent;
    public float xOffset = 0.882f;
    public float zOffset = 0.764f;

    public Plane m_Plane;

    void Awake()
    {
        //GenerateMapSquare();
        m_Plane = new Plane(Vector3.up, Vector3.zero);
        GenerateMapPositions();
        GenerateMapObject();
        GenerateGraph();
        HideIndicators();
        InitColors();

        //GenerateACTrigger();
    }
    public static int hexMapSizeX = 7;
    public static int hexMapSizeZ = 5;
    public static int MaxSize
    {
        get
        {
            return hexMapSizeX * hexMapSizeZ;
        }
    }
    public Transform mapStartPosition;

    [HideInInspector]
    public static Tile[,] tileMap;

    [HideInInspector]
    public static Vector3[,] allMapGridPositions;
    [HideInInspector]
    public static Vector3[,] playerMapGridPositions;
    [HideInInspector]
    public static Vector3[,] enemyMapGridPositions;
    [HideInInspector]
    public static Vector3[,] neutralMapGridPositions;

    [HideInInspector]
    public static GameObject[,] playerMapGO;
    [HideInInspector]
    public static GameObject[,] enemyMapGO;
    [HideInInspector]
    public static GameObject[,] neutralMapGo;
    public GameObject hexaIndicator;
    [HideInInspector]
    public static GameObject[,] mapIndicatorArray;
    public void GenerateMapPositions()
    {
        allMapGridPositions = new Vector3[hexMapSizeX, hexMapSizeZ];
        mapIndicatorArray = new GameObject[hexMapSizeX, hexMapSizeZ];
        playerMapGO = new GameObject[hexMapSizeX/2, hexMapSizeZ];
        playerMapGridPositions = new Vector3[hexMapSizeX / 2, hexMapSizeZ];
        enemyMapGO = new GameObject[hexMapSizeX/2 + 1, hexMapSizeZ];
        enemyMapGridPositions = new Vector3[hexMapSizeX / 2 + 1, hexMapSizeZ];
        neutralMapGo = new GameObject[hexMapSizeX / 2 + 1, hexMapSizeZ];
        neutralMapGridPositions = new Vector3[hexMapSizeX, hexMapSizeZ];
        tileMap = new Tile[hexMapSizeX, hexMapSizeZ];

        for (int x = 0; x < hexMapSizeX; x++)
        {
            for (int z = 0; z < hexMapSizeZ; z++)
            {
                // remove upper corner from enemy field
                if(x == 0 && z != 2)
                {
                    continue;
                }
                // remove lower corners from player field
                if(x == 6 && (z == 0 || z == 4))
                {
                    continue;
                }
                //calculate even or add row
                int rowOffset = z % 2;

                //calculate position x and z
                float offsetX = x * -3f + rowOffset * 1.5f;
                float offsetZ = z * -2.5f;

                //calculate and store the position
                Vector3 position = GetMapHitPoint(mapStartPosition.position + new Vector3(offsetX * 0.7f, 0, offsetZ *0.7f));
                //add position variable to array
                allMapGridPositions[x, z] = position;


            }
        }
    }

    public void GenerateMapObject()
    {
        for (int x = 0; x < hexMapSizeX; x++)
        {
            for (int z = 0; z < hexMapSizeZ; z++)
            {
                // remove upper corner from enemy field
                if (x == 0 && z != 2)
                {
                    continue;
                }
                // remove lower corners from player field
                if (x == 6 && (z == 0 || z == 4))
                {
                    continue;
                }

                GameObject indicatorGO = Instantiate(hexaIndicator);
                Tile tempTile = indicatorGO.GetComponent<Tile>();
                tempTile.position = new Point(x, z);
                tempTile.tileName = "Hex" + x + "_" + z;
                tileMap[x, z] = tempTile;
                if (x < 4)
                {
                    indicatorGO.name = "enemyHex" + x + "_" + z;
                    enemyMapGO[x, z] = indicatorGO;
                    enemyMapGridPositions[x, z] = allMapGridPositions[x, z];
                    tempTile.tileType = 2;
                    if (x == 3 && z % 2 == 0)
                    {
                        indicatorGO.name = "neutralHex" + x + "_" + z;
                        neutralMapGo[x, z] = indicatorGO;
                        tempTile.tileType = 1;
                        //neutralMapGridPositions[x, z] = allMapGridPositions[x, z];
                    }

                }
                else
                {
                    indicatorGO.name = "playerHex" + (x - 4) + "_" + z;
                    playerMapGO[x - 4, z] = indicatorGO;
                    playerMapGridPositions[x - 4, z] = allMapGridPositions[x, z];
                    tempTile.tileType = 0;
                    //GameObject trigger = CreateSphereTrigger(GRIDTYPE_PLAYER_FIELD, x - 4, z);
                    //trigger.transform.SetParent(indicatorGO.transform);

                }


                //set indicator gameobject position
                indicatorGO.transform.position = allMapGridPositions[x, z];

                //set indicator parent
                indicatorGO.transform.parent = mapStartPosition.transform;
                mapIndicatorArray[x, z] = indicatorGO;
            }
        }

    }
    // add neighbours to the tiles
    public void GenerateGraph()
    {
        for (int x = 0; x < hexMapSizeX; x++)
        {
            for (int z = 0; z < hexMapSizeZ; z++)
            {
                if (x == 0 && z != 2)
                {
                    continue;
                }
                // remove lower corners from player field
                if (x == 6 && (z == 0 || z == 4))
                {
                    continue;
                }

                if (z % 2 != 0)// for odd rows
                {
                    // has no top U ... [1,0] , [1,1] , [0,2], [1,3] [1,4]
                    if(x > 1)
                    {
                        tileMap[x, z].neighbours.Add(tileMap[x - 1, z]); // U
                    }

                    if (z < hexMapSizeZ - 1) // for the left tiles
                    {
                        if(z != 3 || x != 1)
                        tileMap[x, z].neighbours.Add(tileMap[x - 1, z + 1]);// UR
                        if (!(x == 6 && z == 3))
                        {
                            tileMap[x, z].neighbours.Add(tileMap[x, z + 1]);    // DR
                        }
                            
                    }
                    if(z > 0) // for the right tiles
                    {
                        if(z != 1 || x != 1)
                        {
                            tileMap[x, z].neighbours.Add(tileMap[x - 1, z - 1]);// UL 
                        }
                            
                        
                        if (!(x == 6 && z == 1))
                            tileMap[x, z].neighbours.Add(tileMap[x, z - 1]);    // DL
                    }
                    // has no bottom ... [5,0] , [6,1] , [6,2] [ 6,3] , [5,4]
                    if(x < hexMapSizeX -1  || (x == 5 && (z == 0 || z == 4)))
                    {
                        tileMap[x, z].neighbours.Add(tileMap[x + 1, z]);    // D
                    }

                }
                else // for even rows
                {
                    if (x > 1 || (x == 1 && z == 2))
                    {
                        tileMap[x, z].neighbours.Add(tileMap[x - 1, z]); // U
                    }
                    if (z < hexMapSizeZ -1) // for the left tiles
                    {
                        if(x != 0)
                        tileMap[x, z].neighbours.Add(tileMap[x, z + 1]);    // UR
                        if (!(x == 6 && z == 2)) 
                        tileMap[x, z].neighbours.Add(tileMap[x + 1, z + 1]);// DR
                    }
                    if (z > 0 ) // for the right tiles
                    {
                        if(x!=0)
                        tileMap[x, z].neighbours.Add(tileMap[x, z - 1]);    // UL 
                        if (!(x == 6 && z == 2))
                        tileMap[x, z].neighbours.Add(tileMap[x + 1, z - 1]);// DL
                    }
                    if (x < hexMapSizeX - 1 && !(x == 5 && (z == 0 || z == 4)))
                    {
                        tileMap[x, z].neighbours.Add(tileMap[x + 1, z]);    // D
                    }
                }
            }
        }
    }
   
    public static bool isLegalFieldPlayer(int x, int z) 
    {
        if(playerMapGO[x,z] != null)
        {
            return true;
        }
        return false;
    }
    public Color playerColor;
    public Color enemyColor;
    public Color neutralColor;
    public Color mouseOverColor;
    TriggerInfo prevTrigger = null;
    void InitColors()
    {
        // enemy colors
        foreach(GameObject enemyTile in enemyMapGO)
        {
            if(enemyTile != null)
            {
                enemyTile.GetComponent<MeshRenderer>().material.color = enemyColor;
            }
        }
        // neutral colors
        foreach (GameObject neutralTile in neutralMapGo)
        {
            if (neutralTile != null)
            {
                neutralTile.GetComponent<MeshRenderer>().material.color = neutralColor;
            }
        }
        // player colors
        foreach (GameObject playerTile in playerMapGO)
        {
            if (playerTile != null)
            {
                playerTile.GetComponent<MeshRenderer>().material.color = playerColor;
            }
        }
    }

    public void ChangeTileColor(TriggerInfo triggerinfo)
    {
        if(prevTrigger == null)
        {
            prevTrigger = triggerinfo;
        }
        if(prevTrigger != triggerinfo)
        {
            playerMapGO[prevTrigger.gridX, prevTrigger.gridZ].GetComponent<MeshRenderer>().material.color = playerColor;
            prevTrigger = triggerinfo;
            playerMapGO[triggerinfo.gridX, triggerinfo.gridZ].GetComponent<MeshRenderer>().material.color = mouseOverColor;
        }
        if(triggerinfo == null)
        {
            playerMapGO[prevTrigger.gridX, prevTrigger.gridZ].GetComponent<MeshRenderer>().material.color = playerColor;
        }
        else
        {
            return;
        }
        

    }
    /// <summary>
    /// Get a point with accurate y axis
    /// </summary>
    /// <returns></returns>
    public Vector3 GetMapHitPoint(Vector3 p)
    {
        Vector3 newPos = p;

        RaycastHit hit;
        if (Physics.Raycast(newPos + new Vector3(0, 10, 0), Vector3.down, out hit, 15))
        {
            newPos = hit.point;
        }
        return newPos;
    }
    /// <summary>
    /// Creates a trigger collider gameobject and returns it
    /// </summary>
    /// <returns></returns>
    private GameObject CreateSphereTrigger(int type, int x, int z)
    {
        //create primitive gameobject
        GameObject trigger = new GameObject();

        //add collider component
        SphereCollider collider = trigger.AddComponent<SphereCollider>();

        //set collider size
        collider.radius = 1f;

        //set collider to trigger 
        collider.isTrigger = true;

        //add and store trigger info
        TriggerInfo trigerInfo = trigger.AddComponent<TriggerInfo>();
        trigerInfo.gridType = type;
        trigerInfo.gridX = x;
        trigerInfo.gridZ = z;

        trigger.layer = LayerMask.NameToLayer("Triggers");

        return trigger;
    }

    /// <summary>
    /// Returns grid indicator from triggerinfo
    /// </summary>
    /// <param name="triggerinfo"></param>
    /// <returns></returns>
    public GameObject GetIndicatorFromTriggerInfo(TriggerInfo triggerinfo)
    {
        GameObject triggerGo = null;

        if (triggerinfo.gridType == GRIDTYPE_OPONENT_FIELD)
        {
            triggerGo = mapIndicatorArray[triggerinfo.gridX, triggerinfo.gridZ];
        }
        else if (triggerinfo.gridType == GRIDTYPE_PLAYER_FIELD)
        {
            triggerGo = mapIndicatorArray[triggerinfo.gridX, triggerinfo.gridZ];
        }
        else if (triggerinfo.gridType == GRIDTYPE_OWN_INVENTORY)
        {
            //triggerGo = mapIndicatorArray[triggerinfo.gridX, triggerinfo.gridZ];
        }

        return triggerGo;
    }

    public static GameObject GetInfoFromTile(Tile tile)
    {
        GameObject triggerGo = null;

        return triggerGo;
    }

    public static void ShowIndicators()
    {
        for (int x = 0; x < hexMapSizeX; x++)
        {
            for (int z = 0; z < hexMapSizeZ; z++)
            {
                if (tileMap[x, z] != null)
                {

                    tileMap[x, z].gameObject.GetComponent<MeshRenderer>().enabled = true;
                }
            }
        }
    }
    public static void HideIndicators()
    {

        for (int x = 0; x < hexMapSizeX; x++)
        {
            for (int z = 0; z < hexMapSizeZ; z++)
            {
                if (tileMap[x, z] != null)
                {
                    //tileMap[x, z].gameObject.GetComponent<MeshRenderer>().enabled = false;
                }
            }
        }
        //        foreach (var tile in tileMap)
        //{
        //    Debug.Log("tile name " + tile.tileName);
        //    tile.gameObject.GetComponent<MeshRenderer>().enabled = false;
        //}
    }
}
