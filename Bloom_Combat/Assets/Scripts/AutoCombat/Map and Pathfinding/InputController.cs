﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
/// <summary>
/// Controlls player input
/// </summary>
public class InputController : MonoBehaviour
{
    public AutoCombatController autoCombatController;
    //map script
    public MapGenerator mapGenerator;
    public UIController uIController;

    private bool dragging = false;


    public LayerMask triggerLayer;

    //declare ray starting position var
    private Vector3 rayCastStartPosition;

    // Start is called before the first frame update
    void Start()
    {
        //set position of ray starting point to trigger objects
        rayCastStartPosition = new Vector3(0, 20, 0);
        uIController = GetComponent<UIController>();
    }

    //to store mouse position
    private Vector3 mousePosition;

    
    //public TriggerInfo triggerInfo = null;
    public bool isOverHeroSelect = false;
    public Tile tileInfo = null;
    /// Update is called once per frame
    void Update()
    {
        //triggerInfo = null;
        // map.resetIndicators();
        tileInfo = null;
        //declare rayhit
        RaycastHit hit;

        //convert mouse screen position to ray
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //if ray hits something
        if (Physics.Raycast(ray, out hit, 100f, triggerLayer, QueryTriggerInteraction.Collide))
        {
            //get trigger info of the  hited object
            //triggerInfo = hit.collider.gameObject.GetComponent<TriggerInfo>();
            tileInfo = hit.collider.gameObject.GetComponent<Tile>();

            //this is a trigger
            if (tileInfo != null)
            {
                //get indicator
                //GameObject indicator = mapGenerator.GetIndicatorFromTriggerInfo(triggerInfo);
                //mapGenerator.ChangeTileColor(triggerInfo);
                //indicator.GetComponent<MeshRenderer>().material.color = map.indicatorActiveColor;
            }

            //else
            // map.resetIndicators(); //reset colors
        }
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if(results.Any(x => x.gameObject.layer == 9))
        {
            isOverHeroSelect = true;
        }
        else
        {
            isOverHeroSelect = false;
        }
        if (Input.GetMouseButtonDown(0))
        {
            StartDrag();
        }

        if (Input.GetMouseButtonUp(0))
        {
/*            StopDrag();*/
        }
        // if we want to differentiate between clicking and holding
        //if (Input.GetMouseButton(0))
        //{

        //    timer += Time.deltaTime;
        //    if (timer < 0.1f)
        //    {
        //        // TODO : need to prevent to interact with enemy map
        //        // autoCombatController.ClickedOnHero();
        //        // maybe if dragged and dropped on invetory remove hero from field
        //    }
        //    else if (dragging == false)
        //    {
        //        autoCombatController.StartDrag();
        //        dragging = true;
        //    }

        //}

        //if (Input.GetMouseButtonUp(0))
        //{
        //    if (timer < 0.1f)
        //    {
        //        // TODO : need to prevent to interact with enemy map
        //        autoCombatController.ClickedOnHero();

        //        // maybe if dragged and dropped on invetory remove hero from field
        //    }
        //    timer = 0f;
        //    autoCombatController.StopDrag();
        //    dragging = false;
        //}

        //store mouse position
        mousePosition = Input.mousePosition;
    }
    public GameObject draggedChampion = null;
    //public TriggerInfo dragStartTrigger = null;
    public Tile dragStartTile = null;

    /// <summary>
    /// When we start dragging heros on map
    /// </summary>
    public void StartDrag()
    {
        if (AutoCombatController.currentGameStage != GameStage.Preparation)
            return;
        //get trigger info
        //TriggerInfo triggerinfo = triggerInfo;
        Tile tileInfoTemp = tileInfo;
        //if mouse cursor on trigger
        if (tileInfoTemp != null)
        {
            MapGenerator.ShowIndicators();
            dragStartTile = tileInfoTemp;

            //GameObject championGO = AutoCombatField.GetChampionFromTriggerInfo(triggerinfo);
            GameObject championGO = tileInfo.heroOnTile.gameObject;
            if (championGO != null)
            {
                //show indicators
                //map.ShowIndicators();

                draggedChampion = championGO;

                //isDragging = true;

                championGO.GetComponent<HeroController>().IsDragged = true;
                //championGO.GetComponent<HeroController>().currentTilePos.AddHero(championGO.GetComponent<HeroController>());
                //Debug.Log("STARTDRAG");
            }
        }
    }

    /// <summary>
    /// When we stop dragging champions on map
    /// </summary>
/*    public void StopDrag()
    {
        MapGenerator.HideIndicators();
        if (draggedChampion != null)
        {
            //set dragged
            draggedChampion.GetComponent<HeroController>().IsDragged = false;

            //get trigger info
            Tile tileTemp = tileInfo;
            //if mouse cursor on trigger
            if (tileTemp != null && tileTemp.tileType == 0)
            {
                //get current champion over mouse cursor
                //GameObject currentTriggerChampion = AutoCombatField.GetChampionFromTriggerInfo(triggerinfo);
                
                //there is another champion in the way
                if (tileInfo.heroOnTile != null)
                {
                    GameObject currentTriggerChampion = tileInfo.heroOnTile.gameObject;
                    //store this champion to start position
                    AutoCombatField.StoreChampionInArray(dragStartTile.position.X-4, dragStartTile.position.Z, currentTriggerChampion);

                    //store this champion to dragged position
                    AutoCombatField.StoreChampionInArray(tileTemp.position.X-4, tileTemp.position.Z, draggedChampion);
                }
                else
                {
                    AutoCombatField.RemoveChampionFromArray(dragStartTile.position.X-4, dragStartTile.position.Z, draggedChampion);
                    AutoCombatField.StoreChampionInArray(tileTemp.position.X-4, tileTemp.position.Z, draggedChampion);
                }
            }
            // if we hold hero over hero select remove it from the field
            if (isOverHeroSelect)
            {
                AutoCombatField.RemoveChampionFromArray(dragStartTile.position.X-4, dragStartTile.position.Z, draggedChampion);
                Destroy(draggedChampion.gameObject);
                uIController.ChangeHeroFrameAfterSelected(draggedChampion.GetComponent<HeroController>().hero, false);
            }
            //CalculateBonuses();

            //currentChampionCount = GetChampionCountOnHexGrid();

            //update ui
            //uIController.UpdateUI();
            draggedChampion = null;
        }
        draggedChampion = null;
    }*/
    ///// <summary>
    ///// When we click heros on map
    ///// </summary>
    //public void ClickedOnHero()
    //{
    //    //if (currentGameStage != GameStage.Preparation)
    //    //    return;

    //    //get trigger info
    //    TriggerInfo triggerinfo = triggerInfo;
    //    //if mouse cursor on trigger
    //    if (triggerinfo != null)
    //    {
    //        dragStartTrigger = triggerinfo;

    //        GameObject championGO = AutoCombatField.GetChampionFromTriggerInfo(triggerinfo);
    //        if (championGO != null)
    //        {
    //            //show indicators
    //            //map.ShowIndicators();

    //            championGO.SetActive(false);
    //            // put back in storage

    //        }
    //    }
    //}
}
