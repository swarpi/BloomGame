using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MoveCalculator : MonoBehaviour
{
    public List<HeroController> allHoersInCombat;
    public AutoCombatController autoCombatController;
    public AIopponent aIopponent;
    Dictionary<string, Tile> nextStepDic;
    public static List<Tile> nextStepList;
    // List of all units in combat
    // dictionary of unit and their next step
    // Start is called before the first frame update
    void Start()
    {

    }
    private float nextUpdate = 1;
    float startWait = 1f;
    float currentTime = 0f;
    // Update is called once per frame
    void Update()
    {
        if (AutoCombatController.currentGameStage == GameStage.Combat)
        {
            if (currentTime > startWait)
            {       // If the next update is reached
                if (Time.time >= nextUpdate)
                {
                    // Change the next update (current second+1)
                    nextUpdate = Time.time + 0.55f;
                    // potential problem : if we look to fast for a new step, heroes didnt go to their target fast enough
                    //                      therefore two heros might go to the same tile;
                    //                      so be careful of slow heros
                    DoStepsQueue();
                }
            }
            else
            {
                currentTime += Time.deltaTime;
            }
            if (Input.GetButtonDown("Jump"))
            {
                //DoStepsQueue();
            }
        }

    }
    public void GetAllHeroesInCombat()
    {
        allHoersInCombat = new List<HeroController>();
        // add all players
        foreach (GameObject hero in AutoCombatField.heroList)
        {
            allHoersInCombat.Add(hero.GetComponent<HeroController>());
        }
        // add all enemies
        foreach (GameObject hero in AIopponent.aiHeroList)
        {
            allHoersInCombat.Add(hero.GetComponent<HeroController>());
        }
    }


    public Queue<HeroMovement> heroMovements;
    public void CalculateStepsQueue()
    {
        GetAllHeroesInCombat();
        nextStepList = new List<Tile>();
        heroMovements = new Queue<HeroMovement>();

        foreach(HeroController hero in allHoersInCombat)
        {
            if (hero == null || hero.heroControllerCombat.isDead) return;
            List<Tile> tileList = PathFinder.FindPath(hero.GetCurrentTile(), hero.targetTile, hero);
            if (tileList == null) return;
            if (tileList.Count > 0
                && hero.currentCombatState == AICombatState.LookingForTarget)
            {
                Tile nextStep = tileList[0];
                // no conflicting next steps and my next step is not my target already
                // CheckAndAddStep(hero);
                if (!heroMovements.Any(x => x.move == nextStep) && nextStep != hero.targetTile)
                {
                    heroMovements.Enqueue(new HeroMovement(hero.heroID, nextStep));
                    nextStepList.Add(nextStep);
                }
            }
        }
    }

    Tile CheckAndAddStep(HeroController hero, Tile blockedTile = null)
    {
        // this is my next step
        Tile tempTile = PathFinder.FindPath(hero.GetCurrentTile(), hero.targetTile, hero, blockedTile)[0];
        // if there is a confliction with other heros for my next step
        // or there a hero on my next step, try to look again but without the blocked tile
        if (heroMovements.Any(x => x.move == tempTile) || tempTile.heroOnTile != null)
        {
            blockedTile = tempTile;
            return CheckAndAddStep(hero, blockedTile);
        }
        nextStepList.Add(tempTile);
        heroMovements.Enqueue(new HeroMovement(hero.heroID, tempTile));
        return tempTile;
    }


    public void DoStepsQueue()
    {
        CalculateStepsQueue();
        
        while(heroMovements.Count > 0)
        {
            HeroMovement hm = heroMovements.Dequeue();
            HeroController hero = allHoersInCombat.Find(x => x.heroID == hm.heroID);
            hero.heroControllerMovement.MoveTowards(hm.move);
        }
    }

    public static Tile FindFurthestPlayerTile(Tile startPosition)
    {
        Tile prevTile = startPosition;
        Vector2 startingPoint = new Vector2(startPosition.transform.position.x, startPosition.transform.position.z);
        float distance = 0f;
        for (int x = 0; x < MapGenerator.hexMapSizeX / 2; x++)
        {
            for (int z = 0; z < MapGenerator.hexMapSizeZ; z++)
            {
                if (MapGenerator.playerMapGO[x, z] == null) continue;
                Tile tempTile = MapGenerator.playerMapGO[x, z].GetComponent<Tile>();
                Vector2 newPoint = new Vector2(tempTile.transform.position.x, tempTile.transform.position.z);
                float newDistance = Vector2.Distance(startingPoint, newPoint);
                if (distance < newDistance && tempTile.heroOnTile == null)
                {
                    distance = newDistance;
                    prevTile = tempTile;
                }
            }
        }
        return prevTile;
    }

    public static GameObject FindFurthestEnemyUnit(GameObject startPosition)
    {
        GameObject prevGO = startPosition;
        Vector3 startingPoint = startPosition.transform.position;
        float distance = 0f;
        foreach(GameObject enemy in AIopponent.aiHeroList)
        {
            Vector3 enemyPoint = enemy.transform.position;
            float newDistance = Vector3.Distance(startingPoint, enemyPoint);
            if(distance < newDistance)
            {
                distance = newDistance;
                prevGO = enemy;
            }
        }
        return prevGO;
    }
}

