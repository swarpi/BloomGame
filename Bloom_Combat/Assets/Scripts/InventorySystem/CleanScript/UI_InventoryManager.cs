using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CodeMonkey.Utils;

public class UI_InventoryManager : MonoBehaviour
{
    //PlayerInventory
    private PlayerInventoryContainer playerRuneInventoryContainer;
    private PlayerInventoryContainer playerItemInventoryContainer;
    
    public Transform runeSlotContainer;
    public Transform runeSlotTemplate;

    private Transform itemSlotContainer;
    private Transform itemSlotTemplate;
    
    //POPUP
    public GameObject runePopUp;
    //Rune Images
    public GameObject runeDetailImageGO;
    public GameObject displayAlphaBetaGamma;
    public Image runeSet;
    //Need to find it automatically
    //Rune MainStats
    public GameObject runeMainStat;
    public GameObject runeMainStatNumber;
    //Rune SubStats
    public GameObject runeFirstSubStat;
    public GameObject runeFirstSubStatValue;
    public GameObject runeSecondSubStat;
    public GameObject runeSecondSubStatValue;
    public GameObject runeThirdSubStat;
    public GameObject runeThirdSubStatValue;
    //RuneButtons
    public Button equipButton;
    public Button upgradeButton;
    public Button removeButton;
    //RuneSlotButton
   
    public HeroRuneInventory heroRunes;
   
    //Runes
    public void SetRuneInventory(PlayerInventoryContainer runeInventory)
    {
        this.playerRuneInventoryContainer = runeInventory;
        runeInventory.OnListChanged += Rune_OnRuneListChanged;
        RefreshInventoryRune();
    }
    private void Rune_OnRuneListChanged(object sender, System.EventArgs e)
    {
        RefreshInventoryRune();
    }
    public void closePopUp()
    {
        runePopUp.SetActive(false);
    }
    public void RefreshInventoryRune()
    {
        foreach (Transform child in runeSlotContainer)
        {
            if (child == runeSlotTemplate) continue;
            Destroy(child.gameObject);
        }
        int x = 0;
        int y = 0;
        float runeSlotCellSize = 110f;
        foreach (Rune rune in playerRuneInventoryContainer.playerRuneInventoryList)
        {
            RectTransform runeSlotRectTransform = Instantiate(runeSlotTemplate, runeSlotContainer).GetComponent<RectTransform>();
            runeSlotRectTransform.gameObject.SetActive(true);

            //RectTransform equipButt = Instantiate(equipButton).GetComponent<RectTransform>();
            //equipButt.gameObject.SetActive(true);
            //equipButt.GetComponent<Button_UI>().ClickFunc = () =>
            //{
            //    Debug.Log("Equip me");
            //};

            runeSlotRectTransform.GetComponent<Button_UI>().ClickFunc = () =>
            {
                runePopUp.SetActive(true);
                //if (runePopUp != null)
                //{
                //    bool isActive = runePopUp.activeSelf;
                //    runePopUp.SetActive(!isActive);
                //}
                // Bilder und Texte muessen im Inspector per Hand eingefuegt werden
                Image runeDetailImage = runeDetailImageGO.GetComponentInChildren<Image>();
                runeDetailImage.sprite = rune.GetRuneSprite(rune);

                runeSet.sprite = rune.GetRuneSetSprite(rune);

                //RuneTypeText
                TextMeshProUGUI rSlotText = displayAlphaBetaGamma.GetComponent<TextMeshProUGUI>();
                rSlotText.text = rune.runeSlot.ToString();

                //RuneTypeIcon


                //MainStat and Value
                TextMeshProUGUI rMainStat = runeMainStat.GetComponent<TextMeshProUGUI>();
                rMainStat.text = rune.mainStat.type.ToString();
                TextMeshProUGUI rMainStatNumber = runeMainStatNumber.GetComponent<TextMeshProUGUI>();
                rMainStatNumber.text = rune.mainStat.value.ToString();

                //SubStat Name and Value
                TextMeshProUGUI r1SubStat = runeFirstSubStat.GetComponent<TextMeshProUGUI>();
                TextMeshProUGUI r2SubStat = runeSecondSubStat.GetComponent<TextMeshProUGUI>();
                TextMeshProUGUI r3SubStat = runeThirdSubStat.GetComponent<TextMeshProUGUI>();
                TextMeshProUGUI r1SubStatValue = runeFirstSubStatValue.GetComponent<TextMeshProUGUI>();
                TextMeshProUGUI r2SubStatValue = runeSecondSubStatValue.GetComponent<TextMeshProUGUI>();
                TextMeshProUGUI r3SubStatValue = runeThirdSubStatValue.GetComponent<TextMeshProUGUI>();
                r1SubStat.SetText("");
                r2SubStat.SetText("");
                r3SubStat.SetText("");
                r1SubStatValue.SetText("");
                r2SubStatValue.SetText("");
                r3SubStatValue.SetText("");
                foreach (SubStat subStat in rune.subStats)
                {
                    if (r1SubStat.text != "")
                    {
                        if (r2SubStat.text != "")
                        {
                            r3SubStat.text = subStat.type.ToString();
                            r3SubStatValue.text = subStat.value.ToString();
                        }
                        else
                        {
                            r2SubStat.text = subStat.type.ToString();
                            r2SubStatValue.text = subStat.value.ToString();
                        }
                    }
                    else
                    {
                        r1SubStat.text = subStat.type.ToString();
                        r1SubStatValue.text = subStat.value.ToString();
                    }
                }
                //Rune equip,upgrade,remove
                Button equipRune = equipButton.GetComponent<Button>();
                Button upgradeRune = upgradeButton.GetComponent<Button>();
                Button removeRune = removeButton.GetComponent<Button>();
                equipRune.onClick.RemoveAllListeners();
                upgradeRune.onClick.RemoveAllListeners();
                removeRune.onClick.RemoveAllListeners();

                equipRune.onClick.AddListener(() => heroRunes.EquipRune(rune));
                //equipRune.onClick.AddListener(() => CloseRunePopUp(runePopUp));
                upgradeRune.onClick.AddListener(() => UpgradeRune(rune));
                removeRune.onClick.AddListener(() => RemoveRune(rune));
            };

            runeSlotRectTransform.anchoredPosition = new Vector2(x * runeSlotCellSize, y * runeSlotCellSize);
            Image runeImage = runeSlotRectTransform.Find("runeImage").GetComponent<Image>();
            runeImage.sprite = rune.GetRuneSprite(rune);
            x++;
            if (x > 6)
            {
                x =0;
                y--;
            }
        }
    }
    
    public void UpgradeRune(Rune rune)
    {
        Debug.Log("Upgrade rune" + rune.runeSlot.ToString());
    }
    public void RemoveRune(Rune rune)
    {
        Debug.Log("Remove rune" + rune.runeSlot.ToString());
        //currentHero.equippedRunes.Remove(rune);
        playerRuneInventoryContainer.playerRuneInventoryList.Add(rune);
        runePopUp.SetActive(false);
        RefreshInventoryRune();
        heroRunes.RefreshHeroRuneInventory();
    }
    //Items
    public void SetInventory(PlayerInventoryContainer inventory)
    {
        this.playerItemInventoryContainer = inventory;
        inventory.OnListChanged += Inventory_OnItemListChanged;
        RefreshInventoryItems();
    }
    private void Inventory_OnItemListChanged(object sender, System.EventArgs e)
    {
        RefreshInventoryItems();
    }
    private void RefreshInventoryItems()
    {
        Debug.Log("Refresh");
        foreach (Transform child in itemSlotContainer)
        {
            if (child == itemSlotTemplate) continue;
            Destroy(child.gameObject);
        }
        int x = 0;
        int y = 0;
        float itemSlotCellSize = 60f;
        foreach (Item item in playerItemInventoryContainer.playerItemInventoryList)
        {
            Debug.Log("Hi");
            RectTransform itemSlotRectTransform = Instantiate(itemSlotTemplate, itemSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.gameObject.SetActive(true);

            itemSlotRectTransform.GetComponent<Button_UI>().ClickFunc = () =>
            {
                Debug.Log("pressed");
            };

            itemSlotRectTransform.anchoredPosition = new Vector2(x * itemSlotCellSize, y * itemSlotCellSize);
            Image image = itemSlotRectTransform.Find("mainImage").GetComponent<Image>();
            image.sprite = item.GetSprite();

            TextMeshProUGUI uitext = itemSlotRectTransform.Find("amountText").GetComponent<TextMeshProUGUI>();
            if (item.Amount > 1)
            {
                uitext.SetText(item.Amount.ToString());
            }
            else
            {
                uitext.SetText("");
            }
            x++;
            if (x > 2)
            {
                x = 0;
                y--;
            }

        }
    }

}
