using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
 public enum ItemType
    {
        Weapon,
        Rune,
        Potion,
        Money,
        Ascension
    }
    public ItemType itemType;
    public int Amount;
    //public Rune rune;

    public Sprite GetSprite()
    {
        switch (itemType)
        {
            default:
            case ItemType.Weapon: return ItemAssets.Instance.weaponSprite;
            case ItemType.Rune: return ItemAssets.Instance.alpharuneSprite;
            case ItemType.Potion: return ItemAssets.Instance.potionSprite;
            case ItemType.Money: return ItemAssets.Instance.moneySprite;
        }
    }
    public Sprite GetRuneSprite(Rune rune)
    {
        if(rune.runeSlot == RuneSlot.DummyRune)
        {
            return RuneAssets.RuneInstance.DummyRuneSprite;
        }
        if(rune.runeSlot == RuneSlot.Alpha)
        {
            return RuneAssets.RuneInstance.alpharuneSprite;
        }
        if (rune.runeSlot == RuneSlot.Beta)
        {
            return RuneAssets.RuneInstance.betaruneSprite;
        }
        if (rune.runeSlot == RuneSlot.Gamma)
        {
            return RuneAssets.RuneInstance.gammaruneSprite;
        }
        
        return rune.GetSprite();
    }
    public Sprite GetRuneSetSprite(Rune rune)
    {
        if (rune.runeSet == RuneSetBonus.ATKSet)
            return RuneAssets.RuneInstance.AKTSetSprite;
        if (rune.runeSet == RuneSetBonus.CRITDMGSet)
            return RuneAssets.RuneInstance.CRITDMGSetSprite;
        if (rune.runeSet == RuneSetBonus.DummySet)
            return RuneAssets.RuneInstance.DummyRuneSetSprite;
        return rune.GetSprite();
    }
    public bool isStackable()
    {
        switch (itemType)
        {
            default:
            case ItemType.Money:
            case ItemType.Potion:
                return true;
            case ItemType.Weapon:
            case ItemType.Rune:
                return false;
        }
       
    }

}
