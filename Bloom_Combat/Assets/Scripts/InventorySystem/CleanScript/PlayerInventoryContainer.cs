using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerInventoryContainer : MonoBehaviour
{
    //Set different Inventorys
    [SerializeField] private UI_InventoryManager uiInventory;
    [SerializeField] private UI_InventoryManager uiRewardInventory;
    [SerializeField] private UI_InventoryManager uiRuneInventory;
    [SerializeField] private UI_InventoryManager uiHeroInventory;

    public event EventHandler OnListChanged;
    public PlayerInventoryContainer playerInventory;
    //Lists
    public List<Rune> playerRuneInventoryList;
    public List<Item> playerItemInventoryList;

    public void Start()
    {
        //uiInventory.SetInventory(playerInventory);
        uiRuneInventory.SetRuneInventory(playerInventory);
        //uiRewardInventory.SetRewardInventory(playerInventory);
    }
    
}
