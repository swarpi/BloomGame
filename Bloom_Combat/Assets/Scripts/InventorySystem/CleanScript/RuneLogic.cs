using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine;


public class RuneLogic : MonoBehaviour
{
    public GameObject equippedRune;
    public GameObject TEMPRune;
    public RuneDisplayer runeList;
    public Rune TEMPalpha1Rune;
    public Rune TEMPbeta1Rune;
    public Rune TEMPbeta2Rune;
    public Rune TEMPgamma1Rune;
    public Rune TEMPgamma2Rune;
    public Rune TEMPgamma3Rune;
    public List<Rune> AllTEMPRune;
    public HeroRuneChanger heroRune;

    public void TempRuneResetForHeroSwitch()
    {
        for (int i = 0; i < AllTEMPRune.Count; i++)
        {
            if(heroRune.currentHero.equippedRunes[i] != AllTEMPRune[i])
            {
                runeList.runeList.Add(AllTEMPRune[i]);
            }
        }
    }
    public void EquipTEMPRune(Rune rune,HeroSaveData currentHero)
    {
        //Check character exist
        //Temp RuneSlot wird identifiziert
        if (currentHero.heroID != "")
        {
            switch (rune.runeSlot)
            {
                case RuneSlot.Alpha:
                    TEMPRune.transform.Find("TEMPalphaSlot").Find("aGlow").gameObject.SetActive(true);
                    TEMPRune.transform.Find("TEMPalphaSlot").Find("aSlotImage").GetComponent<Button>().onClick.AddListener(() => SetRune(0,rune, currentHero));
                    break;
                case RuneSlot.Beta:
                    TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots1").Find("b1Glow").gameObject.SetActive(true);
                    TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots2").Find("b2Glow").gameObject.SetActive(true);
                    TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots1").Find("bSlot1Image").GetComponent<Button>().onClick.AddListener(() => SetRune(1,rune, currentHero));
                    TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots2").Find("bSlot2Image").GetComponent<Button>().onClick.AddListener(() => SetRune(2,rune, currentHero));
                    break;
                case RuneSlot.Gamma:
                    TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots1").Find("g1Glow").gameObject.SetActive(true);
                    TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots2").Find("g2Glow").gameObject.SetActive(true);
                    TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots3").Find("g3Glow").gameObject.SetActive(true);
                    TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots1").Find("gSlot1Image").GetComponent<Button>().onClick.AddListener(() => SetRune(3,rune, currentHero));
                    TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots2").Find("gSlot2Image").GetComponent<Button>().onClick.AddListener(() => SetRune(4,rune, currentHero));
                    TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots3").Find("gSlot3Image").GetComponent<Button>().onClick.AddListener(() => SetRune(5,rune, currentHero));
                    break;
            }
        }
        else
        {
            Debug.Log("No Hero Selected");
        }
    }
    public void RemoveTEMPRune(Rune rune)
    {
        //Rune wird PlayerInventar hinzugefuegt
        //Rune wird von TempRune Liste entfernt und mit DummyRunen ersetzt
        runeList.runeList.Add(rune);
        for (int i = 0; i < AllTEMPRune.Count; i++)
        {
            if(rune == AllTEMPRune[i])
            {
                AllTEMPRune[i] = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
            }
        }
        RefreshHeroRuneInventory();
        heroRune.statLogic.DisplayEquippedRunesStats();
    }
    public void ReplaceTEMPRune(Rune rune,int slot)
    {
        //TempRune wird mit PlayerRune ausgetauscht
        heroRune.closePopUp("ALL");
        runeList.runeList.Add(AllTEMPRune[slot]);
        AllTEMPRune[slot] = rune;
        runeList.runeList.Remove(rune);
        
        RefreshHeroRuneInventory();
        heroRune.statLogic.DisplayEquippedRunesStats();
        SetOffGlow();
    }
    
    public void HeroTEMPRunePopUp()
    {
        RemoveAllSetRuneListener();
        //Open TempHeroRune Infos
        for (int i = 0; i < AllTEMPRune.Count; i++)
        {
            if (AllTEMPRune[i].runeSlot != RuneSlot.DummyRune)
            {
                switch (i)
                {
                    case 0:
                        TEMPRune.transform.Find("TEMPalphaSlot").Find("aSlotImage").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(AllTEMPRune[0], "TEMPRune"));
                        break;
                    case 1:
                        TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots1").Find("bSlot1Image").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(AllTEMPRune[1], "TEMPRune"));
                        break;
                    case 2:
                        TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots2").Find("bSlot2Image").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(AllTEMPRune[2], "TEMPRune"));
                        break;
                    case 3:
                        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots1").Find("gSlot1Image").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(AllTEMPRune[3], "TEMPRune"));
                        break;
                    case 4:
                        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots2").Find("gSlot2Image").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(AllTEMPRune[4], "TEMPRune"));
                        break;
                    case 5:
                        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots3").Find("gSlot3Image").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(AllTEMPRune[5], "TEMPRune"));
                        break;
                }
            }
        }
        for (int i = 0; i < AllTEMPRune.Count; i++)
        {
            if (heroRune.currentHero.equippedRunes[i].runeSlot != RuneSlot.DummyRune)
            {
                switch (i)
                {
                    case 0:
                        equippedRune.transform.Find("alphaSlot").Find("aSlotImage").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(heroRune.currentHero.equippedRunes[0], "EquippedRune"));
                        break;
                    case 1:
                        equippedRune.transform.Find("betaSlot").Find("bSlots1").Find("bSlot1Image").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(heroRune.currentHero.equippedRunes[1], "EquippedRune"));
                        break;
                    case 2:
                        equippedRune.transform.Find("betaSlot").Find("bSlots2").Find("bSlot2Image").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(heroRune.currentHero.equippedRunes[2], "EquippedRune"));
                        break;
                    case 3:
                        equippedRune.transform.Find("gammaSlot").Find("gSlots1").Find("gSlot1Image").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(heroRune.currentHero.equippedRunes[3], "EquippedRune"));
                        break;
                    case 4:
                        equippedRune.transform.Find("gammaSlot").Find("gSlots2").Find("gSlot2Image").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(heroRune.currentHero.equippedRunes[4], "EquippedRune"));
                        break;
                    case 5:
                        equippedRune.transform.Find("gammaSlot").Find("gSlots3").Find("gSlot3Image").GetComponent<Button>().onClick.AddListener(() => heroRune.DisplayRuneInfoWindow(heroRune.currentHero.equippedRunes[5], "EquippedRune"));
                        break;
                }
            }
        }
    }
    public void RemoveAllSetRuneListener()
    {
        TEMPRune.transform.Find("TEMPalphaSlot").Find("aSlotImage").GetComponent<Button>().onClick.RemoveAllListeners();
        TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots1").Find("bSlot1Image").GetComponent<Button>().onClick.RemoveAllListeners();
        TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots2").Find("bSlot2Image").GetComponent<Button>().onClick.RemoveAllListeners();
        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots1").Find("gSlot1Image").GetComponent<Button>().onClick.RemoveAllListeners();
        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots2").Find("gSlot2Image").GetComponent<Button>().onClick.RemoveAllListeners();
        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots3").Find("gSlot3Image").GetComponent<Button>().onClick.RemoveAllListeners();
    }
    public void SetOffGlow()
    {
        TEMPRune.transform.Find("TEMPalphaSlot").Find("aGlow").gameObject.SetActive(false);
        TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots1").Find("b1Glow").gameObject.SetActive(false);
        TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots2").Find("b2Glow").gameObject.SetActive(false);
        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots1").Find("g1Glow").gameObject.SetActive(false);
        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots2").Find("g2Glow").gameObject.SetActive(false);
        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots3").Find("g3Glow").gameObject.SetActive(false);
    }
    public void SetRune(int slot,Rune rune,HeroSaveData currentHero)
    {
        //Player Runen werden zu den TempRune Liste hinzugefuegt
        SetOffGlow();
        if (AllTEMPRune[slot].runeSlot == RuneSlot.DummyRune)
        {
            AllTEMPRune[slot] = rune;
            runeList.runeList.Remove(rune);
            RefreshHeroRuneInventory();
            heroRune.statLogic.DisplayEquippedRunesStats();
            if (GameObject.Find("playerRunePopUp(Clone)"))
            {
                Destroy(GameObject.Find("playerRunePopUp(Clone)"));
                Debug.Log("geunden");
            }
        }
        else
        {
            ReplaceTEMPRune(rune, slot);
        }
    }
    public void RefreshHeroRuneInventory()
    {
        //Alle RunePopups werden geschlossen, Sprite aller Temp und Equip Runen werden geladen
        if (GameObject.Find("runePopUp(Clone)"))
        {
            Destroy(GameObject.Find("runePopUp(Clone)"));
        }
        foreach (Transform child in runeList.contentGO.transform)
        {
            Destroy(child.gameObject);
        }
        heroRune.OpenHeroRuneChanger();

        TEMPRune.transform.Find("TEMPalphaSlot").Find("aSlotImage").GetComponent<Image>().sprite = AllTEMPRune[0].GetRuneSprite(AllTEMPRune[0]);
        TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots1").Find("bSlot1Image").GetComponent<Image>().sprite = AllTEMPRune[1].GetRuneSprite(AllTEMPRune[1]);
        TEMPRune.transform.Find("TEMPbetaSlot").Find("bSlots2").Find("bSlot2Image").GetComponent<Image>().sprite = AllTEMPRune[2].GetRuneSprite(AllTEMPRune[2]);
        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots1").Find("gSlot1Image").GetComponent<Image>().sprite = AllTEMPRune[3].GetRuneSprite(AllTEMPRune[3]);
        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots2").Find("gSlot2Image").GetComponent<Image>().sprite = AllTEMPRune[4].GetRuneSprite(AllTEMPRune[4]);
        TEMPRune.transform.Find("TEMPgammaSlot").Find("gSlots3").Find("gSlot3Image").GetComponent<Image>().sprite = AllTEMPRune[5].GetRuneSprite(AllTEMPRune[5]);

        equippedRune.transform.Find("alphaSlot").Find("aSlotImage").GetComponent<Image>().sprite = heroRune.currentHero.equippedRunes[0].GetRuneSprite(heroRune.currentHero.equippedRunes[0]);
        equippedRune.transform.Find("betaSlot").Find("bSlots1").Find("bSlot1Image").GetComponent<Image>().sprite = heroRune.currentHero.equippedRunes[1].GetRuneSprite(heroRune.currentHero.equippedRunes[1]);
        equippedRune.transform.Find("betaSlot").Find("bSlots2").Find("bSlot2Image").GetComponent<Image>().sprite = heroRune.currentHero.equippedRunes[2].GetRuneSprite(heroRune.currentHero.equippedRunes[2]);
        equippedRune.transform.Find("gammaSlot").Find("gSlots1").Find("gSlot1Image").GetComponent<Image>().sprite = heroRune.currentHero.equippedRunes[3].GetRuneSprite(heroRune.currentHero.equippedRunes[3]);
        equippedRune.transform.Find("gammaSlot").Find("gSlots2").Find("gSlot2Image").GetComponent<Image>().sprite = heroRune.currentHero.equippedRunes[4].GetRuneSprite(heroRune.currentHero.equippedRunes[4]);
        equippedRune.transform.Find("gammaSlot").Find("gSlots3").Find("gSlot3Image").GetComponent<Image>().sprite = heroRune.currentHero.equippedRunes[5].GetRuneSprite(heroRune.currentHero.equippedRunes[5]);
        HeroTEMPRunePopUp();
    }
}
