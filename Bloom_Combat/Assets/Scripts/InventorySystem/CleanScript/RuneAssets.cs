using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuneAssets : MonoBehaviour
{
    public static RuneAssets RuneInstance { get; private set; }

    private void Awake()
    {
        RuneInstance = this;
    }
    //Slot Sprite
    public Sprite alpharuneSprite;
    public Sprite betaruneSprite;
    public Sprite gammaruneSprite;
    public Sprite DummyRuneSprite;

    //Set Sprite
    public Sprite AKTSetSprite;
    public Sprite CRITDMGSetSprite;
    public Sprite DummyRuneSetSprite;

}