using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
public class HeroRuneInventory : MonoBehaviour
{
    //HeroButtons
    public Button hero1;
    public Button hero2;
    public Button hero3;
    //Slot Buttons
    public Button alpha1;
    public Button beta1;
    public Button beta2;
    public Button gamma1;
    public Button gamma2;
    public Button gamma3;


    //Images
    public Image alpha1Slot;
    public Image beta1Slot;
    public Image beta2Slot;
    public Image gamma1Slot;
    public Image gamma2Slot;
    public Image gamma3Slot;

    public PlayerInventoryContainer playerRuneInventoryContainer;
    public UI_InventoryManager inventoryManager;

    //POPUP
    public GameObject heroRunePopUp;
    public GameObject replaceRunePopUp;
    //Rune Images
    public Image heroRuneDetailImageGO;
    public TextMeshProUGUI displayHeroAlphaBetaGamma;
    //Need to find it automatically
    //Rune MainStats
    public TextMeshProUGUI heroRuneMainStat;
    public TextMeshProUGUI heroRuneMainStatNumber;
    //Rune SubStats
    public TextMeshProUGUI heroRuneFirstSubStat;
    public TextMeshProUGUI heroRuneFirstSubStatValue;
    public TextMeshProUGUI heroRuneSecondSubStat;
    public TextMeshProUGUI heroRuneSecondSubStatValue;
    public TextMeshProUGUI heroRuneThirdSubStat;
    public TextMeshProUGUI heroRuneThirdSubStatValue;
    //RuneButtons
    public Button heroUpgradeButton;
    public Button heroRemoveButton;

    public HeroSaveData currentHero;
    public HeroSaveData heroRune;
    public HeroInventory heroInventory;
    //RuneStats
    public TextMeshProUGUI ATK;
    public TextMeshProUGUI CRITDMG;

    public GameObject aGlow;
    public GameObject b1Glow;
    public GameObject b2Glow;
    public GameObject g1Glow;
    public GameObject g2Glow;
    public GameObject g3Glow;

    public Image runeSet;

    //Display stats from runes = Sets
    float ATKDMGVAL = 0;
    float CRITDMGVAL = 0;
    int ATKSetCount = 0;
    int CRITDMGSetCount = 0;
    public TextMeshProUGUI showStats1=null;
    public TextMeshProUGUI showStats2=null;

    public void Awake()
    {
        WhichHero();
    }
    public void closeHeroRunePopUp()
    {
        heroRunePopUp.SetActive(false);
    }
    public void HeroRunePopUp() 
    {
        RemoveListener();
        alpha1.onClick.AddListener(() => Alpha1PopUp());
        beta1.onClick.AddListener(() => Beta1PopUp());
        beta2.onClick.AddListener(() => Beta2PopUp());
        gamma1.onClick.AddListener(() => Gamma1PopUp());
        gamma2.onClick.AddListener(() => Gamma2PopUp());
        gamma3.onClick.AddListener(() => Gamma3PopUp());
    }
    public void WhichHero()
    {
        hero1.onClick.AddListener(() => GetHero1());
        hero2.onClick.AddListener(() => GetHero2());
        hero3.onClick.AddListener(() => GetHero3());
    }
    public void GetHero1()
    {
        currentHero = heroInventory.heroInventoryContainer.heroIDList[0];
        RefreshHeroRuneInventory();
        Debug.Log("Hero " + currentHero.heroID + " selected");
    }
    public void GetHero2()
    {
        currentHero = heroInventory.heroInventoryContainer.heroIDList[1];
        RefreshHeroRuneInventory();
        Debug.Log("Hero " + currentHero.heroID + " selected");
    }
    public void GetHero3()
    {
        currentHero = heroInventory.heroInventoryContainer.heroIDList[2];
        RefreshHeroRuneInventory();
        Debug.Log("Hero " + currentHero.heroID + " selected");
    }
    public void RemoveListener()
    {
        alpha1.onClick.RemoveAllListeners();
        beta1.onClick.RemoveAllListeners();
        beta2.onClick.RemoveAllListeners();
        gamma1.onClick.RemoveAllListeners();
        gamma2.onClick.RemoveAllListeners();
        gamma3.onClick.RemoveAllListeners();
        heroRemoveButton.onClick.RemoveAllListeners();
    }
    public void replaceRune(Rune rune)
    {
        Rune replaceThisRune = currentHero.alpha1Rune;
        currentHero.equippedRunes.Remove(replaceThisRune);
        currentHero.equippedRunes.Add(rune);
        playerRuneInventoryContainer.playerRuneInventoryList.Remove(rune);
        playerRuneInventoryContainer.playerRuneInventoryList.Add(replaceThisRune);
        currentHero.alpha1Rune = rune;
        inventoryManager.RefreshInventoryRune();
        RefreshHeroRuneInventory();
        closeHeroRunePopUp();
    }

    public void setAlpha1(Rune rune)
    {
        if (currentHero.alpha1Rune.runeSlot == RuneSlot.DummyRune)
        {
            aGlow.SetActive(false);
            currentHero.equippedRunes.Add(rune);
            currentHero.alpha1Rune = rune;
            playerRuneInventoryContainer.playerRuneInventoryList.Remove(rune);
            inventoryManager.RefreshInventoryRune();
            RefreshHeroRuneInventory();
        }
        else
        {
            replaceRune(rune);
        }
    }
    public void setBeta1(Rune rune)
    {
        if (currentHero.beta1Rune.runeSlot == RuneSlot.DummyRune)
        {
            b1Glow.SetActive(false);
            b2Glow.SetActive(false);
            currentHero.equippedRunes.Add(rune);
            currentHero.beta1Rune = rune;
            playerRuneInventoryContainer.playerRuneInventoryList.Remove(rune);
            inventoryManager.RefreshInventoryRune();
            RefreshHeroRuneInventory();
        }
        else
        {
            replaceRune(rune);
        }

    }
    public void setBeta2(Rune rune)
    {
        if (currentHero.beta2Rune.runeSlot == RuneSlot.DummyRune)
        {
            b1Glow.SetActive(false);
            b2Glow.SetActive(false);
            currentHero.equippedRunes.Add(rune);
            currentHero.beta2Rune = rune;
            playerRuneInventoryContainer.playerRuneInventoryList.Remove(rune);
            inventoryManager.RefreshInventoryRune();
            RefreshHeroRuneInventory();
        }
        else
        {
            Debug.Log("Beta2 Full");
        }
    }
    public void setGamma1(Rune rune)
    {
        if (currentHero.gamma1Rune.runeSlot == RuneSlot.DummyRune)
        {
            g1Glow.SetActive(false);
            g2Glow.SetActive(false);
            g3Glow.SetActive(false);
            currentHero.equippedRunes.Add(rune);
            currentHero.gamma1Rune = rune;
            playerRuneInventoryContainer.playerRuneInventoryList.Remove(rune);
            inventoryManager.RefreshInventoryRune();
            RefreshHeroRuneInventory();
        }
        else
        {
            Debug.Log("Gamma1 Full");
        }
    }
    public void setGamma2(Rune rune)
    {
        if (currentHero.gamma2Rune.runeSlot == RuneSlot.DummyRune)
        {
            g1Glow.SetActive(false);
            g2Glow.SetActive(false);
            g3Glow.SetActive(false);
            currentHero.equippedRunes.Add(rune);
            currentHero.gamma2Rune = rune;
            playerRuneInventoryContainer.playerRuneInventoryList.Remove(rune);
            inventoryManager.RefreshInventoryRune();
            RefreshHeroRuneInventory();
        }
        else
        {
            Debug.Log("Gamma2 Full");
        }
    }
    public void setGamma3(Rune rune)
    {
        if (currentHero.gamma3Rune.runeSlot == RuneSlot.DummyRune)
        {
            g1Glow.SetActive(false);
            g2Glow.SetActive(false);
            g3Glow.SetActive(false);
            currentHero.equippedRunes.Add(rune);
            currentHero.gamma3Rune = rune;
            playerRuneInventoryContainer.playerRuneInventoryList.Remove(rune);
            inventoryManager.RefreshInventoryRune();
            RefreshHeroRuneInventory();
        }
        else
        {
            Debug.Log("Gamma3 Full");
        }
    }
    public void EquipRune(Rune rune)
    {
        inventoryManager.runePopUp.SetActive(false);
        if (currentHero.heroID != "")
        {
            if (rune.runeSlot == RuneSlot.Alpha)
            {
                aGlow.SetActive(true);
                alpha1.onClick.AddListener(() => setAlpha1(rune));
            }
            if (rune.runeSlot == RuneSlot.Beta)
            {
                b1Glow.SetActive(true);
                b2Glow.SetActive(true);
                beta1.onClick.AddListener(() => setBeta1(rune));
                beta2.onClick.AddListener(() => setBeta2(rune));
            }
            if (rune.runeSlot == RuneSlot.Gamma)
            {
                g1Glow.SetActive(true);
                g2Glow.SetActive(true);
                g3Glow.SetActive(true);
                gamma1.onClick.AddListener(() => setGamma1(rune));
                gamma2.onClick.AddListener(() => setGamma2(rune));
                gamma3.onClick.AddListener(() => setGamma3(rune));
            }
        }
        else
        {
            Debug.Log("No Hero Selected");
        }
    }
    public void Alpha1PopUp()
    {
        if(currentHero.alpha1Rune.runeSlot != RuneSlot.DummyRune)
        {
            heroRunePopUp.SetActive(true);
        }
        Rune rune = currentHero.alpha1Rune;
        runeSet.sprite = currentHero.alpha1Rune.GetRuneSetSprite(rune);
        setRuneDetails(rune);
        heroRemoveButton.onClick.AddListener(() => RemoveHeroRune(rune));
    }
    public void Beta1PopUp()
    {
        if (currentHero.beta1Rune.runeSlot != RuneSlot.DummyRune)
        {
            heroRunePopUp.SetActive(true);
        }
        Rune rune = currentHero.beta1Rune;
        runeSet.sprite = currentHero.beta1Rune.GetRuneSetSprite(rune);
        setRuneDetails(rune);
        heroRemoveButton.onClick.AddListener(() => RemoveHeroRune(rune));
    }
    public void Beta2PopUp()
    {
        if (currentHero.beta2Rune.runeSlot != RuneSlot.DummyRune)
        {
            heroRunePopUp.SetActive(true);
        }
        Rune rune = currentHero.beta2Rune;
        runeSet.sprite = currentHero.beta2Rune.GetRuneSetSprite(rune);
        setRuneDetails(rune);
        heroRemoveButton.onClick.AddListener(() => RemoveHeroRune(rune));
    }
    public void Gamma1PopUp()
    {
        if (currentHero.gamma1Rune.runeSlot != RuneSlot.DummyRune)
        {
            heroRunePopUp.SetActive(true);
        }
        Rune rune = currentHero.gamma1Rune;
        runeSet.sprite = currentHero.gamma1Rune.GetRuneSetSprite(rune);
        setRuneDetails(rune);
        heroRemoveButton.onClick.AddListener(() => RemoveHeroRune(rune));
    }
    public void Gamma2PopUp()
    {
        if (currentHero.gamma2Rune.runeSlot != RuneSlot.DummyRune)
        {
            heroRunePopUp.SetActive(true);
        }
        Rune rune = currentHero.gamma2Rune;
        runeSet.sprite = currentHero.gamma2Rune.GetRuneSetSprite(rune);
        setRuneDetails(rune);
        Debug.Log("gamma2button clicked ");
        heroRemoveButton.onClick.AddListener(() => RemoveHeroRune(rune));
    }
    public void Gamma3PopUp()
    {
        if (currentHero.gamma3Rune.runeSlot != RuneSlot.DummyRune)
        {
            heroRunePopUp.SetActive(true);
        }
        Rune rune = currentHero.gamma3Rune;
        runeSet.sprite = currentHero.gamma3Rune.GetRuneSetSprite(rune);
        setRuneDetails(rune);
        heroRemoveButton.onClick.AddListener(() => RemoveHeroRune(rune));
    }
    public void setRuneDetails(Rune rune)
    {
        heroRuneDetailImageGO.sprite = rune.GetRuneSprite(rune);
        displayHeroAlphaBetaGamma.text = rune.runeSlot.ToString();
        heroRuneMainStat.text = rune.mainStat.type.ToString();
        heroRuneMainStatNumber.text = rune.mainStat.value.ToString();
        heroRuneFirstSubStat.text = ("");
        heroRuneFirstSubStatValue.text = ("");
        heroRuneSecondSubStat.text = ("");
        heroRuneSecondSubStatValue.text = ("");
        heroRuneThirdSubStat.text = ("");
        heroRuneThirdSubStatValue.text = ("");
        foreach (SubStat subStat in rune.subStats)
        {
            if (heroRuneFirstSubStat.text != "")
            {
                if (heroRuneSecondSubStat.text != "")
                {
                    heroRuneThirdSubStat.text = subStat.type.ToString();
                    heroRuneThirdSubStatValue.text = subStat.value.ToString();
                }
                else
                {
                    heroRuneSecondSubStat.text = subStat.type.ToString();
                    heroRuneSecondSubStatValue.text = subStat.value.ToString();
                }
            }
            else
            {
                heroRuneFirstSubStat.text = subStat.type.ToString();
                heroRuneFirstSubStatValue.text = subStat.value.ToString();
            }
        }
    }
    public void RemoveHeroRune(Rune rune)
    {
        heroRemoveButton.onClick.RemoveAllListeners();
        currentHero.equippedRunes.Remove(rune);
        playerRuneInventoryContainer.playerRuneInventoryList.Add(rune);
        if(rune == currentHero.alpha1Rune)
            currentHero.alpha1Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
        if (rune == currentHero.beta1Rune)
            currentHero.beta1Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
        if (rune == currentHero.beta2Rune)
            currentHero.beta2Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
        if (rune == currentHero.gamma1Rune)
            currentHero.gamma1Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
        if (rune == currentHero.gamma2Rune)
            currentHero.gamma2Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
        if (rune == currentHero.gamma3Rune)
            currentHero.gamma3Rune = new Rune(RuneSlot.DummyRune, RuneSetBonus.DummySet);
        RefreshHeroRuneInventory();
        inventoryManager.RefreshInventoryRune();
        
        heroRunePopUp.SetActive(false);
    }
   
    public void RefreshHeroRuneInventory()
    {
        ATKDMGVAL = 0;
        CRITDMGVAL = 0;
        ATKSetCount = 0;
        CRITDMGSetCount = 0;
        alpha1Slot.sprite = currentHero.alpha1Rune.GetRuneSprite(currentHero.alpha1Rune);
        beta1Slot.sprite = currentHero.beta1Rune.GetRuneSprite(currentHero.beta1Rune);
        beta2Slot.sprite = currentHero.beta2Rune.GetRuneSprite(currentHero.beta2Rune);
        gamma1Slot.sprite = currentHero.gamma1Rune.GetRuneSprite(currentHero.gamma1Rune);
        gamma2Slot.sprite = currentHero.gamma2Rune.GetRuneSprite(currentHero.gamma2Rune);
        gamma3Slot.sprite = currentHero.gamma3Rune.GetRuneSprite(currentHero.gamma3Rune);

        foreach (Rune rune in currentHero.equippedRunes)
        {
            switch (rune.mainStat.type)
            {
                case MainStats.AttackDamage:
                    ATKDMGVAL += rune.mainStat.value;
                    break;
                case MainStats.CritDamage:
                    CRITDMGVAL += rune.mainStat.value;
                    break;
            }
        }
        SetEffect();
        ATK.text = ATKDMGVAL.ToString();
        CRITDMG.text = CRITDMGVAL.ToString();
        HeroRunePopUp();

    }
    public void SetEffect()
    {
        List<RuneSetManager> SetCounter = new List<RuneSetManager>();
        foreach(Rune rune in currentHero.equippedRunes)
        {
            switch (rune.runeSet)
            {
                case RuneSetBonus.ATKSet:ATKSetCount = ATKSetCount + 1;
                    break;
                case RuneSetBonus.CRITDMGSet:CRITDMGSetCount = CRITDMGSetCount + 1;
                    break;
            }
        }
        if (ATKSetCount >= 3)
        {
            ATKDMGVAL += 20;
            ShowActiveSet("ATK3");
        }
        if (ATKSetCount >= 6)
        {
            ATKDMGVAL += 80;
            ShowActiveSet("ATK6");
        }
        if (CRITDMGSetCount >= 3)
        {
            CRITDMGVAL += 100;
            ShowActiveSet("CRITDMG3");
        }
        if (CRITDMGSetCount >= 6)
        {
            CRITDMGVAL += 999;
            ShowActiveSet("CRITDMG6");
        }
    }
    public void ShowActiveSet(string setNameAndRank)
    {   
        if (showStats1.text != "")
        {
            showStats2.text = setNameAndRank;
        }
        else
        {
            showStats1.text = setNameAndRank;
        }
    }
}
