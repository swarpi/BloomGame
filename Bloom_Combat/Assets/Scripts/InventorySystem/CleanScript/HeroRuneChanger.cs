using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class HeroRuneChanger : MonoBehaviour
{
    public GameObject runeInfoImagePrefab;
    public GameObject playerRuneInfoImagePrefab;
    public GameObject ConfirmPrefab;
    public HeroSaveData currentHero;
    public HeroInventory heroInventory;
    public RuneLogic runeLogic;
    public GameObject contentGO;
    public Canvas myCanvas;
    public GameObject arrows;
    public RuneStatsLogic statLogic;

    public GameObject TEMPRune;
    public void Start()
    {
        OpenHeroRuneChanger();
        WhichHero();
    }
    public void WhichHero()
    {
        //Find HeroButton, wird erstetzt
        Button hero1Button = myCanvas.transform.Find("UI_Base_Background").Find("hero1").GetComponent<Button>();
        Button hero2Button = myCanvas.transform.Find("UI_Base_Background").Find("hero2").GetComponent<Button>();
        Button swapButton = myCanvas.transform.Find("UI_Base_Background").Find("Swap").GetComponent<Button>();
        hero1Button.onClick.RemoveAllListeners();
        hero2Button.onClick.RemoveAllListeners();
        swapButton.onClick.RemoveAllListeners();
        hero1Button.onClick.AddListener(() => GetHero1());
        hero2Button.onClick.AddListener(() => GetHero2());
        swapButton.onClick.AddListener(() => Debug.Log(currentHero.equippedRunes.Count + " equipped"));
    }
    public void GetHero1()
    {
        runeLogic.TempRuneResetForHeroSwitch();
        currentHero = heroInventory.heroInventoryContainer.heroIDList[0];
        Debug.Log("Hero " + currentHero.heroID + " selected");
        SetTEMPRunesForHero();
        runeLogic.RefreshHeroRuneInventory();
        statLogic.DisplayEquippedRunesStats();
        runeLogic.SetOffGlow();
        closePopUp("ALL");
    }
    public void GetHero2()
    {
        runeLogic.TempRuneResetForHeroSwitch();
        currentHero = heroInventory.heroInventoryContainer.heroIDList[1];
        Debug.Log("Hero " + currentHero.heroID + " selected");
        SetTEMPRunesForHero();
        runeLogic.RefreshHeroRuneInventory();
        statLogic.DisplayEquippedRunesStats();
        runeLogic.SetOffGlow();
        closePopUp("ALL");
    }
    public void SetTEMPRunesForHero()
    {
        //TempRune Liste kopiert runen von EquippedRune Liste
        runeLogic.AllTEMPRune = new List<Rune>(new Rune[6]);
        for (int i = 0; i < runeLogic.AllTEMPRune.Count; i++)
        {
            runeLogic.AllTEMPRune[i] = currentHero.equippedRunes[i];
        }
        
    }
    public void OpenHeroRuneChanger()
    {
        // Fuer jede rune wird ein Gameobj erstellt, die erstellten GAmeobj werden als child von ContentGO
        RuneDisplayer.Instance.DisplayRunesToContentGO(contentGO);
        //durch contentGO children drurchgehen, Background von child finden, diesen Gameobject onclick addlistnere, methode 21 hinzuf
        foreach(Transform child in contentGO.transform)
        {
            child.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
            child.GetComponentInChildren<Button>().onClick.AddListener(() => DisplayRuneInfoWindow(child.GetComponent<RuneContainer>().rune, "PlayerRuneInventory"));
        }
    }
    public void closePopUp(string closeWhat)
    {
        //schliesst alle PopUps
        switch (closeWhat)
        {
            case "TEMP":
                if (GameObject.Find("runePopUp(Clone)"))
                {
                    Destroy(GameObject.Find("runePopUp(Clone)"));
                }
                break;
            case "PLAYER":
                if (GameObject.Find("playerRunePopUp(Clone)"))
                {
                    Destroy(GameObject.Find("playerRunePopUp(Clone)"));
                }
                break;
            case "ALL":
                if (GameObject.Find("runePopUp(Clone)"))
                {
                    Destroy(GameObject.Find("runePopUp(Clone)"));
                }
                if (GameObject.Find("playerRunePopUp(Clone)"))
                {
                    Destroy(GameObject.Find("playerRunePopUp(Clone)"));
                }
                break;
        }
        if (GameObject.Find("confirm(Clone)"))
        {
            Destroy(GameObject.Find("confirm(Clone)"));
        }

    }
    public void DisplayRuneInfoWindow(Rune rune,String whichInventory)
    {
        //RunePopUp Runeninfo
        if (whichInventory == "PlayerRuneInventory")
        {
            runeLogic.SetOffGlow();
            GameObject EquippedRuneImage;
            closePopUp("PLAYER");
             EquippedRuneImage = Instantiate(playerRuneInfoImagePrefab);
            EquippedRuneImage.transform.SetParent(myCanvas.transform);
            EquippedRuneImage.transform.localPosition = new Vector3(-250, -230, 0);
            EquippedRuneImage.transform.Find("runeDetailSlotTemplate").Find("runeDetailImage").GetComponent<Image>().sprite = rune.GetRuneSprite(rune);
            EquippedRuneImage.transform.Find("runeSetIconSlotTemplate").Find("runeSetImage").GetComponent<Image>().sprite = rune.GetRuneSetSprite(rune);
            EquippedRuneImage.transform.Find("runeMainStat").Find("mainStat").GetComponent<TextMeshProUGUI>().text = rune.mainStat.type.ToString();
            EquippedRuneImage.transform.Find("runeMainStat").Find("mainStatNumber").GetComponent<TextMeshProUGUI>().text = rune.mainStat.value.ToString();
            EquippedRuneImage.transform.Find("displayAlphaBetaGamma").GetComponent<TextMeshProUGUI>().text = rune.runeSlot.ToString();
            TextMeshProUGUI Er1SubStat = EquippedRuneImage.transform.Find("runeSubStat").Find("firstSubStat").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI Er2SubStat = EquippedRuneImage.transform.Find("runeSubStat").Find("secondSubStat").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI Er3SubStat = EquippedRuneImage.transform.Find("runeSubStat").Find("thirdSubStat").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI Er1SubStatValue = EquippedRuneImage.transform.Find("runeSubStat").Find("firstSubStat").Find("firstSubStatValue").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI Er2SubStatValue = EquippedRuneImage.transform.Find("runeSubStat").Find("secondSubStat").Find("secondSubStatValue").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI Er3SubStatValue = EquippedRuneImage.transform.Find("runeSubStat").Find("thirdSubStat").Find("thirdSubStatValue").GetComponent<TextMeshProUGUI>();
            Er1SubStat.SetText("");
            Er2SubStat.SetText("");
            Er3SubStat.SetText("");
            Er1SubStatValue.SetText("");
            Er2SubStatValue.SetText("");
            Er3SubStatValue.SetText("");
            foreach (SubStat subStat in rune.subStats)
            {
                if (Er1SubStat.text != "")
                {
                    if (Er2SubStat.text != "")
                    {
                        Er3SubStat.text = subStat.type.ToString();
                        Er3SubStatValue.text = subStat.value.ToString();
                    }
                    else
                    {
                        Er2SubStat.text = subStat.type.ToString();
                        Er2SubStatValue.text = subStat.value.ToString();
                    }
                }
                else
                {
                    Er1SubStat.text = subStat.type.ToString();
                    Er1SubStatValue.text = subStat.value.ToString();
                }
            }
            EquippedRuneImage.transform.Find("closePopUp").GetComponent<Button>().onClick.RemoveAllListeners();
            EquippedRuneImage.transform.Find("closePopUp").GetComponent<Button>().onClick.AddListener(() => Destroy(GameObject.Find("playerRunePopUp(Clone)")));
            EquippedRuneImage.transform.Find("runeButtons").Find("Equip").GetComponent<Button>().onClick.RemoveAllListeners();
            EquippedRuneImage.transform.Find("runeButtons").Find("Equip").GetComponent<Button>().onClick.AddListener(() => runeLogic.EquipTEMPRune(rune, currentHero));
            EquippedRuneImage.transform.Find("runeButtons").Find("Disenchant").GetComponent<Button>().onClick.RemoveAllListeners();
            EquippedRuneImage.transform.Find("runeButtons").Find("Disenchant").GetComponent<Button>().onClick.AddListener(() => Debug.Log("Guten"));
        }
        if(whichInventory == "TEMPRune")
        {
            runeLogic.SetOffGlow();
            GameObject RuneImage;
            closePopUp("TEMP");
            RuneImage = Instantiate(runeInfoImagePrefab);
            RuneImage.transform.SetParent(myCanvas.transform);
            RuneImage.transform.localPosition = new Vector3(-650, -230, 0);
            RuneImage.transform.Find("runeDetailSlotTemplate").Find("runeDetailImage").GetComponent<Image>().sprite = rune.GetRuneSprite(rune);
            RuneImage.transform.Find("runeSetIconSlotTemplate").Find("runeSetImage").GetComponent<Image>().sprite = rune.GetRuneSetSprite(rune);
            RuneImage.transform.Find("runeMainStat").Find("mainStat").GetComponent<TextMeshProUGUI>().text = rune.mainStat.type.ToString();
            RuneImage.transform.Find("runeMainStat").Find("mainStatNumber").GetComponent<TextMeshProUGUI>().text = rune.mainStat.value.ToString();
            RuneImage.transform.Find("displayAlphaBetaGamma").GetComponent<TextMeshProUGUI>().text = rune.runeSlot.ToString();
            TextMeshProUGUI r1SubStat = RuneImage.transform.Find("runeSubStat").Find("firstSubStat").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI r2SubStat = RuneImage.transform.Find("runeSubStat").Find("secondSubStat").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI r3SubStat = RuneImage.transform.Find("runeSubStat").Find("thirdSubStat").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI r1SubStatValue = RuneImage.transform.Find("runeSubStat").Find("firstSubStat").Find("firstSubStatValue").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI r2SubStatValue = RuneImage.transform.Find("runeSubStat").Find("secondSubStat").Find("secondSubStatValue").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI r3SubStatValue = RuneImage.transform.Find("runeSubStat").Find("thirdSubStat").Find("thirdSubStatValue").GetComponent<TextMeshProUGUI>();
            r1SubStat.SetText("");
            r2SubStat.SetText("");
            r3SubStat.SetText("");
            r1SubStatValue.SetText("");
            r2SubStatValue.SetText("");
            r3SubStatValue.SetText("");
            foreach (SubStat subStat in rune.subStats)
            {
                if (r1SubStat.text != "")
                {
                    if (r2SubStat.text != "")
                    {
                        r3SubStat.text = subStat.type.ToString();
                        r3SubStatValue.text = subStat.value.ToString();
                    }
                    else
                    {
                        r2SubStat.text = subStat.type.ToString();
                        r2SubStatValue.text = subStat.value.ToString();
                    }
                }
                else
                {
                    r1SubStat.text = subStat.type.ToString();
                    r1SubStatValue.text = subStat.value.ToString();
                }
            }
            RuneImage.transform.Find("closePopUp").GetComponent<Button>().onClick.RemoveAllListeners();
            RuneImage.transform.Find("closePopUp").GetComponent<Button>().onClick.AddListener(() => Destroy(GameObject.Find("runePopUp(Clone)")));
            Destroy(RuneImage.transform.Find("runeButtons").Find("Disenchant").gameObject);
            RuneImage.transform.Find("runeButtons").Find("Equip").Find("equip").GetComponent<Text>().text = "Remove";
            RuneImage.transform.Find("runeButtons").Find("Equip").GetComponent<Button>().onClick.RemoveAllListeners();
            RuneImage.transform.Find("runeButtons").Find("Equip").GetComponent<Button>().onClick.AddListener(() => runeLogic.RemoveTEMPRune(rune));
        }
        if (currentHero.heroID != "")
        {
            arrows.transform.Find("TempToEquip").GetComponent<Button>().onClick.RemoveAllListeners();
            arrows.transform.Find("TempToEquip").GetComponent<Button>().onClick.AddListener(() => ConfirmSwap());
        }
    }
    public void ConfirmSwap()
    {
        GameObject confirmWindow;
        confirmWindow = Instantiate(ConfirmPrefab);
        confirmWindow.transform.SetParent(myCanvas.transform);
        confirmWindow.transform.localPosition = new Vector3(0, 0, 0);

        confirmWindow.transform.Find("Confirm").GetComponent<Button>().onClick.RemoveAllListeners();
        confirmWindow.transform.Find("Decline").GetComponent<Button>().onClick.RemoveAllListeners();
        confirmWindow.transform.Find("Confirm").GetComponent<Button>().onClick.AddListener(() => SwapTEMPToEquipRunes());
        confirmWindow.transform.Find("Decline").GetComponent<Button>().onClick.AddListener(() => closePopUp("ALL"));
    }
    public void SwapTEMPToEquipRunes()
    {
        //Runen der TempRune Liste werden in die EquippedRune Liste gesetzt/gespeichert
        closePopUp("ALL");
        for (int i = 0; i < runeLogic.AllTEMPRune.Count ; i++)
        {
            currentHero.equippedRunes[i] = runeLogic.AllTEMPRune[i];
        }
        runeLogic.RefreshHeroRuneInventory();
        statLogic.DisplayEquippedRunesStats();
        
    }

}
