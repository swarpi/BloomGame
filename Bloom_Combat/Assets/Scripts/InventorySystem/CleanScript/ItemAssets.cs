using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAssets : MonoBehaviour
{
    public static ItemAssets Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    public Sprite weaponSprite;
    public Sprite alpharuneSprite;
    public Sprite betaruneSprite;
    public Sprite gammaruneSprite;
    public Sprite potionSprite;
    public Sprite moneySprite;

}
