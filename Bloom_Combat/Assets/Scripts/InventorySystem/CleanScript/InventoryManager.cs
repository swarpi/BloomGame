using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InventoryManager : MonoBehaviour
{
    public event EventHandler OnListChanged;
    public PlayerInventoryContainer playerInv;
    public void Start()
    {
        //Adding Runes
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());
        playerInv.playerRuneInventoryList.Add(RuneManager.GenerateRandomRune());

        //Adding Items
        //playerInv.playerItemInventoryList.Add(new Item { itemType = Item.ItemType.Weapon, Amount = 1 });
        //playerInv.playerItemInventoryList.Add(new Item { itemType = Item.ItemType.Potion, Amount = 3 });
        //playerInv.playerItemInventoryList.Add(new Item { itemType = Item.ItemType.Money, Amount = 4 });
    }
    //Runes
    public void AddRune(Rune rewardRune)
    {
        playerInv.playerRuneInventoryList.Add(rewardRune);
        OnListChanged?.Invoke(this, EventArgs.Empty);
    }
    public void RemoveRune(Rune rune)
    {
        playerInv.playerRuneInventoryList.Remove(rune);
        OnListChanged?.Invoke(this, EventArgs.Empty);
    }

    //Items
    public void AddItem(Item item)
    {

        if (item.isStackable())
        {
            bool itemAlreadyInInventory = false;
            foreach (Item inventoryItem in playerInv.playerItemInventoryList)
            {
                if (inventoryItem.itemType == item.itemType)
                {
                    inventoryItem.Amount += item.Amount;
                    itemAlreadyInInventory = true;
                }
            }
            if (!itemAlreadyInInventory)
            {
                playerInv.playerItemInventoryList.Add(item);
            }
        }
        else
        {
            playerInv.playerItemInventoryList.Add(item);
        }
        OnListChanged?.Invoke(this, EventArgs.Empty);
    }
    public void RemoveItem(Item item)
    {
        if (item.isStackable())
        {
            Item itemInInventory = null;
            foreach (Item inventoryItem in playerInv.playerItemInventoryList)
            {
                if (inventoryItem.itemType == item.itemType)
                {
                    inventoryItem.Amount -= item.Amount;
                    itemInInventory = inventoryItem;
                }
            }
            if (itemInInventory != null && itemInInventory.Amount <= 0)
            {
                playerInv.playerItemInventoryList.Remove(itemInInventory);
            }
            else
            {
                playerInv.playerItemInventoryList.Remove(item);
            }
            OnListChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}