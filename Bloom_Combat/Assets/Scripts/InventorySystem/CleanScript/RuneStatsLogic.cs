using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class RuneStatsLogic : MonoBehaviour
{
    public HeroRuneChanger heroRune;
    public RuneLogic runeLogic;
    public GameObject StatDisplayer;
    public GameObject RuneSetDisplay;
    

    float ATK;
    float CritRate;
    float CritDMG;
    float ElementDMG;

    float ATKNew;
    float CritRateNew;
    float CritDMGNew;
    float ElementDMGNew;

    int ATKCount;
    int CRITDMGCount;

    int ATKCountNew;
    int CRITDMGCountNew;

    public void CalculateEquippedRunes()
    {
        //Rechnet alle Equipped Rune Values zusammen
        ATK = 0;
        CritRate = 0;
        CritDMG = 0;
        ElementDMG = 0;
        foreach(Rune rune in heroRune.currentHero.equippedRunes)
        {
            switch (rune.mainStat.type)
            {
                case MainStats.AttackDamage:
                    ATK +=  rune.mainStat.value;
                    break;
                case MainStats.CritDamage:
                    CritDMG += rune.mainStat.value;
                    break;
                case MainStats.ElementalBonusDamage:
                    ElementDMG += rune.mainStat.value;
                    break;
                default:
                    Debug.Log("Empty RuneList");
                    break;
            }

            foreach(SubStat substat in rune.subStats)
            {
                switch (substat.type)
                {
                    case SubStats.AttackDamage:
                        ATK += substat.value;
                        break;
                    case SubStats.CritRate:
                        CritRate += substat.value;
                        break;
                    case SubStats.ElementalBonusDamage:
                        ElementDMG += substat.value;
                        break;
                    default:
                        Debug.Log("Empty RuneList");
                        break;
                }
            }
        }
    }
    public void CalculateTEMPRunes()
    {
        //Rechnet alle TEMP Rune Values zusammen
        ATKNew = 0;
        CritRateNew = 0;
        CritDMGNew = 0;
        ElementDMGNew = 0;
        foreach (Rune rune in runeLogic.AllTEMPRune)
        {
            switch (rune.mainStat.type)
            {
                case MainStats.AttackDamage:
                    ATKNew += rune.mainStat.value;
                    break;
                case MainStats.CritDamage:
                    CritDMGNew += rune.mainStat.value;
                    break;
                case MainStats.ElementalBonusDamage:
                    ElementDMGNew += rune.mainStat.value;
                    break;
                default:
                    Debug.Log("Empty RuneList");
                    break;
            }

            foreach (SubStat substat in rune.subStats)
            {
                switch (substat.type)
                {
                    case SubStats.AttackDamage:
                        ATKNew += substat.value;
                        break;
                    case SubStats.CritRate:
                        CritRateNew += substat.value;
                        break;
                    case SubStats.ElementalBonusDamage:
                        ElementDMGNew += substat.value;
                        break;
                    default:
                        Debug.Log("Empty RuneList");
                        break;
                }
            }
        }
    }
    public void EquippedRunesSet()
    {
        //Checkt Equipped RuneSet : setzt Count eins hoeher wenn er set in der Liste findet
        ATKCount = 0;
        CRITDMGCount = 0;
        foreach (Rune rune in heroRune.currentHero.equippedRunes)
        {
            
            switch (rune.runeSet)
            {
                case RuneSetBonus.ATKSet:
                    ATKCount += 1;
                    break;
                case RuneSetBonus.CRITDMGSet:
                    CRITDMGCount += 1;
                    break;
            }
        }
    }
    public void TEMPRunesSet()
    {
        //Checkt Equipped RuneSet : setzt Count eins hoeher wenn er set in der Liste findet
        ATKCountNew = 0;
        CRITDMGCountNew = 0;
        foreach (Rune rune in heroRune.runeLogic.AllTEMPRune)
        {
            switch (rune.runeSet)
            {
                case RuneSetBonus.ATKSet:
                    ATKCountNew += 1;
                    break;
                case RuneSetBonus.CRITDMGSet:
                    CRITDMGCountNew += 1;
                    break;
            }
        }
    }
    public void SetSet(String RuneSet , int SetCount)
    {
        //Check SetCount fuer Setbonis
        if (SetCount == 2 || SetCount == 3)
        {
            switch (RuneSet.ToString())
            {
                case "ATK":
                    RuneSetDisplay.transform.Find("equippedRuneSet").Find("equippedSet").GetComponent<TextMeshProUGUI>().text += "ATK2";
                    ATK += 100;
                    break;
                case "CRITDMG":
                    RuneSetDisplay.transform.Find("equippedRuneSet").Find("equippedSet").GetComponent<TextMeshProUGUI>().text += "CRITDMG2";
                    CritDMG += 100;
                    break;
                case "ATKNEW":
                    RuneSetDisplay.transform.Find("TEMPRuneSet").Find("TEMPSet").GetComponent<TextMeshProUGUI>().text += "ATK2";
                    ATKNew += 100;
                    break;
                case "CRITDMGNEW":
                    RuneSetDisplay.transform.Find("TEMPRuneSet").Find("TEMPSet").GetComponent<TextMeshProUGUI>().text += "CRITDMG2";
                    CritDMGNew += 100;
                    break;
            }
        }
        if(SetCount >= 4)
        {
            switch (RuneSet.ToString())
            {
                case "ATK":
                    RuneSetDisplay.transform.Find("equippedRuneSet").Find("equippedSet").GetComponent<TextMeshProUGUI>().text += "ATK4";
                    ATK += 999;
                    break;
                case "CRITDMG":
                    RuneSetDisplay.transform.Find("equippedRuneSet").Find("equippedSet").GetComponent<TextMeshProUGUI>().text += "CRITDMG4";
                    CritDMG += 999;
                    break;
                case "ATKNEW":
                    RuneSetDisplay.transform.Find("TEMPRuneSet").Find("TEMPSet").GetComponent<TextMeshProUGUI>().text += "ATK4";
                    ATKNew += 999;
                    break;
                case "CRITDMGNEW":
                    RuneSetDisplay.transform.Find("TEMPRuneSet").Find("TEMPSet").GetComponent<TextMeshProUGUI>().text += "CRITDMG4";
                    CritDMGNew += 999;
                    break;
            }
        }

    }
    public void CompareStats()
    {
        //Vergleicht Stats zwischen Equipped Rune und TEMP Rune Values
        CalculateEquippedRunes();
        CalculateTEMPRunes();
        EquippedRunesSet();
        TEMPRunesSet();
        SetSet("ATK", ATKCount);
        SetSet("CRITDMG", CRITDMGCount);
        SetSet("ATKNEW", ATKCountNew);
        SetSet("CRITDMGNEW", CRITDMGCountNew);

        DisplayDiff(ATKNew, ATK, "ATK");
        DisplayDiff(CritRateNew, CritRate, "CRITRATE");
        DisplayDiff(CritDMGNew, CritDMG, "CRITDMG");
        DisplayDiff(ElementDMGNew, ElementDMG, "ELEMENTDMG");
    }
    public void DisplayEquippedRunesStats()
    {
        //Display Equipped Rune Values
        RuneSetDisplay.transform.Find("equippedRuneSet").Find("equippedSet").GetComponent<TextMeshProUGUI>().text = "";
        RuneSetDisplay.transform.Find("TEMPRuneSet").Find("TEMPSet").GetComponent<TextMeshProUGUI>().text = "";
        CompareStats();
        StatDisplayer.transform.Find("Show_Stats").Find("ATK").Find("ATK VALUE").GetComponent<TextMeshProUGUI>().text = ATK.ToString();
        StatDisplayer.transform.Find("Show_Stats").Find("CRITRATE").Find("CRITRATE VALUE").GetComponent<TextMeshProUGUI>().text = CritRate.ToString();
        StatDisplayer.transform.Find("Show_Stats").Find("CRITDMG").Find("CRITDMG VALUE").GetComponent<TextMeshProUGUI>().text = CritDMG.ToString();
        StatDisplayer.transform.Find("Show_Stats").Find("ELEMENTDMG").Find("ELEMENTDMG VALUE").GetComponent<TextMeshProUGUI>().text = ElementDMG.ToString();
    }
    public void DisplayDiff(float tempStat,float stat,string statName)
    {
        //Unteschiede zwischen Equipped und TEMP werden errechnet und angezeigt
        tempStat = tempStat - stat;

        if (tempStat > 0)
        {
            StatDisplayer.transform.Find("Show_Stats").Find(statName.ToString()).Find(statName.ToString() + " DIFF").GetComponent<TextMeshProUGUI>().text = "+" + tempStat.ToString();
            StatDisplayer.transform.Find("Show_Stats").Find(statName.ToString()).Find(statName.ToString() + " DIFF").GetComponent<TextMeshProUGUI>().color = Color.green;
        }
        else if (tempStat == 0)
        {
            StatDisplayer.transform.Find("Show_Stats").Find(statName.ToString()).Find(statName.ToString() + " DIFF").GetComponent<TextMeshProUGUI>().text = "";
        }
        else
        {
            StatDisplayer.transform.Find("Show_Stats").Find(statName.ToString()).Find(statName.ToString() + " DIFF").GetComponent<TextMeshProUGUI>().text = tempStat.ToString();
            StatDisplayer.transform.Find("Show_Stats").Find(statName.ToString()).Find(statName.ToString() + " DIFF").GetComponent<TextMeshProUGUI>().color = Color.red;
        }

        
     }
}
