using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class MouseManagerCombat : MonoBehaviour
{
	public Camera cam;

	public Color startColor;
	public Color mouseOverColor;
	public GameObject ourHitObject = null;
	Tile oldTile = null;
	Tile currentTile;

	public Skill selectedSkill;
	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

		// Is the mouse over a Unity UI Element?
		if (EventSystem.current.IsPointerOverGameObject())
		{
			// It is, so let's not do any of our own custom
			// mouse stuff, because that would be weird.

			// NOTE!  We might want to ask the system WHAT KIND
			// of object we're over -- so for things that aren't
			// buttons, we might not actually want to bail out early.

			return;
		}
		// could also check if game is paused?
		// if main menu is open?

		//Debug.Log( "Mouse Position: " + Input.mousePosition );

		// This only works in orthographic, and only gives us the
		// world position on the same plane as the camera's
		// near clipping play.  (i.e. It's not helpful for our application.)
		//Vector3 worldPoint = Camera.main.ScreenToWorldPoint( Input.mousePosition );
		//Debug.Log( "World Point: " + worldPoint );

		Ray ray = cam.ScreenPointToRay(Input.mousePosition);

		RaycastHit hitInfo;


		if (Physics.Raycast(ray, out hitInfo))
		{
			ourHitObject= hitInfo.collider.transform.gameObject;

			if (hitInfo.collider.transform.parent != null)
			{
				ourHitObject = hitInfo.collider.transform.parent.gameObject;
			}


			//Debug.Log("Clicked On: " + ourHitObject.name);

			// So...what kind of object are we over?
			if (ourHitObject.GetComponent<Tile>() != null)
			{
				currentTile = ourHitObject.GetComponent<Tile>();
				MouseOver_Hex(ourHitObject);

				//ourHitObject.GetComponentInChildren<MeshRenderer>().material.color = mouseOverColor;
			}
		}
	}

	void MouseOver_Hex(GameObject ourHitObject)
	{

		// We know what we're mousing over. 
		if (Input.GetMouseButtonDown(0))
		{

			// We have clicked on a hex.  Do something about it!
			// This might involve calling a bunch of other functions
			// depending on what mode you happen to be in, in your game.

			// depending on skill range colorize the tile

			// We're just gonna colorize the hex, as an example.
			MeshRenderer mr = ourHitObject.GetComponentInChildren<MeshRenderer>();
			if (oldTile == null)
			{
				oldTile = currentTile;
				oldTile.GetComponentInChildren<MeshRenderer>().material.color = mouseOverColor;
			}
			else if (oldTile != currentTile)
			{
				// change oldTile to old color
				oldTile.GetComponentInChildren<MeshRenderer>().material.color = startColor;
				// change currentTile to new color
				currentTile.GetComponentInChildren<MeshRenderer>().material.color = mouseOverColor;
				// set oldTile to currentTile
				oldTile = currentTile;
			}

			// If we have a unit selected, let's move it to this tile!


		}

	}


}
