using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillDisplayField : MonoBehaviour
{
    public Image imageField;


    private void Awake()
    {
        imageField = GetComponent<Image>();
    }

    public void setDisplayField(Skill skill)
    {
        imageField.sprite = skill.skillImage;
    }
}
