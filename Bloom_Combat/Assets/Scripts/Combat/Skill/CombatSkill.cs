using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Skill", menuName = "Skill/CombatSkill")]
public class CombatSkill : Skill
{
    public CombatSkill()
    {
        skillType = SkillType.COMBAT;
    }

}
