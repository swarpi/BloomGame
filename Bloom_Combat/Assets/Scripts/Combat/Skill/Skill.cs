using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum SkillType
{
    COMBAT,
    BUFF
}

[CreateAssetMenu(fileName = "New Skill", menuName = "Skill")]
public class Skill : ScriptableObject
{
    public string name;
    public SkillType skillType = SkillType.BUFF;
    public Sprite skillImage;

    public List<string> skillAreaString = new List<string>();
    // example : ["U","UL","UR+U"]
    // + ... go to UR then from there go U
}
