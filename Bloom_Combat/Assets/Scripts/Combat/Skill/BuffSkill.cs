using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Skill", menuName = "Skill/BuffSkill")]
public class BuffSkill : Skill
{
    public BuffSkill()
    {
        skillType = SkillType.BUFF;
    }

}
