using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldTranslator
{
    //translates commandList to Tilepositions
    //positions depend on startingPoint
    public List<Tile> ListToTile(List<string> commandList, Tile startingPoint)
{
    List<Tile> tileCoordinates = new List<Tile>();

    Debug.Log("starting point " + startingPoint.gameObject.name);
    // for loop 
    foreach (string command in commandList)
    {
        if (command.Contains("+"))
        {
            Tile tempStartingPoint = startingPoint;
            string[] subs = command.Split('+');
            foreach (string subCommand in subs)
            {
                tempStartingPoint = TilePositionToTile(StringToTile(subCommand, tempStartingPoint));
            }
            tileCoordinates.Add(tempStartingPoint);
        }
        else
        {
            tileCoordinates.Add(TilePositionToTile(StringToTile(command, startingPoint)));
        }
        // translate the command to a tileposition, then translate this tileposition to a Tile

    }
    // translate string to coordinate   
    // add coordinates to list
    return tileCoordinates;
}
public Point StringToTile(string command, Tile startingPoint)
{
    int x_pos = startingPoint.position.X;
    int y_pos = startingPoint.position.Y;
    Debug.Log("x pos" + x_pos);
    Debug.Log("y_pos" + y_pos);
    if (y_pos % 2 == 0) // for even rows
    {
        switch (command)
        {
            case "M":
                x_pos = startingPoint.position.getX();
                y_pos = startingPoint.position.getY();
                break;
            case "U":
                x_pos = startingPoint.position.getX() + 1;
                y_pos = startingPoint.position.getY();
                break;
            case "UL":
                x_pos = startingPoint.position.getX();
                y_pos = startingPoint.position.getY() + 1;
                break;
            case "UR":
                x_pos = startingPoint.position.getX();
                y_pos = startingPoint.position.getY() - 1;
                break;
            case "D":
                x_pos = startingPoint.position.getX() - 1;
                y_pos = startingPoint.position.getY();
                break;
            case "DL":
                x_pos = startingPoint.position.getX() - 1;
                y_pos = startingPoint.position.getY() + 1;
                break;
            case "DR":
                x_pos = startingPoint.position.getX() - 1;
                y_pos = startingPoint.position.getY() - 1;
                break;
        }
    }
    else // for odd rows
    {
        switch (command)
        {
            case "M":
                x_pos = startingPoint.position.getX();
                y_pos = startingPoint.position.getY();
                break;
            case "U":
                x_pos = startingPoint.position.getX() + 1;
                y_pos = startingPoint.position.getY();
                break;
            case "UL":
                x_pos = startingPoint.position.getX() + 1;
                y_pos = startingPoint.position.getY() + 1;
                break;
            case "UR":
                x_pos = startingPoint.position.getX() + 1;
                y_pos = startingPoint.position.getY() - 1;
                break;
            case "D":
                x_pos = startingPoint.position.getX() - 1;
                y_pos = startingPoint.position.getY();
                break;
            case "DL":
                x_pos = startingPoint.position.getX();
                y_pos = startingPoint.position.getY() + 1;
                break;
            case "DR":
                x_pos = startingPoint.position.getX();
                y_pos = startingPoint.position.getY() - 1;
                break;
        }
    }

    return new Point(x_pos, y_pos);
}

Tile TilePositionToTile(Point position)
{
    string tileName = "Hex_" + position.getX() + "_" + position.getY();
    if (GameObject.Find(tileName) != null)
    {
        return GameObject.Find(tileName).GetComponent<Tile>();
    }
    else
    {
        return null;
    }


}
}
