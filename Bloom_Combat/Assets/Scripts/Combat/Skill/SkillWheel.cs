using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillWheel : MonoBehaviour
{
    public Camera cam;
    Vector2 mousePos;
    bool mouseDown = true;
    float angle;
    float angleOld;
    Vector2 mPrevPos = Vector2.zero;
    float mPrevAngle = 0;
    [SerializeField] SkillDisplayField skillDisplayField;
    public Transform middlePoint;

    public Player player;

    private void Start()
    {
       // InitializeSkillDisplayField(player.playerSkillList);
    }


    //
    public void InitializeSkillDisplayField(List<Skill> skillList)
    {
        for (int i = 0; i < skillList.Count; i++)
        {
            /* Distance around the circle */
            var radians = 2 * Mathf.PI / skillList.Count * i;

            /* Get the vector direction */
            var vertical = Mathf.Sin(radians);
            var horizontal = Mathf.Cos(radians);
            Vector3 spawnDir = new Vector3(horizontal, vertical, 0 );

            /* Get the spawn position */
            Vector3 spawnPos = middlePoint.GetComponent<RectTransform>().localPosition + (spawnDir * 50f); // Radius is just the distance away from the point
            
            // Instantiate the Object
            SkillDisplayField newSkill = Instantiate(skillDisplayField, new Vector3(0, 0, 0), Quaternion.identity);

            // Set its parent, its position and its rotation
            newSkill.transform.SetParent(this.transform);
            newSkill.GetComponent<RectTransform>().localPosition = spawnPos;
            newSkill.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, (newSkill.GetComponent<RectTransform>().rotation.z + radians) * Mathf.Rad2Deg);
            
            // Display the skillImage
            newSkill.setDisplayField(skillList[i]);

        }
        //foreach(Skill skill in skillList)
        //{

        //    Debug.Log("instantiating with " + skill.name);
        //    SkillDisplayField newSkill = Instantiate(skillDisplayField, new Vector3(0, 0, 0),Quaternion.identity);
        //    newSkill.transform.parent = this.transform;
        //    newSkill.GetComponent<RectTransform>().localPosition = new Vector3(50,0,0);

        //    newSkill.transform.LookAt(middlePoint);

        //    // calculate position
        //    
        //}

    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            mousePos = Input.mousePosition;
            Vector2 lookDir = mousePos - mPrevPos;
            if (mouseDown)
            {
                angleOld = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
                mouseDown = false;
            }
            angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
            if(angle < 0)
            {
                angle += 360f;
            }
            float angleNew = mPrevAngle + (angle - angleOld);
            GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, angleNew);
        }
        else
        {
            mouseDown = true;
            mPrevAngle = GetComponent<RectTransform>().localRotation.eulerAngles.z;
        }
        mPrevPos = transform.position;
    }
}
