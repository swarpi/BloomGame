using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public enum PlayerCombatStates
{
	NORMAL,
	WAITFORTURN,
	SKILLSELECTION,
	FIELDSELECTION

}

//Client
// Manager for one player, every player has this component
public class PlayerCombatManager : MonoBehaviour
{
	// owner of this manager
	Player player;

	public PlayerCombatStates currentState;

	// Fields for the FieldDisplay
	public Camera cam;
	public Color startColor;
	public Color mouseOverColor;
	public GameObject ourHitObject = null;
	Tile oldTile = null;
	Tile currentTile;
	public GameObject field;

	// Fields for the SkillDisplay
	public Skill selectedSkill;
	public Canvas skillCanvas;


    void Start()
    {

    }

	// Update is called once per frame
	void Update()
	{

		// Is the mouse over a Unity UI Element?
		if (EventSystem.current.IsPointerOverGameObject())
		{
			// It is, so let's not do any of our own custom
			// mouse stuff, because that would be weird.

			// NOTE!  We might want to ask the system WHAT KIND
			// of object we're over -- so for things that aren't
			// buttons, we might not actually want to bail out early.

			return;
		}
		// could also check if game is paused?
		// if main menu is open?

		// This only works in orthographic, and only gives us the
		// world position on the same plane as the camera's
		// near clipping play.  (i.e. It's not helpful for our application.)
		//Vector3 worldPoint = Camera.main.ScreenToWorldPoint( Input.mousePosition );
		//Debug.Log( "World Point: " + worldPoint );
		if(currentState == PlayerCombatStates.SKILLSELECTION)
		{
			// select Skill then change state to field selection
		
			// selectedSkill = skillSelected(); // need to create this method
		}

		if(currentState == PlayerCombatStates.FIELDSELECTION)
		{
			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;
			if (Physics.Raycast(ray, out hitInfo))
			{
				ourHitObject = hitInfo.collider.transform.gameObject;
				if (hitInfo.collider.transform.parent != null)
				{
					ourHitObject = hitInfo.collider.transform.parent.gameObject;
				}
				if (ourHitObject.GetComponent<Tile>() != null)
				{
					currentTile = ourHitObject.GetComponent<Tile>();
					MouseOver_Hex(ourHitObject);

					//ourHitObject.GetComponentInChildren<MeshRenderer>().material.color = mouseOverColor;
				}
			}
			// after confirming field change to next state
			// next state would be to execute the spell and go to normal and wait for your turn
		}
	}

	void MouseOver_Hex(GameObject ourHitObject)
	{
		// We know what we're mousing over. 
		if (Input.GetMouseButtonDown(0))
		{

			// We have clicked on a hex.  Do something about it!
			// This might involve calling a bunch of other functions
			// depending on what mode you happen to be in, in your game.

			// depending on skill range colorize the tile

			// We're just gonna colorize the hex, as an example.
			MeshRenderer mr = ourHitObject.GetComponentInChildren<MeshRenderer>();
			if (oldTile == null)
			{
				oldTile = currentTile;

				ColorTilesForSelectedSkill(oldTile);
				//oldTile.GetComponentInChildren<MeshRenderer>().material.color = mouseOverColor;
			}
			else if (oldTile != currentTile)
			{
				ResetAllTilesColor();
				// change oldTile to old color
				oldTile.GetComponentInChildren<MeshRenderer>().material.color = startColor;
				// change currentTile to new color

				ColorTilesForSelectedSkill(currentTile);
				
				// set oldTile to currentTile
				oldTile = currentTile;
			}
			// If we have a unit selected, let's move it to this tile!
		}

	}

	void ColorTilesForSelectedSkill(Tile currentTile)
	{
		if(selectedSkill.skillType == SkillType.BUFF)
		{
			// change color for buff skilled
		}
		if (selectedSkill.skillType == SkillType.COMBAT)
		{
			foreach(string name in selectedSkill.skillAreaString)
			{
				Debug.Log(name);
			}
			//List<Tile> effectedTiles = new FieldTranslator().ListToTile(selectedSkill.skillAreaString, currentTile);
			//foreach(Tile tile in effectedTiles)
			//{
			//	if(tile == null)
			//	{
			//		continue;
			//	}
			//	tile.GetComponentInChildren<MeshRenderer>().material.color = mouseOverColor;
			//}
		}
	}

	public void ResetAllTilesColor()
	{
		GameObject[] Tiles = GameObject.FindGameObjectsWithTag("Tile");
		foreach(GameObject go in Tiles)
		{
			go.GetComponentInChildren<MeshRenderer>().material.color = startColor;
		}
	}

	public void SetPlayer(Player player)
    {
        this.player = player;
    }

    // Method to confirm and trigger player action
    // send it to CombatManager
    public PlayerMove TriggerPlayerAction()
    {
        // verify playeraction


        return null;
    }



}
